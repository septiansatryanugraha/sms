CREATE TABLE `tbl_branch` (
	`id_branch` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`description` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`created_date` DATETIME NULL DEFAULT NULL,
	`created_by` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`updated_date` DATETIME NULL DEFAULT NULL,
	`updated_by` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id_branch`) USING BTREE,
	UNIQUE INDEX `id_branch` (`id_branch`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

INSERT INTO `tbl_branch` (`id_branch`, `name`, `description`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES (1, 'SJJ-PKP', 'Surabaya Jadi Jaya', '2021-01-07 10:25:53', 'SUPERVISOR', '2021-01-07 10:25:53', 'SUPERVISOR');
INSERT INTO `tbl_branch` (`id_branch`, `name`, `description`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES (2, 'SMS-PKP', 'Surabaya Maju Sesama', '2021-01-07 10:25:53', 'SUPERVISOR', '2021-01-07 10:25:53', 'SUPERVISOR');

INSERT INTO `tbl_item_stock` (`id_branch`, `id_item`, `qty`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES (1, 1, 0, '2021-01-07 13:26:46', 'system', '2021-01-07 13:26:46', 'system');
INSERT INTO `tbl_item_stock` (`id_branch`, `id_item`, `qty`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES (1, 2, 0, '2021-01-07 13:26:46', 'system', '2021-01-07 13:26:46', 'system');

ALTER TABLE `tbl_supplier`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_supplier`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_supplier SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_customer`
	ADD COLUMN `id_branch` INT(11) NULL DEFAULT NULL AFTER `id_customer`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_customer SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_vehicle`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_vehicle`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_vehicle SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_item_manual`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_item_manual`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_item_manual SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_item_stock`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_item_stock`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_item_stock SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_item_history`
	ADD COLUMN `id_branch` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `id_item_history`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_item_history SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_supplier_loan`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_supplier_loan`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_supplier_loan SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_finance_history`
	ADD COLUMN `id_branch` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `id_finance_history`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_finance_history SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_transaction_purchase`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_transaction_purchase`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_transaction_purchase SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_transaction_sales`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_transaction_sales`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_transaction_sales SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_vehicle_rent`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_vehicle_rent`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_vehicle_rent SET id_branch = 2 WHERE id_branch IS NULL

ALTER TABLE `tbl_vehicle_maintenance`
	ADD COLUMN `id_branch` BIGINT(20) NULL DEFAULT NULL AFTER `id_vehicle_maintenance`,
	ADD INDEX `id_branch` (`id_branch`);

UPDATE tbl_vehicle_maintenance SET id_branch = 2 WHERE id_branch IS NULL
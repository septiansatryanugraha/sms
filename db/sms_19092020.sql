-- --------------------------------------------------------
-- Host:                         surabayajadijaya28.com
-- Server version:               10.1.41-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table suraba74_majusesama.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `nama` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `grup_id` int(10) NOT NULL,
  `foto` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `last_created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `created_by` varchar(200) CHARACTER SET latin1 NOT NULL,
  `last_update_by` varchar(200) CHARACTER SET latin1 NOT NULL,
  `last_login_user` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.grup
CREATE TABLE IF NOT EXISTS `grup` (
  `grup_id` int(10) NOT NULL AUTO_INCREMENT,
  `nama_grup` varchar(300) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `last_created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `last_update_by` varchar(200) NOT NULL,
  PRIMARY KEY (`grup_id`),
  UNIQUE KEY `grup_id` (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `updated_by` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.menu_akses
CREATE TABLE IF NOT EXISTS `menu_akses` (
  `id_menuakses` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `grup_id` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `add` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `del` int(11) NOT NULL,
  `back` int(11) NOT NULL,
  `conf` int(11) NOT NULL,
  PRIMARY KEY (`id_menuakses`),
  UNIQUE KEY `id_menuakses` (`id_menuakses`),
  KEY `id_menu` (`id_menu`),
  KEY `grup_id` (`grup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=423 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.status_grup
CREATE TABLE IF NOT EXISTS `status_grup` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(300) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_status`),
  UNIQUE KEY `id_status` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.sys_counter
CREATE TABLE IF NOT EXISTS `sys_counter` (
  `sys_counter_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `counter` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sys_counter_id`),
  UNIQUE KEY `counter_id` (`sys_counter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_bank
CREATE TABLE IF NOT EXISTS `tbl_bank` (
  `id_bank` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_bank`),
  UNIQUE KEY `id_bank` (`id_bank`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_customer
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `address` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_customer`),
  UNIQUE KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_finance_history
CREATE TABLE IF NOT EXISTS `tbl_finance_history` (
  `id_finance_history` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `ref_table` varchar(100) DEFAULT NULL,
  `ref_id` bigint(20) DEFAULT NULL,
  `description` longtext,
  `finance_in` double DEFAULT '0',
  `finance_out` double DEFAULT '0',
  `fixed` bigint(20) DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_finance_history`),
  UNIQUE KEY `id_keuanganhistory` (`id_finance_history`)
) ENGINE=InnoDB AUTO_INCREMENT=2055 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_finance_manual
CREATE TABLE IF NOT EXISTS `tbl_finance_manual` (
  `id_finance_manual` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `type` enum('Debit','Credit') DEFAULT NULL,
  `amount` double DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_finance_manual`),
  UNIQUE KEY `id_keuanganmanual` (`id_finance_manual`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_histori_pembayaran
CREATE TABLE IF NOT EXISTS `tbl_histori_pembayaran` (
  `id_pembayaran` int(255) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(200) NOT NULL,
  `no_pelanggan` varchar(200) NOT NULL,
  `meter_awal` varchar(200) NOT NULL,
  `meter_akhir` varchar(200) NOT NULL,
  `pemakaian` varchar(200) NOT NULL,
  `beban` varchar(200) NOT NULL,
  `tarif` varchar(200) NOT NULL,
  `denda` int(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `updated_by` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_item
CREATE TABLE IF NOT EXISTS `tbl_item` (
  `id_item` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_item`),
  UNIQUE KEY `id_barang` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_item_history
CREATE TABLE IF NOT EXISTS `tbl_item_history` (
  `id_item_history` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_item` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `module` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `ref_table` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `ref_id` bigint(20) DEFAULT NULL,
  `description` longtext CHARACTER SET latin1,
  `before` double DEFAULT '0',
  `item_in` double DEFAULT '0',
  `item_out` double DEFAULT '0',
  `item` double DEFAULT '0',
  `fixed` tinyint(4) DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id_item_history`),
  UNIQUE KEY `id_stokbaranghistory` (`id_item_history`),
  KEY `kodebarang` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=1750 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_item_manual
CREATE TABLE IF NOT EXISTS `tbl_item_manual` (
  `id_item_manual` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_item` bigint(20) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` enum('in','out') DEFAULT NULL,
  `qty` double DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_item_manual`),
  UNIQUE KEY `id_stokbarangmanual` (`id_item_manual`),
  KEY `kodebarang` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_item_stock
CREATE TABLE IF NOT EXISTS `tbl_item_stock` (
  `id_item_stock` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_item` bigint(20) DEFAULT NULL,
  `qty` float NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_item_stock`),
  UNIQUE KEY `id_item_stock` (`id_item_stock`),
  KEY `id_item` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_menu
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id_menu` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(300) NOT NULL,
  `link` varchar(300) NOT NULL,
  `icon` varchar(300) NOT NULL,
  `parent` bigint(20) NOT NULL,
  `kode_menu` varchar(100) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `menu_file` varchar(100) NOT NULL,
  `urutan` bigint(20) NOT NULL,
  `last_created_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `last_update_by` varchar(200) NOT NULL,
  PRIMARY KEY (`id_menu`),
  UNIQUE KEY `id_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_supplier
CREATE TABLE IF NOT EXISTS `tbl_supplier` (
  `id_supplier` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_bank` bigint(20) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` longtext,
  `account_number` varchar(50) DEFAULT NULL,
  `account_holder` varchar(250) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`),
  UNIQUE KEY `id_suplier` (`id_supplier`),
  KEY `id_bank` (`id_bank`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_supplier_item_price
CREATE TABLE IF NOT EXISTS `tbl_supplier_item_price` (
  `id_supplier_item_price` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_supplier` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `price` double DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_supplier_item_price`),
  UNIQUE KEY `id_supplier_item_price` (`id_supplier_item_price`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_item` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_supplier_loan
CREATE TABLE IF NOT EXISTS `tbl_supplier_loan` (
  `id_supplier_loan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_supplier` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `paid` double DEFAULT '0',
  `debt` double DEFAULT '0',
  `description` longtext,
  `is_paid_off` bigint(20) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_supplier_loan`),
  UNIQUE KEY `id_supplier_loan` (`id_supplier_loan`),
  KEY `id_supplier` (`id_supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_supplier_loan_installment
CREATE TABLE IF NOT EXISTS `tbl_supplier_loan_installment` (
  `id_supplier_loan_installment` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_supplier_loan` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `status` bigint(20) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_supplier_loan_installment`),
  UNIQUE KEY `id_supplier_loan_installment` (`id_supplier_loan_installment`),
  KEY `id_supplier_loan` (`id_supplier_loan`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_transaction_purchase
CREATE TABLE IF NOT EXISTS `tbl_transaction_purchase` (
  `id_transaction_purchase` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_supplier` bigint(20) DEFAULT NULL,
  `id_supplier_loan_installment` varchar(500) DEFAULT NULL,
  `id_vehicle_maintenance` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `paid` date DEFAULT NULL,
  `is_drum` bigint(20) DEFAULT NULL,
  `id_item_exp` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `price` double DEFAULT '0',
  `ppn` double DEFAULT '0',
  `nominal` double DEFAULT '0',
  `description` longtext,
  `is_approved` bigint(20) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_transaction_purchase`),
  UNIQUE KEY `id_transaction_buy` (`id_transaction_purchase`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_supplier_loan_installment` (`id_supplier_loan_installment`(255)),
  KEY `id_vehicle_maintenance` (`id_vehicle_maintenance`)
) ENGINE=InnoDB AUTO_INCREMENT=1538 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_transaction_purchase_detail
CREATE TABLE IF NOT EXISTS `tbl_transaction_purchase_detail` (
  `id_transaction_purchase_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_transaction_purchase` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `qty_basic_real` double DEFAULT '0',
  `qty_basic` double DEFAULT '0',
  `multiple` double DEFAULT '1',
  `qty_before` double DEFAULT '0',
  `additional` double DEFAULT '0',
  `depreciation` double DEFAULT '0',
  `weight` double DEFAULT '0',
  `specific_gravity` double DEFAULT '0',
  `water_content` double DEFAULT '0',
  `qty` double DEFAULT '0',
  `type` varchar(50) DEFAULT NULL,
  `total` double DEFAULT '0',
  `grandtotal` double DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_transaction_purchase_detail`),
  UNIQUE KEY `id_transaction_buy_detail` (`id_transaction_purchase_detail`),
  KEY `id_transaction_buy` (`id_transaction_purchase`),
  KEY `id_item` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=1538 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_transaction_sales
CREATE TABLE IF NOT EXISTS `tbl_transaction_sales` (
  `id_transaction_sales` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_customer` bigint(20) DEFAULT NULL,
  `id_vehicle_maintenance` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `paid` date DEFAULT NULL,
  `id_item_exp` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `price` double DEFAULT '0',
  `ppn` double DEFAULT '0',
  `nominal` double DEFAULT '0',
  `description` longtext,
  `is_approved` bigint(20) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_transaction_sales`),
  UNIQUE KEY `id_transaction_sell` (`id_transaction_sales`),
  KEY `id_customer` (`id_customer`),
  KEY `id_vehicle_maintenance` (`id_vehicle_maintenance`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_transaction_sales_detail
CREATE TABLE IF NOT EXISTS `tbl_transaction_sales_detail` (
  `id_transaction_sales_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_transaction_sales` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `qty_basic_real` double DEFAULT '0',
  `qty_basic` double DEFAULT '0',
  `multiple` double DEFAULT '1',
  `qty_before` double DEFAULT '0',
  `additional` double DEFAULT '0',
  `depreciation` double DEFAULT '0',
  `weight` double DEFAULT '0',
  `specific_gravity` varchar(50) DEFAULT '0',
  `water_content` double DEFAULT '0',
  `qty` double DEFAULT '0',
  `type` varchar(50) DEFAULT NULL,
  `total` double DEFAULT '0',
  `grandtotal` double DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_transaction_sales_detail`),
  UNIQUE KEY `id_transaction_sell_detail` (`id_transaction_sales_detail`),
  KEY `id_transaction_sell` (`id_transaction_sales`),
  KEY `id_item` (`id_item`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_vehicle
CREATE TABLE IF NOT EXISTS `tbl_vehicle` (
  `id_vehicle` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand` varchar(250) DEFAULT NULL,
  `police_number` varchar(100) DEFAULT NULL,
  `is_ready` bigint(20) DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`),
  UNIQUE KEY `id_vehicle` (`id_vehicle`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_vehicle_maintenance
CREATE TABLE IF NOT EXISTS `tbl_vehicle_maintenance` (
  `id_vehicle_maintenance` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_vehicle` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nominal` double DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle_maintenance`),
  UNIQUE KEY `id_vehicle_maintenance` (`id_vehicle_maintenance`),
  KEY `id_vehicle` (`id_vehicle`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table suraba74_majusesama.tbl_vehicle_rent
CREATE TABLE IF NOT EXISTS `tbl_vehicle_rent` (
  `id_vehicle_rent` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_vehicle` bigint(20) DEFAULT NULL,
  `id_supplier` bigint(20) DEFAULT NULL,
  `id_vehicle_maintenance` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `back` date DEFAULT NULL,
  `count_day` double DEFAULT '0',
  `nominal_rent` double DEFAULT '0',
  `nominal` double DEFAULT '0',
  `is_done` bigint(20) DEFAULT '0',
  `description` longtext,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle_rent`),
  UNIQUE KEY `id_vehicle_rent` (`id_vehicle_rent`),
  KEY `id_vehicle` (`id_vehicle`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_vehicle_maintenance` (`id_vehicle_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping data for table suraba74_majusesama.admin: ~3 rows (approximately)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `username`, `password`, `email`, `nama`, `grup_id`, `foto`, `status`, `last_created_date`, `last_update_date`, `created_by`, `last_update_by`, `last_login_user`) VALUES
	(1, 'superadmin', '40587bff0e72b6fdbba30c40c95e148a', 'superadmin', 'Superadmin', 1, 'IZham11f_400x400.jpg', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'superadmin', '2020-09-02 09:24:16'),
	(2, 'RUSLI', '039b8624e3de4872bea4394cff550608', 'surabayamajusesama@gmail.com', 'SUPERVISOR', 1, 'logo_FIX_SMS1.jpg', 3, '0000-00-00 00:00:00', '2019-11-01 00:05:43', '', 'SUPERVISOR', '2020-09-19 10:57:47'),
	(3, 'adminsjj2019#', '4f4dce4bbd37d1ad74ff3e22573feecf', 'adminsms@gmail.com', 'SURABAYA MAJU SESAMA', 2, 'LOGO.jpg', 3, '2018-04-14 12:42:35', '2019-06-25 11:05:15', '', 'SURABAYA JADI JAYA', '2020-09-18 15:41:20');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.grup: ~2 rows (approximately)
DELETE FROM `grup`;
/*!40000 ALTER TABLE `grup` DISABLE KEYS */;
INSERT INTO `grup` (`grup_id`, `nama_grup`, `deskripsi`, `last_created_date`, `last_update_date`, `created_by`, `last_update_by`) VALUES
	(1, 'Super Admin', 'full access', '0000-00-00 00:00:00', '2018-05-31 20:34:18', '', 'Moch Wahyu Sugiarto'),
	(2, 'Admin', ' limited access', '0000-00-00 00:00:00', '2018-05-14 17:34:30', '', 'Moch Wahyu Sugiarto');
/*!40000 ALTER TABLE `grup` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.kategori: ~10 rows (approximately)
DELETE FROM `kategori`;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id_kategori`, `nama`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
	(1, 'A-UMUM', '0000-00-00 00:00:00', 'Admin web master', '2018-11-06 09:45:53', '2018-11-06 10:05:13'),
	(4, 'B1', 'Admin web master', 'Admin Web', '2018-11-06 10:09:12', '2018-11-09 13:00:32'),
	(5, 'B1-UMUM', 'Admin web master', '', '2018-11-06 10:09:20', '0000-00-00 00:00:00'),
	(6, 'B2-UMUM', 'Admin web master', '', '2018-11-06 10:09:35', '0000-00-00 00:00:00'),
	(7, 'B2', 'Admin web master', 'Admin Web', '2018-11-06 10:09:43', '2018-11-09 13:00:50'),
	(8, 'A-UMUM (PERP)', 'Admin Web', '', '2018-11-09 12:51:08', '0000-00-00 00:00:00'),
	(9, 'B1 (PERP)', 'Admin Web', 'Admin Web', '2018-11-09 12:52:15', '2018-11-09 12:54:47'),
	(10, 'B1-UMUM (PERP)', 'Admin Web', 'Admin Web', '2018-11-09 12:52:38', '2018-11-09 12:55:05'),
	(11, 'B2 (PERP)', 'Admin Web', 'Admin Web', '2018-11-09 12:53:08', '2018-11-09 12:57:14'),
	(12, 'B2-UMUM (PERP)', 'Admin Web', 'Admin Web', '2018-11-09 12:53:38', '2018-11-09 12:56:53');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.menu_akses: ~48 rows (approximately)
DELETE FROM `menu_akses`;
/*!40000 ALTER TABLE `menu_akses` DISABLE KEYS */;
INSERT INTO `menu_akses` (`id_menuakses`, `id_menu`, `grup_id`, `view`, `add`, `edit`, `del`, `back`, `conf`) VALUES
	(327, 1, 1, 1, 0, 0, 0, 0, 0),
	(328, 10, 1, 1, 0, 0, 0, 0, 0),
	(329, 6, 1, 1, 0, 0, 0, 0, 0),
	(330, 13, 1, 1, 0, 0, 0, 0, 0),
	(331, 2, 1, 1, 0, 0, 0, 0, 0),
	(332, 12, 1, 1, 0, 0, 0, 0, 0),
	(333, 22, 1, 1, 1, 0, 0, 0, 0),
	(334, 11, 1, 1, 0, 0, 0, 0, 0),
	(335, 21, 1, 1, 1, 0, 0, 0, 0),
	(336, 20, 1, 1, 1, 0, 0, 0, 0),
	(337, 19, 1, 1, 1, 0, 0, 0, 0),
	(338, 23, 1, 1, 1, 0, 0, 0, 0),
	(339, 9, 1, 1, 1, 1, 1, 0, 0),
	(340, 8, 1, 1, 1, 1, 1, 0, 0),
	(341, 7, 1, 1, 1, 1, 1, 0, 0),
	(342, 5, 1, 1, 1, 1, 1, 0, 0),
	(343, 4, 1, 1, 1, 1, 1, 0, 0),
	(344, 3, 1, 1, 1, 1, 1, 0, 0),
	(345, 18, 1, 1, 1, 1, 1, 1, 0),
	(346, 17, 1, 1, 1, 1, 1, 0, 0),
	(347, 16, 1, 1, 1, 1, 1, 0, 1),
	(348, 15, 1, 1, 1, 1, 1, 0, 1),
	(349, 14, 1, 1, 1, 0, 0, 0, 0),
	(350, 24, 1, 1, 1, 1, 1, 0, 0),
	(399, 1, 2, 1, 0, 0, 0, 0, 0),
	(400, 10, 2, 1, 0, 0, 0, 0, 0),
	(401, 6, 2, 1, 0, 0, 0, 0, 0),
	(402, 13, 2, 1, 0, 0, 0, 0, 0),
	(403, 2, 2, 0, 0, 0, 0, 0, 0),
	(404, 12, 2, 1, 0, 0, 0, 0, 0),
	(405, 22, 2, 1, 1, 0, 0, 0, 0),
	(406, 11, 2, 1, 0, 0, 0, 0, 0),
	(407, 21, 2, 1, 1, 0, 0, 0, 0),
	(408, 20, 2, 1, 1, 0, 0, 0, 0),
	(409, 19, 2, 1, 1, 0, 0, 0, 0),
	(410, 23, 2, 1, 1, 0, 0, 0, 0),
	(411, 9, 2, 1, 1, 1, 1, 0, 0),
	(412, 8, 2, 1, 1, 1, 1, 0, 0),
	(413, 7, 2, 1, 1, 1, 1, 0, 0),
	(414, 5, 2, 0, 0, 0, 0, 0, 0),
	(415, 4, 2, 0, 0, 0, 0, 0, 0),
	(416, 3, 2, 0, 0, 0, 0, 0, 0),
	(417, 18, 2, 1, 1, 1, 1, 0, 0),
	(418, 17, 2, 1, 1, 1, 1, 0, 0),
	(419, 16, 2, 1, 1, 1, 1, 0, 0),
	(420, 15, 2, 1, 1, 1, 1, 0, 0),
	(421, 14, 2, 0, 0, 0, 0, 0, 0),
	(422, 24, 2, 1, 1, 1, 1, 0, 0);
/*!40000 ALTER TABLE `menu_akses` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.status_grup: ~12 rows (approximately)
DELETE FROM `status_grup`;
/*!40000 ALTER TABLE `status_grup` DISABLE KEYS */;
INSERT INTO `status_grup` (`id_status`, `nama`) VALUES
	(1, 'Unread'),
	(2, 'Read'),
	(3, 'Aktif'),
	(4, 'Belum Aktif'),
	(5, 'Hidden'),
	(6, 'Publish'),
	(7, 'Prospek'),
	(8, 'Out Prospek'),
	(9, 'Batal'),
	(10, 'Deal'),
	(11, 'Tersedia'),
	(12, 'Tidak Tersedia');
/*!40000 ALTER TABLE `status_grup` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.tbl_item: ~2 rows (approximately)
DELETE FROM `tbl_item`;
/*!40000 ALTER TABLE `tbl_item` DISABLE KEYS */;
INSERT INTO `tbl_item` (`id_item`, `name`, `unit`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
	(1, 'Oli', 'Liter', '2019-06-17 22:42:50', 'System', '2019-06-17 22:42:50', 'System'),
	(2, 'Drum', 'Pcs', '2019-06-17 22:42:50', 'System', '2019-06-17 22:42:50', 'System');
/*!40000 ALTER TABLE `tbl_item` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.tbl_item_stock: ~2 rows (approximately)
DELETE FROM `tbl_item_stock`;
/*!40000 ALTER TABLE `tbl_item_stock` DISABLE KEYS */;
INSERT INTO `tbl_item_stock` (`id_item_stock`, `id_item`, `qty`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
	(1, 1, 0, '2019-04-20 20:27:32', 'system', '2019-04-20 20:27:32', 'system'),
	(2, 2, 0, '2019-04-20 20:27:32', 'system', '2019-04-20 20:27:32', 'system');
/*!40000 ALTER TABLE `tbl_item_stock` ENABLE KEYS */;

-- Dumping data for table suraba74_majusesama.tbl_menu: ~24 rows (approximately)
DELETE FROM `tbl_menu`;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `link`, `icon`, `parent`, `kode_menu`, `controller`, `menu_file`, `urutan`, `last_created_date`, `last_update_date`, `created_by`, `last_update_by`) VALUES
	(1, 'Dashboard', 'Dashboard', 'fa fa-home', 0, 'admin', 'Dashboard', 'view', 0, '2018-04-20 09:17:58', '2019-09-28 10:15:06', '', 'Rusli SJJ'),
	(2, 'Setting', '', 'fa fa-wrench', 0, 'setting', '', 'view', 10, '2018-04-20 09:19:49', '2018-05-02 12:47:51', '', 'Moch Wahyu Sugiarto'),
	(3, 'Master Menu', 'menu', 'fa fa-th-list', 2, 'menu-master', 'Menu', 'view,add,edit,del', 2, '2018-04-20 09:22:00', '2018-04-20 09:54:37', '', ''),
	(4, 'Group', 'user-grup', 'fa fa-users', 2, 'user-grup', 'Grup', 'view,add,edit,del', 3, '2018-04-20 09:23:34', '2018-04-20 09:54:06', '', ''),
	(5, 'User', 'user', 'fa fa-user', 2, 'user', 'User', 'view,add,edit,del', 1, '2018-04-20 10:29:57', '0000-00-00 00:00:00', '', ''),
	(6, 'Master', '', 'fa  fa-database', 0, 'data-master', '', 'view', 1, '2019-04-20 09:59:35', '2019-04-20 16:31:29', 'Admin SSJ', 'Admin SSJ'),
	(7, 'Supplier', 'supplier', 'fa fa-circle-o', 6, 'supplier', 'Supplier', 'view,add,edit,del', 1, '2019-04-20 10:01:26', '2019-09-28 10:15:40', 'Admin SSJ', 'Rusli SJJ'),
	(8, 'Customer', 'customer', 'fa fa-circle-o', 6, 'customer', 'Customer', 'view,add,edit,del', 2, '2019-04-20 10:02:41', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(9, 'Armada', 'vehicle', 'fa fa-circle-o', 6, 'vehicle', 'Vehicle', 'view,add,edit,del', 3, '2019-04-20 10:03:50', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(10, 'Laporan', '', 'fa fa-book', 0, 'master-report', '', 'view', 2, '2019-04-20 10:05:10', '2019-04-20 16:31:49', 'Admin SSJ', 'Admin SSJ'),
	(11, 'Customer', 'report-customer', 'fa fa-circle-o', 10, 'report-customer', 'Report_client', 'view', 1, '2019-04-20 10:06:31', '2019-04-20 10:22:50', 'Admin SSJ', 'Admin SSJ'),
	(12, 'Supplier', 'report-supplier', 'fa fa-circle-o', 10, 'report-supplier', 'Report_supplier', 'view', 2, '2019-04-20 10:07:27', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(13, 'Transaksi', '', 'fa fa-refresh', 0, 'transaksi', '', 'view', 1, '2019-04-20 16:36:41', '2019-04-20 19:25:49', 'Admin SSJ', 'Admin SSJ'),
	(14, 'Manual Stok', 'manual-stock', 'fa fa-circle-o', 13, 'manual-stock', 'ManualStock', 'view,add', 1, '2019-04-20 19:27:04', '2019-04-20 19:28:06', 'Admin SSJ', 'Admin SSJ'),
	(15, 'Pembelian', 'pembelian', 'fa fa-circle-o', 13, 'pembelian', 'Purchase', 'view,add,edit,del', 5, '2019-04-27 21:12:35', '2019-04-28 20:05:46', 'Admin SSJ', 'Admin SSJ'),
	(16, 'Penjualan', 'penjualan', 'fa fa-circle-o', 13, 'penjualan', 'Sale', 'view,add,edit,del', 5, '2019-04-27 21:13:14', '2019-04-28 20:05:57', 'Admin SSJ', 'Admin SSJ'),
	(17, 'Peminjaman', 'peminjaman', 'fa fa-circle-o', 13, 'peminjaman', 'SupplierLoan', 'view,add,edit,del', 2, '2019-04-27 22:22:41', '2019-04-28 17:55:09', 'Admin SSJ', 'Admin SSJ'),
	(18, 'Penyewaan', 'penyewaan', 'fa fa-circle-o', 13, 'penyewaan', 'VehicleRent', 'view,add,edit,del', 3, '2019-04-28 20:05:32', '2020-01-03 09:33:22', 'Admin SSJ', 'SUPERVISOR'),
	(19, 'Aktivitas Armada', 'report-armada', 'fa fa-circle-o', 10, 'report-armada', 'Report_armada', 'view,add', 3, '2019-05-08 13:43:07', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(20, 'Stok In / Out', 'report-stock', 'fa fa-circle-o', 10, 'report-stock', 'Report_stock', 'view,add', 4, '2019-05-08 13:43:46', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(21, 'Rep Penjualan', 'report-penjualan', 'fa fa-circle-o', 10, 'report-penjualan', 'Report_penjualan', 'view,add', 5, '2019-05-08 13:44:41', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(22, 'Rep Pembelian', 'report-pembelian', '	fa fa-circle-o', 10, 'report-pembelian', 'Report_pembelian', 'view,add', 6, '2019-05-08 13:45:13', '0000-00-00 00:00:00', 'Admin SSJ', ''),
	(23, 'Rep Piutang', 'report-piutang', 'fa fa-circle-o', 10, 'report-piutang', 'Report_piutang', 'view,add', 7, '2019-05-10 08:31:26', '2019-10-21 05:10:40', 'Admin SSJ', 'Rusli SJJ'),
	(24, 'Pemeliharaan', 'pemeliharaan', 'fa fa-circle-o', 13, 'pemeliharaan', 'VehicleMaintenance', 'view,add,edit,del', 4, '2019-04-28 20:05:32', '0000-00-00 00:00:00', 'Admin SSJ', '');
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

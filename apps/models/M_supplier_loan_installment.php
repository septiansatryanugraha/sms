<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 25, 2019
 * @license Susi Susanti Group
 */
class M_supplier_loan_installment extends CI_Model {

    const __tableName = 'tbl_supplier_loan_installment';
    const __tableId = 'id_supplier_loan_installment';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function selectItem() {
        $data = $this->db->get(self::__tableName);
        return $data->result();
    }

    public function selectByHeaderId($id) {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_supplier_loan = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectById($id) {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

}

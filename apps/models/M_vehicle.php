<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 25, 2019
 * @license Susi Susanti Group
 */
class M_vehicle extends CI_Model
{
    const __tableName = 'tbl_vehicle';
    const __tableId = 'id_vehicle';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0)
    {
        $this->db->from(self::__tableName);
        $this->db->where('id_branch', $this->branch);
        if ($isAjaxList > 0) {
            $this->db->order_by("updated_date", "DESC");
        }
        $data = $this->db->get();
        return $data->result();
    }

    public function selectItem()
    {
        $this->db->from(self::__tableName);
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->result();
    }

    public function selectItemReady()
    {
        $q = "  SELECT * FROM " . self::__tableName . " WHERE id_branch = " . $this->branch . " AND is_ready > 0";
        $result = $this->db->query($q)->result();
        return $result;
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = " . $this->branch . " AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }
}

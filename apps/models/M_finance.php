<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 26, 2019
 * @license Susi Susanti Group
 */
class M_finance extends CI_Model
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function balance($refTable, $arrWhere = array(), $type, $module, $description)
    {
        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }

        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }

        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Type not declared..";
            }
        }

        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Type must 'in' or 'out'.";
            }
        }

        if ($errCode == 0) {
            if (strlen($module) == 0) {
                $errCode++;
                $errMessage = "Modules not declared..";
            }
        }

        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if ($getRef == null) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }

        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        //Insert History Item
        if ($errCode == 0) {
            try {
                $date = $getRef['date'];
                if (strlen($date) == 0) {
                    $date = date('Y-m-d');
                }
                $data = array();
                $data['id_branch'] = $this->branch;
                $data['date'] = $date;
                $data['module'] = $module;
                $data['ref_table'] = $refTable;
                $data['ref_id'] = $refId;
                $data['description'] = $description;
                $dataItem = array();
                if ($type == 'in') {
                    $data['finance_in'] = $getRef['nominal'];
                    $data['finance_out'] = 0;
                } else if ($type == 'out') {
                    $data['finance_in'] = 0;
                    $data['finance_out'] = $getRef['nominal'];
                }
                $data['created_date'] = $getRef['created_date'];
                $data['created_by'] = $getRef['created_by'];
                $data['updated_date'] = $getRef['updated_date'];
                $data['updated_by'] = $getRef['updated_by'];
                $historyItem = $this->db->insert('tbl_finance_history', $data);
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        $result = array(
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        );

        return $result;
    }

    public function updateBalance($refTable, $arrWhere = array(), $type, $module, $description)
    {
        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }

        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }

        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Type not declared..";
            }
        }

        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Type must 'in' or 'out'.";
            }
        }

        if ($errCode == 0) {
            if (strlen($module) == 0) {
                $errCode++;
                $errMessage = "Modules not declared..";
            }
        }

        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if ($getRef == null) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }

        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        if ($errCode == 0) {
            $q = "  SELECT * FROM tbl_finance_history
                    WHERE id_branch = '{$this->branch}'
                    AND ref_table = '{$refTable}'
                    AND ref_id = '{$refId}'
                    AND fixed > 0 ";
            $getLastHistory = $this->db->query($q)->row_array();
            if ($getLastHistory == NULL) {
                $errCode++;
                $errMessage = "History invalid.";
            }
        }

        if ($errCode == 0) {
            try {
                $date = $getRef['date'];
                if (strlen($date) == 0) {
                    $date = date('Y-m-d');
                }
                $data = array();
                $data['id_branch'] = $this->branch;
                $data['date'] = $date;
                $data['description'] = $description;
                if ($type == 'in') {
                    $data['finance_in'] = $getRef['nominal'];
                    $data['finance_out'] = 0;
                } else if ($type == 'out') {
                    $data['finance_in'] = 0;
                    $data['finance_out'] = $getRef['nominal'];
                }
                $data['updated_date'] = $getRef['updated_date'];
                $data['updated_by'] = $getRef['updated_by'];
                $historyFinance = $this->db->update('tbl_finance_history', $data, array('id_branch' => $this->branch, 'id_finance_history' => $getLastHistory['id_finance_history']));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        $result = array(
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        );

        return $result;
    }

    public function deleteBalance($refTable, $arrWhere = array())
    {
        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }

        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }

        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if ($getRef == null) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }

        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        if ($errCode == 0) {
            $q = "  SELECT * FROM tbl_finance_history
                    WHERE id_branch = '{$this->branch}'
                    AND ref_table = '{$refTable}'
                    AND ref_id = '{$refId}'
                    AND fixed > 0 ";
            $getLastHistory = $this->db->query($q)->row_array();
            if ($getLastHistory == NULL) {
                $errCode++;
                $errMessage = "History invalid.";
            }
        }

        if ($errCode == 0) {
            try {
                $historyFinance = $this->db->delete('tbl_finance_history', array('id_branch' => $this->branch, 'id_finance_history' => $getLastHistory['id_finance_history']));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        $result = array(
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        );

        return $result;
    }
}

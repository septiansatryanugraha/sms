<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class M_item extends CI_Model
{
    const __tableName = 'tbl_item';
    const __tableId = 'id_item';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0)
    {
        $this->db->from(self::__tableName);
        if ($isAjaxList > 0) {
            $this->db->order_by("updated_date", "DESC");
        }
        $data = $this->db->get();
        return $data->result();
    }

    public function selectItem()
    {
        $data = $this->db->get(self::__tableName);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function getStockItem($id)
    {
        $sql = "SELECT * FROM tbl_item_stock WHERE id_branch = '{$this->branch}' AND id_item = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row()->qty;
    }

    public function getLastStockItemBeforeDate($idItem, $date, $createdDate = null, $idItemHistory = null)
    {
        if (strlen($idItem) == 0) {
            $idItem = 1;
        }
        if (strlen($date) == 0) {
            $date = date('Y-m-d');
        }
        $q = "  SELECT sum(tih.item_in) as item_in, sum(tih.item_out) as item_out
                FROM tbl_item_history tih 
                WHERE tih.fixed > 0 AND tih.id_branch = '{$this->branch}'
                AND tih.id_item = '{$idItem}' AND date(tih.date) <= '{$date}'";
        if (strlen($idItemHistory) > 0) {
            $q .= " AND tih.created_date < '{$createdDate}' AND tih.id_item_history <> '{$idItemHistory}'";
        }
        $data = $this->db->query($q);
        return $data->row_array();
    }

    public function getHistoryStockItemAfterDate($idItem, $date, $createdDate = null, $idItemHistory = null)
    {
        if (strlen($idItem) == 0) {
            $idItem = 1;
        }
        if (strlen($date) == 0) {
            $date = date('Y-m-d');
        }
        $q = "";
        if (strlen($createdDate) > 0 && strlen($idItemHistory) > 0) {
            $q .= " SELECT tih.id_item_history, item_in, item_out, tih.date, tih.created_date
                    FROM tbl_item_history tih
                    WHERE tih.fixed > 0 AND id_branch = '{$this->branch}'
                    AND tih.id_item = '{$idItem}' AND date(tih.date) = '{$date}'
                    AND tih.created_date > '{$createdDate}'";
            if (strlen($idItemHistory) > 0) {
                $q .= " AND tih.id_item_history <> '{$idItemHistory}'";
            }
            $q .= " UNION ALL";
        }
        $q .= " SELECT tih.id_item_history, item_in, item_out, tih.date, tih.created_date
                FROM tbl_item_history tih
                WHERE tih.fixed > 0 AND id_branch = '{$this->branch}'
                AND tih.id_item = '{$idItem}' AND date(tih.date) > '{$date}'";
        if (strlen($idItemHistory) > 0) {
            $q .= " AND tih.id_item_history <> '{$idItemHistory}'";
        }
        $q .= " ORDER BY date ASC, created_date ASC";

        $data = $this->db->query($q);
        return $data->result_array();
    }

    public function balance($refTable, $arrWhere = array(), $type, $module, $description)
    {
        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }

        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }

        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Type not declared..";
            }
        }

        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Type must 'in' or 'out'.";
            }
        }

        if ($errCode == 0) {
            if (strlen($module) == 0) {
                $errCode++;
                $errMessage = "Modules not declared..";
            }
        }

        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if ($getRef == null) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }

        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        if ($errCode == 0) {
            $idItem = $getRef['id_item'];
            if (strlen($idItem) == 0) {
                $errCode++;
                $errMessage = "Item ID not declared.";
            }
        }

        if ($errCode == 0) {
            $qty = $getRef['qty'];
            if (strlen($qty) == 0) {
                $errCode++;
                $errMessage = "Item count not declared.";
            }
        }

        if ($errCode == 0) {
            $getItem = $this->db->get_where('tbl_item', array('id_item' => $idItem,))->row_array();
            if ($getItem == null) {
                $errCode++;
                $errMessage = "Item invalid";
            }
        }

        if ($type == 'out') {
            if ($errCode == 0) {
                $getLastStockItemBeforeDate = self::getLastStockItemBeforeDate($idItem, $date);
                $lastStockBeforeDate = $getLastStockItemBeforeDate['item_in'] - $getLastStockItemBeforeDate['item_out'];
                if ($qty > $lastStockBeforeDate) {
                    $errCode++;
                    $errMessage = "Item keluar " . $qty . ". Stok item terakhir pada tanggal " . date('d-m-Y', strtotime($date)) . " sebesar " . $lastStockBeforeDate . ".";
                }
            }

            if ($errCode == 0) {
                $getHistoryStockItemAfterDate = self::getHistoryStockItemAfterDate($idItem, $date);
                if (is_array($getHistoryStockItemAfterDate) && count($getHistoryStockItemAfterDate) > 0) {
                    $lastStockAfterDate = $lastStockBeforeDate - $qty;
                    foreach ($getHistoryStockItemAfterDate as $key => $value) {
                        $lastStockAfterDate = $lastStockAfterDate + ($value['item_in'] - $value['item_out']);
                        if ($lastStockAfterDate < 0) {
                            $errCode++;
                            $errMessage = "INVALID.";
                        }
                    }
                }
            }
        }

        if ($errCode == 0) {
            $lastStockItem = self::getStockItem($idItem);

            $stockAfter = 0;
            if ($type == 'in') {
                $stockAfter = $lastStockItem + $qty;
            } else if ($type == 'out') {
                $stockAfter = $lastStockItem - $qty;
            }

            if ($stockAfter < 0) {
                $errCode++;
                $errMessage = "Stok item terakhir sebesar " . $lastStockItem . ".";
            }
        }

        if ($errCode == 0) {
            try {
                $date = $getRef['date'];
                if (strlen($date) == 0) {
                    $date = date('Y-m-d');
                }
                $data = array();
                $data['id_branch'] = $this->branch;
                $data['id_item'] = $idItem;
                $data['date'] = $date;
                $data['module'] = $module;
                $data['ref_table'] = $refTable;
                $data['ref_id'] = $refId;
                $data['description'] = $description;
                if ($type == 'in') {
                    $data['item_in'] = $qty;
                    $data['item_out'] = 0;
                } else if ($type == 'out') {
                    $data['item_in'] = 0;
                    $data['item_out'] = $qty;
                }
                $data['created_date'] = $getRef['created_date'];
                $data['created_by'] = $getRef['created_by'];
                $data['updated_date'] = $getRef['updated_date'];
                $data['updated_by'] = $getRef['updated_by'];
                $historyItem = $this->db->insert('tbl_item_history', $data);
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        if ($errCode == 0) {
            try {
                $updateItem = $this->db->update('tbl_item_stock', array('qty' => $stockAfter), array("id_branch" => $this->branch, "id_item" => $idItem));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        $result = array(
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        );

        return $result;
    }

    public function updateBalance($refTable, $arrWhere = array(), $type, $module, $description)
    {
        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }

        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }

        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Type not declared..";
            }
        }

        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Type must 'in' or 'out'.";
            }
        }

        if ($errCode == 0) {
            if (strlen($module) == 0) {
                $errCode++;
                $errMessage = "Modules not declared..";
            }
        }

        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if (count($getRef) == 0) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }

        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        if ($errCode == 0) {
            $idItem = $getRef['id_item'];
            if (strlen($idItem) == 0) {
                $errCode++;
                $errMessage = "Item ID not declared.";
            }
        }

        if ($errCode == 0) {
            $qty = $getRef['qty'];
            if (strlen($qty) == 0) {
                $errCode++;
                $errMessage = "Item count not declared.";
            }
        }

        if ($errCode == 0) {
            $getItem = $this->db->get_where('tbl_item', array('id_item' => $idItem,))->row_array();
            if ($getItem == null) {
                $errCode++;
                $errMessage = "Item invalid";
            }
        }

        if ($errCode == 0) {
            $q = "  SELECT * FROM tbl_item_history
                    WHERE id_branch = '{$this->branch}'
                    AND id_item = '{$idItem}'
                    AND ref_table = '{$refTable}'
                    AND ref_id = '{$refId}'
                    AND fixed > 0 ";
            $getLastHistory = $this->db->query($q)->row_array();
            if ($getLastHistory == NULL) {
                $errCode++;
                $errMessage = "History invalid.";
            }
        }

        if ($errCode == 0) {
            $getLastStockItemBeforeDate = self::getLastStockItemBeforeDate($idItem, $date, $getLastHistory['created_date'], $getLastHistory['id_item_history']);
            $lastStockBeforeDate = $getLastStockItemBeforeDate['item_in'] - $getLastStockItemBeforeDate['item_out'];
            if ($type == 'out') {
                if ($qty > $lastStockBeforeDate) {
                    $errCode++;
                    $errMessage = "Item keluar " . $qty . ". Stok item terakhir pada tanggal " . date('d-m-Y', strtotime($date)) . " sebesar " . $lastStockBeforeDate . ".";
                }
            }
        }

        if ($errCode == 0) {
            $getHistoryStockItemAfterDate = self::getHistoryStockItemAfterDate($idItem, $date, $getLastHistory['created_date'], $getLastHistory['id_item_history']);
            if (is_array($getHistoryStockItemAfterDate) && count($getHistoryStockItemAfterDate) > 0) {
                $lastStockAfterDate = $lastStockBeforeDate;
                if ($type == 'in') {
                    $lastStockAfterDate = $lastStockAfterDate + $qty;
                }
                if ($type == 'out') {
                    $lastStockAfterDate = $lastStockAfterDate - $qty;
                }
                foreach ($getHistoryStockItemAfterDate as $key => $value) {
                    $lastStockAfterDate = $lastStockAfterDate + ($value['item_in'] - $value['item_out']);
                    if ($lastStockAfterDate < 0) {
                        $errCode++;
                        $errMessage = "INVALID.";
                    }
                }
            }
        }

        if ($errCode == 0) {
            $praUpdateItemIn = $getLastHistory['item_in'];
            $praUpdateItemOut = $getLastHistory['item_out'];
            $lastStockItem = self::getStockItem($idItem);
            $stockBefore = $lastStockItem - ($praUpdateItemIn - $praUpdateItemOut);
            if ($stockBefore < 0) {
                $errCode++;
                $errMessage = "Gagal update. Stok item sebelum update sebesar " . $stockBefore . ".";
            }
        }

        if ($errCode == 0) {
            $stockAfter = 0;
            if ($type == 'in') {
                $stockAfter = $stockBefore + $qty;
            } else if ($type == 'out') {
                $stockAfter = $stockBefore - $qty;
            }
            if ($stockAfter < 0) {
                $errCode++;
                $errMessage = "Gagal update. Stok item setelah update harus lebih dari sama dengan 0.";
            }
        }

        if ($errCode == 0) {
            try {
                $date = $getRef['date'];
                if (strlen($date) == 0) {
                    $date = date('Y-m-d');
                }
                $data = array();
                $data['id_branch'] = $this->branch;
                $data['date'] = $date;
                $data['description'] = $description;
                if ($type == 'in') {
                    $data['item_in'] = $qty;
                    $data['item_out'] = 0;
                } else if ($type == 'out') {
                    $data['item_in'] = 0;
                    $data['item_out'] = $qty;
                }
                $data['updated_date'] = $getRef['updated_date'];
                $data['updated_by'] = $getRef['updated_by'];
                $historyFinance = $this->db->update('tbl_item_history', $data, array('id_branch' => $this->branch, 'id_item_history' => $getLastHistory['id_item_history']));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        if ($errCode == 0) {
            try {
                $updateItem = $this->db->update('tbl_item_stock', array('qty' => $stockAfter), array("id_branch" => $this->branch, "id_item" => $idItem));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        $result = array(
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        );

        return $result;
    }

    public function deleteBalance($refTable, $arrWhere = array())
    {
        $errCode = 0;
        $errMessage = "";

        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }

        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }

        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if (count($getRef) == 0) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }

        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        if ($errCode == 0) {
            $idItem = $getRef['id_item'];
            if (strlen($idItem) == 0) {
                $errCode++;
                $errMessage = "Item ID not declared.";
            }
        }

        if ($errCode == 0) {
            $qty = $getRef['qty'];
            if (strlen($qty) == 0) {
                $errCode++;
                $errMessage = "Item count not declared.";
            }
        }

        if ($errCode == 0) {
            $getItem = $this->db->get_where('tbl_item', array('id_item' => $idItem,))->row_array();
            if ($getItem == null) {
                $errCode++;
                $errMessage = "Item invalid";
            }
        }

        if ($errCode == 0) {
            $q = "  SELECT * FROM tbl_item_history
                    WHERE id_branch = '{$this->branch}'
                    AND id_item = '{$idItem}'
                    AND ref_table = '{$refTable}'
                    AND ref_id = '{$refId}'
                    AND fixed > 0 ";
            $getLastHistory = $this->db->query($q)->row_array();
            if ($getLastHistory == NULL) {
                $errCode++;
                $errMessage = "History invalid.";
            }
        }

        if ($errCode == 0) {
            $type = 'in';
            if ($getLastHistory['item_out'] > 0) {
                $type = 'out';
            }
            $getLastStockItemBeforeDate = self::getLastStockItemBeforeDate($idItem, $getLastHistory['date'], $getLastHistory['created_date'], $getLastHistory['id_item_history']);
            $lastStockBeforeDate = $getLastStockItemBeforeDate['item_in'] - $getLastStockItemBeforeDate['item_out'];

            $getHistoryStockItemAfterDate = self::getHistoryStockItemAfterDate($idItem, $getLastHistory['date'], $getLastHistory['created_date'], $getLastHistory['id_item_history']);
            if (is_array($getHistoryStockItemAfterDate) && count($getHistoryStockItemAfterDate) > 0) {
                $lastStockAfterDate = $lastStockBeforeDate;
                if ($type == 'in') {
                    $lastStockAfterDate = $lastStockAfterDate - $qty;
                }
                if ($type == 'out') {
                    $lastStockAfterDate = $lastStockBeforeDate + $qty;
                }
                foreach ($getHistoryStockItemAfterDate as $key => $value) {
                    $lastStockAfterDate = $lastStockAfterDate + ($value['item_in'] - $value['item_out']);
                    if ($lastStockAfterDate < 0) {
                        $errCode++;
                        $errMessage = "INVALID.";
                    }
                }
            }
        }

        if ($errCode == 0) {
            $praUpdateItemIn = $getLastHistory['item_in'];
            $praUpdateItemOut = $getLastHistory['item_out'];
            $lastStockItem = self::getStockItem($idItem);
            $stockAfter = $lastStockItem - ($praUpdateItemIn - $praUpdateItemOut);
            if ($stockAfter < 0) {
                $errCode++;
                $errMessage = "Gagal hapus. Stok item setelah update harus lebih dari sama dengan 0.";
            }
        }

        if ($errCode == 0) {
            try {
                $historyFinance = $this->db->delete('tbl_item_history', array('id_branch' => $this->branch, 'id_item_history' => $getLastHistory['id_item_history']));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        if ($errCode == 0) {
            try {
                $updateItem = $this->db->update('tbl_item_stock', array('qty' => $stockAfter), array("id_branch" => $this->branch, "id_item" => $idItem));
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }

        $result = array(
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        );

        return $result;
    }
}

<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Nov 11, 2019
 * @license Susi Susanti Group
 */
class M_bank extends CI_Model {

    const __tableName = 'tbl_bank';
    const __tableId = 'id_bank';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0) {
        $this->db->from(self::__tableName);
        if ($isAjaxList > 0) {
            $this->db->order_by("updated_date", "DESC");
        }
        $data = $this->db->get();
        return $data->result();
    }

    public function selectItem() {
        $data = $this->db->get(self::__tableName);
        return $data->result();
    }

    public function selectById($id) {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

}

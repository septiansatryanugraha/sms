<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 25, 2019
 * @license Susi Susanti Group
 */
class M_supplier extends CI_Model
{
    const __tableName = 'tbl_supplier';
    const __tableId = 'id_supplier';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0)
    {
        $this->db->from(self::__tableName);
        $this->db->where('id_branch', $this->branch);
        if ($isAjaxList > 0) {
            $this->db->order_by("updated_date", "DESC");
        }
        $data = $this->db->get();
        return $data->result();
    }

    public function selectItem()
    {
        $this->db->from(self::__tableName);
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = " . $this->branch . " AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function getItemPrice($idSupplier)
    {
        $sql = "SELECT * FROM tbl_supplier_item_price WHERE id_supplier = '{$idSupplier}'";
        $data = $this->db->query($sql);
        return $data->result();
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "Default/Auth";
$route['404_override'] = 'Default/Not_found';
$route['login'] = 'Default/Auth';
$route['logout'] = 'Auth/logout';

/*   route modul profile */
$route['profile'] = 'Setting/Profile/index';

/*   route modul dashboard  */
$route['dashboard'] = 'Support/Dashboard/index';
$route['#'] = 'Support/Dashboard/index';

/*   route modul menu  */
$route['menu'] = 'Setting/Menu/index';
$route['icon'] = 'Setting/Menu/icon';
$route['add-menu'] = 'Setting/Menu/add';
$route['edit-menu/(:any)'] = 'Setting/Menu/edit/$1';

/*   route modul user  */
$route['user'] = 'Setting/User/index';
$route['add-user'] = 'Setting/User/add';
$route['edit-user/(:any)'] = 'Setting/User/edit/$1';

/*   route modul grup  */
$route['user-grup'] = 'Setting/Grup/index';
$route['add-grup'] = 'Setting/Grup/add';
$route['edit-grup/(:any)'] = 'Setting/Grup/edit/$1';

/*    route modul Akses  */
$route['hak-akses/(:any)'] = 'Setting/Akses/hak_akses/$1';

/*   route modul supplier  */
$route['supplier'] = 'Master/Supplier/index';
$route['add-supplier'] = 'Master/Supplier/add';
$route['edit-supplier/(:any)'] = 'Master/Supplier/edit/$1';

/*   route modul customer  */
$route['customer'] = 'Master/Customer/index';
$route['add-customer'] = 'Master/Customer/add';
$route['edit-customer/(:any)'] = 'Master/Customer/edit/$1';

/*   route modul kendaraan  */
$route['vehicle'] = 'Master/Vehicle/index';
$route['add-vehicle'] = 'Master/Vehicle/Add';
$route['edit-vehicle/(:any)'] = 'Master/Vehicle/Edit/$1';

/*   route modul stok   */
$route['manual-stock'] = 'Transaction/ManualStock/index';
$route['add-manual-stock'] = 'Transaction/ManualStock/add';

/*   route modul pembelian    */
$route['pembelian'] = 'Transaction/Purchase/index';
$route['add-pembelian'] = 'Transaction/Purchase/Add';
$route['add-pembelian-custom'] = 'Transaction/Purchase/AddCustom';
$route['edit-pembelian/(:any)'] = 'Transaction/Purchase/Edit/$1';
$route['confirm-pembelian/(:any)'] = 'Transaction/Purchase/Confirm/$1';
$route['edit-confirm-pembelian/(:any)'] = 'Transaction/Purchase/EditConfirm/$1';
$route['cetak-pembelian/(:any)'] = 'Transaction/Purchase/Cetak/$1';
$route['cetak-pembelian-approve/(:any)'] = 'Transaction/Purchase/Cetak_approve/$1';

/*   route modul penjualan   */
$route['penjualan'] = 'Transaction/Sale/index';
$route['add-penjualan'] = 'Transaction/Sale/Add';
$route['add-penjualan-custom'] = 'Transaction/Sale/AddCustom';
$route['edit-penjualan/(:any)'] = 'Transaction/Sale/Edit/$1';
$route['confirm-penjualan/(:any)'] = 'Transaction/Sale/Confirm/$1';
$route['edit-confirm-penjualan/(:any)'] = 'Transaction/Sale/EditConfirm/$1';
$route['cetak-penjualan/(:any)'] = 'Transaction/Sale/Cetak/$1';
$route['cetak-penjualan-approve/(:any)'] = 'Transaction/Sale/Cetak_approve/$1';

/*   route modul peminjaman   */
$route['peminjaman'] = 'Transaction/SupplierLoan/index';
$route['add-peminjaman'] = 'Transaction/SupplierLoan/Add';
$route['edit-peminjaman/(:any)'] = 'Transaction/SupplierLoan/Edit/$1';

/*   route modul penyewaan   */
$route['penyewaan'] = 'Transaction/VehicleRent/index';
$route['add-penyewaan'] = 'Transaction/VehicleRent/Add';
$route['edit-penyewaan/(:any)'] = 'Transaction/VehicleRent/Edit/$1';
$route['kembali-penyewaan/(:any)'] = 'Transaction/VehicleRent/Kembali/$1';

/*   route modul pemeliharaam   */
$route['pemeliharaan'] = 'Transaction/VehicleMaintenance/index';
$route['add-pemeliharaan'] = 'Transaction/VehicleMaintenance/Add';
$route['edit-pemeliharaan/(:any)'] = 'Transaction/VehicleMaintenance/Edit/$1';

/*   route report data customer   */
$route['report-customer'] = 'Report/Report_client/index';
$route['filter-customer'] = 'Report/Report_client/filter';

/*   route report modul supplier   */
$route['report-supplier'] = 'Report/Report_supplier/index';
$route['filter-supplier'] = 'Report/Report_supplier/filter';

/*   route report modul rental  */
$route['report-armada'] = 'Report/Report_armada/index';
$route['filter-armada'] = 'Report/Report_armada/filter';

/*   route report modul stock  */
$route['report-stock'] = 'Report/Report_stock/index';
$route['filter-stock'] = 'Report/Report_stock/filter';

/*   route report modul penjualan  */
$route['report-penjualan'] = 'Report/Report_penjualan/index';
$route['filter-penjualan'] = 'Report/Report_penjualan/filter';

/*   route report modul pembelian  */
$route['report-pembelian'] = 'Report/Report_pembelian/index';
$route['filter-pembelian'] = 'Report/Report_pembelian/filter';

/*   route report modul piutang  */
$route['report-piutang'] = 'Report/Report_piutang/index';
$route['filter-piutang'] = 'Report/Report_piutang/filter';
$route['export-piutang'] = 'Report/Report_piutang/export_excel';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
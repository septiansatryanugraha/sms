<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>

<table>
    <tr><td style="text-align: center;" colspan="11"><h2>Report Data Pembelian <?php echo $branch_description; ?></h2></td></tr>
    <tr><td colspan="11"></td></tr>
    <tr>
        <td colspan="2"><b>Supplier</b></td>
        <td colspan="9">: <?php echo $supplier; ?></td>
    </tr>
    <tr>
        <td colspan="2"><b>Status Pembelian</b></td>
        <td colspan="9">: <?php echo $approved; ?></td>
    </tr>
    <tr>
        <td colspan="2"><b>Periode Report</b></td>
        <td colspan="9">: <?php echo $periode; ?></td>
    </tr>
    <tr><td colspan="11"></td></tr>
    <tr><td colspan="11"><b>Pembelian Oli</b></td></tr>
</table>
<table border="1" width="60%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Kode Pembelian</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Nama Supplier </th>
            <th align="center">Deskripsi Pembelian </th>
            <th align="center">Berat</th>
            <th align="center">Kadar Air</th>
            <th align="center"colspan="2">Qty / Satuan</th>
            <th align="center">Harga / Liter</th>
            <th align="center">Total Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($excelOli)) {
            $hargaTotalOli = 0;
            foreach ($excelOli as $key => $data) {
                $tipe = 'Oli';
                if ($data->id_item > 1) {
                    $tipe = 'Drum';
                }
                $hargaTotalOli += $data->grandtotal;

                ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $data->code ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->date)); ?>&nbsp;</td>
                    <td><?php echo $data->supplier_name ?></td>
                    <td><?php echo $data->deskripsi ?></td>
                    <td><?php echo $data->specific_gravity ?></td>
                    <td><?php echo $data->water_content ?></td>
                    <td><?php echo $data->qty ?></td>
                    <td>liter</td>
                    <td><?php echo $data->total ?></td>
                    <td><?php echo $data->grandtotal ?></td>
                </tr>
                <?php
            }
        }

        ?>
        <tr>
            <td colspan="10" align="right"><b>Total :</b></td>
            <td><b><?php echo $hargaTotalOli ?></b></td>
        </tr>
    </tbody>
</table>
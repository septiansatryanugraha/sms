<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <form method="post" id="myform" action="<?php echo site_url('filter-pembelian'); ?>">
                <div class="search-form">
                    <div class="form-group ">
                        <label class="control-label">Filter</label>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Supplier</label>
                        <div class="col-sm-3">
                            <select name="id_supplier" id="id_supplier" class="form-control select-supplier" aria-describedby="sizing-addon2">
                                <option value="all" <?php echo ($idSupplier == 'all') ? 'selected' : ''; ?>>Semua Supplier</option>
                                <?php foreach ($supplier as $data) { ?>
                                    <option value="<?php echo $data->id_supplier; ?>" <?php echo ($idSupplier == $data->id_supplier) ? 'selected' : ''; ?>>
                                        <?php echo $data->name; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Status Pembelian</label>
                        <div class="col-sm-2">
                            <select name="is_approved" class="form-control is-approved" id="is_approved" aria-describedby="sizing-addon2">
                                <option value="all" <?php echo ($isApproved == 'all') ? 'selected' : ''; ?>>Semua Status</option>
                                <option value="0" <?php echo ($isApproved == '0') ? 'selected' : ''; ?>>Pending</option>
                                <option value="2" <?php echo ($isApproved == '2') ? 'selected' : ''; ?>>Konfirmasi</option>
                                <option value="1" <?php echo ($isApproved == '1') ? 'selected' : ''; ?>>Approved</option>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Tanggal Pembelian</label>
                        <div class="col-sm-6">
                            <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('d-m-Y', strtotime($tanggalAwal)); ?>" readonly="">
                            <span>s/d</span>
                            <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('d-m-Y', strtotime($tanggalAkhir)); ?>" readonly="">
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">  
                        <label class="col-sm-2 control-label"></label>       
                        <div class="col-sm-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="all_date" name="all_date" <?php echo ($allDate == '1') ? 'checked' : ''; ?> />Semua Tanggal
                            </label>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export klik"><i class="fa fa-refresh"></i> Filter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <form method="post" id="myform" action="<?php echo site_url('Report/Report_pembelian/export_excel'); ?>">
                <div class="search-form">
                    <div class="box-footer">
                        <input type="hidden" name="id_supplier"  value="<?php echo $idSupplier; ?>">
                        <input type="hidden" name="is_approved"  value="<?php echo $isApproved; ?>">
                        <input type="hidden" name="tanggal_awal"  value="<?php echo $tanggalAwal; ?>">
                        <input type="hidden" name="tanggal_akhir"  value="<?php echo $tanggalAkhir; ?>">
                        <input type="hidden" name="all_date"  value="<?php echo $allDate; ?>">
                        <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export"><i class="fa fa-download"></i> Export Excel</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="tableku" class=" table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th align="center">No</th>
                            <th align="center">Kode Pembelian</th>
                            <th align="center">Jenis Stok</th>
                            <th align="center">Tanggal Transaksi</th>
                            <th align="center">Nama Supplier</th>
                            <th align="center">Tanggal Pembayaran</th>
                            <th align="center">Di Buat Oleh</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($filter)) {
                            foreach ($filter as $key => $data) {
                                $tipe = 'Oli';
                                if ($data->id_item > 1) {
                                    $tipe = 'Drum';
                                }
                                $paid = "";
                                if (strlen($data->paid) > 0) {
                                    $paid = date('d-m-Y', strtotime($data->paid));
                                }

                                ?>
                                <tr>
                                    <td><?php echo $key + 1 ?></td>
                                    <td><?php echo $data->code ?></td>
                                    <td><?php echo $tipe ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($data->date)) ?></td>
                                    <td><?php echo $data->supplier_name ?></td>
                                    <td><?php echo $paid ?></td>
                                    <td><?php echo $data->created_by ?></td>
                                </tr>
                                <?php
                            }
                        }

                        ?>
                    </tbody>    
                </table>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('#tableku').DataTable();
    });
    $(function () {
        // untuk datetime from
        $("#from").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        // untuk datetime to
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $(".select-supplier").select2({
            placeholder: " -- pilih supplier -- "
        });
    });
</script>
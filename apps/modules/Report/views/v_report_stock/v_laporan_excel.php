<style>
    .LastCol {
        border: 1px solid black;    
    }
</style>
</style>




<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>

<center><h2>Report Data Stok IN / OUT <?php echo $branch_description; ?></h2></center>
<br>

<table>
    <tr><td></td><td><b>Periode Report &nbsp;&nbsp;&nbsp; :</b></td><td><b>Periode awal &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_awal)) ?></b></td></tr>
</table>

<table>
    <tr><td></td><td></td><td><b>Periode akhir &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_akhir)) ?></td></tr></table>
<br><br>

<table>
    <tr><td></td><td><b>Report Stok Oli</b></td></tr>
</table><br><br>

<table>
    <tr><td colspan="10" style="text-align: right;"><b>Stok terakhir = <?php echo round($lastStockOli); ?></b></td></tr>
</table>
<table border="1" width="60%">

    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Customer/Supplier</th>
            <th align="center">Deskripsi Transaksi </th>
            <th align="center"colspan="2">Stok Masuk</th>
            <th align="center"colspan="2">Stok Keluar</th>
            <th align="center"colspan="2">Stok Sisa</th>
        </tr>
    </thead>

    <tbody>
        <?php
        if (!empty($excel_oli)) {
            $no = 1;
            $stok_oli_in = 0;
            $stok_oli_out = 0;
            $stok_oli_total = 0;
            foreach ($excel_oli as $data) {
                $stok_oli_in += $data['item_in'];
                $stok_oli_out += $data['item_out'];
                $stok_oli_total += $data['item'];

                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $data['date'] ?></td>
                    <td><?php echo $data['customersuppplier'] ?></td>
                    <td><?php echo $data['description'] ?></td>
                    <td><?php echo $data['item_in'] ?></td>
                    <td>liter</td>
                    <td><?php echo round($data['item_out']) ?></td>
                    <td>liter</td>
                    <td><?php echo round($data['item']) ?></td>
                    <td>liter</td>
                </tr>
                <?php
                $no++;
            }
        }

        ?>
    </tbody>
</table>
<br><br>







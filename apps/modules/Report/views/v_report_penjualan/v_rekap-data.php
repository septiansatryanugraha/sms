<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #report {
        max-width: 1200px;
        height: 330px;
        margin: 0 auto
    }   
</style>

<section class="content">
    <div class="box">
        <div class="box-header">
            <form method="post" action="<?php echo site_url('filter-penjualan'); ?>">
                <div class="search-form">
                    <div class="form-group ">
                        <label class="control-label">Filter</label>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Customer</label>
                        <div class="col-sm-3">
                            <select name="id_customer" id="id_customer" class="form-control select-customer" aria-describedby="sizing-addon2">
                                <option value="all">Semua Customer</option>
                                <?php foreach ($customer as $data) { ?>
                                    <option value="<?php echo $data->id_customer; ?>">
                                        <?php echo $data->name; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Status Penjualan</label>
                        <div class="col-sm-2">
                            <select name="is_approved" class="form-control is-approved" id="is_approved" aria-describedby="sizing-addon2">
                                <option value="all">Semua Status</option>
                                <option value="0">Pending</option>
                                <option value="2">Konfirmasi</option>
                                <option value="1">Approved</option>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Tanggal Penjualan</label>
                        <div class="col-sm-6">
                            <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                            <span>s/d</span>
                            <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">  
                        <label class="col-sm-2 control-label"></label>       
                        <div class="col-sm-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="all_date" name="all_date"/>Semua Tanggal
                            </label>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export klik"><i class="fa fa-refresh"></i> Filter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box">
        <div id="report"></div>
        <!-- REPORT BARU -->
        <?php
        $tanggalJual = array();
        $nominalJual = array();
        if (!empty($graph1)) {
            foreach ($graph1 as $key => $data2) {
                $tanggalJual[] = date('d-m-Y', strtotime($data2->date));
                $nominalJual[] = (float) $data2->nominal;
            }
        } else {
            echo "Belum Ada data yang masuk";
        }

        ?>
    </div>
</section>

<!-- highchart -->
<script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
<script>
    // Script untuk memanggil library Highcharts
    jQuery(function () {
        Highcharts.setOptions({
            lang: {
                thousandsSep: '.'
            }
        });
        new Highcharts.Chart({
            chart: {
                renderTo: 'report',
                type: 'column',
            },
            title: {
                text: 'Grafik penjualan',
                x: -20
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: <?php echo json_encode($tanggalJual); ?>
            },
            yAxis: {
                labels: {
                    formatter: function () {
                        return IDRFormatter(this.value, 'Rp.');
                    }
                },
                title: {
                    text: 'Nominal'
                }
            }, series: [{name: 'Nominal', data: <?php echo json_encode($nominalJual); ?>}]
        });
    });

    function IDRFormatter(angka, prefix) {
        var number_string = angka.toString().replace(/[^,\d]/g, ''),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    //klik loading ajax
    $(document).ready(function () {
        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='http://belanjaweb.com/sjj/assets/tambahan/gambar/loader-ok.gif' height='18'> ");
            $("#loading2").modal('show');
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $("#loading2").modal('hide');
                }
            });
            return true;
        });
    });

    // untuk datetime from
    $(function () {
        $("#from").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        // untuk datetime to
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $(".select-customer").select2({
            placeholder: " -- pilih customer -- "
        });
    });
</script>
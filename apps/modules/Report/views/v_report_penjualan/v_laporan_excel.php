<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>

<table>
    <tr><td style="text-align: center;" colspan="8"><h2>Report Data Penjualan <?php echo $branch_description; ?></h2></td></tr>
    <tr><td colspan="8"></td></tr>
    <tr>
        <td colspan="2"><b>Customer</b></td>
        <td colspan="6">: <?php echo $customer; ?></td>
    </tr>
    <tr>
        <td colspan="2"><b>Status Penjualan</b></td>
        <td colspan="6">: <?php echo $approved; ?></td>
    </tr>
    <tr>
        <td colspan="2"><b>Periode Report</b></td>
        <td colspan="6">: <?php echo $periode; ?></td>
    </tr>
    <tr><td colspan="8"></td></tr>
    <tr><td colspan="8"><b>Penjualan Oli</b></td></tr>
</table>
<table border="1" width="60%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Kode Penjualan</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Nama Customer </th>
            <th align="center"colspan="2">Qty / Satuan</th>
            <th align="center">Harga</th>
            <th align="center">Total Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($excelOli)) {
            $no = 1;
            $hargaTotalOli = 0;
            foreach ($excelOli as $key => $data) {
                $tipe = 'Oli';
                if ($data->id_item > 1) {
                    $tipe = 'Drum';
                }
                $hargaTotalOli += $data->grandtotal;

                ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $data->code ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->date)); ?>&nbsp;</td>
                    <td><?php echo $data->customer_name ?></td>
                    <td><?php echo $data->qty ?></td>
                    <td><?php echo $data->type ?> </td>
                    <td><?php echo $data->total ?></td>
                    <td><?php echo $data->grandtotal ?></td>
                </tr>
                <?php
            }
        }

        ?>
        <tr>
            <td colspan="7" align="right" ><b>Total :</b></td>
            <td><b><?php echo $hargaTotalOli ?></b></td>
        </tr>
    </tbody>
</table>
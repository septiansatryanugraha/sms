<?php $this->load->view('_heading/_headerContent') ?>
<style>
    #report {
        max-width: 1200px;
        height: 330px;
        margin: 0 auto
    }	
</style>
<section class="content">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <form method="post" action="<?php echo site_url('filter-supplier'); ?>">
                <div class="box-header">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Awal:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_awal" class="form-control" id="from" value="<?php echo date('01-m-Y'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Akhir:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_akhir" class="form-control" id="to" value="<?php echo date('t-m-Y'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label></label>
                            <div class="input-group date">
                                <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export klik"><i class="fa fa-refresh"></i> Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="report"></div>
    <!-- highchart -->
    <script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
    <!-- REPORT BARU -->
    <?php
    foreach ($graph1 as $data) {
        $tanggal[] = date('d-m-Y', strtotime($data->created_date));
        $data1[] = (float) $data->id_supplier;
    }
    ?>
    <script>
        jQuery(function () {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'report',
                    type: 'column',
                },
                title: {
                    text: 'Grafik Data Supplier',
                    x: -20
                },
                credits: {
                      enabled: false
                },
                xAxis: {
                    categories: <?php echo json_encode($tanggal); ?>
                },
                yAxis: {
                    title: {
                        text: 'Jumlah Supplier'
                    }
                },
                series: [{
                        name: 'Data Supplier ',
                        data: <?php echo json_encode($data1); ?>
                    },
                ]
            });
        });
    </script>
</section>
<script>
//klik loading ajax
    $(document).ready(function () {
        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='http://belanjaweb.com/sjj/assets/tambahan/gambar/loader-ok.gif' height='18'> ");
            $("#loading2").modal('show');
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $("#loading2").modal('hide');
                }
            });
            return true;
        });
    });
    // untuk datetime from
    $(function () {
        $("#from").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    // untuk datetime to
    $(function () {
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });


</script>





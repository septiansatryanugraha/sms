<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<center><h2>Report Data Supplier <?php echo $branch_description; ?></h2></center>
<br>
<table>
    <tr><td></td><td><b>Periode Report &nbsp;&nbsp;&nbsp; :</b></td><td><b>Periode awal &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_awal)) ?></b></td></tr>
</table>
<table>
    <tr><td></td><td></td><td><b>Periode akhir &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_akhir)) ?></td></tr></table>
<br><br>
<table border="1" width="80%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Nama Supplier</th>
            <th align="center">Telpon Supplier</th>
            <th align="center">Alamat Supplier</th>
            <th align="center">Tanggal Pembuatan</th>
            <th align="center">Di buat oleh</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($excel)) {
            $no = 1;
            foreach ($excel as $data) {
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $data->name ?></td>
                    <td><?php echo $data->phone ?>&nbsp;</td>
                    <td><?php echo $data->address ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->created_date)); ?>&nbsp;</td>
                    <td><?php echo $data->created_by ?></td>
                </tr>
                <?php
                $no++;
            }
            ?>
        <?php } else { ?>
        <?php } ?>
    </tbody>
</table>
<br><br>






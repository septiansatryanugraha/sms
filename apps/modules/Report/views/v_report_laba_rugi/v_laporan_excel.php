<style>
    .LastCol {
        border: 1px solid black;    
    }
</style>
<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<center><h2>Report Data Laba / Rugi PT Surabaya Jadi Jaya</h2></center>
<br>
<table>
    <tr><td></td><td><b>Periode Report &nbsp;&nbsp;&nbsp; :</b></td><td><b>Periode awal &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_awal)) ?></b></td></tr>
</table>
<table>
    <tr><td></td><td></td><td><b>Periode akhir &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_akhir)) ?></td></tr></table>
<br><br>
<table>
    <tr><td></td><td><b>Data Penjualan Oli</b></td></tr>
</table><br>
<table border="1" width="60%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Nama Customer </th>
            <th align="center">Qty</th>
            <th align="center">Harga / Qty</th>
            <th align="center">Total Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($penjualan_oli)) {
            $no = 1;
            foreach ($penjualan_oli as $data) {
                $idCustomer = $data->customer;
                $dataCustomer = $this->M_rekap_penjualan->selectById($idCustomer);
                $namaCustomer = $dataCustomer->name;
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->created_date)); ?>&nbsp;</td>
                    <td><?php echo $namaCustomer ?></td>
                    <td><?php echo $data->qty ?> liter </td>
                    <td>Rp. <?php echo $data->total ?></td>
                    <td>Rp. <?php echo $data->grandtotal ?> </td>
                </tr>
                <?php
                $no++;
            }
            ?>
        <?php } else { ?>
        <?php } ?>
    </tbody>
</table>
<table>
    <tr style="border-bottom: 1px solid black"><td></td><td></td><td></td><td></td><td><b>Total </b></td><td class="LastCol"><b>Rp. <?php echo $sum_penjualan_oli ?></b></td></tr></table>
<br><br>
<table>
    <tr><td></td><td><b>Data Penjualan Drum</b></td></tr>
</table><br>
<table border="1" width="60%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Nama Customer </th>
            <th align="center">Qty</th>
            <th align="center">Harga / Qty</th>
            <th align="center">Total Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($penjualan_drum)) {
            $no = 1;
            foreach ($penjualan_drum as $data) {
                $idCustomer = $data->customer;
                $dataCustomer = $this->M_rekap_penjualan->selectById($idCustomer);
                $namaCustomer = $dataCustomer->name;
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->created_date)); ?>&nbsp;</td>
                    <td><?php echo $namaCustomer ?></td>
                    <td><?php echo $data->qty ?> pcs </td>
                    <td>Rp. <?php echo $data->total ?></td>
                    <td>Rp. <?php echo $data->grandtotal ?> </td>
                </tr>
                <?php
                $no++;
            }
            ?>
        <?php } else { ?>
        <?php } ?>
    </tbody>
</table>
<table>
    <tr style="border-bottom: 1px solid black"><td></td><td></td><td></td><td></td><td><b>Total </b></td><td class="LastCol"><b>Rp. <?php echo $sum_penjualan_drum ?></b></td></tr></table>
<br>
<table>
    <?php $hasil = $sum_penjualan_oli + $sum_penjualan_drum; ?>
    <tr style="border-bottom: 1px solid black; border-top:1px solid black;"><td></td><td></td><td></td><td></td><td><b>Total Penjualan </b></td><td class="LastCol"><b>Rp. <?php echo $hasil ?></b></td></tr></table>
<br><br>
<table>
    <tr><td></td><td><b>Data Pembelian Oli</b></td></tr>
</table><br>
<table border="1" width="60%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Nama Supplier </th>
            <th align="center">Penyusutan </th>
            <th align="center">Qty</th>
            <th align="center">Harga / Qty</th>
            <th align="center">Total Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($pembelian_oli)) {
            $no = 1;
            foreach ($pembelian_oli as $data) {
                $idSupplier = $data->supplier;
                $dataSupplier = $this->M_rekap_pembelian->selectById($idSupplier);
                $namaSupplier = $dataSupplier->name;
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->created_date)); ?>&nbsp;</td>
                    <td><?php echo $namaSupplier ?></td>
                    <td><?php echo $data->depreciation ?> liter </td>
                    <td><?php echo $data->qty ?> liter </td>
                    <td>Rp. <?php echo $data->total ?></td>
                    <td>Rp. <?php echo $data->grandtotal ?> </td>
                </tr>
                <?php
                $no++;
            }
            ?>
        <?php } else { ?>
        <?php } ?>
    </tbody>
</table>
<table>
    <tr style="border-bottom: 1px solid black"><td></td><td></td><td></td><td></td><td></td><td><b>Total </b></td><td class="LastCol"><b>Rp. <?php echo $sum_pembelian_oli ?></b></td></tr></table>
<br>
<table>
    <tr><td></td><td><b>Data Pembelian Drum</b></td></tr>
</table><br>
<table border="1" width="60%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Tanggal Transaksi</th>
            <th align="center">Nama Supplier </th>
            <th align="center">Penyusutan </th>
            <th align="center">Qty</th>
            <th align="center">Harga / Qty</th>
            <th align="center">Total Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($pembelian_drum)) {
            $no = 1;
            foreach ($pembelian_drum as $data) {
                $idSupplier = $data->supplier;
                $dataSupplier = $this->M_rekap_pembelian->selectById($idSupplier);
                $namaSupplier = $dataSupplier->name;
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->created_date)); ?>&nbsp;</td>
                    <td><?php echo $namaSupplier ?></td>
                    <td><?php echo $data->depreciation ?> </td>
                    <td><?php echo $data->qty ?> pcs </td>
                    <td>Rp. <?php echo $data->total ?></td>
                    <td>Rp. <?php echo $data->grandtotal ?> </td>
                </tr>
                <?php
                $no++;
            }
            ?>
        <?php } else { ?>
        <?php } ?>
    </tbody>
</table>
<table>
    <tr style="border-bottom: 1px solid black"><td></td><td></td><td></td><td></td><td></td><td><b>Total </b></td><td class="LastCol"><b>Rp. <?php echo $sum_pembelian_drum ?></b></td></tr></table>
<br>
<table>
    <?php $hasil2 = $sum_pembelian_oli + $sum_pembelian_drum; ?>
    <tr style="border-bottom: 1px solid black; border-top:1px solid black;"><td></td><td></td><td></td><td></td><td></td><td><b>Total Penjualan </b></td><td class="LastCol"><b>Rp. <?php echo $hasil2 ?></b></td></tr></table>
<br>
<table>
    <tr><td></td><td><b>Nominal Laba / Rugi</b></td></tr>
</table>
<?php
$hasil = $sum_penjualan_oli + $sum_penjualan_drum;
$hasil2 = $sum_pembelian_oli + $sum_pembelian_drum;
if ($hasil > $hasil2) {
    $laba = $hasil - $hasil2;
    echo "<table><tr><td></td><td> Perusahaan Mendapat Laba Sebesar = " . "</td><td><b> Rp. " . $hasil . " - Rp. " . $hasil2 . " = </b></td><td align='left'><b> Rp. " . $laba . "</b></td></tr></table>";
} elseif ($hasi2 > $hasil) {

    $rugi = $hasil2 - $hasil;
    echo "<table><tr><td></td><td> Perusahaan Mendapat Rugi Sebesar = " . "</td><td><b> Rp. " . $hasil2 . " - Rp. " . $hasil . " = </b></td><td align='left'><b> Rp. " . $rugi . "</b></td></tr></table>";
} else {
    echo "<tr><td></td><td><b> Tidak laba dan tidak rugi </b></td></tr>";
}
?>









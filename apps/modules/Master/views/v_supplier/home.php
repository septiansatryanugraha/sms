<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="box">
        <?php if ($accessAdd > 0) { ?>
            <div class="box-header with-border">
                <a class="klik" href="<?php echo site_url('add-' . $menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Tambah <?php echo $judul; ?></button></a>
            </div>
        <?php } ?>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Telpon</th>
                            <th>Alamat</th>
                            <th>Di Buat</th>
                            <th>Di Ubah</th>
                            <th style="width:50px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="tempat-modal"></div>
    </div>
</section>

<script type="text/javascript">
    //untuk load data table ajax	
    var save_method; //for save method string
    var table;
    $(document).ready(function () {
        //datatables
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
            oLanguage: {
                sProcessing: "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='30px'>"
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('Master/Supplier/ajaxList') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-supplier", function () {
        var id_supplier = $(this).attr("data-id");

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('Master/Supplier/prosesDelete'); ?>",
                data: "id_supplier=" + id_supplier,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == 'berhasil') {
                        $("tr[data-id='" + id_supplier + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        hapus_berhasil();
                        reload_table();
                        setTimeout("window.location='<?php echo site_url('Master/Supplier'); ?>'", 450);
                    } else {
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        swal("Peringatan", result.status, "warning");
                    }
                }
            });
        });
    });

    $(document).on("click", ".detail-supplier", function () {
        var id = $(this).attr("data-id");

        $.ajax({
            method: "POST",
            url: "<?php echo base_url('Master/Supplier/Detail'); ?>",
            data: "id=" + id
        }).done(function (data) {
            $('#tempat-modal').html(data);
            $('#detail-supplier').modal('show');
        })
    })

</script>







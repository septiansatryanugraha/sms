<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="box">
        <?php if ($accessAdd > 0) { ?>
            <div class="box-header with-border">
                <a class="klik" href="<?php echo site_url('add-' . $menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Tambah <?php echo $judul; ?></button></a>
            </div>
        <?php } ?>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Customer</th>
                            <th>Telpon Customer</th>
                            <th>Alamat Customer</th>
                            <th>Di Buat</th>
                            <th>Di Ubah</th>
                            <th style="width:50px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //untuk load data table ajax	
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        //datatables
        table = $('#table').DataTable({
            "aLengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "pageLength": 25,
            "processing": true, //Feature control the processing indicator.
            // "scrollX": true,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "No data found in the server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('Master/Customer/ajaxList') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });


    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-customer", function () {
        var id_customer = $(this).attr("data-id");

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('Master/Customer/prosesDelete'); ?>",
                data: "id_customer=" + id_customer,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == 'berhasil') {
                        $("tr[data-id='" + id_customer + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        hapus_berhasil();
                        reload_table();
                        setTimeout("window.location='<?php echo site_url('Master/Customer'); ?>'", 450);
                    } else {
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        swal("Peringatan", result.status, "warning");
                    }
                }
            });
        });
    });

</script>







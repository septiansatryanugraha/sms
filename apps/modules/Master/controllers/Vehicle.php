<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends AUTH_Controller
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_vehicle');
        $this->load->model('M_sidebar');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'vehicle');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = "Armada";
        $data['judul'] = "Armada";

        $this->loadkonten('v_vehicle/home', $data);
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', 'vehicle');
        $accessDel = $this->M_sidebar->access('del', 'vehicle');
        $list = $this->M_vehicle->getData(1);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $status = 'Disewa';
            if ($value->is_ready > 0) {
                $status = 'Ready';
            }

            $row = array();
            $row[] = $no;
            $row[] = $value->brand;
            $row[] = $value->police_number;
            $row[] = $status;
            $row[] = date('d-m-Y H:i:s', strtotime($value->created_date));
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));

            //add html for action
            $action = " <div class='btn-group pull-right'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . site_url('edit-vehicle/' . $value->id_vehicle) . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-vehicle' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_vehicle . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $access = $this->M_sidebar->access('add', 'vehicle');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Armada";
        $data['judul'] = "Tambah Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $this->loadkonten('v_vehicle/tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $brand = trim($this->input->post("brand"));
        $policeNumber = trim($this->input->post("police_number"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessAdd = $this->M_sidebar->access('add', 'vehicle');
            if ($accessAdd->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($brand) == 0) {
                $errCode++;
                $errMessage = "Merk Armada wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($policeNumber) == 0) {
                $errCode++;
                $errMessage = "Nomor Polisi Armada wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkPoliceNumberExist = $this->checkPoliceNumberExist(0, $policeNumber);
            if ($checkPoliceNumberExist == true) {
                $errCode++;
                $errMessage = "Nomor Polisi Armada sudah terdaftar pada Armada lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $dataVehicle = array(
                    'id_branch' => $this->branch,
                    'brand' => $brand,
                    'police_number' => $policeNumber,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_vehicle', $dataVehicle);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $access = $this->M_sidebar->access('edit', 'vehicle');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Armada";
        $data['judul'] = "Ubah Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataVehicle = $this->M_vehicle->selectById($id);
            if ($dataVehicle != null) {
                $data['dataVehicle'] = $dataVehicle;
                $data['idVehicle'] = $id;
                $this->loadkonten('v_vehicle/update', $data);
            } else {
                echo "<script>alert('Armada tidak tersedia.'); window.location = '" . base_url("vehicle") . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $brand = trim($this->input->post("brand"));
        $policeNumber = trim($this->input->post("police_number"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessEdit = $this->M_sidebar->access('edit', 'vehicle');
            if ($accessEdit->menuview == 0) {
                $errMessage = "You don't have access.";
                $errCode++;
            }
        }
        if ($errCode == 0) {
            $checkVehicle = $this->M_vehicle->selectById($id);
            if ($checkVehicle == null) {
                $errCode++;
                $errMessage = "Armada tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($brand) == 0) {
                $errCode++;
                $errMessage = "Merk Armada wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($policeNumber) == 0) {
                $errCode++;
                $errMessage = "Nomor Polisi Armada wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkPoliceNumberExist = $this->checkPoliceNumberExist(0, $policeNumber, $id);
            if ($checkPoliceNumberExist == true) {
                $errCode++;
                $errMessage = "Nomor Polisi Armada sudah terdaftar pada Armada lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $dataVehicle = array(
                    'brand' => $brand,
                    'police_number' => $policeNumber,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_vehicle', $dataVehicle, array('id_vehicle' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_vehicle'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('del', 'vehicle');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getVehicle = $this->M_vehicle->selectById($id);
            if ($getVehicle == NULL) {
                $errCode++;
                $errMessage = "Armada tidak tersedia.";
            }
        }

        if ($errCode == 0) {
            $checkVehicleUsed = $this->checkVehicleUsed(0, $id);
            if ($checkVehicleUsed['is_used'] == true) {
                $errCode++;
                $errMessage = $checkVehicleUsed['message'];
            }
        }

        if ($errCode == 0) {
            try {
                $this->db->delete('tbl_vehicle', array('id_vehicle' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function checkPoliceNumberExist($isAjax = 1, $policeNumber, $idVehicle = NULL)
    {
        $isExist = false;
        if ($isAjax > 0) {
            $policeNumber = trim($this->input->post("police_number"));
            $idVehicle = trim($this->input->post("id_vehicle"));
        }
        if (strlen($policeNumber) > 0) {
            $q = "SELECT * FROM tbl_vehicle WHERE police_number = '{$policeNumber}'";
            $q .= " AND id_branch = '{$this->branch}'";
            if (strlen($idVehicle) > 0) {
                $q .= " AND id_vehicle <> '{$idVehicle}'";
            }
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isExist = true;
            }
        }
        $arr = array('is_exist' => $isExist);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $isExist;
        }
    }

    public function checkVehicleUsed($isAjax = 1, $idVehicle)
    {
        $isUsed = false;
        $message = "";

        if ($isAjax > 0 && strlen($idSupplier) == 0) {
            $idVehicle = trim($this->input->post("id_vehicle"));
        }

        if ($isUsed == false) {
            $q = "SELECT * FROM tbl_vehicle_rent WHERE id_vehicle = '{$idVehicle}'";
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isUsed = true;
                $message = "Armada sudah ada di menu Penyewaan Armada.";
            }
        }
        if ($isUsed == false) {
            $q = "SELECT * FROM tbl_vehicle_maintenance WHERE id_vehicle = '{$idVehicle}'";
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isUsed = true;
                $message = "Armada sudah ada di menu Pemeliharaan Armada.";
            }
        }

        $arr = array('is_used' => $isUsed, 'message' => $message);
        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $arr;
        }
    }
}

<?php
$listBranch = $this->db->query("SELECT * FROM tbl_branch")->result();
if (!isset($_SESSION['id_branch'])) {
    $_SESSION['id_branch'] = 1;
}
if (strlen($_SESSION['id_branch']) == 0) {
    $_SESSION['id_branch'] = 1;
}

$sqlBranch = "SELECT * FROM tbl_branch WHERE id_branch = '{$_SESSION['id_branch']}'";
$nameBranch = $this->db->query($sqlBranch)->row()->name;

$sqlOli = "SELECT * FROM tbl_item_stock WHERE id_branch = '{$_SESSION['id_branch']}' AND id_item = 1";
$stockOli = $this->db->query($sqlOli)->row()->qty;
$stockOli = str_replace(".", ",", $stockOli);
$sqlrum = "SELECT * FROM tbl_item_stock WHERE id_branch = '{$_SESSION['id_branch']}' AND id_item = 2";
$stockDrum = $this->db->query($sqlrum)->row()->qty;
$stockDrum = str_replace(".", ",", $stockDrum);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Apps SJJ-SMS | <?php echo $page; ?></title>
        <link rel="icon" href="<?php echo base_url() ?>assets/tambahan/gambar/logo-sms.png">
        <!-- meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- css --> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/tambahan.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-summernote/summernote.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-green.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-red.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/accounting.js"></script>
        <style type="text/css">
            .select-session{
                width: 110px;
            }
            @media only screen and (min-width: 800px) {
                .table-responsive {
                    overflow: hidden;
                }
            }
        </style>
    </head>
    <body class="hold-transition <?php echo ($_SESSION['id_branch'] == 1) ? 'skin-red' : 'skin-green'; ?> sidebar-mini">
        <div class="wrapper">
            <!-- header -->
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url(); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><small><?php echo substr(strtoupper($nameBranch), 0, 3); ?></small></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><?php echo strtoupper($nameBranch); ?></b> APPS</span>
                </a>
                <!-- nav -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu" style="padding-top: 11px; width: 200px;">
                                <span style="color: #fff;">Filter Aplikasi :</span>
                                <select id="filter_branch" class="select-session" name="filter_branch">
                                    <?php foreach ($listBranch as $data) { ?>
                                        <option value="<?php echo $data->id_branch; ?>" <?php if ($data->id_branch == $_SESSION['id_branch']) echo 'selected'; ?> ><?php echo $data->name; ?></option>
                                    <?php } ?>
                                </select>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs">Jumlah Oli = <?php echo $stockOli; ?></span>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- The user image in the navbar-->
                                    <img src="<?php echo base_url(); ?>upload/user/<?php echo $userdata->foto; ?>" class="user-image" alt="User Image">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs"><?php echo $userdata->nama; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>upload/user/<?php echo $userdata->foto; ?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $userdata->nama; ?> - Administrator
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a class="klik " href="<?php echo site_url('profile'); ?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a class="klik" href="<?php echo site_url('Default/Auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <div id ="loading2"></div>
                <section class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>upload/user/<?php echo $userdata->foto; ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $userdata->nama; ?></p>
                            <!-- Status -->
                            Administrator
                        </div>
                    </div>
                    <!-- Sidebar Menu -->
                    <div id="menu">
                        <ul class="sidebar-menu">
                            <li class="header">LIST MENU</li>
                            <!-- menu sidebar list-->
                            <?php
                            $this->load->model('M_sidebar');
                            $menuActiveNow = $this->uri->rsegment(1);
                            $menu = $this->M_sidebar->left_menu();
                            $getMenuActiveNow = $this->M_sidebar->selectByController($menuActiveNow);
                            if ($menu->num_rows() > 0) {
                                foreach ($menu->result() as $row) {
                                    $menu_child = $this->M_sidebar->left_menu_child($row->id_menu);
                                    $access = $this->M_sidebar->access('view', $row->kode_menu);
                                    // jika view 1 (tampilkan menu)
                                    if ($access->menuview == 1) {
                                        if ($menu_child->num_rows() > 0) {
                                            $activeParent = "";
                                            $open = "";
                                            if ($getMenuActiveNow->parent == $row->id_menu) {
                                                $activeParent = "active";
                                                $open = "open";
                                            }
                                            echo "  <li class='treeview " . $activeParent . "'><a href='javascript:;'><i class='" . $row->icon . "'></i>
                                                    <span class='title'>" . $row->nama_menu . "</span>
                                                    <span class='pull-right-container " . $open . "'>
                                                    <i class='fa fa-angle-left pull-right'></i></span></a>";
                                            // untuk sub menu dropdownya
                                            echo "<ul class='treeview-menu'>";
                                            if ($menu_child->num_rows() > 0) {
                                                foreach ($menu_child->result() as $obj) {
                                                    $access_child = $this->M_sidebar->access('view', $obj->kode_menu);
                                                    if ($access_child->menuview == 1) {
                                                        $active = ($getMenuActiveNow->id_menu == $obj->id_menu) ? 'active' : '';
                                                        echo "  <li class='" . $active . "'>
                                                                    <a class='klik' href='" . site_url($obj->link) . "'>
                                                                        <i class='" . $obj->icon . "'></i>
                                                                        <span class='title'>" . $obj->nama_menu . "</span>
                                                                    </a>
                                                                </li>";
                                                    }
                                                }
                                            }
                                            echo"</ul></li>";
                                        } else {
                                            $activeParent = ($menuActiveNow == $row->link) ? 'active' : '';
                                            echo "  <li class = '" . $activeParent . "'>
                                                        <a class='klik' href='" . site_url($row->link) . "'>
                                                            <i class='" . $row->icon . "'></i>
                                                            <span class='title'>" . $row->nama_menu . "</span>
                                                        </a>
                                                    </li>";
                                        }
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <script type="text/javascript">
                    // untuk select2 ajak pilih tipe
                    $(function () {
                        $(".select-session").select2({
                            placeholder: " -- pilih supplier -- "
                        });
                    });
                </script>


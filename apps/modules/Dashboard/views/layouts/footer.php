</div>
<!-- /.content-wrapper -->
<!-- footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Dashboard Admin
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y') ?>&nbsp; Angkasa Muda Digital.</strong> All rights reserved.
</footer>

<div class="control-sidebar-bg"></div>
</div>
<!-- js -->
<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>admin-lte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>admin-lte/dist/js/adminlte.min.js"></script>
<!-- checkbox -->
<script src="<?php echo base_url(); ?>admin-lte/plugins/iCheck/icheck.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>admin-lte/dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>admin-lte/dist/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-summernote/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ajax.js"></script>
<script type="text/javascript">
    //klik loading ajax
    $(document).ready(function () {
        //----- Number Format -----//
        $(document).on('keypress', '.number_only', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.number_only').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            }
        });
        $('.number_only').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val(0);
            }
        });

        $(document).on('keypress', '.number_decimal', function (event) {
            if ((event.which != 44 && event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.number_decimal').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            }
        });
        $('.number_decimal').on('focusout', function () {
            var x = $(this).val();
            var x_comma = x.split(',').length;
            if (x == 0 || x.length == 0 || x_comma > 2) {
                $(this).val(0);
            }
        });

        //----- Accounting Format -----// 
        $('.formatCurrency').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            } else {
                $(this).val(accounting.unformat(x));
            }
        });
        $('.formatCurrency').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val(0);
            } else {
                $(this).val(accounting.formatMoney(x));
            }
        });

        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='https://www.apps-sms.surabayajadijaya28.com/assets/tambahan/gambar/loader-ok.gif' height='18'> ");
            $("#loading2").modal('show');
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $("#loading2").modal('hide');
                }
            });
            return true;
        });
        $('.datepicker').attr('readonly', 'readonly');
        $('.datepicker').css('background-color', '#fff', 'important');
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        });
        $("#filter_branch").change(function () {
            $.ajax({
                url: '<?php echo site_url('Dashboard/Dashboard/changeBranch'); ?>',
                method: 'POST',
                data: {branch: $(this).val()},
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    setTimeout(location.reload.bind(location), 450);
                },
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

</html>
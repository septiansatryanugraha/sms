<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #report-beli {
        max-width: 1200px;
        height: 330px;
        margin: 0 auto
    }

    #report-jual {
        max-width: 1200px;
        height: 330px;
        margin: 0 auto
    }
</style>

<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><span class="count"><?php echo $supplier ?></span></h3>
                    <p>Supplier</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?php echo site_url('supplier'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><span class="count"><?php echo $customer ?></span></h3>
                    <p>Customer</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?php echo site_url('customer'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><span class="count"><?php echo $vehicle ?></span></h3>
                    <p>Armada</p>
                </div>
                <div class="icon">
                    <i class="fa fa-car"></i>
                </div>
                <a href="<?php echo site_url('vehicle'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><span class="count"><?php echo $trans_pinjam ?></span></h3>
                    <p>Transaksi Peminjaman</p>
                </div>
                <div class="icon">
                    <i class="fa fa-refresh"></i>
                </div>
                <a href="<?php echo site_url('peminjaman'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><span class="count"><?php echo $trans_sewa ?></span></h3>
                    <p>Transaksi Penyewaan</p>
                </div>
                <div class="icon">
                    <i class="fa fa-refresh"></i>
                </div>
                <a href="<?php echo site_url('penyewaan'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><span class="count"><?php echo $trans_beli ?></span></h3>
                    <p>Transaksi Pembelian</p>
                </div>
                <div class="icon">
                    <i class="fa fa-refresh"></i>
                </div>
                <a href="<?php echo site_url('pembelian'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><span class="count"><?php echo $trans_jual ?></span></h3>
                    <p>Transaksi Penjualan</p>
                </div>
                <div class="icon">
                    <i class="fa fa-refresh"></i>
                </div>
                <a href="<?php echo site_url('penjualan'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><span class="count"><?php echo $trans_service ?></span></h3>
                    <p>Maintenance</p>
                </div>
                <div class="icon">
                    <i class="fa fa-cogs"></i>
                </div>
                <a href="<?php echo site_url('pemeliharaan'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <section class="col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                    <!-- pesan customer -->
                    <h3 class="box-title">Grafik data pembelian </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="report-beli"></div>
                            <?php
                            if (!empty($graph1)) {
                                foreach ($graph1 as $data) {
                                    $tanggal_beli[] = date('d-m-Y', strtotime($data->date));
                                    $nominal_beli[] = (float) $data->nominal;
                                }
                            } else {
                                echo "Belum Ada data yang masuk";
                            }
                            ?>
                            <!-- highchart -->
                            <script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
                            <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
                            <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
                            <!-- Script untuk memanggil library Highcharts -->
                            <script>
                                jQuery(function () {
                                    Highcharts.setOptions({
                                        lang: {
                                            thousandsSep: '.'
                                        }
                                    });
                                    new Highcharts.Chart({
                                        chart: {
                                        renderTo: 'report-beli',
                                        type: 'column',
                                        },

                                        title: {
                                            text: 'Grafik Pembelian',
                                            x: -0,
                                        }, subtitle: {
                                    text: 'Grafik pembelian dalam periode 1 bulan',style: {
                                    align: 'center'
                                    }
                                    }, credits: {
                                            enabled: false
                                        },
                                    xAxis: {
                                            categories: <?php echo json_encode($tanggal_beli); ?>
                                        },
                                        yAxis: {
                                            labels: {
                                                formatter: function () {
                                                    return IDRFormatter(this.value, 'Rp.');
                                                }
                                            },
                                            title: {
                                                text: 'Nominal '
                                            }
                                        }, tooltip: {
                                        formatter: function() {
                                        return 'Nominal Tanggal ' + this.x + '  <b>Rp. ' + Highcharts.numberFormat(this.y,0) + '</b>';
                                        }},
                                        series: [{name: 'Data ',shadow : true, data: <?php echo json_encode($nominal_beli); ?>}]
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- data new member-->
        <section class="col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                    <!-- /.box-header -->
                    <h3 class="box-title">Grafik data penjualan</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="report-jual"></div>
                            <?php
                            if (!empty($graph2)) {
                                foreach ($graph2 as $data2) {
                                    $tanggal_jual[] = date('d-m-Y', strtotime($data2->date));
                                    $nominal_jual[] = (float) $data2->nominal;
                                }
                            } else {
                                echo "Belum Ada data yang masuk";
                            }
                            ?>
                            <!-- highchart -->
                            <script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
                            <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
                            <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>  
                            <!-- Script untuk memanggil library Highcharts -->
                            <script>
                                jQuery(function () {
                                    Highcharts.setOptions({
                                        lang: {
                                            thousandsSep: '.'
                                        }
                                    });

                                    new Highcharts.Chart({
                                        chart: {
                                            renderTo: 'report-jual',
                                            type: 'column',
                                        },
                                         title: {
                                            text: 'Grafik Penjualan',
                                            x: -0,
                                        }, subtitle: {
                                    text: 'Grafik penjualan dalam periode 1 bulan',style: {
                                    align: 'center'
                                    }
                                    }, credits: {
                                            enabled: false
                                        },
                                        xAxis: {
                                            categories: <?php echo json_encode($tanggal_jual); ?>
                                        },
                                        yAxis: {
                                            labels: {
                                                formatter: function () {
                                                    return IDRFormatter(this.value, 'Rp.');
                                                }
                                            },
                                            title: {
                                                text: 'Nominal'
                                            }
                                        }, tooltip: {
                                        formatter: function() {
                                        return 'Nominal Tanggal ' + this.x + '  <b>Rp. ' + Highcharts.numberFormat(this.y,0) + '</b>';
                                        }},series: [{name: 'Data',shadow : true, data: <?php echo json_encode($nominal_jual); ?>}]
                                    });
                                });

                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        function IDRFormatter(angka, prefix) {
            var number_string = angka.toString().replace(/[^,\d]/g, ''),
                    split = number_string.split(','),
                    sisa = split[0].length % 3,
                    rupiah = split[0].substr(0, sisa),
                    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>

    <script>
        // Animasi angka bergerak dashboard
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>
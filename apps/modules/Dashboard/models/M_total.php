<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function totalCustomer()
    {
        $this->db->from('tbl_customer');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function totalSupplier()
    {
        $this->db->from('tbl_supplier');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function totalVehicle()
    {
        $this->db->from('tbl_vehicle');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function transaksiPeminjaman()
    {
        $this->db->from('tbl_supplier_loan');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function transaksiPenyewaan()
    {
        $this->db->from('tbl_vehicle_rent');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function transaksiPembelian()
    {
        $this->db->from('tbl_transaction_purchase');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function transaksiPenjualan()
    {
        $this->db->from('tbl_transaction_sales');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function transaksiMaintenance()
    {
        $this->db->from('tbl_vehicle_maintenance');
        $this->db->where('id_branch', $this->branch);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function getTotalPembelian()
    {
        $query = $this->db->query("SELECT date,sum(nominal) AS nominal FROM tbl_transaction_purchase WHERE id_branch = " . $this->branch . " AND date between DATE_ADD(date(now()), INTERVAL -14 DAY) and date(now()) GROUP BY DATE(date)");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getTotalPenjualan()
    {
        $query = $this->db->query("SELECT date,sum(nominal) AS nominal FROM tbl_transaction_sales WHERE id_branch = " . $this->branch . " AND date between DATE_ADD(date(now()), INTERVAL -14 DAY) and date(now()) GROUP BY DATE(date)");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
}

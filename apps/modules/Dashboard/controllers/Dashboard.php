<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_total');
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('layouts/footer', $data);
    }

    public function index()
    {
        $data['customer'] = $this->M_total->totalCustomer();
        $data['supplier'] = $this->M_total->totalSupplier();
        $data['vehicle'] = $this->M_total->totalVehicle();
        $data['trans_pinjam'] = $this->M_total->transaksiPeminjaman();
        $data['trans_sewa'] = $this->M_total->transaksiPenyewaan();
        $data['trans_beli'] = $this->M_total->transaksiPembelian();
        $data['trans_jual'] = $this->M_total->transaksiPenjualan();
        $data['trans_service'] = $this->M_total->transaksiMaintenance();
        $data['graph1'] = $this->M_total->getTotalPembelian();
        $data['graph2'] = $this->M_total->getTotalPenjualan();
        $data['userdata'] = $this->userdata;

        $data['page'] = "Beranda";
        $data['judul'] = "Beranda";
        $this->loadkonten('home', $data);
    }

    public function changeBranch()
    {
        $branch = $this->input->post('branch');
        if (strlen($branch) == 0) {
            $branch = 1;
        } else {
            $sql = "SELECT * FROM tbl_branch WHERE id_branch = '{$branch}'";
            $result = $this->db->query($sql)->row();
            if ($result == null) {
                $branch = 1;
            }
        }
        $_SESSION['id_branch'] = $branch;

        echo json_encode($branch);
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */
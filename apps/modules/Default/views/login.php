<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Apps SJJ-SMS | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/ionicons.min.css">
	
	<!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/tambahan/style-login.css">
	
	<link rel="icon" href="<?php echo base_url()?>assets/tambahan/gambar/logo-sms.png">
	
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
	
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	
	
	<style type="text/css">
	
	/*.login-page { background: url(<?php echo base_url(); ?>assets/tambahan/gambar/login-background2.jpg) !important; no-repeat; }*/

  [ login more ]*/
.login100-more {
  width: calc(100% - 560px);
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  position: relative;
  z-index: 1;
}

.login100-more::before {
  content: "";
  display: block;
  position: absolute;
  z-index: -1;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
 /* background: rgba(0,0,0,0.1);*/
}

@media (max-width: 992px) {

  .login100-more {
    width: 50%;
  }
}

@media (max-width: 768px) {

  .login100-more {
    display: none;
  }
}

.field-icon {
  float: left;
  margin-left: 91%;
  margin-top: -25px;
  font-size: 17px;
  position: relative;
  z-index: 2;
 }
	
	</style>
	

  </head>
  <body class="hold-transition login-page">
     <div class="row">
      <div class="col-md-7 login100-more">
        <img src="<?php echo base_url(); ?>assets/tambahan/gambar/bg-03.png" width="101%"> 
      </div>
      <div class="col-md-5">
    <div class="login-box">
      <div class="login-logo">
        <img src="<?php echo base_url(); ?>assets/tambahan/gambar/logo-sjj.png" height="130">
      </div>

      <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">
          Silahkan Login !!
        </p>

        <form action="<?php echo base_url('Default/Auth/login'); ?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="username" placeholder="Username" name="username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
             <input type="password" class="form-control" placeholder="Password" id="password-field" name="password" aria-describedby="sizing-addon2" >
            <!-- <span toggle="#password-field" class="fa fa-eye field-icon toggle-password form-control-feedback"></span> -->
            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password "></span>
          </div>
		  
          <div class="row">
            <div class="col-xs-offset-8 col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-lock"></i> Masuk</button>
            </div>
          </div>
        </form>
      </div>
    </div>
	  
	  
      <!-- /.login-box-body -->
      <?php
        echo show_err_msg($this->session->flashdata('error_msg'));
      ?>
    </div>
  </div>
	
	<script>
	
		$(document).ready(function(){		
		$('.form-ok').click(function(){
			if($(this).is(':checked')){
				$('#password').attr('type','text');
			}else{
				$('#password').attr('type','password');
			}
		});
	});
	
	</script>

  <script>
    // untuk show hide password
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
    });
  </script>
	
	
	
	
	
    <!-- /.login-box -->
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
	
   
  </body>
</html>

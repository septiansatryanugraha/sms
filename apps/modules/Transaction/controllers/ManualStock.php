<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class ManualStock extends AUTH_Controller
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_manual_stock');
        $this->load->model('M_item');
        $this->load->model('M_sidebar');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'customer');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['menuName'] = "manual-stock";
        $data['userdata'] = $this->userdata;
        $data['page'] = "Manual Stok Barang";
        $data['judul'] = "Manual Stok Barang";

        $this->loadkonten('v_manual_stock/home', $data);
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $list = $this->M_manual_stock->getData(1, $filter);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;

            $getItem = $this->M_item->selectById($value->id_item);
            $nameItem = $getItem->name;

            $type = $value->type;
            if ($type == 'in') {
                $type = 'Masuk';
            } else if ($type == 'out') {
                $type = 'Keluar';
            } else {
                $type = '';
            }

            $row = array();
            $row[] = $no;
            $row[] = $value->code;
            $row[] = $nameItem;
            $row[] = $type;
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($value->qty, 0, ",", ".") . '</div">';
            $row[] = $value->description;
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));
            $row[] = $value->updated_by;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('add', 'manual-stock');
        $data['page'] = "Manual Stok Barang";
        $data['userdata'] = $this->userdata;
        $data['judul'] = "Tambah Manual Stok Barang";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "manual-stock";
            $data['privilegeId'] = $privilegeId;
            $data['item'] = $this->M_item->selectItem();
            $this->loadkonten('v_manual_stock/tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $idItem = $this->input->post("id_item");
        $dateManual = date('d-m-Y');
        if ($privilegeId == 1) {
            $dateManual = $this->input->post("date");
        }
        $code = $this->M_manual_stock->code();
        $type = $this->input->post("type");
        $qty = $this->input->post("qty");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessAdd = $this->M_sidebar->access('add', 'manual-stock');
            if ($accessAdd->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idItem) == 0) {
                $errCode++;
                $errMessage = "Barang harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateManual) == 0) {
                $errCode++;
                $errMessage = "Tanggal Manual harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Jenis harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Jenis harus 'in' atau 'out'.";
            }
        }
        if ($errCode == 0) {
            if (strlen($qty) == 0) {
                $errCode++;
                $errMessage = "Jumlah Barang harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($qty <= 0) {
                $errCode++;
                $errMessage = "Jumlah Barang harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $getItem = $this->M_item->selectById($idItem);
            if ($getItem == null) {
                $errCode++;
                $errMessage = "Item invalid";
            }
        }
        if ($type == 'out') {
            if ($errCode == 0) {
                $checkStockItemForOut = $this->checkStockItemForOut(0, $idItem, $qty);
                if ($checkStockItemForOut['is_ready'] == false) {
                    $errCode++;
                    $errMessage = $checkStockItemForOut['message'];
                }
            }
        }

        if ($errCode == 0) {
            try {
                $data = array(
                    'id_branch' => $this->branch,
                    'id_item' => $idItem,
                    'date' => date('Y-m-d', strtotime($dateManual)),
                    'code' => $code,
                    'date' => $date,
                    'type' => $type,
                    'qty' => $qty,
                    'description' => $description,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_item_manual', $data);
                $idItemManual = $this->db->insert_id();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            $labelType = 'Keluar';
            if ($type == 'in') {
                $labelType = 'Masuk';
            }
            $isok2 = $this->M_item->balance("tbl_item_manual", array('id_item_manual' => $idItemManual), $type, 'MANUAL STOK ' . strtoupper($getItem->name), 'Manual ' . $labelType . ' Stok ' . $getItem->name . ' sebesar ' . $qty . '. ' . $description);
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function checkStockItemForOut($isAjax = 1, $idItem = NULL, $qty = 0)
    {
        if ($isAjax > 0) {
            $idItem = $this->input->post("id_item");
            $qty = $this->input->post("qty");
        }

        $isReady = true;
        $message = "";
        if ($qty > 0) {
            $getStockItem = $this->M_item->getStockItem($idItem);
            if ($qty > $getStockItem) {
                $isReady = false;
                $message = "Stok tidak mencukupi";
            }
        }

        $arr = array('is_ready' => $isReady, 'message' => $message);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $arr;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Unit test">
    public function normalisasiBalance()
    {
        $sql = "SELECT * FROM tbl_item_manual";
        $result = $this->db->query($sql)->result();
        foreach ($result as $key => $value) {
            $qFinanceHistory = "SELECT * FROM tbl_finance_history
                                WHERE ref_table = 'tbl_item_manual'
                                AND ref_id = '{$value->id_item_manual}'
                                AND fixed > 0 ";
            $getLastFinanceHistory = $this->db->query($qFinanceHistory)->row();
            if ($getLastFinanceHistory != NULL) {
                $dataUpdateFinanceHistory = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'updated_date' => $value->updated_date,
                );
                $this->db->update('tbl_finance_history', $dataUpdateFinanceHistory, array('id_finance_history' => $getLastFinanceHistory->id_item_history));
            }
        }
    }
    //</editor-fold>
}

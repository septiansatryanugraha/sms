<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class Purchase extends AUTH_Controller
{
    const __ppn = 0.11;

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_sidebar');
        $this->load->model('M_generate_code');
        $this->load->model('M_purchase');
        $this->load->model('M_supplier');
        $this->load->model('M_bank');
        $this->load->model('M_supplier_loan_installment');
        $this->load->model('M_supplier_loan');
        $this->load->model('M_finance');
        $this->load->model('M_item');
        $this->load->model('M_vehicle');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'pembelian');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['menuName'] = "pembelian";
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pembelian";
        $data['judul'] = "Pembelian ";
        $data['supplier'] = $this->M_supplier->selectItem();
        $this->loadkonten('v_purchase/home', $data);
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    function ambilData()
    {
        $modul = $this->input->post('modul');
        $id = $this->input->post('id_supplier');

        if ($modul == "status") {
            echo $this->M_purchase->getData1($id);
        } else if ($modul == "cicilan") {
            echo $this->M_purchase->getData2($id);
        } else if ($modul == "nominal") {
            echo $this->M_purchase->getData3($id);
        } else if ($modul == "tanggal") {
            echo $this->M_purchase->getData4($id);
        }
    }

    public function ajaxList()
    {
        $idSupplier = $this->input->post('id_supplier');
        $isApproved = $this->input->post('is_approved');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'id_supplier' => $idSupplier,
            'is_approved' => $isApproved,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', 'pembelian');
        $accessApprove = $this->M_sidebar->access('conf', 'pembelian');
        $accessDel = $this->M_sidebar->access('del', 'pembelian');

        $list = $this->M_purchase->getData(1, $filter);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value->code;

            $statusApprove = '<small class="label pull-center bg-yellow">Pending</small">';
            if ($value->nominal > 0) {
                $statusApprove = '<small class="label pull-center bg-blue">Confirm</small">';
            }
            if ($value->is_approved > 0) {
                $statusApprove = '<small class="label pull-center bg-green">Approved</small">';
            }


            $linkPrint = "";
            if ($value->nominal > 0) {
                $linkPrint = "<li><a href='" . site_url('cetak-pembelian-approve/' . $value->id_transaction_purchase) . "' target='__blank'><i class='fa fa-print'></i> Cetak Approve</a></li>";
            }
            if ($value->is_approved > 0) {
                $linkPrint = "<li><a href='" . site_url('cetak-pembelian-approve/' . $value->id_transaction_purchase) . "' target='__blank'><i class='fa fa-print'></i> Cetak Approve</a></li>";
            }

            $strSupplierLoanInstallment = "";
            $nominalSupplierLoanInstallment = 0;

            $subTotal = $value->nominal;
            $grandTotal = $subTotal;
            $idSupplierLoanInstallment = $value->id_supplier_loan_installment;
            if (strlen($idSupplierLoanInstallment) > 0) {
                $expIdSupplierLoanInstallment = explode(',', $idSupplierLoanInstallment);
                if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                    foreach ($expIdSupplierLoanInstallment as $k => $val) {
                        $dataSupplierLoanInstallment = $this->M_supplier_loan_installment->selectById($val);
                        $dataSupplierLoan = $this->M_supplier_loan->selectById($dataSupplierLoanInstallment->id_supplier_loan);
                        $nominalSupplierLoanInstallment += $dataSupplierLoanInstallment->nominal;
                        if ($k > 0) {
                            $strSupplierLoanInstallment .= "<br>";
                        }
                        $html = "<ul style='padding-left: 15px;'>";
                        $html .= "<li>";
                        $html .= number_format($dataSupplierLoanInstallment->nominal, 0, ",", ".");
                        $html .= "<br>" . $dataSupplierLoan->description;
                        $html .= "</li>";
                        $html .= "</ul>";
                        $strSupplierLoanInstallment .= $html;
                    }
                    $strSupplierLoanInstallment .= "<br><b>Total Potongan : " . number_format($nominalSupplierLoanInstallment, 0, ",", ".") . "</b>";
                }
            } else {
                $strSupplierLoanInstallment = '<div width="100%" style="text-align: right;">0</div">';
            }

            $grandTotal = $grandTotal - $nominalSupplierLoanInstallment;

            $strDetailItem = "";
            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($value->id_transaction_purchase);
            foreach ($dataPurchaseDetail as $keyItem => $valItem) {
                $qty = number_format($valItem->qty, 0, ",", ".");
                $html = "<ul style='padding-left: 15px;'>";
                $html .= "<li>";
                if ($valItem->id_item == 1) {
                    $html .= "Oli: " . $qty . " liter";
                }
                if ($valItem->id_item == 2) {
                    $html .= "Drum: " . $qty . " pcs";
                }
                $html .= "</li>";
                $html .= "</ul>";
                $strDetailItem .= $html;
            }

            if ($value->ppn > 0) {
                $ppnBuysubTotal = "<ul style='padding-left: 15px;'>";
                $ppnBuysubTotal .= "<li>Nominal : " . number_format($value->price, 0, ",", ".") . "</li>";
                $ppnBuysubTotal .= "<li>PPN (" . ($value->ppn * 100) . " %) : " . number_format(($value->price * $value->ppn), 0, ",", ".") . "</li>";
                $ppnBuysubTotal .= "</ul>";
                $ppnBuysubTotal .= "<br><b>Total : " . number_format($subTotal, 0, ",", ".") . "</b>";
            } else {
                $ppnBuysubTotal = '<div width="100%" style="text-align: right;">' . number_format($value->price, 0, ",", ".") . '</div>';
            }

            $dataSupplier = $this->M_supplier->selectById($value->id_supplier);

            $row[] = $dataSupplier->name;
            $row[] = $strDetailItem;
            $row[] = $ppnBuysubTotal;
            $row[] = $strSupplierLoanInstallment;
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($grandTotal, 0, ",", ".") . '</div">';
            $row[] = $statusApprove;
            $row[] = date('d-m-Y H:i:s', strtotime($value->created_date));
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));

            //add html for action
            $action = " <div class='btn-group pull-right'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            $action .= "        <li><a href='#' class='detail-pembelian' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_purchase . "'><i class='glyphicon glyphicon-info-sign'></i> Detail</a></li>";
            $action .= "        <li><a href='" . site_url('cetak-pembelian/' . $value->id_transaction_purchase) . "' target='__blank'><i class='fa fa-print'></i> Cetak</a></li>";
            $action .= $linkPrint;
            if ($value->is_approved == 0) {
                if ($value->nominal == 0) {
                    if ($accessEdit->menuview > 0) {
                        $action .= "    <li><a href='" . site_url('edit-pembelian/' . $value->id_transaction_purchase) . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                    }
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='" . site_url('confirm-pembelian/' . $value->id_transaction_purchase) . "'><i class='fa fa-check'></i> Konfirmasi</a></li>";
                    }
                } else {
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='" . site_url('edit-confirm-pembelian/' . $value->id_transaction_purchase) . "'><i class='fa fa-edit'></i> Ubah Konfirmasi</a></li>";
                    }
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='#' class='conf-cancel-pembelian' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_purchase . "'><i class='fa fa-close'></i> Batalkan Konfirmasi</a></li>";
                    }

                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='#' class='conf-approve-pembelian' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_purchase . "'><i class='fa fa-check-square-o'></i> Setujui</a></li>";
                    }
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='conf-hapus-pembelian' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_purchase . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('add', 'pembelian');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pembelian";
        $data['judul'] = "Tambah Pembelian";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "pembelian";
            $data['privilegeId'] = $privilegeId;
            $data['supplier'] = $this->M_supplier->selectItem();
            $this->loadkonten('v_purchase/tambah', $data);
        }
    }

    public function AddCustom()
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('add', 'pembelian');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pembelian";
        $data['judul'] = "Tambah Pembelian";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "pembelian";
            $data['privilegeId'] = $privilegeId;
            $data['supplier'] = $this->M_supplier->selectItem();
            $this->loadkonten('v_purchase/tambah_custom', $data);
        }
    }

    public function prosesAdd()
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $item = array();

        $idSupplier = $this->input->post("id_supplier");
        $datePurchase = date('d-m-Y');
        if ($privilegeId == 1) {
            $datePurchase = $this->input->post("date");
        }
        $weight = $this->input->post("weight");
        $weight = str_replace(",", ".", $weight);
        $specificGravity = $this->input->post("specific_gravity");
        $specificGravity = str_replace(",", ".", $specificGravity);
        $waterContent = $this->input->post("water_content");
        $waterContent = str_replace(",", ".", $waterContent);
        $isKemasanDrum = $this->input->post("is_drum");
        if (!isset($isKemasanDrum)) {
            $isKemasanDrum = 0;
        }
        $jmlKemasan = $this->input->post("jml_kemasan");
        $literPerKemasan = $this->input->post("liter_per_kemasan");
        $literPerKemasan = str_replace(",", ".", $literPerKemasan);
        $penambahanLiter = $this->input->post("penambahan_liter");
        $penyusutanLiter = $this->input->post("penyusutan_liter");
        $isAdditionalDrum = $this->input->post("is_additional_drum");
        if (!isset($isAdditionalDrum)) {
            $isAdditionalDrum = 0;
        }
        $penambahanDrum = $this->input->post("penambahan_drum");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextPurchase();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('add', 'pembelian');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataSupplier == null) {
                $errCode++;
                $errMessage = "Supplier tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($datePurchase) == 0) {
                $errCode++;
                $errMessage = "Tanggal pembelian harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jmlKemasan) == 0) {
                $errCode++;
                $errMessage = "Jumlah Kemasan wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlKemasan <= 0) {
                $errCode++;
                $errMessage = "Jumlah Kemasan harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($literPerKemasan) == 0) {
                $errCode++;
                $errMessage = "Jumlah Liter per Kemasan wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($literPerKemasan <= 0) {
                $errCode++;
                $errMessage = "Jumlah Liter per Kemasan harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $literPerKemasanReal = $literPerKemasan;
            if ($weight > 0 && $specificGravity > 0) {
                $literPerKemasanReal = $weight / $specificGravity;
            }
            if (is_float($literPerKemasanReal)) {
                $literPerKemasanReal = number_format($literPerKemasanReal, 2, ".", "");
            }
            $totalLiter = $jmlKemasan * $literPerKemasan;
            if ($totalLiter <= 0) {
                $errCode++;
                $errMessage = "Total Liter harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($penambahanLiter) == 0) {
                $errCode++;
                $errMessage = "Penambahan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($penyusutanLiter) == 0) {
                $errCode++;
                $errMessage = "Penyusutan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $grandTotalLiter = $totalLiter + $penambahanLiter - $penyusutanLiter;
            if ($grandTotalLiter <= 0) {
                $errCode++;
                $errMessage = "Grand Total Liter harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $itemOli = array(
                'id_item' => 1,
                'item_name' => 'Oli',
                'qty_basic_real' => $literPerKemasanReal,
                'qty_basic' => $literPerKemasan,
                'multiple' => $jmlKemasan,
                'qty_before' => $totalLiter,
                'additional' => $penambahanLiter,
                'depreciation' => $penyusutanLiter,
                'weight' => $weight,
                'specific_gravity' => $specificGravity,
                'water_content' => $waterContent,
                'qty' => $grandTotalLiter,
            );
            $item[0] = $itemOli;
        }
        if ($isAdditionalDrum > 0) {
            if ($errCode == 0) {
                if (strlen($penambahanDrum) == 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($penambahanDrum <= 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                $itemDrum = array(
                    'id_item' => 2,
                    'item_name' => 'Drum',
                    'qty_basic_real' => $penambahanDrum,
                    'qty_basic' => $penambahanDrum,
                    'multiple' => 1,
                    'qty_before' => $penambahanDrum,
                    'additional' => 0,
                    'depreciation' => 0,
                    'weight' => 0,
                    'specific_gravity' => 0,
                    'water_content' => 0,
                    'qty' => $penambahanDrum,
                );
                $item[1] = $itemDrum;
            }
        }
        //TRANSACTION PURCHASE
        if ($errCode == 0) {
            try {
                $strItem = "";
                foreach ($item as $key => $value) {
                    if ($value['item_name'] != NULL) {
                        if (strlen($strItem) > 0)
                            $strItem .= ",";
                        $strItem .= $value['id_item'];
                    }
                }

                $data = array(
                    'id_branch' => $this->branch,
                    'id_supplier' => $idSupplier,
                    'is_drum' => $isKemasanDrum,
                    'id_item_exp' => $strItem,
                    'date' => date('Y-m-d', strtotime($datePurchase)),
                    'code' => $code,
                    'description' => $description,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_transaction_purchase', $data);
                $idTransactionPurchase = $this->db->insert_id();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if (strlen($idTransactionPurchase) > 0) {
            if ($errCode == 0) {
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";
                    if ($err == 0) {
                        try {
                            $dataDetail = array(
                                'id_transaction_purchase' => $idTransactionPurchase,
                                'id_item' => $value['id_item'],
                                'date' => date('Y-m-d', strtotime($datePurchase)),
                                'qty_basic' => $value['qty_basic'],
                                'multiple' => $value['multiple'],
                                'qty_before' => $value['qty_before'],
                                'additional' => $value['additional'],
                                'depreciation' => $value['depreciation'],
                                'weight' => $value['weight'],
                                'specific_gravity' => $value['specific_gravity'],
                                'water_content' => $value['water_content'],
                                'qty' => $value['qty'],
                                'created_by' => $username,
                                'created_date' => $date,
                                'updated_by' => $username,
                                'updated_date' => $date,
                            );
                            $this->db->insert('tbl_transaction_purchase_detail', $dataDetail);
                            $idTransactionPurchaseDetail = $this->db->insert_id();
                        } catch (Exception $ex) {
                            $err++;
                            $msg = $ex->getMessage();
                        }
                    }
                    if ($err == 0) {
                        $isok1 = $this->M_item->balance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail), 'in', 'TRANSACTION PURCHASE ' . $code, 'Transaksi Pembelian ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                        $err = $isok1['errCode'];
                        $msg = $isok1['errMessage'];
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        //TRANSACTION PURCHASE
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesAddCustom()
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $item = array();

        $idSupplier = $this->input->post("id_supplier");
        $datePurchase = date('d-m-Y');
        if ($privilegeId == 1) {
            $datePurchase = $this->input->post("date");
        }
        $jmlDrum = $this->input->post("jml_drum");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextPurchase();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('add', 'pembelian');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataSupplier == null) {
                $errCode++;
                $errMessage = "Supplier tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($datePurchase) == 0) {
                $errCode++;
                $errMessage = "Tanggal pembelian harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jmlDrum) == 0) {
                $errCode++;
                $errMessage = "Jumlah Drum wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlDrum <= 0) {
                $errCode++;
                $errMessage = "Jumlah Drum harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $itemDrum = array(
                'id_item' => 2,
                'item_name' => 'Drum',
                'qty_basic' => $jmlDrum,
                'multiple' => 1,
                'qty_before' => $jmlDrum,
                'additional' => 0,
                'depreciation' => 0,
                'weight' => 0,
                'specific_gravity' => 0,
                'water_content' => 0,
                'qty' => $jmlDrum,
                'description' => $description,
            );
            $item[0] = $itemDrum;
        }
        //TRANSACTION PURCHASE
        if ($errCode == 0) {
            $strItem = "";
            foreach ($item as $key => $value) {
                if ($value['item_name'] != NULL) {
                    if (strlen($strItem) > 0)
                        $strItem .= ",";
                    $strItem .= $value['id_item'];
                }
            }
            try {
                $data = array(
                    'id_branch' => $this->branch,
                    'id_supplier' => $idSupplier,
                    'is_drum' => 1,
                    'id_item_exp' => $strItem,
                    'date' => date('Y-m-d', strtotime($datePurchase)),
                    'code' => $code,
                    'description' => $description,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_transaction_purchase', $data);
                $idTransactionPurchase = $this->db->insert_id();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if (strlen($idTransactionPurchase) > 0) {
            if ($errCode == 0) {
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";
                    if ($err == 0) {
                        try {
                            $dataDetail = array(
                                'id_transaction_purchase' => $idTransactionPurchase,
                                'id_item' => $value['id_item'],
                                'date' => date('Y-m-d', strtotime($datePurchase)),
                                'qty_basic' => $value['qty_basic'],
                                'multiple' => $value['multiple'],
                                'qty_before' => $value['qty_before'],
                                'additional' => $value['additional'],
                                'depreciation' => $value['depreciation'],
                                'weight' => $value['weight'],
                                'specific_gravity' => $value['specific_gravity'],
                                'water_content' => $value['water_content'],
                                'qty' => $value['qty'],
                                'description' => $value['description'],
                                'created_by' => $username,
                                'created_date' => $date,
                                'updated_by' => $username,
                                'updated_date' => $date,
                            );
                            $this->db->insert('tbl_transaction_purchase_detail', $dataDetail);
                            $idTransactionPurchaseDetail = $this->db->insert_id();
                        } catch (Exception $ex) {
                            $err++;
                            $msg = $ex->getMessage();
                            break;
                        }
                    }
                    if ($err == 0) {
                        $isok1 = $this->M_item->balance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail), 'in', 'TRANSACTION PURCHASE ' . $code, 'Transaksi Pembelian ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                        $err = $isok1['errCode'];
                        $msg = $isok1['errMessage'];
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        //TRANSACTION PURCHASE
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('edit', 'pembelian');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pembelian";
        $data['judul'] = "Ubah Pembelian";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataPurchase = $this->M_purchase->selectById($id);
            if ($dataPurchase != null) {
                if ($dataPurchase->is_approved == 0 && $dataPurchase->nominal == 0) {
                    $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);

                    $isCheckedDrum = $dataPurchase->is_drum;
                    if ($isCheckedDrum == null) {
                        $isCheckedDrum = 0;
                    }
                    $isCheckedAdditionalDrum = 0;

                    $weight = 0;
                    $specificGravity = 0;
                    $waterContent = 0;
                    $jmlKemasan = 0;
                    $literPerKemasan = 0;
                    $totalLiter = 0;
                    $penambahanLiter = 0;
                    $penyusutanLiter = 0;
                    $grandTotalLiter = 0;
                    $penambahanDrum = 0;

                    foreach ($dataPurchaseDetail as $key => $value) {
                        if ($value->id_item == 1) {
                            $weight = $value->weight;
                            $weight = str_replace(".", ",", $weight);
                            $specificGravity = $value->specific_gravity;
                            $specificGravity = str_replace(".", ",", $specificGravity);
                            $waterContent = $value->water_content;
                            $waterContent = str_replace(".", ",", $waterContent);
                            $jmlKemasan = $value->multiple;
                            $literPerKemasan = $value->qty_basic;
                            $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                            $totalLiter = $value->qty_before;
                            $totalLiter = str_replace(".", ",", $totalLiter);
                            $penambahanLiter = $value->additional;
                            $penyusutanLiter = $value->depreciation;
                            $grandTotalLiter = $value->qty;
                            $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                        }
                        if ($value->id_item == 2) {
                            $penambahanDrum = $value->qty;
                            if ($penambahanDrum > 0) {
                                $isCheckedAdditionalDrum = 1;
                            }
                        }
                    }

                    $data['menuName'] = "pembelian";
                    $data['dataPurchase'] = $dataPurchase;
                    $data['idTransactionPurchase'] = $id;

                    $data['weight'] = $weight;
                    $data['specificGravity'] = $specificGravity;
                    $data['waterContent'] = $waterContent;

                    $data['isCheckedDrum'] = $isCheckedDrum;
                    $data['jmlKemasan'] = $jmlKemasan;
                    $data['literPerKemasan'] = $literPerKemasan;
                    $data['totalLiter'] = $totalLiter;
                    $data['penambahanLiter'] = $penambahanLiter;
                    $data['penyusutanLiter'] = $penyusutanLiter;
                    $data['grandTotalLiter'] = $grandTotalLiter;

                    $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
                    $data['penambahanDrum'] = $penambahanDrum;
                    $data['privilegeId'] = $privilegeId;

                    $data['supplier'] = $this->M_supplier->selectItem();

                    $isDrumOnly = 0;
                    $idItemExp = $dataPurchase->id_item_exp;
                    if (strlen($idItemExp) > 0) {
                        $arrItem = explode(',', $idItemExp);
                        if (count($arrItem) == 1) {
                            if ($arrItem[0] == 2) {
                                $isDrumOnly = 1;
                            }
                        }
                    }

                    if ($isDrumOnly > 0) {
                        $this->loadkonten('v_purchase/update_custom', $data);
                    } else {
                        $this->loadkonten('v_purchase/update', $data);
                    }
                } else {
                    echo "<script>alert('Pembelian sudah di approve.'); window.location = '" . base_url("pembelian") . "';</script>";
                }
            } else {
                echo "<script>alert('Pembelian tidak valid.'); window.location = '" . base_url("pembelian") . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $isEdit = FALSE;
        $newSupplier = FALSE;
        $errCode = 0;
        $errMessage = "";
        $item = array();

        $idSupplier = $this->input->post("id_supplier");
        $datePurchase = $this->input->post("date");
        $weight = $this->input->post("weight");
        $weight = str_replace(",", ".", $weight);
        $specificGravity = $this->input->post("specific_gravity");
        $specificGravity = str_replace(",", ".", $specificGravity);
        $waterContent = $this->input->post("water_content");
        $waterContent = str_replace(",", ".", $waterContent);
        $isKemasanDrum = $this->input->post("is_drum");
        if (!isset($isKemasanDrum)) {
            $isKemasanDrum = 0;
        }
        $jmlKemasan = $this->input->post("jml_kemasan");
        $literPerKemasan = $this->input->post("liter_per_kemasan");
        $literPerKemasan = str_replace(",", ".", $literPerKemasan);
        $penambahanLiter = $this->input->post("penambahan_liter");
        $penyusutanLiter = $this->input->post("penyusutan_liter");
        $isAdditionalDrum = $this->input->post("is_additional_drum");
        if (!isset($isAdditionalDrum)) {
            $isAdditionalDrum = 0;
        }
        $penambahanDrum = $this->input->post("penambahan_drum");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        //Start Checking
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('edit', 'pembelian');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $dataPurchase = $this->M_purchase->selectById($id);
            if ($dataPurchase == NULL) {
                $errCode++;
                $errMessage = "Data tidak Valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataSupplier == null) {
                $errCode++;
                $errMessage = "Supplier tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($privilegeId != 1) {
                $datePurchase = $dataPurchase->date;
                if (strlen($datePurchase) == 0) {
                    $datePurchase = date('d-m-Y');
                }
            }
            if (strlen($datePurchase) == 0) {
                $errCode++;
                $errMessage = "Tanggal pembelian harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jmlKemasan) == 0) {
                $errCode++;
                $errMessage = "Jumlah Kemasan wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlKemasan <= 0) {
                $errCode++;
                $errMessage = "Jumlah Kemasan harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($literPerKemasan) == 0) {
                $errCode++;
                $errMessage = "Jumlah Liter per Kemasan wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($literPerKemasan <= 0) {
                $errCode++;
                $errMessage = "Jumlah Liter per Kemasan harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $literPerKemasanReal = $literPerKemasan;
            if ($weight > 0 && $specificGravity > 0) {
                $literPerKemasanReal = $weight / $specificGravity;
            }
            if (is_float($literPerKemasanReal)) {
                $literPerKemasanReal = number_format($literPerKemasanReal, 2, ".", "");
            }
            $totalLiter = $jmlKemasan * $literPerKemasan;
            if ($totalLiter <= 0) {
                $errCode++;
                $errMessage = "Total Liter harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($penambahanLiter) == 0) {
                $errCode++;
                $errMessage = "Penambahan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($penyusutanLiter) == 0) {
                $errCode++;
                $errMessage = "Penyusutan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $grandTotalLiter = $totalLiter + $penambahanLiter - $penyusutanLiter;
            if ($grandTotalLiter <= 0) {
                $errCode++;
                $errMessage = "Grand Total Liter harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $itemOli = array(
                'id_item' => 1,
                'item_name' => 'Oli',
                'qty_basic_real' => $literPerKemasanReal,
                'qty_basic' => $literPerKemasan,
                'multiple' => $jmlKemasan,
                'qty_before' => $totalLiter,
                'additional' => $penambahanLiter,
                'depreciation' => $penyusutanLiter,
                'weight' => $weight,
                'specific_gravity' => $specificGravity,
                'water_content' => $waterContent,
                'qty' => $grandTotalLiter,
            );
            $item[0] = $itemOli;
        }
        if ($isAdditionalDrum > 0) {
            if ($errCode == 0) {
                if (strlen($penambahanDrum) == 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($penambahanDrum <= 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                $itemDrum = array(
                    'id_item' => 2,
                    'item_name' => 'Drum',
                    'qty_basic_real' => $penambahanDrum,
                    'qty_basic' => $penambahanDrum,
                    'multiple' => 1,
                    'qty_before' => $penambahanDrum,
                    'additional' => 0,
                    'depreciation' => 0,
                    'weight' => 0,
                    'specific_gravity' => 0,
                    'water_content' => 0,
                    'qty' => $penambahanDrum,
                );
                $item[1] = $itemDrum;
            }
        } else {
            $itemDrum = array(
                'id_item' => 2,
                'item_name' => NULL,
                'qty_basic_real' => NULL,
                'qty_basic' => NULL,
                'multiple' => NULL,
                'qty_before' => NULL,
                'additional' => NULL,
                'depreciation' => NULL,
                'weight' => NULL,
                'specific_gravity' => NULL,
                'water_content' => NULL,
                'qty' => NULL,
            );
            $item[1] = $itemDrum;
        }
        //Untuk cek apakah update tanpa ubah inputan
        if ($errCode == 0) {
            $isCheckedAdditionalDrumOld = 0;
            $weightOld = 0;
            $specificGravityOld = 0;
            $waterContentOld = 0;
            $jmlKemasanOld = 0;
            $literPerKemasanOld = 0;
            $penambahanLiterOld = 0;
            $penyusutanLiterOld = 0;
            $penambahanDrumOld = 0;
            $jmlLiterOld = 0;

            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);
            foreach ($dataPurchaseDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $weightOld = $value->weight;
                    $specificGravityOld = $value->specific_gravity;
                    $waterContentOld = $value->water_content;
                    $jmlKemasanOld = $value->multiple;
                    $literPerKemasanOld = $value->qty_basic;
                    $totalLiterOld = $value->qty_before;
                    $penambahanLiterOld = $value->additional;
                    $penyusutanLiterOld = $value->depreciation;
                    $grandTotalLiterOld = $value->qty;
                }
                if ($value->id_item == 2) {
                    $penambahanDrumOld = $value->qty;
                    if ($penambahanDrumOld > 0) {
                        $isCheckedAdditionalDrumOld = 1;
                    }
                }
            }
            if ($isEdit == FALSE) {
                if ($idSupplier != $dataPurchase->id_supplier) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if (date('Y-m-d', strtotime($datePurchase)) != $dataPurchase->date) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($weight != $weightOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($specificGravity != $specificGravityOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($waterContent != $waterContentOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($isKemasanDrum != $dataPurchase->is_drum) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($jmlKemasan != $jmlKemasanOld || $literPerKemasan != $literPerKemasanOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($penambahanLiter != $penambahanLiterOld || $penyusutanLiter != $penyusutanLiterOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($isAdditionalDrum != $isCheckedAdditionalDrumOld || $penambahanDrumOld != $penambahanDrum) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($description != $dataPurchase->description) {
                    $isEdit = TRUE;
                }
            }
        }
        //End Checking
        if ($isEdit == TRUE) {
            //START TRANSACTION PURCHASE
            if ($errCode == 0) {
                try {
                    $strItem = "";
                    foreach ($item as $key => $value) {
                        if ($value['item_name'] != NULL) {
                            if (strlen($strItem) > 0)
                                $strItem .= ",";
                            $strItem .= $value['id_item'];
                        }
                    }
                    $dataUpdate = array(
                        'id_supplier' => $idSupplier,
                        'id_item_exp' => $strItem,
                        'date' => date('Y-m-d', strtotime($datePurchase)),
                        'is_drum' => $isKemasanDrum,
                        'description' => $description,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->update('tbl_transaction_purchase', $dataUpdate, array('id_transaction_purchase' => $id));
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            if ($errCode == 0) {
                $code = $dataPurchase->code;
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";
                    $getDetail = $this->M_purchase->selectDetailByIdItem($id, $value['id_item']);
                    $idTransactionPurchaseDetail = $getDetail->id_transaction_purchase_detail;
                    $idItem = $getDetail->id_item;
                    $getItem = $this->M_item->selectById($idItem);
                    if (strlen($idTransactionPurchaseDetail) > 0) {
                        if ($value['item_name'] != NULL) {
                            if ($err == 0) {
                                try {
                                    $dataDetail = array(
                                        'date' => date('Y-m-d', strtotime($datePurchase)),
                                        'qty_basic_real' => $value['qty_basic_real'],
                                        'qty_basic' => $value['qty_basic'],
                                        'multiple' => $value['multiple'],
                                        'qty_before' => $value['qty_before'],
                                        'additional' => $value['additional'],
                                        'depreciation' => $value['depreciation'],
                                        'weight' => $value['weight'],
                                        'specific_gravity' => $value['specific_gravity'],
                                        'water_content' => $value['water_content'],
                                        'qty' => $value['qty'],
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->update('tbl_transaction_purchase_detail', $dataDetail, array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                            if ($err == 0) {
                                $isok1 = $this->M_item->updateBalance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail), 'in', 'TRANSACTION PURCHASSE ' . $code, 'Transaksi Pembelian ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                                $err = $isok1['errCode'];
                                $msg = $isok1['errMessage'];
                            }
                        } else {
                            if ($err == 0) {
                                $isok1 = $this->M_item->deleteBalance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
                                $err = $isok1['errCode'];
                                $msg = $isok1['errMessage'];
                            }
                            if ($err == 0) {
                                try {
                                    $this->db->delete('tbl_transaction_purchase_detail', array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                        }
                    } else {
                        if ($value['item_name'] != NULL) {
                            if ($err == 0) {
                                try {
                                    $dataDetail = array(
                                        'id_transaction_purchase' => $id,
                                        'id_item' => $value['id_item'],
                                        'date' => date('Y-m-d', strtotime($datePurchase)),
                                        'qty_basic_real' => $value['qty_basic_real'],
                                        'qty_basic' => $value['qty_basic'],
                                        'multiple' => $value['multiple'],
                                        'qty_before' => $value['qty_before'],
                                        'additional' => $value['additional'],
                                        'depreciation' => $value['depreciation'],
                                        'weight' => $value['weight'],
                                        'specific_gravity' => $value['specific_gravity'],
                                        'water_content' => $value['water_content'],
                                        'qty' => $value['qty'],
                                        'created_by' => $dataPurchase->created_by,
                                        'created_date' => $dataPurchase->created_date,
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->insert('tbl_transaction_purchase_detail', $dataDetail);
                                    $idTransactionPurchaseDetail = $this->db->insert_id();
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                    break;
                                }
                            }
                            if ($err == 0) {
                                $isok1 = $this->M_item->balance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail), 'in', 'TRANSACTION PURCHASE ' . $code, 'Transaksi Pembelian ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                                $err = $isok1['errCode'];
                                $msg = $isok1['errMessage'];
                            }
                        }
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
            //END TRANSACTION PURCHASE
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
            $out['isEdit'] = $isEdit;
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesUpdateCustom($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $newSupplier = FALSE;
        $item = array();

        $idSupplier = $this->input->post("id_supplier");
        $datePurchase = $this->input->post("date");
        $jmlDrum = $this->input->post("jml_drum");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        //Start Checking
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('edit', 'pembelian');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $dataPurchase = $this->M_purchase->selectById($id);
            if ($dataPurchase == NULL) {
                $errCode++;
                $errMessage = "Data tidak Valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier wajib diisi.";
            }
        }
        if ($errCode == 0) {
            //JIka customer sebelumnya berbeda dg customer baru
            if ($idSupplier != $dataPurchase->id_supplier) {
                $newSupplier = TRUE;
            }
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataSupplier == null) {
                $errCode++;
                $errMessage = "Supplier tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($privilegeId != 1) {
                $datePurchase = $dataPurchase->date;
                if (strlen($datePurchase) == 0) {
                    $datePurchase = date('d-m-Y');
                }
            }
            if (strlen($datePurchase) == 0) {
                $errCode++;
                $errMessage = "Tanggal pembelian harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jmlDrum) == 0) {
                $errCode++;
                $errMessage = "Jumlah Drum wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlDrum <= 0) {
                $errCode++;
                $errMessage = "Jumlah Drum harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $itemDrum = array(
                'id_item' => 2,
                'item_name' => 'Drum',
                'qty_basic' => $jmlDrum,
                'multiple' => 1,
                'qty_before' => $jmlDrum,
                'additional' => 0,
                'depreciation' => 0,
                'weight' => 0,
                'specific_gravity' => 0,
                'water_content' => 0,
                'qty' => $jmlDrum,
                'description' => $description,
            );
            $item[0] = $itemDrum;
        }
        //Untuk cek apakah update tanpa ubah inputan
        if ($errCode == 0) {
            $isEdit = FALSE;
            $jmlDrumOld = 0;
            if ($isEdit == FALSE) {
                if ($newSupplier == TRUE) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if (date('Y-m-d', strtotime($datePurchase)) != $dataPurchase->date) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($description != $dataPurchase->description) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);
                foreach ($dataPurchaseDetail as $key => $value) {
                    if ($value->id_item == 2) {
                        $jmlDrumOld = $value->qty_before;
                    }
                }
            }
            if ($isEdit == FALSE) {
                if ($jmlDrum != $jmlDrumOld) {
                    $isEdit = TRUE;
                }
            }
        }
        //End Checking
        if ($isEdit == TRUE) {
            //START TRANSACTION PURCHASE
            if ($errCode == 0) {
                try {
                    $strItem = "";
                    foreach ($item as $key => $value) {
                        if ($value['item_name'] != NULL) {
                            if (strlen($strItem) > 0)
                                $strItem .= ",";
                            $strItem .= $value['id_item'];
                        }
                    }
                    $dataUpdate = array(
                        'id_supplier' => $idSupplier,
                        'id_item_exp' => $strItem,
                        'date' => date('Y-m-d', strtotime($datePurchase)),
                        'description' => $description,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->update('tbl_transaction_purchase', $dataUpdate, array('id_transaction_purchase' => $id));
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            $code = $dataPurchase->code;
            if ($errCode == 0) {
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";
                    $getDetail = $this->M_purchase->selectDetailByIdItem($id, $value['id_item']);
                    $idTransactionPurchaseDetail = $getDetail->id_transaction_purchase_detail;
                    $idItem = $getDetail->id_item;
                    $getItem = $this->M_item->selectById($idItem);
                    if (strlen($idTransactionPurchaseDetail) > 0) {
                        if ($err == 0) {
                            $dataDetail = array(
                                'date' => date('Y-m-d', strtotime($datePurchase)),
                                'qty_basic' => $value['qty_basic'],
                                'multiple' => $value['multiple'],
                                'qty_before' => $value['qty_before'],
                                'additional' => $value['additional'],
                                'depreciation' => $value['depreciation'],
                                'weight' => $value['weight'],
                                'specific_gravity' => $value['specific_gravity'],
                                'water_content' => $value['water_content'],
                                'qty' => $value['qty'],
                                'description' => $value['description'],
                                'updated_by' => $username,
                                'updated_date' => $date,
                            );
                            $this->db->update('tbl_transaction_purchase_detail', $dataDetail, array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
                        }
                        if ($err == 0) {
                            $isok1 = $this->M_item->updateBalance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail), 'in', 'TRANSACTION PURCHASSE ' . $code, 'Transaksi Pembelian ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                            $err = $isok1['errCode'];
                            $msg = $isok1['errMessage'];
                        }
                        if ($err > 0) {
                            $errCode++;
                            $errMessage = $msg;
                            break;
                        }
                    }
                }
            }
            //END TRANSACTION PURCHASE
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Confirm($id)
    {
        $access = $this->M_sidebar->access('conf', 'pembelian');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pembelian";
        $data['judul'] = "Konfirmasi Pembelian";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataPurchase = $this->M_purchase->selectById($id);
            if ($dataPurchase != null) {
                if ($dataPurchase->is_approved == 0 && $dataPurchase->nominal == 0) {
                    $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);

                    $isCheckedDrum = $dataPurchase->is_drum;
                    if ($isCheckedDrum == null) {
                        $isCheckedDrum = 0;
                    }
                    $isCheckedAdditionalDrum = 0;

                    $weight = 0;
                    $specificGravity = 0;
                    $waterContent = 0;
                    $jmlKemasan = 0;
                    $literPerKemasan = 0;
                    $totalLiter = 0;
                    $penambahanLiter = 0;
                    $penyusutanLiter = 0;
                    $grandTotalLiter = 0;
                    $penambahanDrum = 0;

                    foreach ($dataPurchaseDetail as $key => $value) {
                        if ($value->id_item == 1) {
                            $weight = $value->weight;
                            $weight = str_replace(".", ",", $weight);
                            $specificGravity = $value->specific_gravity;
                            $specificGravity = str_replace(".", ",", $specificGravity);
                            $waterContent = $value->water_content;
                            $waterContent = str_replace(".", ",", $waterContent);
                            $jmlKemasan = $value->multiple;
                            $literPerKemasan = $value->qty_basic;
                            $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                            $totalLiter = $value->qty_before;
                            $totalLiter = str_replace(".", ",", $totalLiter);
                            $penambahanLiter = $value->additional;
                            $penyusutanLiter = $value->depreciation;
                            $grandTotalLiter = $value->qty;
                            $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                        }
                        if ($value->id_item == 2) {
                            $penambahanDrum = $value->qty;
                            if ($penambahanDrum > 0) {
                                $isCheckedAdditionalDrum = 1;
                            }
                        }
                    }

                    $idSupplier = $dataPurchase->id_supplier;
                    $dataSupplier = $this->M_supplier->selectById($idSupplier);
                    $namaSupplier = $dataSupplier->name;

                    $bank = "";
                    if (strlen($dataSupplier->id_bank) > 0) {
                        $getBank = $this->M_bank->selectById($dataSupplier->id_bank);
                        $bank = $getBank->name;
                    }

                    $qOli = "SELECT * FROM tbl_supplier_item_price WHERE id_supplier = '{$idSupplier}' AND id_item = 1";
                    $qOliPrice = $this->db->query($qOli)->row();
                    $oliPrice = 0;
                    if (count($qOliPrice) > 0) {
                        $oliPrice = $qOliPrice->price;
                    }
                    $qDrum = "SELECT * FROM tbl_supplier_item_price WHERE id_supplier = '{$idSupplier}' AND id_item = 2";
                    $qDrumPrice = $this->db->query($qDrum)->row();
                    $drumPrice = 0;
                    if (count($qDrumPrice) > 0) {
                        $drumPrice = $qDrumPrice->price;
                    }

                    $listSupplierLoan = array();
                    $SupplierLoan = $this->M_supplier_loan->getSupplierLoanList($idSupplier);
                    if (count($SupplierLoan) > 0) {
                        foreach ($SupplierLoan as $key => $value) {
                            $getSupplierLoan = $this->M_supplier_loan->selectById($value->id_supplier_loan);
                            if ($getSupplierLoan != NULL) {
                                $isPaidOff = $getSupplierLoan->is_paid_off;
                                $resultSupplierLoanInstallment = $this->M_supplier_loan->getSupplierLoanInstallment($value->id_supplier_loan, 0);
                                $countSupplierLoanInstallment = count($resultSupplierLoanInstallment);

                                $grandTotalInstallment = 0;
                                if ($countSupplierLoanInstallment > 0) {
                                    foreach ($resultSupplierLoanInstallment as $k => $val) {
                                        $grandTotalInstallment += $val->nominal;
                                    }
                                }

                                $paid = $grandTotalInstallment;
                                $debt = $getSupplierLoan->nominal - $grandTotalInstallment;
                                $installment = $countSupplierLoanInstallment + 1;
                                if ($debt > 0) {
                                    $listSupplierLoan[$key] = array(
                                        'id_supplier_loan' => $value->id_supplier_loan,
                                        'nominal' => $getSupplierLoan->nominal,
                                        'paid' => $paid,
                                        'debt' => $debt,
                                        'installment' => $installment,
                                        'description' => $value->description,
                                    );
                                }
                            }
                        }
                    }

                    $isSupplierLoan = 0;
                    if (count($listSupplierLoan) > 0) {
                        $isSupplierLoan = 1;
                    }

                    $data['menuName'] = "pembelian";
                    $data['dataPurchase'] = $dataPurchase;
                    $data['idTransactionPurchase'] = $id;

                    $data['dataSupplier'] = $dataSupplier;
                    $data['bank'] = $bank;
                    $data['namaSupplier'] = $namaSupplier;
                    $data['oliPrice'] = $oliPrice;
                    $data['drumPrice'] = $drumPrice;
                    $data['weight'] = $weight;
                    $data['specificGravity'] = $specificGravity;
                    $data['waterContent'] = $waterContent;

                    $data['isCheckedDrum'] = $isCheckedDrum;
                    $data['jmlKemasan'] = $jmlKemasan;
                    $data['literPerKemasan'] = $literPerKemasan;
                    $data['totalLiter'] = $totalLiter;
                    $data['penambahanLiter'] = $penambahanLiter;
                    $data['penyusutanLiter'] = $penyusutanLiter;
                    $data['grandTotalLiter'] = $grandTotalLiter;
                    $data['ppn'] = self::__ppn;

                    $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
                    $data['penambahanDrum'] = $penambahanDrum;

                    $data['isSupplierLoan'] = $isSupplierLoan;
                    $data['listSupplierLoan'] = $listSupplierLoan;

                    $isDrumOnly = 0;
                    $idItemExp = $dataPurchase->id_item_exp;
                    if (strlen($idItemExp) > 0) {
                        $arrItem = explode(',', $idItemExp);
                        if (count($arrItem) == 1) {
                            if ($arrItem[0] == 2) {
                                $isDrumOnly = 1;
                            }
                        }
                    }

                    if ($isDrumOnly > 0) {
                        $this->loadkonten('v_purchase/approve_custom', $data);
                    } else {
                        $this->loadkonten('v_purchase/approve', $data);
                    }
                } else {
                    echo "<script>alert('Pembelian sudah di konfirmasi.'); window.location = '" . base_url("pembelian") . "';</script>";
                }
            } else {
                echo "<script>alert('Pembelian tidak valid.'); window.location = '" . base_url("pembelian") . "';</script>";
            }
        }
    }

    public function prosesConfirm($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $subTotal = 0;
        $grandTotal = 0;
        $lastGrandTotal = 0;
        $totalHargaOli = 0;
        $totalHargaDrum = 0;

        $isLoan = FALSE;
        $idSupplierLoanInstallment = NULL;
        $strIdSupplierLoanInstallment = "";
        $code = "";
        $nominalLoan = 0;
        $paidLoan = 0;
        $paidDebt = 0;
        $installmentLoan = 0;
        $arrSupplierLoan = array();

        $hargaPerDrum = $this->input->post("harga_per_drum");
        $hargaPerDrum = str_replace(".", "", $hargaPerDrum);
        $hargaPerDrum = str_replace(",", "", $hargaPerDrum);
        $hargaPerLiter = $this->input->post("harga_per_liter");
        $hargaPerLiter = str_replace(".", "", $hargaPerLiter);
        $hargaPerLiter = str_replace(",", "", $hargaPerLiter);
        $isPpn = $this->input->post("is_ppn");
        if (!isset($isPpn)) {
            $isPpn = 0;
        }
        $arrIdSupplierLoan = $this->input->post("id_supplier_loan");
        if (is_array($arrIdSupplierLoan)) {
            $arrNominalInstallment = $this->input->post("nominal_installment");
            $listSupplierLoan = parent::rotate(array('id_supplier_loan' => $arrIdSupplierLoan, 'nominal_installment' => $arrNominalInstallment));
            foreach ($listSupplierLoan as $key => $value) {
                $nominalInstallment = $value['nominal_installment'];
                $nominalInstallment = str_replace(".", "", $nominalInstallment);
                $nominalInstallment = str_replace(",", "", $nominalInstallment);
                if ($nominalInstallment > 0) {
                    $arrLoan = array(
                        'id_supplier_loan' => $value['id_supplier_loan'],
                        'nominal_installment' => $nominalInstallment
                    );
                    $arrSupplierLoan[$key] = $arrLoan;
                }
            }
        }
        $sumInstallment = $this->input->post("installment");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'pembelian');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }

        if ($errCode == 0) {
            $dataPurchase = $this->M_purchase->selectById($id);
            $code = $dataPurchase->code;
            $idSupplier = $dataPurchase->id_supplier;
            $getSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataPurchase == NULL) {
                $errCode++;
                $errMessage = "Data tidak Valid.";
            }
        }

        if ($errCode == 0) {
            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);
            foreach ($dataPurchaseDetail as $key => $value) {
                if ($value->id_item == 1) {
                    if ($errCode == 0) {
                        if (strlen($hargaPerLiter) == 0) {
                            $errCode++;
                            $errMessage = "Harga per Liter wajib diisi.";
                        }
                    }
                    if ($errCode == 0) {
                        if ($hargaPerLiter <= 0) {
                            $errCode++;
                            $errMessage = "Harga per Liter harus lebih dari 0.";
                        }
                    }
                    if ($errCode == 0) {
                        $totalHargaOli = $value->qty * $hargaPerLiter;
                    }
                }
                if ($value->id_item == 2) {
                    if ($errCode == 0) {
                        if (strlen($hargaPerDrum) == 0) {
                            $errCode++;
                            $errMessage = "Harga per Drum wajib diisi.";
                        }
                    }
                    if ($errCode == 0) {
                        if ($hargaPerDrum <= 0) {
                            $errCode++;
                            $errMessage = "Harga per Drum harus lebih dari 0.";
                        }
                    }
                    if ($errCode == 0) {
                        $totalHargaDrum = $value->qty * $hargaPerDrum;
                    }
                }
            }
        }

        if ($errCode == 0) {
            $grandTotal = $totalHargaDrum + $totalHargaOli;
            $lastGrandTotal = $grandTotal;
            if ($grandTotal <= 0) {
                $errCode++;
                $errMessage = "Mohon Lengkapi pengisian data anda.";
            }
        }

        // Installment
        if (is_array($arrSupplierLoan) && count($arrSupplierLoan) > 0) {
            foreach ($arrSupplierLoan as $key => $value) {
                $idSupplierLoan = $value['id_supplier_loan'];
                $nominalInstallment = $value['nominal_installment'];

                if ($errCode == 0) {
                    if (strlen($idSupplierLoan) == 0) {
                        $errCode++;
                        $errMessage = "Nominal Cicilan wajib diisi.";
                    }
                }
                if ($errCode == 0) {
                    if (strlen($nominalInstallment) == 0) {
                        $errCode++;
                        $errMessage = "Nominal Cicilan wajib diisi.";
                    }
                }
                if ($errCode == 0) {
                    if ($nominalInstallment <= 0) {
                        $errCode++;
                        $errMessage = "Nominal Cicilan harus lebih dari 0.";
                    }
                }
                if ($errCode == 0) {
                    $checkSupplierLoan = $this->getSupplierLoanInfo(0, $idSupplierLoan, 0);
                    $isPaidOff = $checkSupplierLoan['is_paid_off'];
                    $response = $checkSupplierLoan['response'];
                    $paidLoan = $response['paid'];
                    $installmentLoan = $response['installment'];
                    $paidDebt = $response['debt'];
                    $nominalLoan = $response['nominal'];
                    if ($isPaidOff > 0) {
                        $errCode++;
                        $errMessage = "Cicilan sudah lunas.";
                    }
                }
                if ($errCode == 0) {
                    if ($nominalInstallment > $paidDebt) {
                        $errCode++;
                        $errMessage = 'Nominal Cicilan tidak boleh lebih dari Nominal yang belum di bayar.';
                    }
                }
                if ($errCode == 0) {
                    if ($nominalInstallment > $lastGrandTotal) {
                        $errCode++;
                        $errMessage = 'Nominal Grand total harus lebih dari 0.';
                    }
                }
                if ($errCode == 0) {
                    try {
                        $dataInstallment = array(
                            'id_supplier_loan' => $idSupplierLoan,
                            'date' => $dataPurchase->date,
                            'nominal' => $nominalInstallment,
                            'created_by' => $dataPurchase->created_by,
                            'created_date' => $dataPurchase->created_date,
                            'updated_by' => $username,
                            'updated_date' => $date,
                        );
                        $this->db->insert('tbl_supplier_loan_installment', $dataInstallment);
                        $idSupplierLoanInstallment = $this->db->insert_id();
                    } catch (Exception $ex) {
                        $errCode++;
                        $errMessage = $ex->getMessage();
                    }
                }
                if (strlen($idSupplierLoanInstallment) > 0) {
                    if (strlen($strIdSupplierLoanInstallment) > 0) {
                        $strIdSupplierLoanInstallment .= ",";
                    }
                    $strIdSupplierLoanInstallment .= $idSupplierLoanInstallment;
                }
            }
        }

        //TRANSACTION PURCHASE
        if ($errCode == 0) {
            foreach ($dataPurchaseDetail as $key => $value) {
                $idTransactionPurchaseDetail = $value->id_transaction_purchase_detail;
                $dataDetailUpdate = array(
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                if ($value->id_item == 1) {
                    $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                        "total" => $hargaPerLiter,
                        "grandtotal" => $totalHargaOli,
                    ));
                }
                if ($value->id_item == 2) {
                    $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                        "total" => $hargaPerDrum,
                        "grandtotal" => $totalHargaDrum,
                    ));
                }
                $this->db->update('tbl_transaction_purchase_detail', $dataDetailUpdate, array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
            }
        }

        if ($errCode == 0) {
            try {
                $ppn = 0;
                if ($isPpn > 0) {
                    $ppn = self::__ppn;
                }
                $grandTotalFinal = $grandTotal + ($grandTotal * $ppn);
                $dataUpdate = array(
                    'id_supplier_loan_installment' => $strIdSupplierLoanInstallment,
                    'price' => $grandTotal,
                    'ppn' => $ppn,
                    'nominal' => $grandTotalFinal,
                    'description' => $description,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_purchase', $dataUpdate, array('id_transaction_purchase' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        //TRANSACTION PURCHASE

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function EditConfirm($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pembelian";
        $data['judul'] = "Ubah Konfirmasi Pembelian";
        $access = $this->M_sidebar->access('conf', 'pembelian');
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataPurchase = $this->M_purchase->selectById($id);
            if ($dataPurchase->is_approved == 0 && $dataPurchase->nominal > 0) {
                $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);

                $ppn = $dataPurchase->ppn;
                $isPpn = 0;
                if ($ppn > 0) {
                    $isPpn = 1;
                } else {
                    $ppn = self::__ppn;
                }

                $isCheckedDrum = $dataPurchase->is_drum;
                if ($isCheckedDrum == null) {
                    $isCheckedDrum = 0;
                }
                $isCheckedAdditionalDrum = 0;

                $weight = 0;
                $specificGravity = 0;
                $waterContent = 0;
                $jmlKemasan = 0;
                $literPerKemasan = 0;
                $totalLiter = 0;
                $penambahanLiter = 0;
                $penyusutanLiter = 0;
                $grandTotalLiter = 0;
                $hargaPerLiter = 0;
                $penambahanDrum = 0;
                $hargaPerDrum = 0;

                foreach ($dataPurchaseDetail as $key => $value) {
                    if ($value->id_item == 1) {
                        $weight = $value->weight;
                        $weight = str_replace(".", ",", $weight);
                        $specificGravity = $value->specific_gravity;
                        $specificGravity = str_replace(".", ",", $specificGravity);
                        $waterContent = $value->water_content;
                        $waterContent = str_replace(".", ",", $waterContent);
                        $jmlKemasan = $value->multiple;
                        $literPerKemasan = $value->qty_basic;
                        $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                        $totalLiter = $value->qty_before;
                        $totalLiter = str_replace(".", ",", $totalLiter);
                        $penambahanLiter = $value->additional;
                        $penyusutanLiter = $value->depreciation;
                        $grandTotalLiter = $value->qty;
                        $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                        $hargaPerLiter = $value->total;
                        $hargaPerLiter = str_replace(".", ",", $hargaPerLiter);
                    }
                    if ($value->id_item == 2) {
                        $penambahanDrum = $value->qty;
                        $hargaPerDrum = $value->total;
                        $hargaPerDrum = str_replace(".", ",", $hargaPerDrum);
                        if ($penambahanDrum > 0) {
                            $isCheckedAdditionalDrum = 1;
                        }
                    }
                }

                $idSupplier = $dataPurchase->id_supplier;
                $dataSupplier = $this->M_supplier->selectById($idSupplier);
                $namaSupplier = $dataSupplier->name;

                $bank = "";
                if (strlen($dataSupplier->id_bank) > 0) {
                    $getBank = $this->M_bank->selectById($dataSupplier->id_bank);
                    $bank = $getBank->name;
                }

                $listSupplierLoan = array();
                $subtotalInstallment = 0;
                $SupplierLoan = $this->M_supplier_loan->getSupplierLoanList($idSupplier);
                if (count($SupplierLoan) > 0) {
                    foreach ($SupplierLoan as $key => $value) {
                        $getSupplierLoan = $this->M_supplier_loan->selectById($value->id_supplier_loan);
                        if ($getSupplierLoan != NULL) {
                            $isPaidOff = $getSupplierLoan->is_paid_off;
                            $resultSupplierLoanInstallment = $this->M_supplier_loan->getSupplierLoanInstallment($value->id_supplier_loan, 0);
                            $countSupplierLoanInstallment = count($resultSupplierLoanInstallment);

                            $grandTotalInstallment = 0;
                            if ($countSupplierLoanInstallment > 0) {
                                foreach ($resultSupplierLoanInstallment as $k => $val) {
                                    $grandTotalInstallment += $val->nominal;
                                }
                            }
//
                            $paid = $grandTotalInstallment;
                            $debt = $getSupplierLoan->nominal - $grandTotalInstallment;
                            $installment = $countSupplierLoanInstallment + 1;
//
                            $idSupplierLoanInstallment = null;
                            $nominalInstallment = 0;
                            $strIdSupplierLoanInstallment = $dataPurchase->id_supplier_loan_installment;
                            $expIdSupplierLoanInstallment = explode(',', $strIdSupplierLoanInstallment);
                            if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                                foreach ($expIdSupplierLoanInstallment as $k => $val) {
                                    $dataSupplierLoanInstallment = $this->M_supplier_loan_installment->selectById($val);
                                    if ($dataSupplierLoanInstallment->id_supplier_loan == $value->id_supplier_loan) {
                                        $idSupplierLoanInstallment = $dataSupplierLoanInstallment->id_supplier_loan_installment;
                                        $nominalInstallment = $dataSupplierLoanInstallment->nominal;
                                        $subtotalInstallment += $nominalInstallment;

                                        $q = "  SELECT count(*) as installment
                                                FROM tbl_supplier_loan_installment
                                                WHERE id_supplier_loan = {$value->id_supplier_loan}
                                                AND id_supplier_loan_installment <= {$val}";
                                        $res = $this->db->query($q)->row();

                                        $paid = ($paid - $nominalInstallment);
                                        $debt = ($debt + $nominalInstallment);
                                        $installment = $res->installment;
                                    }
                                }
                            }

                            if ($debt > 0) {
                                $listSupplierLoan[$key] = array(
                                    'id_supplier_loan_installment' => $idSupplierLoanInstallment,
                                    'id_supplier_loan' => $value->id_supplier_loan,
                                    'nominal' => $getSupplierLoan->nominal,
                                    'paid' => $paid,
                                    'debt' => $debt,
                                    'installment' => $installment,
                                    'nominal_installment' => $nominalInstallment,
                                    'description' => $value->description,
                                );
                            }
                        }
                    }
                }

                $isSupplierLoan = 0;
                if (count($listSupplierLoan) > 0) {
                    $isSupplierLoan = 1;
                }

                $data['menuName'] = "pembelian";
                $data['dataPurchase'] = $dataPurchase;
                $data['idTransactionPurchase'] = $id;

                $data['dataSupplier'] = $dataSupplier;
                $data['bank'] = $bank;
                $data['namaSupplier'] = $namaSupplier;
                $data['weight'] = $weight;
                $data['specificGravity'] = $specificGravity;
                $data['waterContent'] = $waterContent;

                $data['isCheckedDrum'] = $isCheckedDrum;
                $data['jmlKemasan'] = $jmlKemasan;
                $data['literPerKemasan'] = $literPerKemasan;
                $data['totalLiter'] = $totalLiter;
                $data['penambahanLiter'] = $penambahanLiter;
                $data['penyusutanLiter'] = $penyusutanLiter;
                $data['grandTotalLiter'] = $grandTotalLiter;
                $data['hargaPerLiter'] = $hargaPerLiter;
                $data['hargaPerDrum'] = $hargaPerDrum;
                $data['subTotalHarga'] = $dataPurchase->nominal;
                $data['subtotalInstallment'] = $subtotalInstallment;
                $data['grandTotal'] = $dataPurchase->nominal - $subtotalInstallment;
                $data['ppn'] = $ppn;
                $data['isPpn'] = $isPpn;

                $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
                $data['penambahanDrum'] = $penambahanDrum;

                $data['isSupplierLoan'] = $isSupplierLoan;
                $data['listSupplierLoan'] = $listSupplierLoan;

                $isDrumOnly = 0;
                $idItemExp = $dataPurchase->id_item_exp;
                if (strlen($idItemExp) > 0) {
                    $arrItem = explode(',', $idItemExp);
                    if (count($arrItem) == 1) {
                        if ($arrItem[0] == 2) {
                            $isDrumOnly = 1;
                        }
                    }
                }

                if ($isDrumOnly > 0) {
                    $this->loadkonten('v_purchase/edit_approve_custom', $data);
                } else {
                    $this->loadkonten('v_purchase/edit_approve', $data);
                }
            } else {
                echo "<script>alert('Pembelian sudah di approve.'); window.location = '" . base_url("pembelian") . "';</script>";
            }
        }
    }

    public function prosesUpdateConfirm($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $subTotal = 0;
        $grandTotal = 0;
        $lastGrandTotal = 0;
        $totalHargaOli = 0;
        $totalHargaDrum = 0;

        $isLoan = FALSE;
        $idSupplierLoanInstallment = NULL;
        $strIdSupplierLoanInstallment = "";
        $code = "";
        $sumInstallment = 0;
        $arrSupplierLoan = array();

        $hargaPerDrum = $this->input->post("harga_per_drum");
        $hargaPerDrum = str_replace(".", "", $hargaPerDrum);
        $hargaPerDrum = str_replace(",", "", $hargaPerDrum);
        $hargaPerLiter = $this->input->post("harga_per_liter");
        $hargaPerLiter = str_replace(".", "", $hargaPerLiter);
        $hargaPerLiter = str_replace(",", "", $hargaPerLiter);
        $isPpn = $this->input->post("is_ppn");
        if (!isset($isPpn)) {
            $isPpn = 0;
        }
        $arrIdSupplierLoan = $this->input->post("id_supplier_loan");
        $arrIdSupplierLoanInstallment = $this->input->post("id_supplier_loan_installment");
        if (is_array($arrIdSupplierLoan)) {
            $arrNominalInstallment = $this->input->post("nominal_installment");
            $listSupplierLoan = parent::rotate(array('id_supplier_loan_installment' => $arrIdSupplierLoanInstallment, 'id_supplier_loan' => $arrIdSupplierLoan, 'nominal_installment' => $arrNominalInstallment));
            foreach ($listSupplierLoan as $key => $value) {
                $idSupplierLoanInstallment = $value['id_supplier_loan_installment'];
                $nominalInstallment = $value['nominal_installment'];
                $nominalInstallment = str_replace(".", "", $nominalInstallment);
                $nominalInstallment = str_replace(",", "", $nominalInstallment);
                $isNew = 0;
                $isUpdate = 0;
                $isExist = 0;
                $isDelete = 0;
                if (strlen($idSupplierLoanInstallment) > 0) {
                    if ($nominalInstallment > 0) {
                        $dataSupplierLoanInstallment = $this->M_supplier_loan_installment->selectById($idSupplierLoanInstallment);
                        if ($dataSupplierLoanInstallment->nominal != $nominalInstallment) {
                            $isUpdate = 1;
                        } else {
                            $isExist = 1;
                        }
                    } else {
                        $isDelete = 1;
                    }
                } else {
                    if ($nominalInstallment > 0) {
                        $isNew = 1;
                    }
                }
                if ($isNew > 0 || $isUpdate > 0 || $isExist > 0 || $isDelete > 0) {
                    $sumInstallment += $nominalInstallment;
                    $arrLoan = array(
                        'is_new' => $isNew,
                        'is_update' => $isUpdate,
                        'is_exist' => $isExist,
                        'is_delete' => $isDelete,
                        'id_supplier_loan_installment' => $idSupplierLoanInstallment,
                        'id_supplier_loan' => $value['id_supplier_loan'],
                        'nominal_installment' => $nominalInstallment
                    );
                    $arrSupplierLoan[$key] = $arrLoan;
                }
            }
        }
        $sumInstallment = $this->input->post("installment");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'pembelian');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }

        if ($errCode == 0) {
            $dataPurchase = $this->M_purchase->selectById($id);
            $code = $dataPurchase->code;
            $idSupplier = $dataPurchase->id_supplier;
            $getSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataPurchase == NULL) {
                $errCode++;
                $errMessage = "Data tidak Valid.";
            }
        }

        if ($errCode == 0) {
            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);
            foreach ($dataPurchaseDetail as $key => $value) {
                if ($value->id_item == 1) {
                    if ($errCode == 0) {
                        if (strlen($hargaPerLiter) == 0) {
                            $errCode++;
                            $errMessage = "Harga per Liter wajib diisi.";
                        }
                    }
                    if ($errCode == 0) {
                        if ($hargaPerLiter <= 0) {
                            $errCode++;
                            $errMessage = "Harga per Liter harus lebih dari 0.";
                        }
                    }
                    if ($errCode == 0) {
                        $totalHargaOli = $value->qty * $hargaPerLiter;
                    }
                }
                if ($value->id_item == 2) {
                    if ($errCode == 0) {
                        if (strlen($hargaPerDrum) == 0) {
                            $errCode++;
                            $errMessage = "Harga per Drum wajib diisi.";
                        }
                    }
                    if ($errCode == 0) {
                        if ($hargaPerDrum <= 0) {
                            $errCode++;
                            $errMessage = "Harga per Drum harus lebih dari 0.";
                        }
                    }
                    if ($errCode == 0) {
                        $totalHargaDrum = $value->qty * $hargaPerDrum;
                    }
                }
            }
        }

        if ($errCode == 0) {
            $grandTotal = $totalHargaDrum + $totalHargaOli;
            $lastGrandTotal = $grandTotal;
            if ($grandTotal <= 0) {
                $errCode++;
                $errMessage = "Mohon Lengkapi pengisian data anda.";
            }
        }

        // Installment
        if (is_array($arrSupplierLoan) && count($arrSupplierLoan) > 0) {
            foreach ($arrSupplierLoan as $key => $value) {
                $isNew = $value['is_new'];
                $isUpdate = $value['is_update'];
                $isExist = $value['is_exist'];
                $isDelete = $value['is_delete'];
                $idSupplierLoanInstallment = $value['id_supplier_loan_installment'];
                if (strlen($idSupplierLoanInstallment) == 0) {
                    $idSupplierLoanInstallment = null;
                }
                $idSupplierLoan = $value['id_supplier_loan'];
                $nominalInstallment = $value['nominal_installment'];

                if ($errCode == 0) {
                    if (strlen($idSupplierLoan) == 0) {
                        $errCode++;
                        $errMessage = "Peminjaman Supplier tidak valid.";
                    }
                }
                if ($errCode == 0) {
                    if ($isNew == 0) {
                        if (strlen($idSupplierLoanInstallment) == 0) {
                            $errCode++;
                            $errMessage = "Cicilan Supplier tidak valid.";
                        }
                    }
                }
                if ($isDelete > 0) {
                    if ($errCode == 0) {
                        $getSupplierLoanInstallmentDelete = $this->M_supplier_loan->selectByIdInstallment($idSupplierLoanInstallment);
                        if ($getSupplierLoanInstallmentDelete != NULL) {
                            $err = 0;
                            $msg = "";
                            //Proses Hapus CIcilan
                            $idSupplierLoanDelete = $getSupplierLoanInstallmentDelete->id_supplier_loan;
                            $getSupplierDelete = $this->M_supplier->selectById($dataPurchase->id_supplier);
                            $getSupplierLoanDelete = $this->M_supplier_loan->selectById($idSupplierLoanDelete);
                            $nominalLoanDelete = $getSupplierLoanDelete->nominal;
                            $paidUpdateDelete = $getSupplierLoanDelete->paid - $getSupplierLoanInstallmentDelete->nominal;
                            $debtUpdateDelete = $getSupplierLoanDelete->debt + $getSupplierLoanInstallmentDelete->nominal;

                            if ($err == 0) {
                                if ($paidUpdateDelete < 0) {
                                    $err++;
                                    $msg = "Something Wrong on Installment";
                                }
                            }
                            if ($err == 0) {
                                try {
                                    $this->db->delete('tbl_supplier_loan_installment', array('id_supplier_loan_installment' => $idSupplierLoanInstallment));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                            if ($err > 0) {
                                $errCode = $err;
                                $errMessage = $msg;
                            }
                        }
                    }
                } else {
                    if ($isExist == 0) {
                        if ($errCode == 0) {
                            if (strlen($nominalInstallment) == 0) {
                                $errCode++;
                                $errMessage = "Nominal Cicilan wajib diisi.";
                            }
                        }
                        if ($errCode == 0) {
                            if ($nominalInstallment <= 0) {
                                $errCode++;
                                $errMessage = "Nominal Cicilan harus lebih dari 0.";
                            }
                        }
                        if ($errCode == 0) {
                            $checkSupplierLoan = $this->getSupplierLoanInfo(0, $idSupplierLoan, $idSupplierLoanInstallment, 0);
                            $isPaidOff = $checkSupplierLoan['is_paid_off'];
                            $response = $checkSupplierLoan['response'];
                            $nominalLoan = $response['nominal'];
                            $paidLoan = $response['paid'];
                            $installmentLoan = $response['installment'];
                            $paidDebt = $response['debt'];
                            $nominalLoan = $response['nominal'];
                            if ($isNew > 0) {
                                if ($isPaidOff > 0) {
                                    $errCode++;
                                    $errMessage = "Cicilan sudah lunas.";
                                }
                            }
                        }
                        if ($errCode == 0) {
                            if ($nominalInstallment > $paidDebt) {
                                $errCode++;
                                $errMessage = 'Nominal Cicilan tidak boleh lebih dari Nominal yang belum di bayar.';
                            }
                        }
                        if ($errCode == 0) {
                            if ($nominalInstallment > $lastGrandTotal) {
                                $errCode++;
                                $errMessage = 'Nominal Grand total harus lebih dari 0.';
                            }
                        }
                        if ($errCode == 0) {
                            if ($isNew > 0) {
                                try {
                                    $dataInstallment = array(
                                        'id_supplier_loan' => $idSupplierLoan,
                                        'date' => $dataPurchase->date,
                                        'nominal' => $nominalInstallment,
                                        'created_by' => $dataPurchase->created_by,
                                        'created_date' => $dataPurchase->created_date,
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->insert('tbl_supplier_loan_installment', $dataInstallment);
                                    $idSupplierLoanInstallment = $this->db->insert_id();
                                } catch (Exception $ex) {
                                    $errCode++;
                                    $errMessage = $ex->getMessage();
                                }
                            } else if ($isUpdate > 0) {
                                try {
                                    $dataInstallment = array(
                                        'date' => $dataPurchase->date,
                                        'nominal' => $nominalInstallment,
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->update('tbl_supplier_loan_installment', $dataInstallment, array('id_supplier_loan_installment' => $idSupplierLoanInstallment));
                                } catch (Exception $ex) {
                                    $errCode++;
                                    $errMessage = $ex->getMessage();
                                }
                            }
                        }
                    }
                }
                if ($errCode == 0) {
                    if ($isDelete == 0) {
                        if (strlen($strIdSupplierLoanInstallment) > 0) {
                            $strIdSupplierLoanInstallment .= ",";
                        }
                        $strIdSupplierLoanInstallment .= $idSupplierLoanInstallment;
                    }
                } else {
                    break;
                }
            }
        }

        //TRANSACTION PURCHASE
        if ($errCode == 0) {
            foreach ($dataPurchaseDetail as $key => $value) {
                $idTransactionPurchaseDetail = $value->id_transaction_purchase_detail;
                $dataDetailUpdate = array(
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                if ($value->id_item == 1) {
                    $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                        "total" => $hargaPerLiter,
                        "grandtotal" => $totalHargaOli,
                    ));
                }
                if ($value->id_item == 2) {
                    $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                        "total" => $hargaPerDrum,
                        "grandtotal" => $totalHargaDrum,
                    ));
                }
                $this->db->update('tbl_transaction_purchase_detail', $dataDetailUpdate, array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
            }
        }

        if ($errCode == 0) {
            try {
                $ppn = 0;
                if ($isPpn > 0) {
                    $ppn = self::__ppn;
                }
                $grandTotalFinal = $grandTotal + ($grandTotal * $ppn);
                $dataUpdate = array(
                    'id_supplier_loan_installment' => $strIdSupplierLoanInstallment,
                    'price' => $grandTotal,
                    'ppn' => $ppn,
                    'nominal' => $grandTotalFinal,
                    'description' => $description,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_purchase', $dataUpdate, array('id_transaction_purchase' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        //TRANSACTION PURCHASE

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Cetak($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $dataPurchase = $this->M_purchase->selectById($id);
        if ($dataPurchase != NULL) {
            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);

            $isCheckedDrum = 0;
            $isCheckedAdditionalDrum = 0;
            $jmlKemasan = 0;
            $literPerKemasan = 0;
            $jmlDrum = 0;
            $literPerDrum = 0;
            $penambahanDrum = 0;
            $penambahanLiter = 0;
            $penyusutanLiter = 0;
            $weight = 0;
            $specificGravity = 0;
            $waterContent = 0;
            $jmlLiter = 0;

            foreach ($dataPurchaseDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $jmlKemasan = $value->multiple;
                    $literPerKemasan = $value->qty_basic;
                    $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                    $literPerDrum = $value->qty_before;
                    $penambahanLiter = $value->additional;
                    $penyusutanLiter = $value->depreciation;
                    $weight = $value->weight;
                    $weight = str_replace(".", ",", $weight);
                    $specificGravity = $value->specific_gravity;
                    $specificGravity = str_replace(".", ",", $specificGravity);
                    $waterContent = $value->water_content;
                    $waterContent = str_replace(".", ",", $waterContent);
                    $jmlLiter = $value->qty;
                }
                if ($value->id_item == 2) {
                    $jmlDrum = $value->qty_before;
                    $penambahanDrum = $value->additional;
                    if ($penambahanDrum > 0 && $jmlDrum == 0) {
                        $isCheckedAdditionalDrum = 1;
                    }
                    if ($jmlDrum > 0) {
                        $isCheckedDrum = 1;
                    }
                }
            }
            if ($jmlDrum > 0) {
                $literPerDrum = $literPerDrum / $jmlDrum;
            }

            $idSupplier = $dataPurchase->id_supplier;
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;
            $alamatSupplier = $dataSupplier->address;
            $phoneSupplier = $dataSupplier->phone;

            $sql = "SELECT * FROM tbl_branch WHERE id_branch = '{$_SESSION['id_branch']}'";
            $branch = $this->db->query($sql)->row();

            $data['menuName'] = "pembelian";
            $data['page'] = "Pembelian";
            $data['judul'] = "Pembelian ";
            $data['dataPurchase'] = $dataPurchase;
            $data['branch'] = $branch;
            $data['dataPurchaseDetail'] = $dataPurchaseDetail;
            $data['idTransactionPurchase'] = $id;
            $data['namaSupplier'] = $namaSupplier;
            $data['alamatSupplier'] = $alamatSupplier;
            $data['phoneSupplier'] = $phoneSupplier;
            $data['isCheckedDrum'] = $isCheckedDrum;
            $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
            $data['jmlKemasan'] = $jmlKemasan;
            $data['literPerKemasan'] = $literPerKemasan;
            $data['jmlDrum'] = $jmlDrum;
            $data['literPerDrum'] = $literPerDrum;
            $data['penambahanDrum'] = $penambahanDrum;
            $data['penambahanLiter'] = $penambahanLiter;
            $data['penyusutanLiter'] = $penyusutanLiter;
            $data['weight'] = $weight;
            $data['specificGravity'] = $specificGravity;
            $data['waterContent'] = $waterContent;
            $data['jmlLiter'] = $jmlLiter;
            $data['description'] = $dataPurchase->description;

            $this->load->view('v_purchase/nota', $data);
        } else {
            echo "<script>alert('Penjualan tidak valid.'); window.location = '" . base_url("penjualan") . "';</script>";
        }
    }

    public function Cetak_approve($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $dataPurchase = $this->M_purchase->selectById($id);
        $nominalPembelian = $dataPurchase->price;
        if ($dataPurchase != NULL) {
            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);

            $isCheckedDrum = 0;
            $isCheckedAdditionalDrum = 0;
            $jmlKemasan = 0;
            $literPerKemasan = 0;
            $jmlDrum = 0;
            $literPerDrum = 0;
            $penambahanDrum = 0;
            $penambahanLiter = 0;
            $penyusutanLiter = 0;
            $weight = 0;
            $specificGravity = 0;
            $waterContent = 0;
            $jmlLiter = 0;

            foreach ($dataPurchaseDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $jmlKemasan = $value->multiple;
                    $literPerKemasan = $value->qty_basic;
                    $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                    $literPerDrum = $value->qty_before;
                    $penambahanLiter = $value->additional;
                    $penyusutanLiter = $value->depreciation;
                    $weight = $value->weight;
                    $weight = str_replace(".", ",", $weight);
                    $specificGravity = $value->specific_gravity;
                    $specificGravity = str_replace(".", ",", $specificGravity);
                    $waterContent = $value->water_content;
                    $waterContent = str_replace(".", ",", $waterContent);
                    $jmlLiter = $value->qty;
                }
                if ($value->id_item == 2) {
                    $jmlDrum = $value->qty_before;
                    $penambahanDrum = $value->additional;
                    if ($penambahanDrum > 0 && $jmlDrum == 0) {
                        $isCheckedAdditionalDrum = 1;
                    }
                    if ($jmlDrum > 0) {
                        $isCheckedDrum = 1;
                    }
                }
            }
            if ($jmlDrum > 0) {
                $literPerDrum = $literPerDrum / $jmlDrum;
            }

            $idSupplier = $dataPurchase->id_supplier;
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;
            $alamatSupplier = $dataSupplier->address;
            $phoneSupplier = $dataSupplier->phone;

            $sql = "SELECT * FROM tbl_branch WHERE id_branch = '{$this->branch}'";
            $branch = $this->db->query($sql)->row();

            $data['menuName'] = "pembelian";
            $data['page'] = "Pembelian";
            $data['judul'] = "Pembelian ";
            $data['dataPurchase'] = $dataPurchase;
            $data['branch'] = $branch;
            $data['dataPurchaseDetail'] = $dataPurchaseDetail;
            $data['idTransactionPurchase'] = $id;
            $data['namaSupplier'] = $namaSupplier;
            $data['alamatSupplier'] = $alamatSupplier;
            $data['phoneSupplier'] = $phoneSupplier;
            $data['isCheckedDrum'] = $isCheckedDrum;
            $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
            $data['jmlKemasan'] = $jmlKemasan;
            $data['literPerKemasan'] = $literPerKemasan;
            $data['jmlDrum'] = $jmlDrum;
            $data['literPerDrum'] = $literPerDrum;
            $data['penambahanDrum'] = $penambahanDrum;
            $data['penambahanLiter'] = $penambahanLiter;
            $data['penyusutanLiter'] = $penyusutanLiter;
            $data['weight'] = $weight;
            $data['specificGravity'] = $specificGravity;
            $data['waterContent'] = $waterContent;
            $data['jmlLiter'] = $jmlLiter;
            $data['nominalPembelian'] = $nominalPembelian;
            $data['description'] = $dataPurchase->description;

            $this->load->view('v_purchase/nota_approve', $data);
        } else {
            echo "<script>alert('Penjualan tidak valid.'); window.location = '" . base_url("penjualan") . "';</script>";
        }
    }

    public function Detail($type = 0)
    {
        $judul = "Detail Pembelian";
        $isDelete = 0;
        if ($type == 1) {
            $judul = "Hapus Pembelian";
            $isDelete = 1;
        }
        $isApprove = 0;
        if ($type == 2) {
            $judul = "Approve Pembelian";
            $isApprove = 1;
        }
        $isCancel = 0;
        if ($type == 3) {
            $judul = "Batalkan Konfirmasi Pembelian";
            $isCancel = 1;
        }

        $id = $_POST['id'];
        $dataPurchase = $this->M_purchase->selectById($id);
        if ($dataPurchase) {
            $dataPurchaseDetail = $this->M_purchase->selectDetailByHeaderId($id);

            $isApproved = $dataPurchase->is_approved;
            $isCheckedDrum = $dataPurchase->is_drum;
            if ($isCheckedDrum == null) {
                $isCheckedDrum = 0;
            }
            $isCheckedAdditionalDrum = 0;

            $weight = 0;
            $specificGravity = 0;
            $waterContent = 0;
            $jmlKemasan = 0;
            $literPerKemasan = 0;
            $totalLiter = 0;
            $penambahanLiter = 0;
            $penyusutanLiter = 0;
            $grandTotalLiter = 0;
            $hargaPerLiter = 0;
            $penambahanDrum = 0;
            $hargaPerDrum = 0;

            foreach ($dataPurchaseDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $weight = $value->weight;
                    $weight = str_replace(".", ",", $weight);
                    $specificGravity = $value->specific_gravity;
                    $specificGravity = str_replace(".", ",", $specificGravity);
                    $waterContent = $value->water_content;
                    $waterContent = str_replace(".", ",", $waterContent);
                    $jmlKemasan = $value->multiple;
                    $literPerKemasan = $value->qty_basic;
                    $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                    $totalLiter = $value->qty_before;
                    $totalLiter = str_replace(".", ",", $totalLiter);
                    $penambahanLiter = $value->additional;
                    $penyusutanLiter = $value->depreciation;
                    $grandTotalLiter = $value->qty;
                    $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                    $hargaPerLiter = $value->total;
                    $hargaPerLiter = str_replace(".", ",", $hargaPerLiter);
                }
                if ($value->id_item == 2) {
                    $penambahanDrum = $value->qty;
                    $hargaPerDrum = $value->total;
                    $hargaPerDrum = str_replace(".", ",", $hargaPerDrum);
                    if ($penambahanDrum > 0) {
                        $isCheckedAdditionalDrum = 1;
                    }
                }
            }

            $idSupplier = $dataPurchase->id_supplier;
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;

            $listSupplierLoan = array();
            $subtotalInstallment = 0;
            $isSupplierLoan = 0;
            $nominalInstallment = 0;
            $strIdSupplierLoanInstallment = $dataPurchase->id_supplier_loan_installment;
            if (strlen($strIdSupplierLoanInstallment) > 0) {
                $expIdSupplierLoanInstallment = explode(',', $strIdSupplierLoanInstallment);
                if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                    $isSupplierLoan = 1;
                    foreach ($expIdSupplierLoanInstallment as $k => $val) {
                        $dataSupplierLoanInstallment = $this->M_supplier_loan_installment->selectById($val);
                        $getSupplierLoan = $this->M_supplier_loan->selectById($dataSupplierLoanInstallment->id_supplier_loan);
                        $idSupplierLoanInstallment = $dataSupplierLoanInstallment->id_supplier_loan_installment;
                        $nominalInstallment = $dataSupplierLoanInstallment->nominal;
                        $subtotalInstallment += $nominalInstallment;

                        $q = "SELECT count(*) as installment FROM tbl_supplier_loan_installment WHERE id_supplier_loan = {$dataSupplierLoanInstallment->id_supplier_loan} AND id_supplier_loan_installment <= {$val}";
                        $res = $this->db->query($q)->row();

                        $paid = ($getSupplierLoan->paid - $nominalInstallment);
                        $debt = ($getSupplierLoan->debt + $nominalInstallment);
                        $installment = $res->installment;

                        $listSupplierLoan[$k] = array(
                            'id_supplier_loan_installment' => $idSupplierLoanInstallment,
                            'id_supplier_loan' => $dataSupplierLoanInstallment->id_supplier_loan,
                            'nominal' => $getSupplierLoan->nominal,
                            'installment' => $installment,
                            'nominal_installment' => $nominalInstallment,
                            'description' => $getSupplierLoan->description,
                        );
                    }
                }
            }

            $data['menuName'] = "pembelian";
            $data['page'] = "Pembelian";
            $data['judul'] = $judul;
            $data['dataPurchase'] = $dataPurchase;
            $data['isApproved'] = $isApproved;
            $data['idTransactionPurchase'] = $id;

            $data['dataSupplier'] = $dataSupplier;
            $data['bank'] = $bank;
            $data['namaSupplier'] = $namaSupplier;
            $data['weight'] = $weight;
            $data['specificGravity'] = $specificGravity;
            $data['waterContent'] = $waterContent;

            $data['isCheckedDrum'] = $isCheckedDrum;
            $data['jmlKemasan'] = $jmlKemasan;
            $data['literPerKemasan'] = $literPerKemasan;
            $data['totalLiter'] = $totalLiter;
            $data['penambahanLiter'] = $penambahanLiter;
            $data['penyusutanLiter'] = $penyusutanLiter;
            $data['grandTotalLiter'] = $grandTotalLiter;
            $data['hargaPerLiter'] = $hargaPerLiter;
            $data['hargaPerDrum'] = $hargaPerDrum;
            $data['price'] = $dataPurchase->price;
            $data['ppn'] = $dataPurchase->ppn;
            $data['totalPpn'] = $dataPurchase->ppn * $dataPurchase->price;
            $data['subTotalHarga'] = $dataPurchase->nominal;
            $data['subtotalInstallment'] = $subtotalInstallment;
            $data['grandTotal'] = $dataPurchase->nominal - $subtotalInstallment;

            $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
            $data['penambahanDrum'] = $penambahanDrum;

            $data['isSupplierLoan'] = $isSupplierLoan;
            $data['listSupplierLoan'] = $listSupplierLoan;
            $data['isCancel'] = $isCancel;
            $data['isApprove'] = $isApprove;
            $data['isDelete'] = $isDelete;

            $isDrumOnly = 0;
            $idItemExp = $dataPurchase->id_item_exp;

            if (strlen($idItemExp) > 0) {
                $arrItem = explode(',', $idItemExp);
                if (count($arrItem) == 1) {
                    if ($arrItem[0] == 2) {
                        $isDrumOnly = 1;
                    }
                }
            }

            if ($isDrumOnly > 0) {
                echo show_my_modal('v_purchase/detail_custom', 'detail-purchase', $data, 'md');
            } else {
                echo show_my_modal('v_purchase/detail', 'detail-purchase', $data, 'md');
            }
        } else {
            echo "<script>alert('Pembelian tidak tersedia.'); window.location = '" . base_url("pembelian") . "';</script>";
        }
    }

    public function prosesCancel()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_transaction_purchase'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'pembelian');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getPurchase = $this->M_purchase->selectById($id);
            $isApproved = $getPurchase->is_approved;
            $code = $getPurchase->code;
            $idSupplierLoanInstallment = $getPurchase->id_supplier_loan_installment;
            $nominal = $getPurchase->nominal;
            if ($getPurchase == NULL) {
                $errCode++;
                $errMessage = "Purchase Invalid.";
            }
        }
        if ($errCode == 0) {
            if ($isApproved > 0) {
                $errCode++;
                $errMessage = "Pembelian sudah di approve.";
            }
        }
        if ($errCode == 0) {
            if ($getPurchase->nominal <= 0) {
                $errCode++;
                $errMessage = "Pembelian belum di konfirmasi.";
            }
        }
        if ($errCode == 0) {
            $strIdSupplierLoanInstallment = $getPurchase->id_supplier_loan_installment;
            if (strlen($strIdSupplierLoanInstallment) > 0) {
                $expIdSupplierLoanInstallment = explode(',', $strIdSupplierLoanInstallment);
                if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                    foreach ($expIdSupplierLoanInstallment as $k => $val) {
                        $idSupplierLoanInstallment = $val;
                        $err = 0;
                        $msg = "";
                        if ($err == 0) {
                            if (strlen($idSupplierLoanInstallment) == 0) {
                                $err++;
                                $msg = 'Installment is empty !';
                            }
                        }
                        if ($err == 0) {
                            $getSupplierLoanInstallment = $this->M_supplier_loan->selectByIdInstallment($idSupplierLoanInstallment);
                            if ($getSupplierLoanInstallment == NULL) {
                                $err++;
                                $msg = 'Installment is empty !';
                            }
                        }
                        if ($getSupplierLoanInstallment->status == 0) {
                            if ($err == 0) {
                                try {
                                    $this->db->delete('tbl_supplier_loan_installment', array('id_supplier_loan_installment' => $idSupplierLoanInstallment));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                        }
                        if ($err > 0) {
                            $errCode = $err;
                            $errMessage = $msg;
                            break;
                        }
                    }
                }
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdate = array(
                    'id_supplier_loan_installment' => null,
                    'price' => 0,
                    'nominal' => 0,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_purchase', $dataUpdate, array('id_transaction_purchase' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdate = array(
                    'total' => 0,
                    'grandtotal' => 0,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_purchase_detail', $dataUpdate, array('id_transaction_purchase' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesApprove()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_transaction_purchase'];
        $paid = $_POST['paid'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'pembelian');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($paid) == 0) {
                $errCode++;
                $errMessage = "Tanggal Bayar wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $getPurchase = $this->M_purchase->selectById($id);
            $isApproved = $getPurchase->is_approved;
            $code = $getPurchase->code;
            $idSupplierLoanInstallment = $getPurchase->id_supplier_loan_installment;
            $nominal = $getPurchase->nominal;
            if ($getPurchase == NULL) {
                $errCode++;
                $errMessage = "Purchase Invalid.";
            }
        }
        if ($errCode == 0) {
            if ($isApproved > 0) {
                $errCode++;
                $errMessage = "Pembelian sudah di approve.";
            }
        }
        if ($errCode == 0) {
            if ($getPurchase->nominal <= 0) {
                $errCode++;
                $errMessage = "Pembelian belum di konfirmasi.";
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdate = array(
                    'paid' => date('Y-m-d', strtotime($paid)),
                    'is_approved' => 1,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_purchase', $dataUpdate, array('id_transaction_purchase' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            $isok2 = $this->M_finance->balance("tbl_transaction_purchase", array('id_transaction_purchase' => $id), 'out', 'TRANSACTION PURCHASE ' . $code, 'Transaksi Pembelian ' . $code . ' sebesar ' . number_format($nominal, 0, ",", "."));
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }
        if ($errCode == 0) {
            $strIdSupplierLoanInstallment = $getPurchase->id_supplier_loan_installment;
            if (strlen($strIdSupplierLoanInstallment) > 0) {
                $expIdSupplierLoanInstallment = explode(',', $strIdSupplierLoanInstallment);
                if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                    foreach ($expIdSupplierLoanInstallment as $k => $val) {
                        $idSupplierLoanInstallment = $val;
                        $err = 0;
                        $msg = "";
                        if ($err == 0) {
                            if (strlen($idSupplierLoanInstallment) == 0) {
                                $err++;
                                $msg = 'Installment is empty !';
                            }
                        }
                        if ($err == 0) {
                            $getSupplierLoanInstallment = $this->M_supplier_loan->selectByIdInstallment($idSupplierLoanInstallment);
                            if ($getSupplierLoanInstallment == NULL) {
                                $err++;
                                $msg = 'Installment is empty !';
                            }
                        }
                        if ($getSupplierLoanInstallment->status == 0) {
                            if ($err == 0) {
                                $idSupplierLoan = $getSupplierLoanInstallment->id_supplier_loan;
                                $nominalInstallment = $getSupplierLoanInstallment->nominal;
                                $getSupplier = $this->M_supplier->selectById($getPurchase->id_supplier);
                                $getSupplierLoan = $this->M_supplier_loan->selectById($idSupplierLoan);
                                $nominalLoan = $getSupplierLoan->nominal;
                                $paidLoan = $getSupplierLoan->paid;
                                $debtLoan = $getSupplierLoan->debt;
                                $paidUpdate = $paidLoan + $nominalInstallment;
                                $debtUpdate = $debtLoan - $nominalInstallment;

                                try {
                                    $this->db->update('tbl_supplier_loan_installment', array('status' => 1), array('id_supplier_loan_installment' => $idSupplierLoanInstallment));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                            if ($err == 0) {
                                try {
                                    $isPaidOff = 0;
                                    if ($debtUpdate == 0 && $paidUpdate == $nominalLoan) {
                                        $isPaidOff = 1;
                                    }
                                    $dataSupplierLoan = array(
                                        'paid' => $paidUpdate,
                                        'debt' => $debtUpdate,
                                        'is_paid_off' => $isPaidOff,
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->update('tbl_supplier_loan', $dataSupplierLoan, array('id_supplier_loan' => $idSupplierLoan));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                            if ($err == 0) {
                                $isok3 = $this->M_finance->balance("tbl_supplier_loan_installment", array('id_supplier_loan_installment' => $idSupplierLoanInstallment), 'in', 'PAYMENT INSTALLMENT  ' . $getSupplier->name, 'Pembayaran Cicilan ' . $getSupplier->name . ' sebesar ' . number_format($nominalInstallment, 0, ",", "."));
                                $err = $isok3['errCode'];
                                $msg = $isok3['errMessage'];
                            }
                        }
                        if ($err > 0) {
                            $errCode = $err;
                            $errMessage = $msg;
                            break;
                        }
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $isApproved = 0;
        $id = $_POST['id_transaction_purchase'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('del', 'pembelian');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getPurchase = $this->M_purchase->selectById($id);
            $isApproved = $getPurchase->is_approved;
            $code = $getPurchase->code;
            $idSupplierLoanInstallment = $getPurchase->id_supplier_loan_installment;
            $nominal = $getPurchase->nominal;
            if ($getPurchase == NULL) {
                $errCode++;
                $errMessage = "Purchase Invalid.";
            }
        }
        if ($errCode == 0) {
            $strIdSupplierLoanInstallment = $getPurchase->id_supplier_loan_installment;
            if (strlen($strIdSupplierLoanInstallment) > 0) {
                $expIdSupplierLoanInstallment = explode(',', $strIdSupplierLoanInstallment);
                if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                    $isSupplierLoan = 1;
                    foreach ($expIdSupplierLoanInstallment as $k => $val) {
                        $idSupplierLoanInstallment = $val;
                        $err = 0;
                        $msg = "";
                        if ($err == 0) {
                            if (strlen($idSupplierLoanInstallment) == 0) {
                                $err++;
                                $msg = 'Installment is empty !';
                            }
                        }
                        if ($err == 0) {
                            $getSupplierLoanInstallmentDelete = $this->M_supplier_loan->selectByIdInstallment($idSupplierLoanInstallment);
                            if ($getSupplierLoanInstallmentDelete == NULL) {
                                $err++;
                                $msg = 'Installment is empty !';
                            }
                        }
                        if ($err == 0) {
                            //Proses Hapus CIcilan
                            $idSupplierLoanDelete = $getSupplierLoanInstallmentDelete->id_supplier_loan;
                            $getSupplierDelete = $this->M_supplier->selectById($getPurchase->id_supplier);
                            $getSupplierLoanDelete = $this->M_supplier_loan->selectById($idSupplierLoanDelete);
                            $nominalLoanDelete = $getSupplierLoanDelete->nominal;
                            $paidUpdateDelete = $getSupplierLoanDelete->paid - $getSupplierLoanInstallmentDelete->nominal;
                            $debtUpdateDelete = $getSupplierLoanDelete->debt + $getSupplierLoanInstallmentDelete->nominal;
                            if ($paidUpdateDelete < 0) {
                                $err++;
                                $msg = "Something Wrong on Installment";
                            }
                        }
                        if ($err == 0) {
                            if ($isApproved > 0) {
                                $isokDelete = $this->M_finance->deleteBalance("tbl_supplier_loan_installment", array('id_supplier_loan_installment' => $idSupplierLoanInstallment), 'out', 'DELETE PAYMENT INSTALLMENT  ' . $getSupplierDelete->name, 'Hapus Pembayaran Cicilan ' . $getSupplierDelete->name . ' sebesar ' . number_format($getSupplierLoanDelete->nominal, 0, ",", "."));
                                $err = $isokDelete['errCode'];
                                $msg = $isokDelete['errMessage'];
                            }
                        }
                        if ($err == 0) {
                            try {
                                $this->db->delete('tbl_supplier_loan_installment', array('id_supplier_loan_installment' => $idSupplierLoanInstallment));
                            } catch (Exception $ex) {
                                $err++;
                                $msg = $ex->getMessage();
                            }
                        }
                        if ($err == 0) {
                            if ($isApproved > 0) {
                                try {
                                    $isPaidOffDelete = 0;
                                    if ($debtUpdateDelete == 0 && $paidUpdateDelete == $nominalLoanDelete) {
                                        $isPaidOffDelete = 1;
                                    }
                                    $dataSupplierLoan = array('paid' => $paidUpdateDelete,
                                        'debt' => $debtUpdateDelete,
                                        'is_paid_off' => $isPaidOffDelete,
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->update('tbl_supplier_loan', $dataSupplierLoan, array('id_supplier_loan' => $idSupplierLoanDelete));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                        }
                        if ($err > 0) {
                            $errCode = $err;
                            $errMessage = $msg;
                            break;
                        }
                    }
                }
            }
        }
        if ($errCode == 0) {
            $getDetail = $this->M_purchase->selectDetailByHeaderId($id);
            if (count($getDetail) > 0) {
                foreach ($getDetail as $key => $value) {
                    $err = 0;
                    $msg = "";

                    $idTransactionPurchaseDetail = $value->id_transaction_purchase_detail;
                    $getItem = $this->M_item->selectById($value->id_item);

                    if ($err == 0) {
                        $isok1 = $this->M_item->deleteBalance("tbl_transaction_purchase_detail", array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
                        $err = $isok1['errCode'];
                        $msg = $isok1['errMessage'];
                    }
                    if ($err == 0) {
                        try {
                            $this->db->delete('tbl_transaction_purchase_detail', array('id_transaction_purchase_detail' => $idTransactionPurchaseDetail));
                        } catch (Exception $ex) {
                            $err++;
                            $msg = $ex->getMessage();
                        }
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        // proses balance finance
        if ($errCode == 0) {
            if ($isApproved > 0) {
                $isok2 = $this->M_finance->deleteBalance("tbl_transaction_purchase", array('id_transaction_purchase' => $id), 'in', 'DELETE TRANSACTION PURCHASE ' . $code, 'Transaksi Pembelian ' . $code . ' sebesar ' . number_format($nominal, 0, ",", "."));
                $errCode = $isok2['errCode'];
                $errMessage = $isok2['errMessage'];
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->delete('tbl_transaction_purchase', array('id_transaction_purchase' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function getSupplierLoanInfo($isAjax = 1, $idSupplierLoan = NULL, $idSupplierLoanInstallment = NULL, $isPaid = 1)
    {
        if ($isAjax > 0) {
            $idSupplierLoan = $this->input->post("id_supplier_loan");
        }

        $response = array();
        $isPaidOff = 0;
        $isLoan = false;
        if (strlen($idSupplierLoan) > 0) {
            $getSupplierLoan = $this->M_supplier_loan->selectById($idSupplierLoan);
            if ($getSupplierLoan != NULL) {
                $isPaidOff = $getSupplierLoan->is_paid_off;
                $resultSupplierLoanInstallment = $this->M_supplier_loan->getSupplierLoanInstallment($idSupplierLoan, $isPaid);
                $countSupplierLoanInstallment = count($resultSupplierLoanInstallment);
                $isLoan = true;

                $paid = 0;
                $nominalLoadn = $getSupplierLoan->nominal;
                if ($countSupplierLoanInstallment > 0) {
                    foreach ($resultSupplierLoanInstallment as $key => $value) {
                        $paid += $value->nominal;
                    }
                }
                $debt = $nominalLoadn - $paid;

                $installment = $countSupplierLoanInstallment + 1;
                if ($idSupplierLoanInstallment != NULL) {
                    if ($countSupplierLoanInstallment > 0) {
                        $installment = 0;
                        foreach ($resultSupplierLoanInstallment as $key => $value) {
                            if ($value->id_supplier_loan_installment == $idSupplierLoanInstallment) {
                                $paid = $paid - $value->nominal;
                                $debt = $debt + $value->nominal;
                                $installment++;
                            }
                        }
                    }
                }

                $response = array(
                    'id_supplier_loan' => $idSupplierLoan,
                    'nominal' => $getSupplierLoan->nominal,
                    'paid' => $paid,
                    'debt' => $debt,
                    'installment' => $installment,
                );
            }
        }

        $arr = array('is_loan'
            => $isLoan, 'is_paid_off' => $isPaidOff, 'response' => $response);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $arr;
        }
    }

    public function getSupplierLoan($isAjax = 1, $idSupplier = NULL, $idSupplierLoanInstallment = NULL)
    {
        if ($isAjax > 0) {
            $idSupplier = $this->input->post("id_supplier");
            $idSupplierLoanInstallment = $this->input->post("id_supplier_loan_installment");
        }

        if ($idSupplierLoanInstallment == 0)
            $idSupplierLoanInstallment = NULL;
        if (strlen($idSupplierLoanInstallment) == 0)
            $idSupplierLoanInstallment = NULL;

        $response = array();
        $isLoan = false;
        if (strlen($idSupplier) > 0) {
            $getSupplierLoan = $this->M_supplier_loan->getSupplierLoan($idSupplier);
            if ($getSupplierLoan != NULL) {
                $idSupplierLoan = $getSupplierLoan->id_supplier_loan;
                $resultSupplierLoanInstallment = $this->M_supplier_loan->getSupplierLoanInstallment($idSupplierLoan);
                $countSupplierLoanInstallment = count($resultSupplierLoanInstallment);

                $paid = $getSupplierLoan->paid;
                $debt = $getSupplierLoan->debt;
                $nominalInstallment = 0;
                $installment = $countSupplierLoanInstallment + 1;
                if ($idSupplierLoanInstallment != NULL) {
                    if ($countSupplierLoanInstallment > 0) {
                        $installment = 0;
                        foreach ($resultSupplierLoanInstallment as $key => $value) {
                            if ($value->id_supplier_loan_installment == $idSupplierLoanInstallment) {
                                $paid = $paid - $value->nominal;
                                $debt = $debt + $value->nominal;
                                $nominalInstallment = $value->nominal;
                                $installment++;
                            }
                        }
                    }
                }

                $isLoan = true;
                $response = array(
                    'id_supplier_loan' => $idSupplierLoan,
                    'nominal' => $getSupplierLoan->nominal,
                    'paid' => $paid,
                    'debt' => $debt,
                    'nominal_installment' => $nominalInstallment,
                    'installment' => $installment,
                );
            }
        }

        $arr = array('is_loan'
            => $isLoan, 'response' => $response);

        if ($isAjax > 0) {
            echo

            json_encode($arr);
        } else {
            return $arr;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Unit Test">
    public function testCode()
    {
        echo "<pre>";
        print_r($this->M_sidebar->access('add', 'pembelian'));
        echo "</pre>";
        echo "<br/>";
    }

    public function testPurchase()
    {
        $sql = "SELECT * FROM tbl_transaction_purchase";
        $result = $this->db->query($sql)->result();

        if (count($result) > 0) {
            foreach ($result as $key => $value) {
                $strItem = "";
                $q = "SELECT * FROM tbl_transaction_purchase_detail WHERE id_transaction_purchase = '" . $value->id_transaction_purchase . "' ";
                $detail = $this->db->query($q)->result();
                if (count($detail) > 0) {
                    foreach ($detail as $k => $val) {
                        if ($k > 0)
                            $strItem .= ",";
                        $strItem .= $val->id_item;
                    }
                }

                $data = array('id_item_exp' => $strItem,
                );
                echo "<pre>";
                print_r($data);
                echo "</pre>";
                echo "<br/>";
                $this->db->update('tbl_transaction_purchase', $data, array('id_transaction_purchase' => $value->id_transaction_purchase));
            }
        }
    }

    public function normalisasiBalance()
    {
        $sqlHeader = "SELECT * FROM tbl_transaction_purchase";
        $resultHeader = $this->db->query($sqlHeader)->result();
        foreach ($resultHeader as $key => $value) {
            $resultDetail = $this->M_purchase->selectDetailByHeaderId($value->id_transaction_purchase);
            foreach ($resultDetail as $k => $val) {
                $dataUpdateDetail = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'created_by' => $value->created_by,
                );
                $this->db->update('tbl_transaction_purchase_detail', $dataUpdateDetail, array('id_transaction_purchase_detail' => $val->id_transaction_purchase_detail));

                $qItemHistory = "   SELECT * FROM tbl_item_history
                                    WHERE id_item = '{$val->id_item}'
                                    AND ref_table = 'tbl_transaction_purchase_detail}'
                                    AND ref_id = '{$val->id_transaction_purchase_detail}'
                                    AND fixed > 0 ";
                $getItemHistory = $this->db->query($qItemHistory)->row();
                if ($getItemHistory != NULL) {
                    $dataUpdateItemHistory = array(
                        'date' => $value->date,
                        'created_date' => $value->created_date,
                        'updated_date' => $val->updated_date,
                    );
                    $this->db->update('tbl_item_history', $dataUpdateItemHistory, array('id_item_history' => $getItemHistory->id_item_history));
                }
            }

            $qFinanceHistory = "SELECT * FROM tbl_finance_history
                                WHERE ref_table = 'tbl_transaction_purchase'
                                AND ref_id = '{$value->id_transaction_purchase}'
                                AND fixed > 0 ";
            $getLastFinanceHistory = $this->db->query($qFinanceHistory)->row();
            if ($getLastFinanceHistory != NULL) {
                $dataUpdateFinanceHistory = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'updated_date' => $value->updated_date,
                );
                $this->db->update('tbl_finance_history', $dataUpdateFinanceHistory, array('id_finance_history' => $getLastFinanceHistory->id_item_history));
            }

            $strIdSupplierLoanInstallment = $value->id_supplier_loan_installment;
            if (strlen($strIdSupplierLoanInstallment) > 0) {
                $expIdSupplierLoanInstallment = explode(',', $strIdSupplierLoanInstallment);
                if (is_array($expIdSupplierLoanInstallment) && count($expIdSupplierLoanInstallment) > 0) {
                    foreach ($expIdSupplierLoanInstallment as $k => $val) {
                        $getLoanInstallment = $this->M_supplier_loan_installment->selectById($val);
                        if ($getLoanInstallment != null) {
                            $dataUpdateInstallment = array(
                                'date' => $value->date,
                                'created_date' => $value->created_date,
                            );
                            $this->db->update('tbl_supplier_loan_installment', $dataUpdateInstallment, array('id_supplier_loan_installment' => $val));

                            $qFinanceHistory = "SELECT * FROM tbl_finance_history
                                                WHERE ref_table = 'tbl_supplier_loan_installment'
                                                AND ref_id = '{$val}'
                                                AND fixed > 0 ";
                            $getLastFinanceHistory = $this->db->query($qFinanceHistory)->row();
                            if ($getLastFinanceHistory != NULL) {
                                $dataUpdateFinanceHistory = array(
                                    'date' => $value->date,
                                    'created_date' => $value->created_date,
                                    'updated_date' => $getLoanInstallment->updated_date,
                                );
                                $this->db->update('tbl_finance_history', $dataUpdateFinanceHistory, array('id_finance_history' => $getLastFinanceHistory->id_item_history));
                            }
                        }
                    }
                }
            }
        }
    }
    //</editor-fold>
}

<?php

/**
 *
 * @author Susi Susanti
 * @since  May 24, 2019
 * @license http://piposystem.com Piposystem
 */
class VehicleMaintenance extends AUTH_Controller
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_sidebar');
        $this->load->model('M_vehicle');
        $this->load->model('M_finance');
        $this->load->model('M_vehicle_maintenance');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'pemeliharaan');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['menuName'] = "pemeliharaan";
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pemeliharaan Armada";
        $data['judul'] = "Pemeliharaan Armada";

        $this->loadkonten('v_vehicle_maintenance/home', $data);
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', 'pemeliharaan');
        $accessBack = $this->M_sidebar->access('back', 'pemeliharaan');
        $accessDel = $this->M_sidebar->access('del', 'pemeliharaan');

        $list = $this->M_vehicle_maintenance->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $dataVehicle = $this->M_vehicle->selectById($value->id_vehicle);

            $row = array();
            $row[] = $no;
            $row[] = $dataVehicle->brand . ' - ' . $dataVehicle->police_number;
            $row[] = date('d-m-Y', strtotime($value->date));
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($value->nominal, 0, ",", ".") . '</div">';
            $row[] = $value->description;
            $row[] = date('d-m-Y H:i:s', strtotime($value->created_date));
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));

            //add html for action
            $action = " <div class='btn-group pull-right'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . site_url('edit-pemeliharaan/' . $value->id_vehicle_maintenance) . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $getVehicleRent = $this->db->get_where('tbl_vehicle_rent', array('id_branch' => $this->branch, 'id_vehicle_maintenance' => $value->id_vehicle_maintenance,))->row();
                if ($getVehicleRent == null) {
                    $action .= "    <li><a href='#' class='hapus-pemeliharaan' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_vehicle_maintenance . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
                }
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $access = $this->M_sidebar->access('add', 'pemeliharaan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pemeliharaan Armada";
        $data['judul'] = "Tambah Pemeliharaan Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "pemeliharaan";
            $data['vehicle'] = $this->M_vehicle->selectItem();
            $this->loadkonten('v_vehicle_maintenance/tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idVehicle = $this->input->post("id_vehicle");
        $dateMaintenance = $this->input->post("date_maintenance");
        $description = $this->input->post("description");
        $amountMaintenance = $this->input->post("amount_maintenance");
        $amountMaintenance = str_replace(".", "", $amountMaintenance);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessAdd = $this->M_sidebar->access('add', 'pemeliharaan');
            if ($accessAdd->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idVehicle) == 0) {
                $errCode++;
                $errMessage = "Armada harus di isi.";
            }
        }
        if ($errCode == 0) {
            $getVehicle = $this->M_vehicle->selectById($idVehicle);
            if ($getVehicle == null) {
                $errCode++;
                $errMessage = "Armada tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateMaintenance) == 0) {
                $errCode++;
                $errMessage = "Tanggal Pemeliharaan harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($amountMaintenance) == 0) {
                $errCode++;
                $errMessage = "Biaya Maintenance harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($amountMaintenance <= 0) {
                $errCode++;
                $errMessage = "Biaya Maintenance harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $dataVehicleMaintenance = array(
                'id_branch' => $this->branch,
                'id_vehicle' => $idVehicle,
                'date' => date('Y-m-d', strtotime($dateMaintenance)),
                'nominal' => $amountMaintenance,
                'description' => $description,
                'created_by' => $username,
                'created_date' => $date,
                'updated_by' => $username,
                'updated_date' => $date,
            );
            $this->db->insert('tbl_vehicle_maintenance', $dataVehicleMaintenance);
            $idVehicleMaintenance = $this->db->insert_id();
        }
        if ($errCode == 0) {
            $vehicle = $getVehicle->brand . ' - ' . $getVehicle->police_number;

            $isok2 = $this->M_finance->balance("tbl_vehicle_maintenance", array('id_vehicle_maintenance' => $idVehicleMaintenance), 'out', 'MAINTENANCE FEE VEHICLE ' . $vehicle, 'Pemeliharaan Armada ' . $vehicle . ' sebesar ' . number_format($amountMaintenance, 0, ",", "."));
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $access = $this->M_sidebar->access('edit', 'pemeliharaan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Pemeliharaan Armada";
        $data['judul'] = "Ubah Pemeliharaan Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataVehicleMaintenance = $this->M_vehicle_maintenance->selectById($id);
            if ($dataVehicleMaintenance != null) {
                $getVehicleRent = $this->db->get_where('tbl_vehicle_rent', array('id_branch' => $this->branch, 'id_vehicle_maintenance' => $id,))->row();
                $dataVehicle = $this->M_vehicle->selectById($dataVehicleMaintenance->id_vehicle);

                $data['menuName'] = "pemeliharaan";
                $data['dataVehicleMaintenance'] = $dataVehicleMaintenance;
                $data['getVehicleRent'] = $getVehicleRent;
                $data['idVehicleMaintenance'] = $id;
                $data['dataVehicle'] = $dataVehicle;
                $data['vehicle'] = $this->M_vehicle->selectItem();
                $this->loadkonten('v_vehicle_maintenance/update', $data);
            } else {
                echo "<script>alert('Pemeliharaan armada tidak ditemukan.'); window.location = '" . base_url("pemeliharaan") . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idVehicle = $this->input->post("id_vehicle");
        $dateMaintenance = $this->input->post("date_maintenance");
        $description = $this->input->post("description");
        $amountMaintenance = $this->input->post("amount_maintenance");
        $amountMaintenance = str_replace(".", "", $amountMaintenance);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessEdit = $this->M_sidebar->access('edit', 'pemeliharaan');
            if ($accessEdit->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $getVehicleMaintenance = $this->M_vehicle_maintenance->selectById($id);
            if ($getVehicleMaintenance == null) {
                $errCode++;
                $errMessage = "Pemeliharaan armada tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idVehicle) == 0) {
                $errCode++;
                $errMessage = "Armada harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateMaintenance) == 0) {
                $errCode++;
                $errMessage = "Tanggal Pemeliharaan harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($amountMaintenance) == 0) {
                $errCode++;
                $errMessage = "Biaya Maintenance harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($amountMaintenance <= 0) {
                $errCode++;
                $errMessage = "Biaya Maintenance harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $getVehicle = $this->M_vehicle->selectById($idVehicle);
            if ($getVehicle == null) {
                $errCode++;
                $errMessage = "Vehicle invalid";
            }
        }
        if ($errCode == 0) {
            $isEdit = 1;
            $q = "  SELECT * FROM tbl_vehicle_maintenance
                    WHERE id_branch = '{$this->branch}'
                    AND id_vehicle_maintenance = '{$id}'
                    AND nominal = '{$amountMaintenance}'
                    AND date = '" . date('Y-m-d', strtotime($dateMaintenance)) . "'
                    AND id_vehicle = '{$idVehicle}'
                    AND description = '{$description}'";
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isEdit = 0;
            }
        }
        if ($isEdit > 0) {
            if ($errCode == 0) {
                try {
                    $dataVehicleMaintenance = array(
                        'id_vehicle' => $idVehicle,
                        'date' => date('Y-m-d', strtotime($dateMaintenance)),
                        'nominal' => $amountMaintenance,
                        'description' => $description,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->update('tbl_vehicle_maintenance', $dataVehicleMaintenance, array('id_vehicle_maintenance' => $id));
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            if ($errCode == 0) {
                $vehicle = $getVehicle->brand . ' - ' . $getVehicle->police_number;

                $isok2 = $this->M_finance->updateBalance("tbl_vehicle_maintenance", array('id_vehicle_maintenance' => $id), 'out', 'MAINTENANCE FEE VEHICLE ' . $vehicle, 'Pemeliharaan Armada ' . $vehicle . ' sebesar ' . number_format($amountMaintenance, 0, ",", "."));
                $errCode = $isok2['errCode'];
                $errMessage = $isok2['errMessage'];
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_vehicle_maintenance'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('del', 'pemeliharaan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getVehicleMaintenance = $this->M_vehicle_maintenance->selectById($id);
            if ($getVehicleMaintenance == NULL) {
                $errCode++;
                $errMessage = "Vehicle Maintenance Invalid.";
            }
        }
        if ($errCode == 0) {
//            $q = "  SELECT * FROM tbl_transaction_purchase 
//                    WHERE id_vehicle_maintenance = {$id} ";
//            $checkTransactionPurchase = $this->db->query($q)->row();
//            if ($checkTransactionPurchase != NULL) {
//                $errCode++;
//                $errMessage = "Gagal menghapus Pemeliharaan Kendaraan karena terdapat pada Transaksi Pembelian " . $checkTransactionPurchase->code;
//            }
            $getVehicleRent = $this->db->get_where('tbl_vehicle_rent', array('id_branch' => $this->branch, 'id_vehicle_maintenance' => $id,))->row();
            if ($getVehicleRent != NULL) {
                $errCode++;
                $errMessage = "Gagal menghapus Pemeliharaan Kendaraan karena terdapat pada penyewaan armada.";
            }
        }
        if ($errCode == 0) {
            $isok2 = $this->M_finance->deleteBalance("tbl_vehicle_maintenance", array('id_vehicle_maintenance' => $id));
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }
        if ($errCode == 0) {
            try {
                $this->db->delete('tbl_vehicle_maintenance', array('id_vehicle_maintenance' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    //<editor-fold defaultstate="collapsed" desc="Unit test">
    public function normalisasiBalance()
    {
        $sql = "SELECT * FROM tbl_vehicle_maintenance";
        $result = $this->db->query($sql)->result();
        foreach ($result as $key => $value) {
            $qFinanceHistory = "SELECT * FROM tbl_finance_history
                                WHERE ref_table = 'tbl_vehicle_maintenance'
                                AND ref_id = '{$value->id_vehicle_maintenance}'
                                AND fixed > 0 ";
            $getLastFinanceHistory = $this->db->query($qFinanceHistory)->row();
            if ($getLastFinanceHistory != NULL) {
                $dataUpdateFinanceHistory = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'updated_date' => $value->updated_date,
                );
                $this->db->update('tbl_finance_history', $dataUpdateFinanceHistory, array('id_finance_history' => $getLastFinanceHistory->id_item_history));
            }
        }
    }
    //</editor-fold>
}

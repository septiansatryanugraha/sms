
<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class Sale extends AUTH_Controller
{
    const __ppn = 0.11;

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_sidebar');
        $this->load->model('M_generate_code');
        $this->load->model('M_sale');
        $this->load->model('M_item');
        $this->load->model('M_finance');
        $this->load->model('M_customer');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'penjualan');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['menuName'] = "penjualan";
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penjualan";
        $data['judul'] = "Penjualan ";
        $data['customer'] = $this->M_customer->selectItem();
        $this->loadkonten('v_sale/home', $data);
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function ajaxList()
    {
        $idCustomer = $this->input->post('id_customer');
        $isApproved = $this->input->post('is_approved');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'id_customer' => $idCustomer,
            'is_approved' => $isApproved,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', 'penjualan');
        $accessApprove = $this->M_sidebar->access('conf', 'penjualan');
        $accessDel = $this->M_sidebar->access('del', 'penjualan');

        $list = $this->M_sale->getData(1, $filter);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value->code;

            $statusApprove = '<small class="label pull-center bg-yellow">Pending</small">';
            if ($value->nominal > 0) {
                $statusApprove = '<small class="label pull-center bg-blue">Confirm</small">';
            }
            if ($value->is_approved > 0) {
                $statusApprove = '<small class="label pull-center bg-green">Approved</small">';
            }

            $linkPrint = "";
            if ($value->nominal > 0) {
                $linkPrint = "<li><a href='" . site_url('cetak-penjualan-approve/' . $value->id_transaction_sales) . "' target='__blank'><i class='fa fa-print'></i> Cetak Approve</a></li>";
            }
            if ($value->is_approved > 0) {
                $linkPrint = "<li><a href='" . site_url('cetak-penjualan-approve/' . $value->id_transaction_sales) . "' target='__blank'><i class='fa fa-print'></i> Cetak Approve</a></li>";
            }

            $strDetailItem = "";
            $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($value->id_transaction_sales);
            foreach ($dataSaleDetail as $keyItem => $valItem) {
                $qty = number_format($valItem->qty, 0, ",", ".");
                $html = "<ul style='padding-left: 15px;'>";
                $html .= "<li>";
                if ($valItem->id_item == 1) {
                    $html .= "Oli: " . $qty . " liter";
                }
                if ($valItem->id_item == 2) {
                    $html .= "Drum: " . $qty . " pcs";
                }
                $html .= "</li>";
                $html .= "</ul>";
                $strDetailItem .= $html;
            }

            if ($value->ppn > 0) {
                $ppnBuysubTotal = "<ul style='padding-left: 15px;'>";
                $ppnBuysubTotal .= "<li>Nominal : " . number_format($value->price, 0, ",", ".") . "</li>";
                $ppnBuysubTotal .= "<li>PPN (" . ($value->ppn * 100) . " %) : " . number_format(($value->price * $value->ppn), 0, ",", ".") . "</li>";
                $ppnBuysubTotal .= "</ul>";
                $ppnBuysubTotal .= "<br><b>Total : " . number_format($value->nominal, 0, ",", ".") . "</b>";
            } else {
                $ppnBuysubTotal = '<div width="100%" style="text-align: right;">' . number_format($value->price, 0, ",", ".") . '</div>';
            }

            $dataCustomer = $this->M_customer->selectById($value->id_customer);

            $row[] = $dataCustomer->name;
            $row[] = $strDetailItem;
            $row[] = $ppnBuysubTotal;
            $row[] = $statusApprove;
            $row[] = date('d-m-Y H:i:s', strtotime($value->created_date));
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));

            $action = " <div class='btn-group pull-right'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            $action .= "        <li><a href='#' class='detail-penjualan' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_sales . "'><i class='glyphicon glyphicon-info-sign'></i> Detail</a></li>";
            $action .= "        <li><a href='" . site_url('cetak-penjualan/' . $value->id_transaction_sales) . "' target='__blank'><i class='fa fa-print'></i> Cetak</a></li>";
            $action .= $linkPrint;
            if ($value->is_approved == 0) {
                if ($value->nominal == 0) {
                    if ($accessEdit->menuview > 0) {
                        $action .= "    <li><a href='" . site_url('edit-penjualan/' . $value->id_transaction_sales) . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                    }
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='" . site_url('confirm-penjualan/' . $value->id_transaction_sales) . "'><i class='fa fa-check'></i> Konfirmasi</a></li>";
                    }
                } else {
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='" . site_url('edit-confirm-penjualan/' . $value->id_transaction_sales) . "'><i class='fa fa-edit'></i> Ubah Konfirmasi</a></li>";
                    }
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='#' class='conf-cancel-penjualan' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_sales . "'><i class='fa fa-close'></i> Batalkan Konfirmasi</a></li>";
                    }
                    if ($accessApprove->menuview > 0) {
                        $action .= "    <li><a href='#' class='conf-approve-penjualan' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_sales . "'><i class='fa fa-check-square-o'></i> Setujui</a></li>";
                    }
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='conf-hapus-penjualan' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_transaction_sales . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('add', 'penjualan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penjualan";
        $data['judul'] = "Tambah Penjualan";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "penjualan";
            $data['privilegeId'] = $privilegeId;
            $data['customer'] = $this->M_customer->selectItem();
            $this->loadkonten('v_sale/tambah', $data);
        }
    }

    public function AddCustom()
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('add', 'penjualan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penjualan";
        $data['judul'] = "Tambah Penjualan";
        if ($access->menuview == 0) {
            $data['page'] = "Penjualan";
            $data['judul'] = "Penjualan ";
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "penjualan";
            $data['privilegeId'] = $privilegeId;
            $data['customer'] = $this->M_customer->selectItem();
            $this->loadkonten('v_sale/tambah_custom', $data);
        }
    }

    public function prosesAdd()
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idCustomer = $this->input->post("id_customer");
        $dateSale = date('d-m-Y');
        if ($privilegeId == 1) {
            $dateSale = $this->input->post("date");
        }
        $weight = $this->input->post("weight");
        $weight = str_replace(",", ".", $weight);
        $specificGravity = $this->input->post("specific_gravity");
        $specificGravity = str_replace(",", ".", $specificGravity);
        $waterContent = $this->input->post("water_content");
        $waterContent = str_replace(",", ".", $waterContent);
        $isDrum = $this->input->post("is_drum");
        $isAdditionalDrum = $this->input->post("is_additional_drum");
        $jmlDrum = $this->input->post("jml_drum");
        $literPerDrum = $this->input->post("liter_per_drum");
        $jmlKemasan = $this->input->post("jml_kemasan");
        $literPerKemasan = $this->input->post("liter_per_kemasan");
        $literPerKemasan = str_replace(",", ".", $literPerKemasan);
        $penambahanDrum = $this->input->post("penambahan_drum");
        $penambahanLiter = $this->input->post("penambahan_liter");
        $penyusutanLiter = $this->input->post("penyusutan_liter");
        $descriptionDrum = $this->input->post("description_drum");
        $descriptionOli = $this->input->post("description_oli");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextSale();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('add', 'penjualan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idCustomer) == 0) {
                $errCode++;
                $errMessage = "Customer wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            if ($dataCustomer == null) {
                $errCode++;
                $errMessage = "Customer tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateSale) == 0) {
                $errCode++;
                $errMessage = "Tanggal pembelian harus di isi.";
            }
        }
        if (isset($isAdditionalDrum) && $isAdditionalDrum > 0) {
            if ($errCode == 0) {
                if (strlen($penambahanDrum) == 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($penambahanDrum <= 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum harus lebih dari 0.";
                }
            }
        } else {
            $penambahanDrum = 0;
        }
        $item = array();
        if (isset($isDrum) && $isDrum > 0) {
            if ($errCode == 0) {
                if (strlen($jmlDrum) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($jmlDrum <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Drum harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($literPerDrum) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($literPerDrum <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Drum harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                $jmlLiterBefore = ($jmlDrum * $literPerDrum);
                $jmlLiter = ($jmlLiterBefore + $penambahanLiter - $penyusutanLiter);

                $itemOli = array(
                    'id_item' => 1,
                    'item_name' => 'Oli',
                    'qty_basic_real' => $literPerDrum,
                    'qty_basic' => $literPerDrum,
                    'multiple' => $jmlDrum,
                    'qty_before' => $jmlLiterBefore,
                    'additional' => $penambahanLiter,
                    'depreciation' => $penyusutanLiter,
                    'weight' => $weight,
                    'specific_gravity' => $specificGravity,
                    'water_content' => $waterContent,
                    'qty' => $jmlLiter,
                    'description' => $descriptionOli,
                );
                $itemDrum = array(
                    'id_item' => 2,
                    'item_name' => 'Drum',
                    'qty_basic_real' => $jmlDrum,
                    'qty_basic' => $jmlDrum,
                    'multiple' => 1,
                    'qty_before' => $jmlDrum,
                    'additional' => $penambahanDrum,
                    'depreciation' => 0,
                    'weight' => 0,
                    'specific_gravity' => 0,
                    'water_content' => 0,
                    'qty' => $jmlDrum + $penambahanDrum,
                    'description' => $descriptionDrum,
                );
                $item[0] = $itemOli;
                $item[1] = $itemDrum;
            }
        } else {
//            if ($errCode == 0) {
//                if ($weight > 0 || $specificGravity > 0) {
//                    if ($weight <= 0) {
//                        $errCode++;
//                        $errMessage = "Berat Oli harus lebih dari 0.";
//                    }
//                    if ($specificGravity <= 0) {
//                        $errCode++;
//                        $errMessage = "Berat Jenis Oli harus lebih dari 0.";
//                    }
//                }
//            }
            if ($errCode == 0) {
                if (strlen($jmlKemasan) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Kemasan wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($jmlKemasan <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Kemasan harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($literPerKemasan) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Kemasan wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($literPerKemasan <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Kemasan harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                $literPerKemasanReal = $literPerKemasan;
                if ($weight > 0 && $specificGravity > 0) {
                    $literPerKemasanReal = $weight / $specificGravity;
                }
                if (is_float($literPerKemasanReal)) {
                    $literPerKemasanReal = number_format($literPerKemasanReal, 2, ".", "");
                }
                $jmlLiterBefore = ($jmlKemasan * $literPerKemasan);
                $jmlLiter = ($jmlLiterBefore + $penambahanLiter - $penyusutanLiter);

                $itemOli = array(
                    'id_item' => 1,
                    'item_name' => 'Oli',
                    'qty_basic_real' => $literPerKemasanReal,
                    'qty_basic' => $literPerKemasan,
                    'multiple' => $jmlKemasan,
                    'qty_before' => $jmlLiterBefore,
                    'additional' => $penambahanLiter,
                    'depreciation' => $penyusutanLiter,
                    'weight' => $weight,
                    'specific_gravity' => $specificGravity,
                    'water_content' => $waterContent,
                    'qty' => $jmlLiter,
                    'description' => $descriptionOli,
                );
                $item[0] = $itemOli;
                if (isset($isAdditionalDrum) && $isAdditionalDrum > 0) {
                    $itemDrum = array(
                        'id_item' => 2,
                        'item_name' => 'Drum',
                        'qty_basic_real' => 0,
                        'qty_basic' => 0,
                        'multiple' => 1,
                        'qty_before' => 0,
                        'additional' => $penambahanDrum,
                        'depreciation' => 0,
                        'weight' => 0,
                        'specific_gravity' => 0,
                        'water_content' => 0,
                        'qty' => $penambahanDrum,
                        'description' => $descriptionDrum,
                    );
                    $item[1] = $itemDrum;
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($penambahanLiter) == 0) {
                $errCode++;
                $errMessage = "Penambahan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($penyusutanLiter) == 0) {
                $errCode++;
                $errMessage = "Penyusutan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlLiter <= 0) {
                $errCode++;
                $errMessage = "Jumlah Liter harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $checkStockItem = $this->checkStockItem(0, $jmlLiter, $jmlDrum);
            if ($checkStockItem['is_ready'] == false) {
                $errCode++;
                $errMessage = $checkStockItem['message'];
            }
        }
        if ($errCode == 0) {
            try {
                $strItem = "";
                foreach ($item as $key => $value) {
                    if ($value['item_name'] != NULL) {
                        if (strlen($strItem) > 0)
                            $strItem .= ",";
                        $strItem .= $value['id_item'];
                    }
                }
                $data = array(
                    'id_branch' => $this->branch,
                    'id_customer' => $idCustomer,
                    'date' => date('Y-m-d', strtotime($dateSale)),
                    'code' => $code,
                    'id_item_exp' => $strItem,
                    'description' => $description,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_transaction_sales', $data);
                $idTransactionSales = $this->db->insert_id();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if (strlen($idTransactionSales) > 0) {
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";
                    if ($err == 0) {
                        try {
                            $dataDetail = array(
                                'id_transaction_sales' => $idTransactionSales,
                                'id_item' => $value['id_item'],
                                'date' => date('Y-m-d', strtotime($dateSale)),
                                'qty_basic_real' => $value['qty_basic_real'],
                                'qty_basic' => $value['qty_basic'],
                                'multiple' => $value['multiple'],
                                'qty_before' => $value['qty_before'],
                                'additional' => $value['additional'],
                                'depreciation' => $value['depreciation'],
                                'weight' => $value['weight'],
                                'specific_gravity' => $value['specific_gravity'],
                                'water_content' => $value['water_content'],
                                'qty' => $value['qty'],
                                'description' => $value['description'],
                                'created_by' => $username,
                                'created_date' => $date,
                                'updated_by' => $username,
                                'updated_date' => $date,
                            );
                            $this->db->insert('tbl_transaction_sales_detail', $dataDetail);
                            $idTransactionSalesDetail = $this->db->insert_id();
                        } catch (Exception $ex) {
                            $err++;
                            $msg = $ex->getMessage();
                            break;
                        }
                    }
                    if ($err == 0) {
                        $isok1 = $this->M_item->balance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail), 'out', 'TRANSACTION SALE ' . $code, 'Transaksi Penjualan ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                        $err = $isok1['errCode'];
                        $msg = $isok1['errMessage'];
                    }

                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesAddCustom()
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $item = array();

        $idCustomer = $this->input->post("id_customer");
        $dateSale = date('d-m-Y');
        if ($privilegeId == 1) {
            $dateSale = $this->input->post("date");
        }
        $jmlDrum = $this->input->post("jml_drum");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextSale();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('add', 'penjualan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idCustomer) == 0) {
                $errCode++;
                $errMessage = "Customer wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            if ($dataCustomer == null) {
                $errCode++;
                $errMessage = "Customer tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateSale) == 0) {
                $errCode++;
                $errMessage = "Tanggal pembelian harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jmlDrum) == 0) {
                $errCode++;
                $errMessage = "Jumlah Drum wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlDrum <= 0) {
                $errCode++;
                $errMessage = "Jumlah Drum harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $itemDrum = array(
                'id_item' => 2,
                'item_name' => 'Drum',
                'qty_basic_real' => $jmlDrum,
                'qty_basic' => $jmlDrum,
                'multiple' => 1,
                'qty_before' => $jmlDrum,
                'additional' => 0,
                'depreciation' => 0,
                'weight' => 0,
                'specific_gravity' => 0,
                'water_content' => 0,
                'qty' => $jmlDrum,
                'description' => $description,
            );
            $item[0] = $itemDrum;
        }

        if ($errCode == 0) {
            $checkStockItem = $this->checkStockItem(0, 0, $jmlDrum);
            if ($checkStockItem['is_ready'] == false) {
                $errCode++;
                $errMessage = $checkStockItem['message'];
            }
        }
        if ($errCode == 0) {
            try {
                $strItem = "";
                foreach ($item as $key => $value) {
                    if ($value['item_name'] != NULL) {
                        if (strlen($strItem) > 0)
                            $strItem .= ",";
                        $strItem .= $value['id_item'];
                    }
                }
                $data = array(
                    'id_branch' => $this->branch,
                    'id_customer' => $idCustomer,
                    'date' => date('Y-m-d', strtotime($dateSale)),
                    'code' => $code,
                    'id_item_exp' => $strItem,
                    'description' => $description,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_transaction_sales', $data);
                $idTransactionSales = $this->db->insert_id();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if (strlen($idTransactionSales) > 0) {
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";
                    if ($err == 0) {
                        try {
                            $dataDetail = array(
                                'id_transaction_sales' => $idTransactionSales,
                                'id_item' => $value['id_item'],
                                'date' => date('Y-m-d', strtotime($dateSale)),
                                'qty_basic_real' => $value['qty_basic_real'],
                                'qty_basic' => $value['qty_basic'],
                                'multiple' => $value['multiple'],
                                'qty_before' => $value['qty_before'],
                                'additional' => $value['additional'],
                                'depreciation' => $value['depreciation'],
                                'weight' => $value['weight'],
                                'specific_gravity' => $value['specific_gravity'],
                                'water_content' => $value['water_content'],
                                'qty' => $value['qty'],
                                'description' => $value['description'],
                                'created_by' => $username,
                                'created_date' => $date,
                                'updated_by' => $username,
                                'updated_date' => $date,
                            );
                            $this->db->insert('tbl_transaction_sales_detail', $dataDetail);
                            $idTransactionSalesDetail = $this->db->insert_id();
                        } catch (Exception $ex) {
                            $err++;
                            $msg = $ex->getMessage();
                            break;
                        }
                    }
                    if ($err == 0) {
                        $isok1 = $this->M_item->balance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail), 'out', 'TRANSACTION SALE ' . $code, 'Transaksi Penjualan ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                        $err = $isok1['errCode'];
                        $msg = $isok1['errMessage'];
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('edit', 'penjualan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penjualan";
        $data['judul'] = "Ubah Penjualan";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataSale = $this->M_sale->selectById($id);
            if ($dataSale != null) {
                if ($dataSale->is_approved == 0) {
                    $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);

                    $isCheckedDrum = 0;
                    $isCheckedAdditionalDrum = 0;
                    $jmlKemasan = 0;
                    $literPerKemasan = 0;
                    $jmlDrum = 0;
                    $literPerDrum = 0;
                    $penambahanDrum = 0;
                    $penambahanLiter = 0;
                    $penyusutanLiter = 0;
                    $grandTotalLiter = 0;
                    $weight = 0;
                    $specificGravity = 0;
                    $waterContent = 0;
                    $jmlLiter = 0;
                    $descriptionDrum = "";
                    $descriptionOli = "";

                    foreach ($dataSaleDetail as $key => $value) {
                        if ($value->id_item == 1) {
                            $jmlKemasan = $value->multiple;
                            $literPerKemasan = $value->qty_basic;
                            $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                            $literPerDrum = $value->qty_before;
                            $grandTotalLiter = $value->qty;
                            $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                            $penambahanLiter = $value->additional;
                            $penyusutanLiter = $value->depreciation;
                            $weight = $value->weight;
                            $weight = str_replace(".", ",", $weight);
                            $specificGravity = $value->specific_gravity;
                            $specificGravity = str_replace(".", ",", $specificGravity);
                            $waterContent = $value->water_content;
                            $waterContent = str_replace(".", ",", $waterContent);
                            $jmlLiter = $value->qty;
                            $descriptionOli = $value->description;
                        }
                        if ($value->id_item == 2) {
                            $jmlDrum = $value->qty_before;
                            $penambahanDrum = $value->additional;
                            $descriptionDrum = $value->description;
                            if ($penambahanDrum > 0) {
                                $isCheckedAdditionalDrum = 1;
                            }
                            if ($jmlDrum > 0) {
                                $isCheckedDrum = 1;
                            }
                        }
                    }
                    if ($jmlDrum > 0) {
                        $literPerDrum = $literPerDrum / $jmlDrum;
                    }

                    $idCustomer = $dataSale->id_customer;
                    $dataCustomer = $this->M_customer->selectById($idCustomer);
                    $namaCustomer = $dataCustomer->name;

                    $data['menuName'] = "penjualan";
                    $data['dataSale'] = $dataSale;
                    $data['namaCustomer'] = $namaCustomer;
                    $data['idTransactionSales'] = $id;
                    $data['isCheckedDrum'] = $isCheckedDrum;
                    $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
                    $data['jmlKemasan'] = $jmlKemasan;
                    $data['literPerKemasan'] = $literPerKemasan;
                    $data['jmlDrum'] = $jmlDrum;
                    $data['literPerDrum'] = $literPerDrum;
                    $data['penambahanDrum'] = $penambahanDrum;
                    $data['penambahanLiter'] = $penambahanLiter;
                    $data['penyusutanLiter'] = $penyusutanLiter;
                    $data['grandTotalLiter'] = $grandTotalLiter;
                    $data['weight'] = $weight;
                    $data['specificGravity'] = $specificGravity;
                    $data['waterContent'] = $waterContent;
                    $data['jmlLiter'] = $jmlLiter;
                    $data['descriptionDrum'] = $descriptionDrum;
                    $data['descriptionOli'] = $descriptionOli;
                    $data['privilegeId'] = $privilegeId;
                    $data['customer'] = $this->M_customer->selectItem();

                    $isDrumOnly = 0;
                    $idItemExp = $dataSale->id_item_exp;
                    if (strlen($idItemExp) > 0) {
                        $arrItem = explode(',', $idItemExp);
                        if (count($arrItem) == 1) {
                            if ($arrItem[0] == 2) {
                                $isDrumOnly = 1;
                            }
                        }
                    }

                    if ($isDrumOnly > 0) {
                        $this->loadkonten('v_sale/update_custom', $data);
                    } else {
                        $this->loadkonten('v_sale/update', $data);
                    }
                } else {
                    echo "<script>alert('Penjualan sudah di approve.'); window.location = '" . base_url("penjualan") . "';</script>";
                }
            } else {
                echo "<script>alert('Penjualan tidak valid.'); window.location = '" . base_url("penjualan") . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idCustomer = $this->input->post("id_customer");
        $dateSale = $this->input->post("date");
        $weight = $this->input->post("weight");
        $weight = str_replace(",", ".", $weight);
        $specificGravity = $this->input->post("specific_gravity");
        $specificGravity = str_replace(",", ".", $specificGravity);
        $waterContent = $this->input->post("water_content");
        $waterContent = str_replace(",", ".", $waterContent);
        $isDrum = $this->input->post("is_drum");
        $isAdditionalDrum = $this->input->post("is_additional_drum");
        $jmlDrum = $this->input->post("jml_drum");
        $literPerDrum = $this->input->post("liter_per_drum");
        $jmlKemasan = $this->input->post("jml_kemasan");
        $literPerKemasan = $this->input->post("liter_per_kemasan");
        $literPerKemasan = str_replace(",", ".", $literPerKemasan);
        $penambahanDrum = $this->input->post("penambahan_drum");
        $penambahanLiter = $this->input->post("penambahan_liter");
        $penyusutanLiter = $this->input->post("penyusutan_liter");
        $descriptionDrum = $this->input->post("description_drum");
        $descriptionOli = $this->input->post("description_oli");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('edit', 'penjualan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $dataSale = $this->M_sale->selectById($id);
            if ($dataSale == NULL) {
                $errCode++;
                $errMessage = "Penjualan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idCustomer) == 0) {
                $errCode++;
                $errMessage = "Customer wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            if ($dataCustomer == null) {
                $errCode++;
                $errMessage = "Customer tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($privilegeId != 1) {
                $dateSale = $dataSale->date;
                if (strlen($dateSale) == 0) {
                    $dateSale = date('d-m-Y');
                }
            }
            if (strlen($dateSale) == 0) {
                $errCode++;
                $errMessage = "Tanggal penjualan harus di isi.";
            }
        }
        if (isset($isAdditionalDrum) && $isAdditionalDrum > 0) {
            if ($errCode == 0) {
                if (strlen($penambahanDrum) == 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($penambahanDrum <= 0) {
                    $errCode++;
                    $errMessage = "Penambahan Jumlah Drum harus lebih dari 0.";
                }
            }
        } else {
            $penambahanDrum = 0;
        }

        $item = array();
        if (isset($isDrum) && $isDrum > 0) {
            if ($errCode == 0) {
                if (strlen($jmlDrum) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($jmlDrum <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Drum harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($literPerDrum) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Drum wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($literPerDrum <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Drum harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                $jmlLiterBefore = ($jmlDrum * $literPerDrum);
                $jmlLiter = ($jmlLiterBefore + $penambahanLiter - $penyusutanLiter);

                $itemOli = array(
                    'id_item' => 1,
                    'item_name' => 'Oli',
                    'qty_basic_real' => $literPerDrum,
                    'qty_basic' => $literPerDrum,
                    'multiple' => $jmlDrum,
                    'qty_before' => $jmlLiterBefore,
                    'additional' => $penambahanLiter,
                    'depreciation' => $penyusutanLiter,
                    'weight' => $weight,
                    'specific_gravity' => $specificGravity,
                    'water_content' => $waterContent,
                    'qty' => $jmlLiter,
                    'description' => $descriptionOli,
                );
                $itemDrum = array(
                    'id_item' => 2,
                    'item_name' => 'Drum',
                    'qty_basic_real' => $jmlDrum,
                    'qty_basic' => $jmlDrum,
                    'multiple' => 1,
                    'qty_before' => $jmlDrum,
                    'additional' => $penambahanDrum,
                    'depreciation' => 0,
                    'weight' => 0,
                    'specific_gravity' => 0,
                    'water_content' => 0,
                    'qty' => $jmlDrum + $penambahanDrum,
                    'description' => $descriptionDrum,
                );
                $item[0] = $itemOli;
                $item[1] = $itemDrum;
            }
        } else {
//            if ($errCode == 0) {
//                if ($weight > 0 || $specificGravity > 0) {
//                    if ($weight <= 0) {
//                        $errCode++;
//                        $errMessage = "Berat Oli harus lebih dari 0.";
//                    }
//                    if ($specificGravity <= 0) {
//                        $errCode++;
//                        $errMessage = "Berat Jenis Oli harus lebih dari 0.";
//                    }
//                }
//            }
            if ($errCode == 0) {
                if (strlen($jmlKemasan) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Kemasan wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($jmlKemasan <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Kemasan harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($literPerKemasan) == 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Kemasan wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($literPerKemasan <= 0) {
                    $errCode++;
                    $errMessage = "Jumlah Liter per Kemasan harus lebih dari 0.";
                }
            }
            if ($errCode == 0) {
                $literPerKemasanReal = $literPerKemasan;
                if ($weight > 0 && $specificGravity > 0) {
                    $literPerKemasanReal = $weight / $specificGravity;
                }
                if (is_float($literPerKemasanReal)) {
                    $literPerKemasanReal = number_format($literPerKemasanReal, 2, ".", "");
                }
                $jmlLiterBefore = ($jmlKemasan * $literPerKemasan);
                $jmlLiter = ($jmlLiterBefore + $penambahanLiter - $penyusutanLiter);

                $itemOli = array(
                    'id_item' => 1,
                    'item_name' => 'Oli',
                    'qty_basic_real' => $literPerKemasanReal,
                    'qty_basic' => $literPerKemasan,
                    'multiple' => $jmlKemasan,
                    'qty_before' => $jmlLiterBefore,
                    'additional' => $penambahanLiter,
                    'depreciation' => $penyusutanLiter,
                    'weight' => $weight,
                    'specific_gravity' => $specificGravity,
                    'water_content' => $waterContent,
                    'qty' => $jmlLiter,
                    'description' => $descriptionOli,
                );
                if (isset($isAdditionalDrum) && $isAdditionalDrum > 0) {
                    $itemDrum = array(
                        'id_item' => 2,
                        'item_name' => 'Drum',
                        'qty_basic_real' => 0,
                        'qty_basic' => 0,
                        'multiple' => 1,
                        'qty_before' => 0,
                        'additional' => $penambahanDrum,
                        'depreciation' => 0,
                        'weight' => 0,
                        'specific_gravity' => 0,
                        'water_content' => 0,
                        'qty' => $penambahanDrum,
                        'description' => $descriptionDrum,
                    );
                } else {
                    $itemDrum = array(
                        'id_item' => 2,
                        'item_name' => NULL,
                        'qty_basic' => NULL,
                        'multiple' => NULL,
                        'qty_before' => NULL,
                        'additional' => NULL,
                        'depreciation' => NULL,
                        'weight' => NULL,
                        'specific_gravity' => NULL,
                        'water_content' => NULL,
                        'qty' => NULL,
                        'description' => NULL,
                    );
                }
                $item[0] = $itemOli;
                $item[1] = $itemDrum;
            }
        }
        if ($errCode == 0) {
            if (strlen($penambahanLiter) == 0) {
                $errCode++;
                $errMessage = "Penambahan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($penyusutanLiter) == 0) {
                $errCode++;
                $errMessage = "Penyusutan Liter wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlLiter <= 0) {
                $errCode++;
                $errMessage = "Jumlah Liter harus lebih dari 0.";
            }
        }

        //Untuk cek apakah update tanpa ubah inputan
        if ($errCode == 0) {
            $isEdit = FALSE;

            $isCheckedDrum = 0;
            if (isset($isDrum) && $isDrum > 0) {
                $isCheckedDrum = 1;
            }
            $isCheckedAdditionalDrum = 0;
            if (isset($isAdditionalDrum) && $isAdditionalDrum > 0) {
                $isCheckedAdditionalDrum = 1;
            }
            $isCheckedDrumOld = 0;
            $isCheckedAdditionalDrumOld = 0;

            $jmlKemasanOld = 0;
            $literPerKemasanOld = 0;
            $jmlDrumOld = 0;
            $literPerDrumOld = 0;
            $penambahanDrumOld = 0;
            $penambahanLiterOld = 0;
            $penyusutanLiterOld = 0;
            $weightOld = 0;
            $specificGravityOld = 0;
            $waterContentOld = 0;
            $jmlLiterOld = 0;

            if ($isEdit == FALSE) {
                if ($idCustomer != $dataSale->id_customer) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if (date('Y-m-d', strtotime($dateSale)) != $dataSale->date) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($description != $dataSale->description) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);
                foreach ($dataSaleDetail as $key => $value) {
                    if ($value->id_item == 1) {
                        $jmlKemasanOld = $value->multiple;
                        $literPerKemasanOld = $value->qty_basic;
                        $literPerDrumOld = $value->qty_before;
                        $penambahanLiterOld = $value->additional;
                        $penyusutanLiterOld = $value->depreciation;
                        $weightOld = $value->weight;
                        $specificGravityOld = $value->specific_gravity;
                        $waterContentOld = $value->water_content;
                        $jmlLiterOld = $value->qty;
                        $descriptionOliOld = $value->description;
                    }
                    if ($value->id_item == 2) {
                        $jmlDrumOld = $value->qty_before;
                        $penambahanDrumOld = $value->additional;
                        if ($penambahanDrumOld > 0) {
                            $isCheckedAdditionalDrumOld = 1;
                        }
                        if ($jmlDrumOld > 0) {
                            $isCheckedDrumOld = 1;
                        }
                        $descriptionDrumOld = $value->description;
                    }
                }
                if ($jmlDrumOld > 0) {
                    $literPerDrumOld = $literPerDrumOld / $jmlDrumOld;
                }
                if (isset($isDrum) && $isDrum > 0) {
                    $isCheckedDrum = 1;
                }
            }
            if ($isEdit == FALSE) {
                if ($weight != $weightOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($specificGravity != $specificGravityOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($waterContent != $waterContentOld) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($isCheckedDrum == $isCheckedDrumOld) {
                    if ($isCheckedDrum == 0) {
                        if ($jmlKemasanOld != $jmlKemasan ||
                            $literPerKemasanOld != $literPerKemasan ||
                            $descriptionOli != $descriptionOliOld) {
                            $isEdit = TRUE;
                        }
                    } else {
                        if ($jmlDrumOld != $jmlDrum ||
                            $literPerDrumOld != $literPerDrum ||
                            $descriptionOli != $descriptionOliOld ||
                            $descriptionDrum != $descriptionDrumOld) {
                            $isEdit = TRUE;
                        }
                    }
                } else {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($penambahanLiterOld != $penambahanLiter || $penyusutanLiterOld != $penyusutanLiter) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($isCheckedAdditionalDrum == $isCheckedAdditionalDrumOld) {
                    if ($isCheckedAdditionalDrum > 0) {
                        if ($penambahanDrumOld != $penambahanDrum) {
                            $isEdit = TRUE;
                        }
                    }
                } else {
                    $isEdit = TRUE;
                }
            }
        }
        //End Checking
        if ($isEdit == TRUE) {
            if ($errCode == 0) {
                try {
                    $strItem = "";
                    foreach ($item as $key => $value) {
                        if ($value['item_name'] != NULL) {
                            if (strlen($strItem) > 0)
                                $strItem .= ",";
                            $strItem .= $value['id_item'];
                        }
                    }
                    $dataUpdate = array(
                        'id_customer' => $idCustomer,
                        'date' => date('Y-m-d', strtotime($dateSale)),
                        'id_item_exp' => $strItem,
                        'description' => $description,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->update('tbl_transaction_sales', $dataUpdate, array('id_transaction_sales' => $id));
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            if ($errCode == 0) {
                $code = $dataSale->code;
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";

                    $getDetail = $this->M_sale->selectDetailByIdItem($id, $value['id_item']);
                    $idTransactionSalesDetail = $getDetail->id_transaction_sales_detail;
                    $idItem = $getDetail->id_item;
                    $getItem = $this->M_item->selectById($idItem);
                    if (strlen($idTransactionSalesDetail) > 0) {
                        if ($value['item_name'] != NULL) {
                            if ($err == 0) {
                                try {
                                    $dataDetail = array(
                                        'date' => date('Y-m-d', strtotime($dateSale)),
                                        'qty_basic_real' => $value['qty_basic_real'],
                                        'qty_basic' => $value['qty_basic'],
                                        'multiple' => $value['multiple'],
                                        'qty_before' => $value['qty_before'],
                                        'additional' => $value['additional'],
                                        'depreciation' => $value['depreciation'],
                                        'weight' => $value['weight'],
                                        'specific_gravity' => $value['specific_gravity'],
                                        'water_content' => $value['water_content'],
                                        'qty' => $value['qty'],
                                        'description' => $value['description'],
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->update('tbl_transaction_sales_detail', $dataDetail, array('id_transaction_sales_detail' => $idTransactionSalesDetail));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                            if ($err == 0) {
                                $isok1 = $this->M_item->updateBalance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail), 'out', 'TRANSACTION SALE ' . $code, 'Transaksi Penjualan ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                                $err = $isok1['errCode'];
                                $msg = $isok1['errMessage'];
                            }
                        } else {
                            if ($err == 0) {
                                $isok1 = $this->M_item->deleteBalance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail));
                                $err = $isok1['errCode'];
                                $msg = $isok1['errMessage'];
                            }
                            if ($err == 0) {
                                try {
                                    $this->db->delete('tbl_transaction_sales_detail', array('id_transaction_sales_detail' => $idTransactionSalesDetail));
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                }
                            }
                        }
                    } else {
                        if ($value['item_name'] != NULL) {
                            if ($err == 0) {
                                try {
                                    $dataDetail = array(
                                        'id_transaction_sales' => $id,
                                        'id_item' => $value['id_item'],
                                        'date' => date('Y-m-d', strtotime($dateSale)),
                                        'qty_basic_real' => $value['qty_basic_real'],
                                        'qty_basic' => $value['qty_basic'],
                                        'multiple' => $value['multiple'],
                                        'qty_before' => $value['qty_before'],
                                        'additional' => $value['additional'],
                                        'depreciation' => $value['depreciation'],
                                        'weight' => $value['weight'],
                                        'specific_gravity' => $value['specific_gravity'],
                                        'water_content' => $value['water_content'],
                                        'qty' => $value['qty'],
                                        'description' => $value['description'],
                                        'created_by' => $dataSale->created_by,
                                        'created_date' => $dataSale->created_date,
                                        'updated_by' => $username,
                                        'updated_date' => $date,
                                    );
                                    $this->db->insert('tbl_transaction_sales_detail', $dataDetail);
                                    $idTransactionSalesDetail = $this->db->insert_id();
                                } catch (Exception $ex) {
                                    $err++;
                                    $msg = $ex->getMessage();
                                    break;
                                }
                            }
                            if ($err == 0) {
                                $isok1 = $this->M_item->balance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail), 'out', 'TRANSACTION SALE ' . $code, 'Transaksi Penjualan ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                                $err = $isok1['errCode'];
                                $msg = $isok1['errMessage'];
                            }
                        }
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesUpdateCustom($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $item = array();

        $idCustomer = $this->input->post("id_customer");
        $dateSale = $this->input->post("date");
        $jmlDrum = $this->input->post("jml_drum");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('edit', 'penjualan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $dataSale = $this->M_sale->selectById($id);
            if ($dataSale == NULL) {
                $errCode++;
                $errMessage = "Penjualan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idCustomer) == 0) {
                $errCode++;
                $errMessage = "Customer wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            if ($dataCustomer == null) {
                $errCode++;
                $errMessage = "Customer tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($privilegeId != 1) {
                $dateSale = $dataSale->date;
                if (strlen($dateSale) == 0) {
                    $dateSale = date('d-m-Y');
                }
            }
            if (strlen($dateSale) == 0) {
                $errCode++;
                $errMessage = "Tanggal penjualan harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jmlDrum) == 0) {
                $errCode++;
                $errMessage = "Jumlah Drum wajib diisi.";
            }
        }
        if ($errCode == 0) {
            if ($jmlDrum <= 0) {
                $errCode++;
                $errMessage = "Jumlah Drum harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $itemDrum = array(
                'id_item' => 2,
                'item_name' => 'Drum',
                'qty_basic_real' => $jmlDrum,
                'qty_basic' => $jmlDrum,
                'multiple' => 1,
                'qty_before' => $jmlDrum,
                'additional' => 0,
                'depreciation' => 0,
                'weight' => 0,
                'specific_gravity' => 0,
                'water_content' => 0,
                'qty' => $jmlDrum,
                'description' => $description,
            );
            $item[0] = $itemDrum;
        }

        //Untuk cek apakah update tanpa ubah inputan
        if ($errCode == 0) {
            $isEdit = FALSE;
            $jmlDrumOld = 0;
            if ($isEdit == FALSE) {
                if ($idCustomer != $dataSale->id_customer) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if (date('Y-m-d', strtotime($dateSale)) != $dataSale->date) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                if ($description != $dataSale->description) {
                    $isEdit = TRUE;
                }
            }
            if ($isEdit == FALSE) {
                $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);
                foreach ($dataSaleDetail as $key => $value) {
                    if ($value->id_item == 2) {
                        $jmlDrumOld = $value->qty_before;
                    }
                }
            }
            if ($isEdit == FALSE) {
                if ($jmlDrum != $jmlDrumOld) {
                    $isEdit = TRUE;
                }
            }
        }
        //End Checking

        if ($isEdit == TRUE) {
            if ($errCode == 0) {
                if ($jmlDrum > $jmlDrumOld) {
                    $checkStockItem = $this->checkStockItem(0, 0, ($jmlDrum - $jmlDrumOld));
                    if ($checkStockItem['is_ready'] == false) {
                        $errCode++;
                        $errMessage = $checkStockItem['message'];
                    }
                }
            }
            if ($errCode == 0) {
                try {
                    $dataUpdate = array(
                        'id_customer' => $idCustomer,
                        'date' => date('Y-m-d', strtotime($dateSale)),
                        'description' => $description,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->update('tbl_transaction_sales', $dataUpdate, array('id_transaction_sales' => $id));
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            if ($errCode == 0) {
                $code = $dataSale->code;
                foreach ($item as $key => $value) {
                    $err = 0;
                    $msg = "";

                    $getDetail = $this->M_sale->selectDetailByIdItem($id, $value['id_item']);
                    $idTransactionSalesDetail = $getDetail->id_transaction_sales_detail;
                    if (strlen($idTransactionSalesDetail) > 0) {
                        if ($err == 0) {
                            try {
                                $dataDetail = array(
                                    'date' => date('Y-m-d', strtotime($dateSale)),
                                    'qty_basic_real' => $value['qty_basic_real'],
                                    'qty_basic' => $value['qty_basic'],
                                    'multiple' => $value['multiple'],
                                    'qty_before' => $value['qty_before'],
                                    'additional' => $value['additional'],
                                    'depreciation' => $value['depreciation'],
                                    'weight' => $value['weight'],
                                    'specific_gravity' => $value['specific_gravity'],
                                    'water_content' => $value['water_content'],
                                    'qty' => $value['qty'],
                                    'description' => $value['description'],
                                    'updated_by' => $username,
                                    'updated_date' => $date,
                                );
                                $this->db->update('tbl_transaction_sales_detail', $dataDetail, array('id_transaction_sales_detail' => $idTransactionSalesDetail));
                            } catch (Exception $ex) {
                                $err++;
                                $msg = $ex->getMessage();
                                break;
                            }
                        }
                        if ($err == 0) {
                            $isok1 = $this->M_item->updateBalance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail), 'out', 'TRANSACTION SALE ' . $code, 'Transaksi Penjualan ' . $code . ' item ' . $value['item_name'] . ' sejumlah ' . number_format($value['qty'], 0, ",", "."));
                            $err = $isok1['errCode'];
                            $msg = $isok1['errMessage'];
                        }
                        if ($err > 0) {
                            $errCode++;
                            $errMessage = $msg;
                            break;
                        }
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Confirm($id)
    {
        self::EditConfirm($id);
    }

    public function EditConfirm($id)
    {
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('conf', 'penjualan');
        if ($access->menuview == 0) {
            $data['page'] = "Penjualan";
            $data['judul'] = "Penjualan";
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataSale = $this->M_sale->selectById($id);
            if ($dataSale != null) {
                if ($dataSale->is_approved == 0) {
                    $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);

                    $isPpn = 0;
                    $ppn = self::__ppn;
                    $judul = "Konfirmasi Penjualan";
                    IF ($dataSale->nominal > 0) {
                        $judul = "Ubah Konfirmasi Penjualan";
                        $ppn = $dataSale->ppn;
                        if ($ppn > 0) {
                            $isPpn = 1;
                        } else {
                            $ppn = self::__ppn;
                        }
                    }
                    $type = "liter";
                    $isCheckedDrum = 0;
                    $isCheckedAdditionalDrum = 0;
                    $jmlKemasan = 0;
                    $literPerKemasan = 0;
                    $jmlDrum = 0;
                    $literPerDrum = 0;
                    $penambahanDrum = 0;
                    $penambahanLiter = 0;
                    $penyusutanLiter = 0;
                    $grandTotalLiter = 0;
                    $weight = 0;
                    $specificGravity = 0;
                    $waterContent = 0;
                    $jmlLiter = 0;
                    $descriptionDrum = "";
                    $descriptionOli = "";
                    $hargaPerLiter = 0;
                    $hargaPerKilogram = 0;
                    $hargaPerDrum = 0;

                    foreach ($dataSaleDetail as $key => $value) {
                        if ($value->id_item == 1) {
                            if (strlen($value->type) > 0) {
                                $type = $value->type;
                            }
                            $jmlKemasan = $value->multiple;
                            $literPerKemasan = $value->qty_basic;
                            $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                            $literPerDrum = $value->qty_before;
                            $grandTotalLiter = $value->qty;
                            $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                            $penambahanLiter = $value->additional;
                            $penyusutanLiter = $value->depreciation;
                            $weight = $value->weight;
                            $weight = str_replace(".", ",", $weight);
                            $specificGravity = $value->specific_gravity;
                            $specificGravity = str_replace(".", ",", $specificGravity);
                            $waterContent = $value->water_content;
                            $waterContent = str_replace(".", ",", $waterContent);
                            $jmlLiter = $value->qty;
                            $descriptionOli = $value->description;
                            $hargaPerLiter = $value->total;
                            $hargaPerLiter = str_replace(".", ",", $hargaPerLiter);
                            $hargaPerKilogram = $value->total;
                            $hargaPerKilogram = str_replace(".", ",", $hargaPerKilogram);
                        }
                        if ($value->id_item == 2) {
                            $jmlDrum = $value->qty_before;
                            $penambahanDrum = $value->additional;
                            $descriptionDrum = $value->description;
                            $hargaPerDrum = $value->total;
                            $hargaPerDrum = str_replace(".", ",", $hargaPerDrum);
                            if ($penambahanDrum > 0) {
                                $isCheckedAdditionalDrum = 1;
                            }
                            if ($jmlDrum > 0) {
                                $isCheckedDrum = 1;
                            }
                        }
                    }
                    if ($jmlDrum > 0) {
                        $literPerDrum = $literPerDrum / $jmlDrum;
                    }

                    $idCustomer = $dataSale->id_customer;
                    $dataCustomer = $this->M_customer->selectById($idCustomer);
                    $namaCustomer = $dataCustomer->name;

                    $data['menuName'] = "penjualan";
                    $data['page'] = "Penjualan";
                    $data['judul'] = $judul;
                    $data['dataSale'] = $dataSale;
                    $data['namaCustomer'] = $namaCustomer;
                    $data['idTransactionSales'] = $id;
                    $data['isCheckedDrum'] = $isCheckedDrum;
                    $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
                    $data['jmlKemasan'] = $jmlKemasan;
                    $data['literPerKemasan'] = $literPerKemasan;
                    $data['jmlDrum'] = $jmlDrum;
                    $data['literPerDrum'] = $literPerDrum;
                    $data['penambahanDrum'] = $penambahanDrum;
                    $data['penambahanLiter'] = $penambahanLiter;
                    $data['penyusutanLiter'] = $penyusutanLiter;
                    $data['grandTotalLiter'] = $grandTotalLiter;
                    $data['weight'] = $weight;
                    $data['specificGravity'] = $specificGravity;
                    $data['waterContent'] = $waterContent;
                    $data['jmlLiter'] = $jmlLiter;
                    $data['descriptionDrum'] = $descriptionDrum;
                    $data['descriptionOli'] = $descriptionOli;
                    $data['hargaPerLiter'] = $hargaPerLiter;
                    $data['hargaPerKilogram'] = $hargaPerKilogram;
                    $data['hargaPerDrum'] = $hargaPerDrum;
                    $data['ppn'] = $ppn;
                    $data['isPpn'] = $isPpn;
                    $data['type'] = $type;
                    $data['label'] = $label;

                    $isDrumOnly = 0;
                    $idItemExp = $dataSale->id_item_exp;
                    if (strlen($idItemExp) > 0) {
                        $arrItem = explode(',', $idItemExp);
                        if (count($arrItem) == 1) {
                            if ($arrItem[0] == 2) {
                                $isDrumOnly = 1;
                            }
                        }
                    }

                    if ($isDrumOnly > 0) {
                        $this->loadkonten('v_sale/approve_custom', $data);
                    } else {
                        $this->loadkonten('v_sale/approve', $data);
                    }
                } else {
                    echo "<script>alert('Penjualan sudah di approve.'); window.location = '" . base_url("penjualan") . "';</script>";
                }
            } else {
                echo "<script>alert('Penjualan tidak valid.'); window.location = '" . base_url("penjualan") . "';</script>";
            }
        }
    }

    public function prosesConfirm($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $grandTotal = 0;
        $totalHargaOli = 0;
        $totalHargaDrum = 0;

        $tipeHarga = $this->input->post("tipe_harga");
        $hargaPerDrum = $this->input->post("harga_per_drum");
        $hargaPerDrum = str_replace(".", "", $hargaPerDrum);
        $hargaPerDrum = str_replace(",", "", $hargaPerDrum);
        $hargaPerLiter = $this->input->post("harga_per_liter");
        $hargaPerLiter = str_replace(".", "", $hargaPerLiter);
        $hargaPerLiter = str_replace(",", "", $hargaPerLiter);
        $hargaPerKilogram = $this->input->post("harga_per_kilogram");
        $hargaPerKilogram = str_replace(".", "", $hargaPerKilogram);
        $hargaPerKilogram = str_replace(",", "", $hargaPerKilogram);
        $isPpn = $this->input->post("is_ppn");
        if (!isset($isPpn)) {
            $isPpn = 0;
        }
//        $datePaid = $this->input->post("date_paid");
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'penjualan');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $dataSale = $this->M_sale->selectById($id);
            $code = $dataSale->code;
            if ($dataSale == NULL) {
                $errCode++;
                $errMessage = "Penjualan tidak valid.";
            }
        }
        if ($errCode == 0) {
            $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);
            foreach ($dataSaleDetail as $key => $value) {
                if ($value->id_item == 1) {
                    if ($errCode == 0) {
                        if (strlen($tipeHarga) == 0) {
                            $errCode++;
                            $errMessage = "Tipe Harga wajib diisi.";
                        }
                    }
                    if ($tipeHarga == 'liter') {
                        if ($errCode == 0) {
                            if (strlen($hargaPerLiter) == 0) {
                                $errCode++;
                                $errMessage = "Harga per Liter wajib diisi.";
                            }
                        }
                        if ($errCode == 0) {
                            if ($hargaPerLiter <= 0) {
                                $errCode++;
                                $errMessage = "Harga per Liter harus lebih dari 0.";
                            }
                        }
                        if ($errCode == 0) {
                            $totalHargaOli = $value->qty * $hargaPerLiter;
                        }
                    } else if ($tipeHarga == 'kilogram') {
                        if ($errCode == 0) {
                            if (strlen($hargaPerKilogram) == 0) {
                                $errCode++;
                                $errMessage = "Harga per Kg wajib diisi.";
                            }
                        }
                        if ($errCode == 0) {
                            if ($hargaPerKilogram <= 0) {
                                $errCode++;
                                $errMessage = "Harga per Kg harus lebih dari 0.";
                            }
                        }
                        if ($errCode == 0) {
                            $totalHargaOli = $value->weight * $hargaPerKilogram;
                        }
                    }
                }
                if ($value->id_item == 2) {
                    if ($errCode == 0) {
                        if (strlen($hargaPerDrum) == 0) {
                            $errCode++;
                            $errMessage = "Harga per Drum wajib diisi.";
                        }
                    }
                    if ($errCode == 0) {
                        if ($hargaPerDrum <= 0) {
                            $errCode++;
                            $errMessage = "Harga per Drum harus lebih dari 0.";
                        }
                    }
                    if ($errCode == 0) {
                        $totalHargaDrum = $value->qty * $hargaPerDrum;
                    }
                }
            }
        }
        if ($errCode == 0) {
            $grandTotal = $totalHargaDrum + $totalHargaOli;
            $lastGrandTotal = $grandTotal;
            if ($grandTotal <= 0) {
                $errCode++;
                $errMessage = "Mohon Lengkapi pengisian data anda.";
            }
        }
//        if ($errCode == 0) {
//            if ($datePaid <= 0) {
//                $errCode++;
//                $errMessage = "Tanggal Bayar wajib diisi.";
//            }
//        }
        //Proses
        if ($errCode == 0) {
            foreach ($dataSaleDetail as $key => $value) {
                $idTransactionSalesDetail = $value->id_transaction_sales_detail;
                $dataDetailUpdate = array(
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                if ($value->id_item == 1) {
                    if ($tipeHarga == 'liter') {
                        $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                            "total" => $hargaPerLiter,
                        ));
                    } else if ($tipeHarga == 'kilogram') {
                        $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                            "total" => $hargaPerKilogram,
                        ));
                    }
                    $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                        "type" => $tipeHarga,
                        "grandtotal" => $totalHargaOli,
                    ));
                }
                if ($value->id_item == 2) {
                    $dataDetailUpdate = array_merge($dataDetailUpdate, array(
                        "type" => 'pcs',
                        "total" => $hargaPerDrum,
                        "grandtotal" => $totalHargaDrum,
                    ));
                }
                $this->db->update('tbl_transaction_sales_detail', $dataDetailUpdate, array('id_transaction_sales_detail' => $idTransactionSalesDetail));
            }
        }
        if ($errCode == 0) {
            try {
//                if (strlen($datePaid) == 0) {
//                    $datePaid == null;
//                }
                $ppn = 0;
                if ($isPpn > 0) {
                    $ppn = self::__ppn;
                }
                $grandTotalFinal = $grandTotal + ($grandTotal * $ppn);
                $dataUpdate = array(
                    'price' => $grandTotal,
                    'ppn' => $ppn,
                    'nominal' => $grandTotalFinal,
                    'description' => $description,
//                    'date_paid' => $datePaid,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_sales', $dataUpdate, array('id_transaction_sales' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        //End Proses
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Cetak($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;

        $dataSale = $this->M_sale->selectById($id);
        if ($dataSale != NULL) {
            $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);

            $isCheckedDrum = 0;
            $jmlDrum = 0;
            $literPerDrum = 0;
            $penyusutanLiter = 0;
            $hargaPerDrum = 0;
            $hargaPerLiter = 0;
            $weight = 0;
            $specificGravity = 0;
            $waterContent = 0;
            $jmlLiter = 0;
            $descriptionDrum = "";
            $descriptionOli = "";

            foreach ($dataSaleDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $literPerDrum = $value->qty_before;
                    $literPerDrum = str_replace(".", ",", $literPerDrum);
                    $penyusutanLiter = $value->depreciation;
                    $hargaPerLiter = $value->total;
                    $weight = $value->weight;
                    $weight = str_replace(".", ",", $weight);
                    $specificGravity = $value->specific_gravity;
                    $specificGravity = str_replace(".", ",", $specificGravity);
                    $waterContent = $value->water_content;
                    $waterContent = str_replace(".", ",", $waterContent);
                    $jmlLiter = $value->qty;
                    $descriptionOli = $value->description;
                }
                if ($value->id_item == 2) {
                    $isCheckedDrum = 1;
                    $jmlDrum = $value->qty;
                    $hargaPerDrum = $value->total;
                    $descriptionDrum = $value->description;
                }
            }
            if ($jmlDrum > 0) {
                $literPerDrum = $literPerDrum / $jmlDrum;
            }

            $idCustomer = $dataSale->id_customer;
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            $namaCustomer = $dataCustomer->name;
            $alamatCustomer = $dataCustomer->address;
            $phoneCustomer = $dataCustomer->phone;

            $sql = "SELECT * FROM tbl_branch WHERE id_branch = '{$this->branch}'";
            $branch = $this->db->query($sql)->row();

            $data['menuName'] = "penjualan";
            $data['page'] = "Penjualan";
            $data['judul'] = "Penjualan";
            $data['dataSale'] = $dataSale;
            $data['branch'] = $branch;
            $data['namaCustomer'] = $namaCustomer;
            $data['alamatCustomer'] = $alamatCustomer;
            $data['phoneCustomer'] = $phoneCustomer;
            $data['idTransactionSales'] = $id;
            $data['isCheckedDrum'] = $isCheckedDrum;
            $data['jmlDrum'] = $jmlDrum;
            $data['literPerDrum'] = $literPerDrum;
            $data['penyusutanLiter'] = $penyusutanLiter;
            $data['hargaPerDrum'] = $hargaPerDrum;
            $data['hargaPerLiter'] = $hargaPerLiter;
            $data['weight'] = $weight;
            $data['specificGravity'] = $specificGravity;
            $data['waterContent'] = $waterContent;
            $data['jmlLiter'] = $jmlLiter;
            $data['descriptionDrum'] = $descriptionDrum;
            $data['descriptionOli'] = $descriptionOli;

            $this->load->view('v_sale/surat_jalan', $data);
        } else {
            echo "<script>alert('Penjualan tidak valid.'); window.location = '" . base_url("penjualan") . "';</script>";
        }
    }

    public function Cetak_approve($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;

        $dataSale = $this->M_sale->selectById($id);
        $nominalPenjualan = $dataSale->price;
        if ($dataSale != NULL) {
            $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);

            $isCheckedDrum = 0;
            $jmlDrum = 0;
            $literPerDrum = 0;
            $penyusutanLiter = 0;
            $hargaPerDrum = 0;
            $hargaPerLiter = 0;
            $weight = 0;
            $specificGravity = 0;
            $waterContent = 0;
            $jmlLiter = 0;
            $descriptionDrum = "";
            $descriptionOli = "";

            foreach ($dataSaleDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $literPerDrum = $value->qty_before;
                    $literPerDrum = str_replace(".", ",", $literPerDrum);
                    $penyusutanLiter = $value->depreciation;
                    $hargaPerLiter = $value->total;
                    $weight = $value->weight;
                    $weight = str_replace(".", ",", $weight);
                    $specificGravity = $value->specific_gravity;
                    $specificGravity = str_replace(".", ",", $specificGravity);
                    $waterContent = $value->water_content;
                    $waterContent = str_replace(".", ",", $waterContent);
                    $jmlLiter = $value->qty;
                    $descriptionOli = $value->description;
                }
                if ($value->id_item == 2) {
                    $isCheckedDrum = 1;
                    $jmlDrum = $value->qty;
                    $hargaPerDrum = $value->total;
                    $descriptionDrum = $value->description;
                }
            }
            if ($jmlDrum > 0) {
                $literPerDrum = $literPerDrum / $jmlDrum;
            }


            $idCustomer = $dataSale->id_customer;
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            $namaCustomer = $dataCustomer->name;
            $alamatCustomer = $dataCustomer->address;
            $phoneCustomer = $dataCustomer->phone;

            $sql = "SELECT * FROM tbl_branch WHERE id_branch = '{$this->branch}'";
            $branch = $this->db->query($sql)->row();

            $data['menuName'] = "penjualan";
            $data['page'] = "Penjualan";
            $data['judul'] = "Penjualan";
            $data['dataSale'] = $dataSale;
            $data['branch'] = $branch;
            $data['namaCustomer'] = $namaCustomer;
            $data['alamatCustomer'] = $alamatCustomer;
            $data['phoneCustomer'] = $phoneCustomer;
            $data['idTransactionSales'] = $id;
            $data['isCheckedDrum'] = $isCheckedDrum;
            $data['jmlDrum'] = $jmlDrum;
            $data['literPerDrum'] = $literPerDrum;
            $data['penyusutanLiter'] = $penyusutanLiter;
            $data['hargaPerDrum'] = $hargaPerDrum;
            $data['hargaPerLiter'] = $hargaPerLiter;
            $data['weight'] = $weight;
            $data['specificGravity'] = $specificGravity;
            $data['waterContent'] = $waterContent;
            $data['jmlLiter'] = $jmlLiter;
            $data['descriptionDrum'] = $descriptionDrum;
            $data['nominalPenjualan'] = $nominalPenjualan;
            $data['descriptionOli'] = $descriptionOli;

            $this->load->view('v_sale/surat_jalan_approve', $data);
        } else {
            echo "<script>alert('Penjualan tidak valid.'); window.location = '" . base_url("penjualan") . "';</script>";
        }
    }

    public function Detail($type = 0)
    {
        $judul = "Detail Penjualan";
        $isDelete = 0;
        if ($type == 1) {
            $judul = "Hapus Penjualan";
            $isDelete = 1;
        }
        $isApprove = 0;
        if ($type == 2) {
            $judul = "Approve Penjualan";
            $isApprove = 1;
        }
        $isCancel = 0;
        if ($type == 3) {
            $judul = "Batalkan Konfirmasi Pembelian";
            $isCancel = 1;
        }

        $id = $_POST['id'];
        $dataSale = $this->M_sale->selectById($id);
        if ($dataSale != NULL) {
            $dataSaleDetail = $this->M_sale->selectDetailByHeaderId($id);

            $isApproved = $dataSale->is_approved;

            $isCheckedDrum = 0;
            $isCheckedAdditionalDrum = 0;
            $jmlKemasan = 0;
            $literPerKemasan = 0;
            $jmlDrum = 0;
            $literPerDrum = 0;
            $penambahanDrum = 0;
            $penambahanLiter = 0;
            $penyusutanLiter = 0;
            $weight = 0;
            $specificGravity = 0;
            $waterContent = 0;
            $jmlLiter = 0;
            $hargaPerLiter = 0;
            $grandTotalLiter = 0;
            $hargaPerDrum = 0;
            $descriptionDrum = "";
            $descriptionOli = "";
            $type = "Liter";

            foreach ($dataSaleDetail as $key => $value) {
                if ($value->id_item == 1) {
                    $type = $value->type;
                    if (strlen($type) == 0)
                        $type = 'Liter';
                    $jmlKemasan = $value->multiple;
                    $literPerKemasan = $value->qty_basic;
                    $literPerKemasan = str_replace(".", ",", $literPerKemasan);
                    $literPerDrum = $value->qty_before;
                    $penambahanLiter = $value->additional;
                    $penyusutanLiter = $value->depreciation;
                    $grandTotalLiter = $value->qty;
                    $grandTotalLiter = str_replace(".", ",", $grandTotalLiter);
                    $weight = $value->weight;
                    $weight = str_replace(".", ",", $weight);
                    $specificGravity = $value->specific_gravity;
                    $specificGravity = str_replace(".", ",", $specificGravity);
                    $waterContent = $value->water_content;
                    $waterContent = str_replace(".", ",", $waterContent);
                    $jmlLiter = $value->qty;
                    $hargaPerLiter = $value->total;
                    $hargaPerKilogram = $value->total;
                    $descriptionOli = $value->description;
                }
                if ($value->id_item == 2) {
                    $jmlDrum = $value->qty_before;
                    $penambahanDrum = $value->additional;
                    $descriptionDrum = $value->description;
                    if ($penambahanDrum > 0) {
                        $isCheckedAdditionalDrum = 1;
                    }
                    if ($jmlDrum > 0) {
                        $isCheckedDrum = 1;
                    }
                    $hargaPerDrum = $value->total;
                }
            }
            if ($jmlDrum > 0) {
                $literPerDrum = $literPerDrum / $jmlDrum;
            }

            $idCustomer = $dataSale->id_customer;
            $dataCustomer = $this->M_customer->selectById($idCustomer);
            $namaCustomer = $dataCustomer->name;

            $data['menuName'] = "penjualan";
            $data['page'] = "Penjualan";
            $data['judul'] = $judul;

            $data['isApproved'] = $isApproved;
            $data['dataSale'] = $dataSale;
            $data['namaCustomer'] = $namaCustomer;
            $data['idTransactionSales'] = $id;
            $data['isCheckedDrum'] = $isCheckedDrum;
            $data['isCheckedAdditionalDrum'] = $isCheckedAdditionalDrum;
            $data['jmlKemasan'] = $jmlKemasan;
            $data['literPerKemasan'] = $literPerKemasan;
            $data['jmlDrum'] = $jmlDrum;
            $data['literPerDrum'] = $literPerDrum;
            $data['penambahanDrum'] = $penambahanDrum;
            $data['penambahanLiter'] = $penambahanLiter;
            $data['penyusutanLiter'] = $penyusutanLiter;
            $data['grandTotalLiter'] = $grandTotalLiter;
            $data['weight'] = $weight;
            $data['specificGravity'] = $specificGravity;
            $data['waterContent'] = $waterContent;
            $data['jmlLiter'] = $jmlLiter;
            $data['type'] = $type;
            $data['hargaPerLiter'] = $hargaPerLiter;
            $data['hargaPerKilogram'] = $hargaPerKilogram;
            $data['hargaPerDrum'] = $hargaPerDrum;

            $data['price'] = $dataSale->price;
            $data['ppn'] = $dataSale->ppn;
            $data['totalPpn'] = $dataSale->ppn * $dataSale->price;
            $data['gandTotal'] = $dataSale->nominal;

            $data['descriptionDrum'] = $descriptionDrum;
            $data['descriptionOli'] = $descriptionOli;
            $data['isCancel'] = $isCancel;
            $data['isApprove'] = $isApprove;
            $data['isDelete'] = $isDelete;

            $isDrumOnly = 0;
            $idItemExp = $dataSale->id_item_exp;
            if (strlen($idItemExp) > 0) {
                $arrItem = explode(',', $idItemExp);
                if (count($arrItem) == 1) {
                    if ($arrItem[0] == 2) {
                        $isDrumOnly = 1;
                    }
                }
            }

            if ($isDrumOnly > 0) {
                echo show_my_modal('v_sale/detail_custom', 'detail-sale', $data, 'md');
            } else {
                echo show_my_modal('v_sale/detail', 'detail-sale', $data, 'md');
            }
        } else {
            echo "<script>alert('Penjualan tidak tersedia.'); window.location = '" . base_url("penjualan") . "';</script>";
        }
    }

    public function prosesCancel()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_transaction_sales'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'penjualan');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getSale = $this->M_sale->selectById($id);
            if ($getSale == NULL) {
                $errCode++;
                $errMessage = "Penjualan tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdate = array(
                    'nominal' => 0,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_sales', $dataUpdate, array('id_transaction_sales' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdate = array(
                    'total' => 0,
                    'grandtotal' => 0,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_sales_detail', $dataUpdate, array('id_transaction_sales' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesApprove()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_transaction_sales'];
        $paid = $_POST['paid'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessConf = $this->M_sidebar->access('conf', 'penjualan');
            if ($accessConf->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($paid) == 0) {
                $errCode++;
                $errMessage = "Tanggal Bayar wajib diisi.";
            }
        }
        if ($errCode == 0) {
            $dataSale = $this->M_sale->selectById($id);
            $code = $dataSale->code;
            $nominal = $dataSale->nominal;
            if ($dataSale == NULL) {
                $errCode++;
                $errMessage = "Penjualan tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdate = array(
                    'paid' => date('Y-m-d', strtotime($paid)),
                    'is_approved' => 1,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_transaction_sales', $dataUpdate, array('id_transaction_sales' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            $isok2 = $this->M_finance->balance("tbl_transaction_sales", array('id_transaction_sales' => $id), 'in', 'TRANSACTION SALE ' . $code, 'Transaksi Penjualan ' . $code . ' sebesar ' . number_format($nominal, 0, ",", "."));
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_transaction_sales'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('del', 'penjualan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getSale = $this->M_sale->selectById($id);
            if ($getSale == NULL) {
                $errCode++;
                $errMessage = "Penjualan tidak valid.";
            }
        }
        if ($errCode == 0) {
            $code = $getSale->code;
            $getDetail = $this->M_sale->selectDetailByHeaderId($id);
            if (count($getDetail) > 0) {
                foreach ($getDetail as $key => $value) {
                    $err = 0;
                    $msg = "";
                    $idTransactionSalesDetail = $value->id_transaction_sales_detail;
                    $getItem = $this->M_item->selectById($value->id_item);
                    if ($err == 0) {
                        $isok1 = $this->M_item->deleteBalance("tbl_transaction_sales_detail", array('id_transaction_sales_detail' => $idTransactionSalesDetail));
                        $err = $isok1['errCode'];
                        $msg = $isok1['errMessage'];
                    }
                    if ($err == 0) {
                        try {
                            $this->db->delete('tbl_transaction_sales_detail', array('id_transaction_sales_detail' => $idTransactionSalesDetail));
                        } catch (Exception $ex) {
                            $err++;
                            $msg = $ex->getMessage();
                        }
                    }
                    if ($err > 0) {
                        $errCode++;
                        $errMessage = $msg;
                        break;
                    }
                }
            }
        }
        // proses balance finance
        if ($errCode == 0) {
            $isApproved = $getSale->is_approved;
            if ($isApproved > 0) {
                $isok2 = $this->M_finance->deleteBalance("tbl_transaction_sales", array('id_transaction_sales' => $id));
                $errCode = $isok2['errCode'];
                $errMessage = $isok2['errMessage'];
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->delete('tbl_transaction_sales', array('id_transaction_sales' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function checkStockItem($isAjax = 1, $jmlOli = 0, $jmlDrum = 0)
    {
        if ($isAjax > 0) {
            $jmlDrum = $this->input->post("jml_drum");
            $jmlOli = $this->input->post("jml_oli");
        }

        $isReady = true;
        $message = "";
        if ($jmlOli > 0) {
            $getStockOli = $this->M_item->getStockItem(1);
            if ($jmlOli > $getStockOli) {
                $isReady = false;
                $message = "Stok Oli tidak mencukupi";
            }
        }
        if ($jmlDrum > 0) {
            $getStockDrum = $this->M_item->getStockItem(2);
            if ($jmlDrum > $getStockDrum) {
                $isReady = false;
                $message = "Stok Drum tidak mencukupi";
            }
        }

        $arr = array('is_ready' => $isReady, 'message' => $message);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $arr;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Unit test">
    public function testSale()
    {
        $sql = "SELECT * FROM tbl_transaction_sales";
        $result = $this->db->query($sql)->result();

        if (count($result) > 0) {
            foreach ($result as $key => $value) {
                $strItem = "";
                $q = "SELECT * FROM tbl_transaction_sales_detail WHERE id_transaction_sales = '" . $value->id_transaction_sales . "' ";
                $detail = $this->db->query($q)->result();
                if (count($detail) > 0) {
                    foreach ($detail as $k => $val) {
                        if ($k > 0)
                            $strItem .= ",";
                        $strItem .= $val->id_item;
                    }
                }

                $data = array(
                    'id_item_exp' => $strItem,
                );
                echo "<pre>";
                print_r($data);
                echo "</pre>";
                echo "<br/>";
                $this->db->update('tbl_transaction_sales', $data, array('id_transaction_sales' => $value->id_transaction_sales));
            }
        }
    }

    public function normalisasiBalance()
    {
        $sqlHeader = "SELECT * FROM tbl_transaction_sales";
        $resultHeader = $this->db->query($sqlHeader)->result();
        foreach ($resultHeader as $key => $value) {
            $resultDetail = $this->M_sale->selectDetailByHeaderId($value->id_transaction_sales);
            foreach ($resultDetail as $k => $val) {
                $dataUpdateDetail = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'created_by' => $value->created_by,
                );
                $this->db->update('tbl_transaction_sales_detail', $dataUpdateDetail, array('id_transaction_sales_detail' => $val->id_transaction_sales_detail));

                $qItemHistory = "   SELECT * FROM tbl_item_history
                                    WHERE id_item = '{$val->id_item}'
                                    AND ref_table = 'tbl_transaction_sales_detail}'
                                    AND ref_id = '{$val->id_transaction_sales_detail}'
                                    AND fixed > 0 ";
                $getItemHistory = $this->db->query($qItemHistory)->row();
                if ($getItemHistory != NULL) {
                    $dataUpdateItemHistory = array(
                        'date' => $value->date,
                        'created_date' => $value->created_date,
                        'updated_date' => $val->updated_date,
                    );
                    $this->db->update('tbl_item_history', $dataUpdateItemHistory, array('id_item_history' => $getItemHistory->id_item_history));
                }
            }

            $qFinanceHistory = "SELECT * FROM tbl_finance_history
                                WHERE ref_table = 'tbl_transaction_sales'
                                AND ref_id = '{$value->id_transaction_sales}'
                                AND fixed > 0 ";
            $getLastFinanceHistory = $this->db->query($qFinanceHistory)->row();
            if ($getLastFinanceHistory != NULL) {
                $dataUpdateFinanceHistory = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'updated_date' => $value->updated_date,
                );
                $this->db->update('tbl_finance_history', $dataUpdateFinanceHistory, array('id_finance_history' => $getLastFinanceHistory->id_item_history));
            }
        }
    }
    //</editor-fold>
}

<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 23, 2019
 * @license Susi Susanti Group
 */
class VehicleRent extends AUTH_Controller
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_sidebar');
        $this->load->model('M_supplier');
        $this->load->model('M_vehicle');
        $this->load->model('M_finance');
        $this->load->model('M_vehicle_rent');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'penyewaan');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['menuName'] = "penyewaan";
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penyewaan Armada";
        $data['judul'] = "Penyewaan Armada";
        $data['supplier'] = $this->M_supplier->selectItem();

        $this->loadkonten('v_vehicle_rent/home', $data);
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function ajaxList()
    {
        $idSupplier = $this->input->post('id_supplier');
        $isDone = $this->input->post('is_done');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'id_supplier' => $idSupplier,
            'is_done' => $isDone,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', 'penyewaan');
        $accessBack = $this->M_sidebar->access('back', 'penyewaan');
        $accessDel = $this->M_sidebar->access('del', 'penyewaan');

        $list = $this->M_vehicle_rent->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $dataSupplier = $this->M_supplier->selectById($value->id_supplier);
            $dataVehicle = $this->M_vehicle->selectById($value->id_vehicle);

            $status = '<small class="label pull-center bg-yellow">Disewa</small">';
            $dateBack = ' (Disewa)';
            if ($value->is_done > 0) {
                $status = '<small class="label pull-center bg-green">Kembali</small">';
                $dateBack = ' s/d ' . date('d-m-Y', strtotime($value->back)) . ' (' . $value->count_day . ' hari)';
            }

            $row = array();
            $row[] = $no;
            $row[] = $dataSupplier->name;
            $row[] = $dataVehicle->brand . ' - ' . $dataVehicle->police_number;
            $row[] = date('d-m-Y', strtotime($value->date)) . $dateBack;
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($value->nominal_rent, 0, ",", ".") . '</div">';
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($value->nominal, 0, ",", ".") . '</div">';
            $row[] = $value->description;
            $row[] = $status;
            $row[] = date('d-m-Y H:i:s', strtotime($value->created_date));
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));

            //add html for action
            $action = " <div class='btn-group pull-right'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($value->is_done == 0) {
                if ($accessEdit->menuview > 0) {
                    $action .= "    <li><a href='" . site_url('edit-penyewaan/' . $value->id_vehicle_rent) . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                }
                if ($accessBack->menuview > 0) {
                    $action .= "    <li><a href='" . site_url('kembali-penyewaan/' . $value->id_vehicle_rent) . "'><i class='fa fa-check'></i> Kembali</a></li>";
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-penyewaan' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_vehicle_rent . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $access = $this->M_sidebar->access('add', 'penyewaan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penyewaan Armada";
        $data['judul'] = "Tambah Penyewaan Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "penyewaan";
            $data['supplier'] = $this->M_supplier->selectItem();
            $data['vehicle'] = $this->M_vehicle->selectItemReady();
            $this->loadkonten('v_vehicle_rent/tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $idSupplier = $this->input->post("id_supplier");
        $idVehicle = $this->input->post("id_vehicle");
        $dateRent = $this->input->post("date_rent");
        $amountRent = $this->input->post("amount_rent");
        $amountRent = str_replace(".", "", $amountRent);
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessAdd = $this->M_sidebar->access('add', 'penyewaan');
            if ($accessAdd->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier harus di isi.";
            }
        }
        if ($errCode == 0) {
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            if ($dataSupplier == null) {
                $errCode++;
                $errMessage = "Supplier tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkSupplierVehicleRent = $this->checkSupplierVehicleRent(0, $idSupplier);
            if ($checkSupplierVehicleRent == true) {
                $errCode++;
                $errMessage = "Supplier masih menyewa Armada.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idVehicle) == 0) {
                $errCode++;
                $errMessage = "Armada harus di isi.";
            }
        }
        if ($errCode == 0) {
            $dataVehicle = $this->M_vehicle->selectById($idVehicle);
            if ($dataVehicle == null) {
                $errCode++;
                $errMessage = "Armada tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateRent) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sewa harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($amountRent) == 0) {
                $errCode++;
                $errMessage = "Harga Sewa (per hari) harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($amountRent <= 0) {
                $errCode++;
                $errMessage = "Harga Sewa (per hari) harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $dataVehicleRent = array(
                'id_branch' => $this->branch,
                'id_vehicle' => $idVehicle,
                'id_supplier' => $idSupplier,
                'date' => date('Y-m-d', strtotime($dateRent)),
                'nominal_rent' => $amountRent,
                'description' => $description,
                'created_by' => $username,
                'created_date' => $date,
                'updated_by' => $username,
                'updated_date' => $date,
            );
            $this->db->insert('tbl_vehicle_rent', $dataVehicleRent);
            $idVehicleRent = $this->db->insert_id();
            if (strlen($idVehicleRent) > 0) {
                $dataUpdateVehicle = array(
                    'is_ready' => 0,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_vehicle', $dataUpdateVehicle, array('id_vehicle' => $idVehicle));
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $access = $this->M_sidebar->access('edit', 'penyewaan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penyewaan Armada";
        $data['judul'] = "Ubah Penyewaan Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataVehicleRent = $this->M_vehicle_rent->selectById($id);
            if ($dataVehicleRent != null) {
                $checkVehicleRent = $this->checkVehicleRent(0, $id);
                if ($checkVehicleRent == false) {
                    $dataSupplier = $this->M_supplier->selectById($dataVehicleRent->id_supplier);
                    $dataVehicle = $this->M_vehicle->selectById($dataVehicleRent->id_vehicle);

                    $data['menuName'] = "penyewaan";
                    $data['dataVehicleRent'] = $dataVehicleRent;
                    $data['supplier_name'] = $dataSupplier->name;
                    $data['vehicle'] = $dataVehicle->brand . ' - ' . $dataVehicle->police_number;
                    $data['idVehicleRent'] = $id;
                    $this->loadkonten('v_vehicle_rent/update', $data);
                } else {
                    echo "<script>alert('Supplier sudah mengembalikan armada.'); window.location = '" . base_url("penyewaan") . "';</script>";
                }
            } else {
                echo "<script>alert('Penyewaan armada tidak valid.'); window.location = '" . base_url("penyewaan") . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $dateRent = $this->input->post("date_rent");
        $amountRent = $this->input->post("amount_rent");
        $amountRent = str_replace(".", "", $amountRent);
        $description = $this->input->post("description");
        $updatedBy = $this->input->post("updated_by");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessEdit = $this->M_sidebar->access('edit', 'penyewaan');
            if ($accessEdit->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $getVehicleRent = $this->M_vehicle_rent->selectById($id);
            if ($getVehicleRent == null) {
                $errCode++;
                $errMessage = "Penyewaan armada tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkVehicleRent = $this->checkVehicleRent(0, $id);
            if ($checkVehicleRent == true) {
                $errCode++;
                $errMessage = "Supplier sudah mengembalikan armada.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateRent) == 0) {
                $errCode++;
                $errMessage = "Tanggal Sewa harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($amountRent) == 0) {
                $errCode++;
                $errMessage = "Harga Sewa (per hari) harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($amountRent <= 0) {
                $errCode++;
                $errMessage = "Harga Sewa (per hari) harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            try {
                $dataVehicleRent = array(
                    'date' => date('Y-m-d', strtotime($dateRent)),
                    'nominal_rent' => $amountRent,
                    'description' => $description,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_vehicle_rent', $dataVehicleRent, array('id_vehicle_rent' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Kembali($id)
    {
        $access = $this->M_sidebar->access('back', 'penyewaan');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Penyewaan Armada";
        $data['judul'] = "Pengembalian Penyewaan Armada";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $dataVehicleRent = $this->M_vehicle_rent->selectById($id);
            if ($dataVehicleRent != null) {
                $checkVehicleRent = $this->checkVehicleRent(0, $id);
                if ($checkVehicleRent == false) {
                    $dataSupplier = $this->M_supplier->selectById($dataVehicleRent->id_supplier);
                    $dataVehicle = $this->M_vehicle->selectById($dataVehicleRent->id_vehicle);

                    $data['menuName'] = "penyewaan";
                    $data['dataVehicleRent'] = $dataVehicleRent;
                    $data['supplier_name'] = $dataSupplier->name;
                    $data['vehicle'] = $dataVehicle->brand . ' - ' . $dataVehicle->police_number;
                    $data['idVehicleRent'] = $id;
                    $this->loadkonten('v_vehicle_rent/kembali', $data);
                } else {
                    echo "<script>alert('Supplier sudah mengembalikan armada.'); window.location = '" . base_url("penyewaan") . "';</script>";
                }
            } else {
                echo "<script>alert('Penyewaan armada tidak valid.'); window.location = '" . base_url("penyewaan") . "';</script>";
            }
        }
    }

    public function prosesKembali($id)
    {
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";
        $nominalRent = 0;
        $nominal = 0;
        $idVehicleMaintenance = null;
        $idVehicle = null;

        $dateRent = $this->input->post("date_rent");
        $dateBack = $this->input->post("date_back");
        $countDay = $this->input->post("count_day");
        $description = $this->input->post("description");
        $isRentAgain = $this->input->post("is_rent_again");
        $dateRentAgain = $this->input->post("date_rent_again");

        $isMaintenanceVehicle = $this->input->post("is_maintenance_vehicle");
        $amountMaintenance = $this->input->post("amount_maintenance");
        $amountMaintenance = str_replace(".", "", $amountMaintenance);
        $amountMaintenance = str_replace(",", "", $amountMaintenance);
        $descriptionMaintenance = $this->input->post("description_maintenance");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessBack = $this->M_sidebar->access('back', 'penyewaan');
            if ($accessBack->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $dataVehicleRent = $this->M_vehicle_rent->selectById($id);
            if ($dataVehicleRent == null) {
                $errCode++;
                $errMessage = "Penyewaan armada tidak tersedia.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateBack) == 0) {
                $errCode++;
                $errMessage = "Tanggal Kembali harus di isi.";
            }
        }
        if ($errCode == 0) {
            $idSupplier = $dataVehicleRent->id_supplier;
            $getSupplier = $this->M_supplier->selectById($idSupplier);
            if ($getSupplier == null) {
                $errCode++;
                $errMessage = "Supplier invalid";
            } else {
                $namaSupplier = $getSupplier->name;
            }
        }
        if ($errCode == 0) {
            $idVehicle = $dataVehicleRent->id_vehicle;
            $getVehicle = $this->M_vehicle->selectById($idVehicle);
            if ($getVehicle == null) {
                $errCode++;
                $errMessage = "Vehicle invalid";
            } else {
                $vehicle = $getVehicle->brand . ' - ' . $getVehicle->police_number;
            }
        }
        if ($errCode == 0) {
            $dateBack = date('Y-m-d', strtotime($dateBack));
            $dateRent = date('Y-m-d', strtotime($dataVehicleRent->date));
            if ($dateRent > $dateBack) {
                $errCode++;
                $errMessage = "Tanggal Kembali harus lebih dari tanggal Sewa.";
            }
        }
        if ($errCode == 0) {
            if (strlen($countDay) == 0) {
                $errCode++;
                $errMessage = "Jumlah Hari harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($countDay <= 0) {
                $errCode++;
                $errMessage = "Jumlah Hari harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $nominalRent = $dataVehicleRent->nominal_rent;
            $nominal = $countDay * $nominalRent;
            if (isset($isRentAgain) && $isRentAgain == 1) {
                $dateBack = date('Y-m-d', strtotime($dateBack));
                $dateRentAgain = date('Y-m-d', strtotime($dateRentAgain));
                if ($dateBack > $dateRentAgain) {
                    $errCode++;
                    $errMessage = "Tanggal Sewa Kembali harus lebih dari tanggal Kembali.";
                }
            }
        }
        if (isset($isMaintenanceVehicle) && $isMaintenanceVehicle > 0) {
            if ($errCode == 0) {
                if (strlen($idVehicle) == 0) {
                    $errCode++;
                    $errMessage = "Armada harus di isi.";
                }
            }
            if ($errCode == 0) {
                if (strlen($amountMaintenance) == 0) {
                    $errCode++;
                    $errMessage = "Biaya maintenance wajib diisi.";
                }
            }
            if ($errCode == 0) {
                if ($amountMaintenance <= 0) {
                    $errCode++;
                    $errMessage = "Biaya maintenance harus lebih dari 0.";
                }
            }
//            if ($errCode == 0) {
//                $lastGrandTotal = $nominal - $amountMaintenance;
//                if ($lastGrandTotal <= 0) {
//                    $errCode++;
//                    $errMessage = "Grand total harus lebih dari 0.";
//                }
//            }
        }
        if ($errCode == 0) {
            $checkVehicleRent = $this->checkVehicleRent(0, $id);
            if ($checkVehicleRent == true) {
                $errCode++;
                $errMessage = "Supplier sudah mengembalikan armada.";
            }
        }
        //MAINTENANCE VEHICLE
        if (isset($isMaintenanceVehicle) && $isMaintenanceVehicle > 0) {
            if ($errCode == 0) {
                try {
                    $dataVehicleMaintenance = array(
                        'id_branch' => $this->branch,
                        'id_vehicle' => $idVehicle,
                        'date' => date('Y-m-d', strtotime($dateRent)),
                        'nominal' => $amountMaintenance,
                        'description' => $descriptionMaintenance,
                        'created_by' => $username,
                        'created_date' => $date,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->insert('tbl_vehicle_maintenance', $dataVehicleMaintenance);
                    $idVehicleMaintenance = $this->db->insert_id();
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            if ($errCode == 0) {
                try {
                    $isok3 = $this->M_finance->balance("tbl_vehicle_maintenance", array('id_vehicle_maintenance' => $idVehicleMaintenance), 'out', 'MAINTENANCE FEE VEHICLE ' . $vehicle, 'Pemeliharaan Armada ' . $vehicle . ' sebesar ' . number_format($amountMaintenance, 0, ",", ".") . ' ' . $descriptionMaintenance);
                    $errCode = $isok3['errCode'];
                    $errMessage = $isok3['errMessage'];
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
        }
        if ($errCode == 0) {
            try {
                $dataVehicleRentUpdate = array(
                    'id_vehicle_maintenance' => $idVehicleMaintenance,
                    'count_day' => $countDay,
                    'back' => date('Y-m-d', strtotime($dateBack)),
                    'nominal' => $nominal,
                    'description' => $description,
                    'is_done' => 1,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_vehicle_rent', $dataVehicleRentUpdate, array('id_vehicle_rent' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            try {
                $dataUpdateVehicle = array(
                    'is_ready' => 1,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->update('tbl_vehicle', $dataUpdateVehicle, array('id_vehicle' => $idVehicle));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            try {
                $isok2 = $this->M_finance->balance("tbl_vehicle_rent", array('id_vehicle_rent' => $id), 'in', 'RETURN RENT VEHICLE ' . strtoupper($namaSupplier), 'Pengembalian Penyewaan Supplier ' . $namaSupplier . ' dengan Armada ' . $vehicle);
                $errCode = $isok2['errCode'];
                $errMessage = $isok2['errMessage'];
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if (isset($isRentAgain) && $isRentAgain == 1) {
                try {
                    $dataVehicleRentAgain = array(
                        'id_branch' => $this->branch,
                        'id_vehicle' => $idVehicle,
                        'id_supplier' => $idSupplier,
                        'date' => date('Y-m-d', strtotime($dateRentAgain)),
                        'nominal_rent' => $dataVehicleRent->nominal_rent,
                        'created_by' => $username,
                        'created_date' => $date,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->insert('tbl_vehicle_rent', $dataVehicleRentAgain);
                    $idVehicleRent = $this->db->insert_id();
                    if (strlen($idVehicleRent) > 0) {
                        $dataUpdateVehicleAgain = array(
                            'is_ready' => 0,
                            'updated_by' => $username,
                            'updated_date' => $date,
                        );
                        $this->db->update('tbl_vehicle', $dataUpdateVehicleAgain, array('id_branch' => $this->branch, 'id_vehicle' => $idVehicle));
                    }
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $username = $this->userdata->nama;
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_vehicle_rent'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('del', 'penyewaan');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getVehicleRent = $this->M_vehicle_rent->selectById($id);
            if ($getVehicleRent == NULL) {
                $errCode++;
                $errMessage = "Supplier Vehicle Rent Invalid.";
            }
        }
        if ($errCode == 0) {
            $getSupplier = $this->M_supplier->selectById($getVehicleRent->id_supplier);
            if ($getSupplier == null) {
                $errCode++;
                $errMessage = "Supplier invalid";
            }
        }
        if ($errCode == 0) {
            $idVehicle = $getVehicleRent->id_vehicle;
            $getVehicle = $this->M_vehicle->selectById($idVehicle);
            if ($getVehicle == null) {
                $errCode++;
                $errMessage = "Vehicle invalid";
            }
        }
        // proses balance finance
        if ($errCode == 0) {
            $checkVehicleRent = $this->checkVehicleRent(0, $id);
            if ($checkVehicleRent == true) {
                $namaSupplier = $getSupplier->name;
                $vehicle = $getVehicle->brand . ' - ' . $getVehicle->police_number;

                $isok2 = $this->M_finance->deleteBalance("tbl_vehicle_rent", array('id_vehicle_rent' => $id), 'out', 'DELETE RENT VEHICLE ' . strtoupper($namaSupplier), 'Hapus Penyewaan Supplier ' . $namaSupplier . ' dengan Armada ' . $vehicle);
                $errCode = $isok2['errCode'];
                $errMessage = $isok2['errMessage'];
            }
        }
        if ($errCode == 0) {
            $dataUpdateVehicle = array(
                'is_ready' => 1,
                'updated_by' => $username,
                'updated_date' => $date,
            );
            $this->db->update('tbl_vehicle', $dataUpdateVehicle, array('id_branch' => $this->branch, 'id_vehicle' => $idVehicle));
        }
        if ($errCode == 0) {
            try {
                $this->db->delete('tbl_vehicle_rent', array('id_vehicle_rent' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function checkSupplierVehicleRent($isAjax = 1, $idSupplier)
    {
        if ($isAjax > 0 && strlen($idSupplier) == 0) {
            $idSupplier = $this->input->post("id_supplier");
        }
        $isRented = false;
        if (strlen($idSupplier) > 0) {
            $q = "SELECT * FROM tbl_vehicle_rent WHERE id_branch = '{$this->branch}' AND is_done = 0 AND id_supplier = '{$idSupplier}'";
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isRented = true;
            }
        }
        $arr = array('is_rented' => $isRented);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $isRented;
        }
    }

    public function checkVehicleRent($isAjax = 1, $id)
    {
        if ($isAjax > 0 && strlen($id) == 0) {
            $id = $this->input->post("id_vehicle_rent");
        }

        $isRent = false;
        $q = "SELECT * FROM tbl_vehicle_rent WHERE id_branch = '{$this->branch}' AND is_done = 1 AND id_vehicle_rent = '{$id}'";
        $result = $this->db->query($q)->row_array();
        if ($result != NULL) {
            $isRent = true;
        }

        $arr = array('is_rented' => $isRent);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $isRent;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Unit test">
    public function normalisasiBalance()
    {
        $sql = "SELECT * FROM tbl_vehicle_rent";
        $result = $this->db->query($sql)->result();
        foreach ($result as $key => $value) {
            $qFinanceHistory = "SELECT * FROM tbl_finance_history
                                WHERE ref_table = 'tbl_vehicle_rent'
                                AND ref_id = '{$value->id_vehicle_rent}'
                                AND fixed > 0 ";
            $getLastFinanceHistory = $this->db->query($qFinanceHistory)->row();
            if ($getLastFinanceHistory != NULL) {
                $dataUpdateFinanceHistory = array(
                    'date' => $value->date,
                    'created_date' => $value->created_date,
                    'updated_date' => $value->updated_date,
                );
                $this->db->update('tbl_finance_history', $dataUpdateFinanceHistory, array('id_finance_history' => $getLastFinanceHistory->id_item_history));
            }
        }
    }
    //</editor-fold>
}

<?php

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 23, 2019
 * @license Susi Susanti Group
 */
class SupplierLoan extends AUTH_Controller
{
    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_sidebar');
        $this->load->model('M_supplier');
        $this->load->model('M_finance');
        $this->load->model('M_supplier_loan');
        $this->load->model('M_supplier_loan_installment');
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', 'peminjaman');
        $data['accessAdd'] = $accessAdd->menuview;
        $data['menuName'] = "peminjaman";
        $data['userdata'] = $this->userdata;
        $data['page'] = "Peminjaman Dana";
        $data['judul'] = "Peminjaman Dana";
        $data['supplier'] = $this->M_supplier->selectItem();

        $this->loadkonten('v_supplier_loan/home', $data);
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function ajaxList()
    {
        $idSupplier = $this->input->post('id_supplier');
        $isPaidOff = $this->input->post('is_paid_off');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'id_supplier' => $idSupplier,
            'is_paid_off' => $isPaidOff,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', 'peminjaman');
        $accessDel = $this->M_sidebar->access('del', 'peminjaman');

        $list = $this->M_supplier_loan->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;

            $idSupplier = $value->id_supplier;
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;

            $row = array();

            $statusLunas = '<small class="label pull-center bg-yellow">Belum Lunas</small">';
            if ($value->is_paid_off > 0) {
                $statusLunas = '<small class="label pull-center bg-green">Lunas</small">';
            }

            $paidAll = 0;
            $resultSupplierLoanInstallment = $this->M_supplier_loan->getSupplierLoanInstallment($value->id_supplier_loan, 0);
            if (count($resultSupplierLoanInstallment) > 0) {
                foreach ($resultSupplierLoanInstallment as $k => $val) {
                    $paidAll += $val->nominal;
                }
            }

            $row[] = $no;
            $row[] = $namaSupplier;
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($value->nominal, 0, ",", ".") . '</div">';
            $row[] = '<div width="100%" style="text-align: right;">' . number_format(($paidAll - $value->paid), 0, ",", ".") . '</div">';
            $row[] = '<div width="100%" style="text-align: right;">' . number_format($value->paid, 0, ",", ".") . '</div">';
            $row[] = '<div width="100%" style="text-align: right;">' . number_format(($value->nominal - $paidAll), 0, ",", ".") . '</div">';
            $row[] = $statusLunas;
            $row[] = $value->description;
            $row[] = date('d-m-Y H:i:s', strtotime($value->created_date));
            $row[] = date('d-m-Y H:i:s', strtotime($value->updated_date));

            //add html for action
            $action = " <div class='btn-group pull-right'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            $action .= "        <li><a href='#' class='detail-peminjaman' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_supplier_loan . "'><i class='glyphicon glyphicon-info-sign'></i> Detail</a></li>";
            if ($value->is_paid_off == 0) {
                if ($accessEdit->menuview > 0) {
                    $action .= "    <li><a href='" . site_url('edit-peminjaman/' . $value->id_supplier_loan) . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                }
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-peminjaman' data-toggle='tooltip' data-placement='top' data-id='" . $value->id_supplier_loan . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('add', 'peminjaman');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Peminjaman Dana";
        $data['judul'] = "Tambah Peminjaman Dana";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = "peminjaman";
            $data['privilegeId'] = $privilegeId;
            $data['supplier'] = $this->M_supplier->selectItem();
            $this->loadkonten('v_supplier_loan/tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $idSupplier = $this->input->post("id_supplier");
        $dateLoan = date('d-m-Y');
        if ($privilegeId == 1) {
            $dateLoan = $this->input->post("date");
        }
        $amountLoan = $this->input->post("amount_loan");
        $amountLoan = str_replace(".", "", $amountLoan);
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessAdd = $this->M_sidebar->access('add', 'peminjaman');
            if ($accessAdd->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($dateLoan) == 0) {
                $errCode++;
                $errMessage = "Tanggal peminjaman harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($amountLoan) == 0) {
                $errCode++;
                $errMessage = "Nominal Peminjaman harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($amountLoan <= 0) {
                $errCode++;
                $errMessage = "Nominal Peminjaman harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $getSupplier = $this->db->get_where('tbl_supplier', array('id_branch' => $this->branch, 'id_supplier' => $idSupplier,))->row_array();
            if (count($getSupplier) == 0) {
                $errCode++;
                $errMessage = "Supplier invalid";
            }
        }
        if ($errCode == 0) {
            try {
                $dataSupplierLoan = array(
                    'id_branch' => $this->branch,
                    'id_supplier' => $idSupplier,
                    'date' => date('Y-m-d', strtotime($dateLoan)),
                    'nominal' => $amountLoan,
                    'debt' => $amountLoan,
                    'description' => $description,
                    'created_by' => $username,
                    'created_date' => $date,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $this->db->insert('tbl_supplier_loan', $dataSupplierLoan);
                $idSupplierLoan = $this->db->insert_id();
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            $isok2 = $this->M_finance->balance("tbl_supplier_loan", array('id_supplier_loan' => $idSupplierLoan), 'out', 'SUPPLIER LOAN ' . strtoupper($getSupplier['name']), 'Peminjaman Dana ' . $getSupplier['name'] . ' sebesar ' . number_format($amountLoan, 0, ",", "."));
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $access = $this->M_sidebar->access('edit', 'peminjaman');
        $data['userdata'] = $this->userdata;
        $data['page'] = "Peminjaman Dana";
        $data['judul'] = "Ubah Peminjaman Dana";
        if ($access->menuview == 0) {
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
//            $checkInstallment = $this->checkSupplierLoanInstallment(0, $id);
            $dataSupplierLoan = $this->M_supplier_loan->selectById($id);
            if (count($dataSupplierLoan) > 0) {
                if ($dataSupplierLoan->is_paid_off == 0) {
                    $idSupplier = $dataSupplierLoan->id_supplier;
                    $dataSupplier = $this->M_supplier->selectById($idSupplier);
                    $namaSupplier = $dataSupplier->name;

                    $data['menuName'] = "peminjaman";
                    $data['dataSupplierLoan'] = $dataSupplierLoan;
                    $data['supplier_name'] = $namaSupplier;
                    $data['idSupplierLoan'] = $id;
                    $data['privilegeId'] = $privilegeId;
                    $this->loadkonten('v_supplier_loan/update', $data);
                } else {
                    echo "<script>alert('Supplier sudah melunasi cicilan pembayaran piutang.'); window.location = '" . base_url("peminjaman") . "';</script>";
                }
            } else {
                echo "<script>alert('Supplier sudah melakukan cicilan pembayaran piutang.'); window.location = '" . base_url("peminjaman") . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $privilegeId = $this->userdata->grup_id;
        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";
        $paid = 0;

        $dateLoan = $this->input->post("date");
        $amountLoan = $this->input->post("amount_loan");
        $amountLoan = str_replace(".", "", $amountLoan);
        $description = $this->input->post("description");

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessEdit = $this->M_sidebar->access('edit', 'peminjaman');
            if ($accessEdit->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $getSupplierLoan = $this->M_supplier_loan->selectById($id);
            if ($getSupplierLoan == null) {
                $errCode++;
                $errMessage = "Peminjaman tidak Valid.";
            }
        }
        if ($errCode == 0) {
            if ($getSupplierLoan->is_paid_off > 0) {
                $errCode++;
                $errMessage = "Supplier sudah melunasi cicilan pembayaran piutang.";
            }
        }
        if ($errCode == 0) {
            if ($privilegeId != 1) {
                $dateLoan = $getSupplierLoan->date;
                if (strlen($dateLoan) == 0) {
                    $dateLoan = date('d-m-Y');
                }
            }
            if (strlen($dateLoan) == 0) {
                $errCode++;
                $errMessage = "Tanggal peminjaman harus di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($amountLoan) == 0) {
                $errCode++;
                $errMessage = "Nominal Peminjaman harus di isi.";
            }
        }
        if ($errCode == 0) {
            if ($amountLoan <= 0) {
                $errCode++;
                $errMessage = "Nominal Peminjaman harus lebih dari 0.";
            }
        }
        if ($errCode == 0) {
            $paidAll = 0;
            $resultSupplierLoanInstallment = $this->M_supplier_loan->getSupplierLoanInstallment($id, 0);
            if (count($resultSupplierLoanInstallment) > 0) {
                foreach ($resultSupplierLoanInstallment as $key => $value) {
                    $paidAll += $value->nominal;
                }
            }
            if ($amountLoan < $paidAll) {
                $errCode++;
                $errMessage = "Nominal Peminjaman harus lebih dari " . $paidAll . ".";
            }
        }
        if ($errCode == 0) {
            $isEdit = 1;
            $q = "  SELECT * FROM tbl_supplier_loan
                    WHERE id_branch = '{$this->branch}'
                    AND id_supplier_loan = '{$id}'
                    AND date = '" . date('Y-m-d', strtotime($dateLoan)) . "'
                    AND nominal = '{$amountLoan}'
                    AND description = '{$description}'
                    AND id_supplier = '{$getSupplierLoan->id_supplier}'";
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isEdit = 0;
            }
        }

        if ($isEdit > 0) {
            if ($errCode == 0) {
                $paid = $getSupplierLoan->paid;
                $isPaidOff = 0;
                if ($amountLoan == $paid) {
                    $isPaidOff = 1;
                }
                try {
                    $dataSupplierLoan = array(
                        'date' => date('Y-m-d', strtotime($dateLoan)),
                        'nominal' => $amountLoan,
                        'debt' => $amountLoan - $paid,
                        'is_paid_off' => $isPaidOff,
                        'description' => $description,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->update('tbl_supplier_loan', $dataSupplierLoan, array('id_supplier_loan' => $id));
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
            // proses balance finance
            if ($errCode == 0) {
                $getSupplier = $this->db->get_where('tbl_supplier', array('id_supplier' => $getSupplierLoan->id_supplier))->row_array();
                $isok2 = $this->M_finance->updateBalance("tbl_supplier_loan", array('id_supplier_loan' => $id), 'out', 'SUPPLIER LOAN ' . strtoupper($getSupplier['name']), 'Peminjaman Dana ' . $getSupplier['name'] . ' sebesar ' . number_format($amountLoan, 0, ",", "."));
                $errCode = $isok2['errCode'];
                $errMessage = $isok2['errMessage'];
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id_supplier_loan'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $accessDel = $this->M_sidebar->access('del', 'peminjaman');
            if ($accessDel->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID invalid.";
            }
        }
        if ($errCode == 0) {
            $getSupplierLoan = $this->M_supplier_loan->selectById($id);
            if ($getSupplierLoan == NULL) {
                $errCode++;
                $errMessage = "Supplier Loan Invalid.";
            }
        }

        if ($errCode == 0) {
            $checkInstallment = $this->checkSupplierLoanInstallment(0, $id);
            if ($checkInstallment == true) {
                $errCode++;
                $errMessage = "Supplier sudah pernah melakukan cicilan piutang.";
            }
        }

        // proses balance finance
        if ($errCode == 0) {
            $isok2 = $this->M_finance->deleteBalance("tbl_supplier_loan", array('id_supplier_loan' => $id));
            $errCode = $isok2['errCode'];
            $errMessage = $isok2['errMessage'];
        }

        if ($errCode == 0) {
            try {
                $this->db->delete('tbl_supplier_loan', array('id_supplier_loan' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = 'berhasil';
        } else {
            $this->db->trans_rollback();
            $out['status'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Detail()
    {
        $id = $_POST['id'];
        $dataSupplierLoan = $this->M_supplier_loan->selectById($id);
        if ($dataSupplierLoan != NULL) {
            $idSupplier = $dataSupplierLoan->id_supplier;
            $dataSupplier = $this->M_supplier->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;

            $statusLunas = '<small class="label pull-center bg-yellow">Belum Lunas</small">';
            if ($dataSupplierLoan->is_paid_off > 0) {
                $statusLunas = '<small class="label pull-center bg-green">Lunas</small">';
            }

            $q = "  SELECT * FROM tbl_supplier_loan_installment 
                    WHERE id_supplier_loan = {$id} AND status > 0";
            $dataInstallment = $this->db->query($q)->result();

            $arrInstallment = array();
            if (count($dataInstallment) > 0) {
                foreach ($dataInstallment as $key => $value) {
                    $qPembelian = " SELECT * FROM tbl_transaction_purchase WHERE id_branch = '{$this->branch}'
                                    AND id_supplier_loan_installment LIKE '%{$value->id_supplier_loan_installment}%'";
                    $dataPembelian = $this->db->query($qPembelian)->row();
                    $arrInstallment[$key]['id_supplier_loan_installment'] = $value->id_supplier_loan_installment;
                    $arrInstallment[$key]['id_transaction_purchase'] = $dataPembelian->id_transaction_purchase;
                    $arrInstallment[$key]['description'] = "Cicilan pemotongan transaksi pembelian " . $dataPembelian->code;
                    $arrInstallment[$key]['nominal'] = $value->nominal;
                    $arrInstallment[$key]['created_date'] = $value->created_date;
                    $arrInstallment[$key]['created_by'] = $value->created_by;
                }
            }

            $qPending = "   SELECT * FROM tbl_supplier_loan_installment 
                            WHERE id_supplier_loan = {$id} AND status = 0";
            $dataInstallmentPending = $this->db->query($qPending)->result();

            $arrInstallmentPending = array();
            if (count($dataInstallmentPending) > 0) {
                foreach ($dataInstallmentPending as $key => $value) {
                    $qPembelian = " SELECT * FROM tbl_transaction_purchase WHERE id_branch = '{$this->branch}'
                                    AND id_supplier_loan_installment LIKE '%{$value->id_supplier_loan_installment}%'";
                    $dataPembelian = $this->db->query($qPembelian)->row();
                    $arrInstallmentPending[$key]['id_supplier_loan_installment'] = $value->id_supplier_loan_installment;
                    $arrInstallmentPending[$key]['id_transaction_purchase'] = $dataPembelian->id_transaction_purchase;
                    $arrInstallmentPending[$key]['description'] = "Cicilan pemotongan transaksi pembelian " . $dataPembelian->code;
                    $arrInstallmentPending[$key]['nominal'] = $value->nominal;
                    $arrInstallmentPending[$key]['created_date'] = $value->created_date;
                    $arrInstallmentPending[$key]['created_by'] = $value->created_by;
                }
            }

            $data['menuName'] = "peminjaman";
            $data['page'] = "Peminjaman Dana";
            $data['judul'] = "Peminjaman Dana";
            $data['dataSupplierLoan'] = $dataSupplierLoan;
            $data['namaSupplier'] = $namaSupplier;
            $data['statusLunas'] = $statusLunas;
            $data['arrInstallment'] = $arrInstallment;
            $data['arrInstallmentPending'] = $arrInstallmentPending;

            echo show_my_modal('v_supplier_loan/detail', 'detail-peminjaman', $data, 'md');
        } else {
            echo "<script>alert('Cicilan Suplier tidak tersedia.'); window.location = '" . base_url("peminjaman") . "';</script>";
        }
    }

    public function checkSupplierLoan($isAjax = 1, $idSupplier)
    {
        if ($isAjax > 0) {
            $idSupplier = $this->input->post("id_supplier");
        }
        $isLoan = false;
        if (strlen($idSupplier) > 0) {
            $q = "  SELECT * FROM tbl_supplier_loan WHERE id_branch = '{$this->branch}'
                    AND is_paid_off = 0 AND id_supplier = '{$idSupplier}'";
            $result = $this->db->query($q)->row_array();
            if ($result != NULL) {
                $isLoan = true;
            }
        }
        $arr = array('is_loan' => $isLoan);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $isLoan;
        }
    }

    public function checkSupplierLoanInstallment($isAjax = 1, $id)
    {
        if ($isAjax > 0) {
            $id = $this->input->post("id_supplier_loan");
        }

        $isInstallment = false;
        $getData = $this->M_supplier_loan_installment->selectByHeaderId($id);
        if ($getData != NULL) {
            $isInstallment = true;
        }

        $arr = array('is_loan_installment' => $isInstallment);

        if ($isAjax > 0) {
            echo json_encode($arr);
        } else {
            return $isInstallment;
        }
    }
}

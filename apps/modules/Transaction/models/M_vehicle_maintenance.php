<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author Susi Susanti
 * @since  May 24, 2019
 * @license http://piposystem.com Piposystem
 */
class M_vehicle_maintenance extends CI_Model
{
    const __tableName = 'tbl_vehicle_maintenance';
    const __tableId = 'id_vehicle_maintenance';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}'";
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY created_date DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}' AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }
}

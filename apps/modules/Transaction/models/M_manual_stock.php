<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class M_manual_stock extends CI_Model
{
    const __tableName = 'tbl_item_manual';
    const __tableId = 'id_item_manual';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0, $filter = array())
    {
//        $this->db->from(self::__tableName);
//        if ($isAjaxList > 0) {
//            $this->db->order_by("updated_date", "DESC");
//        }
//        $data = $this->db->get();
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}'";
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY created_date DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function code()
    {
        $this->db->select('RIGHT(' . self::__tableName . '.code,2) as code', FALSE);
        $this->db->where('id_branch', $this->branch);
        $this->db->order_by('code', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get(self::__tableName);  //cek dulu apakah ada sudah ada kode di tabel.    
        if ($query->num_rows() <> 0) {
            //cek kode jika telah tersedia    
            $data = $query->row();
            $kode = intval($data->code) + 1;
        } else {
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodetampil = "MI";
        if ($this->branch == 1) {
            $kodetampil .= "P";
        }
        $kodetampil .= "5" . $batas;  //format kode
        return $kodetampil;
    }
}

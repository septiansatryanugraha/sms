<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class M_purchase extends CI_Model
{
    const __tableName = 'tbl_transaction_purchase';
    const __tableId = 'id_transaction_purchase';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0, $filter = array())
    {
//        $this->db->from(self::__tableName);
//        if ($isAjaxList > 0) {
//            $this->db->order_by("updated_date", "DESC");
//        }
//        $data = $this->db->get();
        $idSupplier = $filter['id_supplier'];
        $isApproved = $filter['is_approved'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}'";
        if (strlen($idSupplier) > 0 && $idSupplier != 'all') {
            $sql .= " AND id_supplier = '{$idSupplier}'";
        }
        if (strlen($isApproved) > 0 && $isApproved != 'all') {
            if ($isApproved == 0) {
                $sql .= " AND is_approved = 0 AND nominal = 0 ";
            }
            if ($isApproved == 1) {
                $sql .= " AND is_approved = '{$isApproved}'";
            }
            if ($isApproved == 2) {
                $sql .= " AND is_approved = 0 AND nominal > 0 ";
            }
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY created_date DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}' AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function save($data)
    {
        $result = $this->db->insert('tbl_item_manual', $data);
        return $result;
    }

    //menampilkan histori last transaksi
    public function getData1($id)
    {
        $this->db->order_by('status', 'DESC');
        $hasil = $this->db->get_where(self::__tableName, array('id_branch' => $this->branch, 'id_supplier' => $id));
        $this->db->limit(0, 1);
        foreach (array_slice($hasil->result_array(), 0, 1) as $data) {
            $res1 .= "<option value='$data[status]'>$data[status]</option>";
        }

        return $res1;
    }

    public function getData2($id)
    {
        $this->db->order_by('cicilan', 'DESC');
        $hasil = $this->db->get_where(self::__tableName, array('id_branch' => $this->branch, 'id_supplier' => $id));
        foreach (array_slice($hasil->result_array(), 0, 1) as $data) {
            $res2 .= "<option value='$data[cicilan]'>$data[cicilan]</option>";
        }

        return $res2;
    }

    public function getData3($id)
    {
        $this->db->order_by('amount', 'DESC');
        $hasil = $this->db->get_where('tbl_supplier_loan', array('id_branch' => $this->branch, 'id_supplier' => $id));
        foreach (array_slice($hasil->result_array(), 0, 1) as $data) {
            $res3 .= "<b>$data[amount]</b>";
        }

        return $res3;
    }

    public function getData4($id)
    {
        $this->db->order_by('created_date', 'DESC');
        $hasil = $this->db->get_where('tbl_supplier_loan', array('id_branch' => $this->branch, 'id_supplier' => $id));
        foreach (array_slice($hasil->result_array(), 0, 1) as $data) {
            $tanggal = date('d-m-Y', strtotime($data[created_date]));
            $res4 .= "<option value='$tanggal'>$tanggal</option>";
        }

        return $res4;
    }

    public function selectDetailByHeaderId($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_detail WHERE " . self::__tableId . " = '{$id}'";
        return $this->db->query($sql)->result();
    }

    public function selectDetailById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_detail WHERE " . self::__tableId . "_detail = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectDetailByIdItem($id, $idItem)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_detail
                WHERE " . self::__tableId . " = '{$id}' AND id_item = '{$idItem}'";
        $data = $this->db->query($sql);
        return $data->row();
    }
}

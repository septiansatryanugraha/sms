<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
class M_sale extends CI_Model
{
    const __tableName = 'tbl_transaction_sales';
    const __tableId = 'id_transaction_sales';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0, $filter = array())
    {
//        $this->db->from(self::__tableName);
//        if ($isAjaxList > 0) {
//            $this->db->order_by("updated_date", "DESC");
//        }
//        $data = $this->db->get();
        $idCustomer = $filter['id_customer'];
        $isApproved = $filter['is_approved'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}'";
        if (strlen($idCustomer) > 0 && $idCustomer != 'all') {
            $sql .= " AND id_customer = '{$idCustomer}'";
        }
        if (strlen($isApproved) > 0 && $isApproved != 'all') {
            if ($isApproved == 0) {
                $sql .= " AND is_approved = 0 AND nominal = 0 ";
            }
            if ($isApproved == 1) {
                $sql .= " AND is_approved = '{$isApproved}'";
            }
            if ($isApproved == 2) {
                $sql .= " AND is_approved = 0 AND nominal > 0 ";
            }
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY created_date DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}' AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectDetailByHeaderId($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_detail WHERE " . self::__tableId . " = '{$id}'";
        return $this->db->query($sql)->result();
    }

    public function selectDetailById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_detail WHERE " . self::__tableId . "_detail = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectDetailByIdItem($id, $idItem)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_detail
                WHERE " . self::__tableId . " = '{$id}' AND id_item = '{$idItem}'";
        $data = $this->db->query($sql);
        return $data->row();
    }
}

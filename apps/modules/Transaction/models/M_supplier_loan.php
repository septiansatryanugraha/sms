<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author Susi Susanti
 * @since  Apr 27, 2019
 * @license http://piposystem.com Piposystem
 */
class M_supplier_loan extends CI_Model
{
    const __tableName = 'tbl_supplier_loan';
    const __tableId = 'id_supplier_loan';

    private $branch = '1';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->branch = isset($_SESSION['id_branch']) ? $_SESSION['id_branch'] : 1;
    }

    function getData($isAjaxList = 0, $filter = array())
    {
//        $this->db->from(self::__tableName);
//        if ($isAjaxList > 0) {
//            $this->db->order_by("updated_date", "DESC");
//        }
//        $data = $this->db->get();
        $idSupplier = $filter['id_supplier'];
        $isPaidOff = $filter['is_paid_off'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}'";
        if (strlen($idSupplier) > 0 && $idSupplier != 'all') {
            $sql .= " AND id_supplier = '{$idSupplier}'";
        }
        if (strlen($isPaidOff) > 0 && $isPaidOff != 'all') {
            $sql .= " AND is_paid_off = '{$isPaidOff}'";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY created_date DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}' AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function getSupplierLoan($idSupplier)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}' AND is_paid_off = 0 AND id_supplier = '{$idSupplier}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function getSupplierLoanList($idSupplier)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE id_branch = '{$this->branch}' AND is_paid_off = 0 AND id_supplier = '{$idSupplier}'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function getSupplierLoanInstallment($idSupplierLoan, $isPaid = 1)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_installment WHERE " . self::__tableId . " = '{$idSupplierLoan}'";
        if ($isPaid > 0) {
            $sql .= " AND status = '{$isPaid}'";
        }
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectByIdInstallment($idSupplierLoanInstallment)
    {
        $sql = "SELECT * FROM " . self::__tableName . "_installment WHERE " . self::__tableId . "_installment = '{$idSupplierLoanInstallment}'";
        $data = $this->db->query($sql);
        return $data->row();
    }
}

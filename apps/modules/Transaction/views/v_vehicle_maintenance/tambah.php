<?php
$this->load->view('_heading/_headerContent');
/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
?>
<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }
    .number_only {
        text-align: right;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-tambah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Armada</label>
                                        <div class="col-sm-7">
                                            <select name="id_vehicle" class="form-control select-vehicle" id="id_vehicle">
                                                <option></option>
                                                <?php foreach ($vehicle as $key => $value) { ?>
                                                    <option value="<?php echo $value->id_vehicle; ?>">
                                                        <?php echo $value->brand . ' - ' . $value->police_number; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tanggal Pemeliharaan</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="date_maintenance" class="form-control datepicker" id="date_maintenance" placeholder="Tanggal Pemeliharaan"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y') ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Harga Pemeliharaan</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="amount_maintenance" id="amount_maintenance" class="form-control number_only formatCurrency" placeholder="Harga Pemeliharaan"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Keterangan</label>
                                        <div class="col-sm-7">
                                            <textarea name="description" id="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-warning btn-flat"><i class="fa fa-retweet"></i> Bersihkan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $("#amount_maintenance").val(0);

        //Number Format
        $(document).on('keypress', '.number_only', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        //Format Currency
        $('.formatCurrency').on('focusin', function () {
            var x = $(this).val();
            $(this).val(accounting.unformat(x));
        });
        $('.formatCurrency').on('focusout', function () {
            var x = $(this).val();
            $(this).val(accounting.formatMoney(x));
        });

        $('#amount_maintenance').on('focusout', function () {
            var amount_maintenance = $("#amount_maintenance").val();
            if (amount_maintenance.length == 0) {
                $("#amount_maintenance").val(0);
            }
        });
    });

    $(function () {
        // untuk datetime date_rent
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        // untuk select2 ajak pilih menu
        $(".select-vehicle").select2({
            placeholder: " -- Pilih Kendaraan -- "
        });
    });

    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/VehicleMaintenance/prosesAdd'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

</script>

<?php
$this->load->view('_heading/_headerContent');
/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
?>
<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }
    .number_only {
        text-align: right;
    }
</style>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-tambah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Barang</label>
                                        <div class="col-sm-7">
                                            <select name="id_item" class="form-control select-item" id="id_item">
                                                <option></option>
                                                <?php foreach ($item as $key => $value) { ?>
                                                    <option value="<?php echo $value->id_item; ?>">
                                                        <?php echo $value->name; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($privilegeId == 1) { ?>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Manual </label>
                                            <div class="col-sm-5">
                                                <input type="text" name="date" class="form-control datepicker" id="date" placeholder="Tanggal Manual"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y') ?>">
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Jenis</label>
                                        <div class="col-sm-5">
                                            <select name="type" class="form-control select-type" id="type">
                                                <option></option>
                                                <option value="in">Masuk</option>
                                                <option value="out">Keluar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Jumlah Barang</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="qty" id="qty" class="form-control number_only" placeholder="Jumlah Barang"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Keterangan</label>
                                        <div class="col-sm-7">
                                            <textarea rows="3" cols="40" name="description" id="description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-warning btn-flat"><i class="fa fa-retweet"></i> Bersihkan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $("#qty").val(0);
        //Number Format
        $(document).on('keypress', '.number_only', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    });
    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/ManualStock/prosesAdd'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $(".select-item").select2({
            placeholder: " -- Pilih Barang -- "
        });
        $(".select-type").select2({
            placeholder: " -- Pilih Jenis -- "
        });
    });
</script>
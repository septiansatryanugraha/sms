<?php $this->load->view('_heading/_headerContent') ?>
<!-- style loading -->
<style>
    .number_only {
        text-align: right;
    }
    .gaya {
        position:absolute;
        margin-left : -20px;
        margin-top : 4px;
    }
</style>
<div class="loading2"></div>
<!-- -->
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-ubah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kode Penjualan </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="code" name="code" class="form-control" placeholder="Kode"  aria-describedby="sizing-addon2" value="<?php echo $dataSale->code; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Customer </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="customer" name="customer" class="form-control" placeholder="Customer"  aria-describedby="sizing-addon2" value="<?php echo $namaCustomer; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Penjualan </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="date" name="date" class="form-control" placeholder="Tanggal" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Jumlah Drum </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="jml_drum" name="jml_drum" class="form-control number_only" placeholder="Jumlah Drum"  aria-describedby="sizing-addon2" value="<?php echo $jmlDrum; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"><?php echo $dataSale->description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Harga per Drum </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="harga_per_drum" name="harga_per_drum" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Drum"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="sub-total" style="display: none;">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Sub Total</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="sub_total" name="sub_total" class="form-control number_only" placeholder="Sub Total" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_ppn" name="is_ppn" value="1" onclick="isPpn();"/>PPN <?php echo ($ppn * 100) ?> % ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="div-ppn" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="ppn" name="ppn" class="form-control number_only" placeholder="PPN <?php echo ($ppn * 100) ?> %"  aria-describedby="sizing-addon2" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grand_total" name="grand_total" class="form-control number_only formatCurrency" placeholder="Grand Total"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer pull-right">
                                <button id="conf_simpan" type="button" data-toggle="modal" data-target="#exampleModal"  class="btn btn-success btn-flat approve"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php //<editor-fold defaultstate="collapsed" desc="Modal Confirmation">?>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="col-md-12 well">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h3 style="display:block; text-align:center;">Konfirmasi Penjualan</h3>
                <div class="box box-body">
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Penjualan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_code"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Customer </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_customer"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Penjualan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_date"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Jumlah Drum </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_jml_drum"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_description"></label></div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_drum"></label></div>
                                    </div>
                                    <div class="c-sub-total" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Sub Total </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_sub_total" name="sub_total"></label></div>
                                        </div>
                                    </div>
                                    <div class="div-ppn" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_ppn" name="ppn"></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                                        <div class="col-sm-3">: <label for="inputEmail3" class="control-label" id="c_grand_total"></label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div id="btn_loading"></div>
                    <div id="buka">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" name="simpan" id="simpan">Konfirmasi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //</editor-fold> ?>

<script type="text/javascript">
    var isCheckedPpn = <?php echo $isPpn; ?>;

    $(document).ready(function () {
        $("#code").val('<?php echo $dataSale->code; ?>');
        $("#c_code").html('<?php echo $dataSale->code; ?>');
        $("#customer").val('<?php echo $namaCustomer; ?>');
        $("#c_customer").html('<?php echo $namaCustomer; ?>');
        $("#date").val('<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>');
        $("#c_date").html('<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>');

        $("#jml_drum").val('<?php echo $jmlDrum; ?>');
        $("#c_jml_drum").html('<?php echo $jmlDrum; ?>');
        $("#harga_per_drum").val(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));
//        $("#date_paid").val('<?php // echo date('d-m-Y')                 ?>');

        if (isCheckedPpn > 0) {
            document.getElementById("is_ppn").checked = true;
        }

        hitungGrandTotal();
    });

    // ===== Action PPN ===== //
    function isPpn() {
        var is_ppn = document.getElementById("is_ppn");
        if (is_ppn.checked == true) {
            $(".div-ppn").fadeIn();
            $(".sub-total").fadeIn('slow');
            $(".c-sub-total").fadeIn('slow');
        } else {
            $(".div-ppn").fadeOut();
            $(".sub-total").fadeOut('slow');
            $(".c-sub-total").fadeOut('slow');
        }
        hitungGrandTotal();
    }

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var jml_drum = $("#jml_drum").val();
        var ppn = 0;
        var is_ppn = document.getElementById("is_ppn");

        if (jml_drum.length == 0) {
            jml_drum = 0;
        }
        var harga_per_drum = $("#harga_per_drum").val();
        if (harga_per_drum.length == 0) {
            harga_per_drum = 0;
        }
        harga_per_drum = accounting.unformat(harga_per_drum);
        var grand_total = jml_drum * harga_per_drum;

        if (is_ppn.checked == true) {
            $("#sub_total").val(accounting.formatMoney(grand_total));
            $("#c_sub_total").html(accounting.formatMoney(grand_total));

            ppn = <?php echo $ppn; ?>;
            var total_ppn = ppn * grand_total;
            grand_total = grand_total + (total_ppn);
            $("#ppn").val(accounting.formatMoney(total_ppn));
        }

        $("#grand_total").val(accounting.formatMoney(grand_total));
        $("#c_grand_total").html(accounting.formatMoney(grand_total));
    }

    $('.hitungGrandTotal').on('focusout', function () {
        hitungGrandTotal();
    });

    $('.hitungGrandTotal').on('keyup', function () {
        hitungGrandTotal();
    });

    // ===== Confirmation ===== //
    $("#conf_simpan").click(function () {
        $("#c_ppn").html($("#ppn").val());
        $("#c_description").html($("#description").val());
        $("#c_harga_per_drum").html($("#harga_per_drum").val());
        //        $("#c_date_paid").html($("#date_paid").val());
    });

    // ===== Proses Controller logic ajax ===== //
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/Sale/prosesConfirm/' . $idTransactionSales); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-ubah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo site_url('Transaction/Sale'); ?>'", 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    // ===== Open Modal Confirmation Approve ===== //
    $(document).on("click", ".approve", function () {
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded

    });

    // ===== Datepicker ===== //
//    $(function () {
//        $("#date_paid").datepicker({
//            orientation: "left",
//            autoclose: !0,
//            format: 'dd-mm-yyyy'
//        })
//    });
</script>
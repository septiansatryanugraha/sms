<?php $this->load->view('_heading/_headerContent') ?>
<!-- style loading -->
<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 50%;
    }
</style>
<div class="loading2"></div>
<!-- -->
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-ubah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kode Penjualan </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="code" name="code" class="form-control" placeholder="Kode"  aria-describedby="sizing-addon2" value="<?php echo $dataSale->code; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Customer </label>
                                        <div class="col-sm-7">
                                            <select name="id_customer" id="id_customer" class="form-control select-customer" aria-describedby="sizing-addon2">
                                                <?php foreach ($customer as $data) { ?>
                                                    <option></option>
                                                    <?php
                                                    $selected = "";
                                                    if ($data->id_customer == $dataSale->id_customer) {
                                                        $selected = 'selected';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $data->id_customer; ?>" <?php echo $selected; ?>>
                                                        <?php echo $data->name; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Penjualan </label>
                                        <div class="col-sm-5">
                                            <?php if ($privilegeId == 1) { ?>
                                                <input type="text" name="date" class="form-control datepicker" id="date" placeholder="Tanggal Penjualan"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>">
                                            <?php } else { ?>
                                                <input type="text" class="form-control" placeholder="Tanggal Penjualan" aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>" disabled>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Berat Oli (Kg) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="weight" name="weight" class="form-control number_decimal hitungBerat" placeholder="Berat"  aria-describedby="sizing-addon2" value="<?php echo $weight; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Berat Jenis Oli </label>
                                        <div class="col-sm-5">
                                            <input type="text" style="text-align: right;" id="specific_gravity" name="specific_gravity" class="form-control number_decimal hitungBerat" placeholder="Berat Jenis"  aria-describedby="sizing-addon2" value="<?php echo $specificGravity; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kadar Air (%) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="water_content" name="water_content" class="form-control number_decimal hitungBerat" placeholder="Kadar Air"  aria-describedby="sizing-addon2" value="<?php echo $waterContent; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-7">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_drum" name="is_drum" value="1" onclick="isDrum();"/>dengan drum ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Catatan Oli</label>
                                        <div class="col-sm-7">
                                            <textarea id="description_oli" name="description_oli" class="form-control" placeholder="Catatan Oli"  aria-describedby="sizing-addon2"><?php echo $descriptionOli; ?></textarea>
                                        </div>
                                    </div>
                                    <div id="menu_no_drum" class="menu-no-drum" style="display:block">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Kemasan / Liter </label>
                                            <div class="col-sm-7">
                                                <input type="text" id="jml_kemasan" name="jml_kemasan" class="form-control jml-style number_only hitungGrandTotal" placeholder="Jumlah Kemasan"  aria-describedby="sizing-addon2">
                                                <span style="width: 5%;">@</span>
                                                <input type="text" id="liter_per_kemasan" name="liter_per_kemasan" class="form-control liter-style number_decimal hitungGrandTotal" placeholder="Liter per Kemasan" aria-describedby="sizing-addon2">
                                                <span style="width: 5%;">Liter</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu_drum" class="menu-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Catatan Drum </label>
                                            <div class="col-sm-7">
                                                <textarea id="description_drum" name="description_drum" class="form-control" placeholder="Catatan Drum"  aria-describedby="sizing-addon2"><?php echo $descriptionDrum; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Drum / Liter </label>
                                            <div class="col-sm-7">
                                                <input type="text" id="jml_drum" name="jml_drum" class="form-control jml-style number_only hitungGrandTotal" placeholder="Jumlah Drum"  aria-describedby="sizing-addon2">
                                                <span style="width: 5%;">@</span>
                                                <input type="text" id="liter_per_drum" name="liter_per_drum" class="form-control liter-style number_only hitungGrandTotal" placeholder="Liter per Drum" aria-describedby="sizing-addon2">
                                                <span style="width: 5%;">Liter</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Total Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="total_liter" name="total_liter" class="form-control number_decimal" placeholder="pembayaran drum"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Penambahan Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="penambahan_liter" name="penambahan_liter" class="form-control number_only hitungGrandTotal" placeholder="Penambahan Liter"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Penyusutan Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="penyusutan_liter" name="penyusutan_liter" class="form-control number_only hitungGrandTotal" placeholder="Penyusutan Liter"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grandtotal_liter" name="grandtotal_liter" class="form-control number_decimal" placeholder="Grand Total Liter"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-7">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_additional_drum" name="is_additional_drum" value="1" onclick="isAdditionalDrum();"/>penambahan jumlah drum ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div id="additional_drum" class="additional-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Penambahan Drum </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="penambahan_drum" name="penambahan_drum" class="form-control number_only" placeholder="Penambahan Drum"  aria-describedby="sizing-addon2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"><?php echo $dataSale->description; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;

    $(document).ready(function () {
        $("#code").val('<?php echo $dataSale->code; ?>');
        $("#customer").val('<?php echo $namaCustomer; ?>');
        $("#weight").val('<?php echo $weight; ?>');
        $("#specific_gravity").val('<?php echo $specificGravity; ?>');
        $("#water_content").val('<?php echo $waterContent; ?>');

        var menu_drum = document.getElementById("menu_drum");
        var menu_no_drum = document.getElementById("menu_no_drum");
        if (isCheckedDrum > 0) {
            document.getElementById("is_drum").checked = true;
            menu_drum.style.display = "block";
            menu_no_drum.style.display = "none";
            $("#description_drum").val('<?php echo $descriptionDrum; ?>');
            $("#jml_drum").val('<?php echo $jmlDrum; ?>');
            $("#liter_per_drum").val('<?php echo $literPerDrum; ?>');
        } else {
            document.getElementById("is_drum").checked = false;
            menu_no_drum.style.display = "block";
            menu_drum.style.display = "none";
            $("#description_oli").val('<?php echo $descriptionOli; ?>');
            $("#jml_kemasan").val('<?php echo $jmlKemasan; ?>');
            $("#liter_per_kemasan").val('<?php echo $literPerKemasan; ?>');
        }
        $("#penambahan_liter").val('<?php echo $penambahanLiter; ?>');
        $("#penyusutan_liter").val('<?php echo $penyusutanLiter; ?>');
        $("#total_liter").val('<?php echo $totalLiter; ?>');
        $("#grandtotal_liter").val('<?php echo $grandTotalLiter; ?>');

        var additional_drum = document.getElementById("additional_drum");
        if (isCheckedAdditionalDrum > 0) {
            document.getElementById("is_additional_drum").checked = true;
            additional_drum.style.display = "block";
            $("#penambahan_drum").val(<?php echo $penambahanDrum; ?>);
        } else {
            document.getElementById("is_additional_drum").checked = false;
            additional_drum.style.display = "none";
        }

        hitungGrandTotal();
    });

    // ===== Action Checkbox Kemasan Drum ===== //
    function isDrum() {
        var is_drum = document.getElementById("is_drum");
        var menu_drum = document.getElementById("menu_drum");
        var menu_no_drum = document.getElementById("menu_no_drum");
        if (is_drum.checked == true) {
            menu_drum.style.display = "block";
            menu_no_drum.style.display = "none";
            $("#jml_drum").val(1);
            $("#liter_per_drum").val(210);
        } else {
            menu_no_drum.style.display = "block";
            menu_drum.style.display = "none";
            $("#jml_kemasan").val(1);
            var weight = $("#weight").val();
            var specific_gravity = $("#specific_gravity").val();
            weight = weight.replace(",", ".")
            specific_gravity = specific_gravity.replace(",", ".")
            if (weight > 0 && specific_gravity > 0) {
                hitungBerat();
            } else {
                $("#liter_per_kemasan").val(0);
            }
        }
        $("#penambahan_liter").val(0);
        $("#penyusutan_liter").val(0);
        hitungGrandTotal();
        hitungGrandTotal();
    }

    // ===== Action Checkbox Additional Drum ===== //
    function isAdditionalDrum() {
        var is_additional_drum = document.getElementById("is_additional_drum");
        var additional_drum = document.getElementById("additional_drum");
        if (is_additional_drum.checked == true) {
            additional_drum.style.display = "block";
        } else {
            additional_drum.style.display = "none";
        }
        $("#penambahan_drum").val(0);
    }

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var grandtotal_liter = 0;
        var is_drum = document.getElementById("is_drum");
        if (is_drum.checked == true) {
            var jml_drum = $("#jml_drum").val();
            if (jml_drum.length == 0) {
                jml_drum = 1;
            }
            var liter_per_drum = $("#liter_per_drum").val();
            if (liter_per_drum.length == 0) {
                liter_per_drum = 0;
            }
            var total_liter = jml_drum * liter_per_drum;
        } else {
            var jml_kemasan = $("#jml_kemasan").val();
            if (jml_kemasan.length == 0) {
                jml_kemasan = 1;
            }
            var liter_per_kemasan = $("#liter_per_kemasan").val();
            var liter_per_kemasan_coma = liter_per_kemasan.split(',').length;
            if (liter_per_kemasan.length == 0) {
                liter_per_kemasan = 0;
            }
            if (liter_per_kemasan_coma > 2) {
                liter_per_kemasan = 0;
            } else {
                if (liter_per_kemasan_coma == 2) {
                    liter_per_kemasan = liter_per_kemasan.replace(",", ".");
                }
            }
            var total_liter = (jml_kemasan * liter_per_kemasan);
            if (total_liter % 1 !== 0) {
                total_liter = parseFloat(total_liter).toFixed(2);
                total_liter = total_liter.replace(".", ",");
            }
        }
        var penambahan_liter = $("#penambahan_liter").val();
        if (penambahan_liter.length == 0) {
            penambahan_liter = 0;
        }
        var penyusutan_liter = $("#penyusutan_liter").val();
        if (penyusutan_liter.length == 0) {
            penyusutan_liter = 0;
        }
        grandtotal_liter = total_liter + parseInt(penambahan_liter) - parseInt(penyusutan_liter);
        if (grandtotal_liter % 1 !== 0) {
            grandtotal_liter = parseFloat(grandtotal_liter).toFixed(2);
            grandtotal_liter = grandtotal_liter.replace(".", ",");
        }
        $("#total_liter").val(total_liter);
        $("#grandtotal_liter").val(grandtotal_liter);
    }

    $('.hitungGrandTotal').on('focusout', function () {
        hitungGrandTotal();
    });

    $('.hitungGrandTotal').on('keyup', function () {
        hitungGrandTotal();
    });

    // ===== Proses Hitung Berat ===== //
    function hitungBerat() {
        var is_drum = document.getElementById("is_drum");
        if (is_drum.checked == false) {
            var liter = 0;
            var total_liter = 0;
            var grandtotal_liter = 0;
            var weight = $("#weight").val();
            var weight_comma = weight.split(',').length;
            var specific_gravity = $("#specific_gravity").val();
            var specific_gravity_comma = specific_gravity.split(',').length;

            if (weight.length == 0) {
                weight = 0;
            }
            if (weight_comma > 2) {
                weight = 0;
            } else {
                if (weight_comma == 2) {
                    weight = weight.replace(",", ".");
                }
            }
            if (specific_gravity.length == 0) {
                specific_gravity = 0;
            }
            if (specific_gravity_comma > 2) {
                specific_gravity = 0;
            } else {
                if (specific_gravity_comma == 2) {
                    specific_gravity = specific_gravity.replace(",", ".");
                }
            }
            if (weight > 0 && specific_gravity > 0) {
                var jml_kemasan = $("#jml_kemasan").val();
                if (jml_kemasan.length == 0) {
                    jml_kemasan = 0;
                }
                liter = Math.round(weight / specific_gravity);
                total_liter = Math.round(jml_kemasan * liter);
                grandtotal_liter = total_liter + parseInt(penambahan_liter) - parseInt(penyusutan_liter);
                if (liter % 1 !== 0) {
                    liter = parseFloat(liter).toFixed(2);
                    liter = liter.replace(".", ",");
                }
                if (total_liter % 1 !== 0) {
                    total_liter = parseFloat(total_liter).toFixed(2);
                    total_liter = total_liter.replace(".", ",");
                }
                if (grandtotal_liter % 1 !== 0) {
                    grandtotal_liter = parseFloat(grandtotal_liter).toFixed(2);
                    grandtotal_liter = grandtotal_liter.replace(".", ",");
                }
                $("#liter_per_kemasan").val(liter);
                $("#total_liter").val(total_liter);
                $("#grandtotal_liter").val(grandtotal_liter);
            }
        }
    }

    $('.hitungBerat').on('focusout', function () {
        hitungBerat();
    });

    $('.hitungBerat').on('keyup', function () {
        hitungBerat();
    });

    // ===== Proses Controller logic ajax ===== //
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/Sale/prosesUpdate/' . $idTransactionSales); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-ubah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo site_url('Transaction/Sale'); ?>'", 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        // ===== untuk select2 ajak pilih tipe ===== //
        $(".select-customer").select2({
            placeholder: " -- pilih customer -- "
        });
    });
</script>
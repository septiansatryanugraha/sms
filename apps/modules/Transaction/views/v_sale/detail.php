<?php
$paid = "-";
if (strlen($dataSale->paid) > 0) {
    $paid = date('d-m-Y', strtotime($dataSale->paid));
}

?>
<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 50%;
    }
</style>
<div class="col-md-12 well">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;">
        <i class="fa fa-location-arrow"></i> <?php echo $judul; ?>
    </h3>
    <div class="col-sm-12">
        <form class="form-horizontal" id="form-detail" method="POST">
            <div class="box box-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="box-header with-border no-padding">
                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Penjualan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="code"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Customer </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="customer"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Penjualan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="date"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Bayar </label>
                        <div class="col-sm-7">
                            <?php if ($isApprove > 0) { ?>
                                <input type="text" name="paid" class="form-control datepicker" id="paid" placeholder="Tanggal Bayar" style="border-color: red;" aria-describedby="sizing-addon2">
                            <?php } else { ?>
                                : <label for="inputEmail3" class="control-label" id="label_paid" name="label_paid"></label>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Oli (Kg) </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="weight"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Jenis Oli </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="specific_gravity"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Kadar Air (%) </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="water_content"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Catatan Oli</label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="description_oli"><?php echo $descriptionOli; ?></label></div>
                    </div>
                    <div id="menu_no_drum" class="menu-no-drum" style="display:block">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Kemasan / Liter </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="jml_kemasan"></label> <b>@</b> <label for="inputEmail3" class="control-label" id="liter_per_kemasan"></label> <b>Liter</b></div>
                        </div>
                    </div>
                    <div id="menu_drum" class="menu-drum" style="display:none">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Catatan Drum </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="description_drum"><?php echo $descriptionDrum; ?></label></div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Drum / Liter </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="jml_drum"></label> <b>@</b> <label for="inputEmail3" class="control-label" id="liter_per_drum"></label> <b>Liter</b></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Total Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="total_liter"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="penambahan_liter"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Penyusutan Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="penyusutan_liter"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="grandtotal_liter"></label></div>
                    </div>
                    <div id="additional_drum" class="additional-drum" style="display:none">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Drum </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="penambahan_drum"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="description"><?php echo $dataSale->description; ?></label></div>
                    </div>
                    <div class="form-group"><div></div></div>
                </div>
                <div class="col-sm-6">
                    <div class="is-approved" style="display:none">
                        <div class="form-group">
                            <div class="box-header with-border no-padding">
                                <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                            </div>
                        </div>
                        <div class="is-approved" style="display:none">
                            <div id="harga_drum" class="harga-drum" style="display:none">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="harga_per_drum"></label></div>
                                </div>
                            </div>
                            <div id="div_harga_per_liter">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">Harga per Liter </label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="harga_per_liter"></label></div>
                                </div>
                            </div>
                            <div id="div_harga_per_kilogram" style="display:none">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">Harga per Kg </label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="harga_per_kilogram"></label></div>
                                </div>
                            </div>
                            <?php if ($ppn > 0) { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">Subtotal </label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="total" name="total"></label></div>
                                </div>
                                <div class="form-group">  
                                    <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="total_ppn" name="total_ppn"></label></div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                                <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="grand_total"></label></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="text-right">
            <button class="btn btn-danger" data-dismiss="modal"> Close</button>
            <?php if ($isCancel > 0) { ?>
                <button type="button" class="btn btn-danger cancel-penjualan" data-id="<?php echo $dataSale->id_transaction_sales; ?>"><i class="fa fa-close"></i> &nbsp;Batalkan Konfirmasi</button>
            <?php } ?>
            <?php if ($isApprove > 0) { ?>
                <button type="button" class="btn btn-danger approve-penjualan" data-id="<?php echo $dataSale->id_transaction_sales; ?>"><i class="fa fa-check-square-o"></i> &nbsp;Approve</button>
            <?php } ?>
            <?php if ($isDelete > 0) { ?>
                <button type="button" class="btn btn-danger hapus-penjualan" data-id="<?php echo $dataSale->id_transaction_sales; ?>"><i class="fa fa-trash"></i> &nbsp;Hapus</button>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    var is_approved = <?php echo $isApproved; ?>;
    var tipe_harga = '<?php echo $type; ?>';
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;

    $(document).ready(function () {
        var nominal = <?php echo $dataSale->nominal; ?>;
        $(".is-approved").fadeOut();
        if (nominal > 0) {
            $(".is-approved").fadeIn();
        }

        $("#code").html('<?php echo $dataSale->code; ?>');
        $("#customer").html('<?php echo $namaCustomer; ?>');
        $("#date").html('<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>');
        $("#label_paid").html('<?php echo $paid; ?>');
        $("#weight").html('<?php echo $weight; ?>');
        $("#specific_gravity").html('<?php echo $specificGravity; ?>');
        $("#water_content").html('<?php echo $waterContent; ?>');
//        $("#description").html('<?php // echo $dataSale->description; ?>');

        $("#total_liter").html('<?php echo $totalLiter; ?>');
        $("#penambahan_liter").html('<?php echo $penambahanLiter; ?>');
        $("#penyusutan_liter").html('<?php echo $penyusutanLiter; ?>');
        $("#grandtotal_liter").html('<?php echo $grandTotalLiter; ?>');
        $("#penambahan_drum").html('<?php echo $penambahanDrum; ?>');

        var menu_drum = document.getElementById("menu_drum");
        var menu_no_drum = document.getElementById("menu_no_drum");
        if (isCheckedDrum > 0) {
            menu_drum.style.display = "block";
            menu_no_drum.style.display = "none";
            $("#jml_drum").html('<?php echo $jmlDrum; ?>');
            $("#liter_per_drum").html('<?php echo $literPerDrum; ?>');
        } else {
            menu_no_drum.style.display = "block";
            menu_drum.style.display = "none";
            $("#jml_kemasan").html('<?php echo $jmlKemasan; ?>');
            $("#liter_per_kemasan").html('<?php echo $literPerKemasan; ?>');
        }

        var additional_drum = document.getElementById("additional_drum");
        if (isCheckedAdditionalDrum > 0) {
            additional_drum.style.display = "block";
            $("#penambahan_drum").html(<?php echo $penambahanDrum; ?>);
        } else {
            additional_drum.style.display = "none";
        }

        var harga_drum = document.getElementById("harga_drum");
        if (isCheckedDrum > 0 || isCheckedAdditionalDrum > 0) {
            harga_drum.style.display = "block";
            $("#harga_per_drum").html(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));
        }

        var div_harga_per_kilogram = document.getElementById("div_harga_per_kilogram");
        var div_harga_per_liter = document.getElementById("div_harga_per_liter");
        if (tipe_harga == 'liter') {
            $("#harga_per_liter").html(accounting.formatMoney('<?php echo $hargaPerLiter; ?>'));
            div_harga_per_liter.style.display = "block";
            div_harga_per_kilogram.style.display = "none";
        } else if (tipe_harga == 'kilogram') {
            $("#harga_per_kilogram").html(accounting.formatMoney('<?php echo $hargaPerKilogram; ?>'));
            div_harga_per_kilogram.style.display = "block";
            div_harga_per_liter.style.display = "none";
        }

        $("#total").html(accounting.formatMoney('<?php echo $price; ?>'));
        $("#total_ppn").html(accounting.formatMoney('<?php echo $totalPpn; ?>'));

        hitungGrandTotal();
    });

    //Hitung Total
    function hitungGrandTotal() {
        var grand_total = parseFloat('<?php echo $gandTotal; ?>');
        var total_drum = 0;
        var penambahan_drum = 0;
        var harga_per_drum = 0;

        var harga_per_liter = $("#harga_per_liter").html();
        if (harga_per_liter.length == 0) {
            harga_per_liter = 0;
        }
        harga_per_liter = accounting.unformat(harga_per_liter);
        var harga_per_kilogram = $("#harga_per_kilogram").html();
        if (harga_per_kilogram.length == 0) {
            harga_per_kilogram = 0;
        }
        harga_per_kilogram = accounting.unformat(harga_per_kilogram);
        var total_liter_label = 0;
        if (isCheckedDrum > 0) {
            var jml_drum = $("#jml_drum").html();
            if (jml_drum.length == 0) {
                jml_drum = 0;
            }
            var liter_per_drum = $("#liter_per_drum").html();
            if (liter_per_drum.length == 0) {
                liter_per_drum = 0;
            }
            var total_liter = jml_drum * liter_per_drum;
            total_liter_label = total_liter;
            var total_drum = jml_drum;
        } else {
            var jml_kemasan = $("#jml_kemasan").html();
            if (jml_kemasan.length == 0) {
                jml_kemasan = 0;
            }
            var liter_per_kemasan = $("#liter_per_kemasan").html();
            var liter_per_kemasan_coma = liter_per_kemasan.split(',').length;
            if (liter_per_kemasan.length == 0) {
                liter_per_kemasan = 0;
            }
            if (liter_per_kemasan_coma == 2) {
                liter_per_kemasan = liter_per_kemasan.replace(",", ".");
            }
            var total_liter = jml_kemasan * liter_per_kemasan;
            total_liter_label = total_liter;

            if (total_liter % 1 !== 0) {
                total_liter = parseFloat(total_liter).toFixed(2);
                total_liter_label = total_liter.replace(".", ",");
            }
        }
        var penambahan_liter = $("#penambahan_liter").html();
        if (penambahan_liter.length == 0) {
            penambahan_liter = 0;
        }
        var penyusutan_liter = $("#penyusutan_liter").html();
        if (penyusutan_liter.length == 0) {
            penyusutan_liter = 0;
        }
        if (isCheckedAdditionalDrum > 0) {
            var penambahan_drum = $("#penambahan_drum").html();
            if (penambahan_drum.length == 0) {
                penambahan_drum = 0;
            }
        }
        if (isCheckedAdditionalDrum > 0 || isCheckedDrum > 0) {
            harga_per_drum = $("#harga_per_drum").html();
            if (harga_per_drum.length == 0) {
                harga_per_drum = 0;
            }
            harga_per_drum = accounting.unformat(harga_per_drum);
        }
        $("#total_liter").html(total_liter_label);
        $("#grand_total").html(accounting.formatMoney(grand_total));
    }

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $('.datepicker').attr('readonly', 'readonly');
        $('.datepicker').css('background-color', '#fff', 'important');
    });

</script>
<?php
$paid = "-";
if (strlen($dataSale->paid) > 0) {
    $paid = date('d-m-Y', strtotime($dataSale->paid));
}
?>
<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 50%;
    }
</style>
<div class="col-md-12 well">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;">
        <i class="fa fa-location-arrow"></i> <?php echo $judul; ?>
    </h3>
    <div class="col-sm-12">
        <form class="form-horizontal" id="form-detail" method="POST">
            <div class="box box-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="box-header with-border no-padding">
                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Penjualan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="code"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Customer </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="customer"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Penjualan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="date"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Bayar </label>
                        <div class="col-sm-7">
                            <?php if ($isApprove > 0) { ?>
                                <input type="text" name="paid" class="form-control datepicker" id="paid" placeholder="Tanggal Bayar" style="border-color: red;" aria-describedby="sizing-addon2">
                            <?php } else { ?>
                                : <label for="inputEmail3" class="control-label" id="label_paid" name="label_paid"></label>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Jumlah Drum </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="jml_drum"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="description"><?php echo $dataSale->description; ?></label></div>
                    </div>
                    <div class="form-group"><div></div></div>
                </div>
                <div class="col-sm-6">
                    <div class="is-approved" style="display:none">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="harga_per_drum"></label></div>
                        </div>
                        <?php if ($ppn > 0) { ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Subtotal </label>
                                <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="total" name="total"></label></div>
                            </div>
                            <div class="form-group">  
                                <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="total_ppn" name="total_ppn"></label></div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="grand_total"></label></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="text-right">
            <button class="btn btn-danger" data-dismiss="modal"> Close</button>
            <?php if ($isCancel > 0) { ?>
                <button type="button" class="btn btn-danger cancel-penjualan" data-id="<?php echo $dataSale->id_transaction_sales; ?>"><i class="fa fa-close"></i> &nbsp;Batalkan Konfirmasi</button>
            <?php } ?>
            <?php if ($isApprove > 0) { ?>
                <button type="button" class="btn btn-danger approve-penjualan" data-id="<?php echo $dataSale->id_transaction_sales; ?>"><i class="fa fa-check-square-o"></i> &nbsp;Approve</button>
            <?php } ?>
            <?php if ($isDelete > 0) { ?>
                <button type="button" class="btn btn-danger hapus-penjualan" data-id="<?php echo $dataSale->id_transaction_sales; ?>"><i class="fa fa-trash"></i> &nbsp;Hapus</button>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    var is_approved = <?php echo $isApproved; ?>;
    var tipe_harga = '<?php echo $type; ?>';
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;

    $(document).ready(function () {
        var nominal = <?php echo $dataSale->nominal; ?>;
        $(".is-approved").fadeOut();
        if (nominal > 0) {
            $(".is-approved").fadeIn();
        }

        $("#code").html('<?php echo $dataSale->code; ?>');
        $("#customer").html('<?php echo $namaCustomer; ?>');
        $("#date").html('<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>');
        $("#label_paid").html('<?php echo $paid; ?>');
        $("#jml_drum").html('<?php echo $jmlDrum; ?>');
        $("#harga_per_drum").html(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));

        $("#total").html(accounting.formatMoney('<?php echo $price; ?>'));
        $("#total_ppn").html(accounting.formatMoney('<?php echo $totalPpn; ?>'));
        $("#grand_total").html(accounting.formatMoney(parseFloat('<?php echo $gandTotal; ?>')));
    });

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $('.datepicker').attr('readonly', 'readonly');
        $('.datepicker').css('background-color', '#fff', 'important');
    });

</script>
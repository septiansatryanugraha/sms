<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-12" style="margin-left: -15px; margin-top:-5px;">
                <?php if ($accessAdd > 0) { ?>
                    <div class="box-header with-border">
                        <button class="btn btn-success dropdown-toggle" id="menu1" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <li role=""><a role="menuitem" tabindex="-1" href="<?php echo site_url('add-' . $menuName); ?>"><i class="glyphicon glyphicon-plus-sign"></i><b> Tambah ( Oli & Drum )</b></a></li>
                            <li role=""><a role="menuitem" tabindex="-1" href="<?php echo site_url('add-' . $menuName . '-custom'); ?>"><i class="glyphicon glyphicon-plus-sign"></i><b> Tambah ( Hanya Drum )</b></a></li>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <br><br><br>
            <div class="search-form" style="">
                <div class="form-group ">
                    <label class="control-label">Filter</label>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Customer</label>
                    <div class="col-sm-3">
                        <select name="id_customer" id="id_customer" class="form-control select-customer" aria-describedby="sizing-addon2">
                            <option value="all">Semua Customer</option>
                            <?php foreach ($customer as $data) { ?>
                                <option value="<?php echo $data->id_customer; ?>"><?php echo $data->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Status Penjualan</label>
                    <div class="col-sm-2">
                        <select name="is_approved" class="form-control is-approved" id="is_approved" aria-describedby="sizing-addon2">
                            <option value="all">Semua Status</option>
                            <option value="0">Pending</option>
                            <option value="2">Konfirmasi</option>
                            <option value="1">Approved</option>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Tanggal Penjualan</label>
                    <div class="col-sm-6">
                        <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                        <span>s/d</span>
                        <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group">  
                    <label class="col-sm-2 control-label"></label>       
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="all_date" name="all_date"/>Semua Tanggal
                        </label>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="box-footer">
                    <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Tampilkan</button>
                </div>
                <div class="box-footer"><br></div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="table_data" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="100px">Kode</th>
                            <th>Customer</th>
                            <th width="100px">Item</th>
                            <th>Grandtotal</th>
                            <th>Status</th>
                            <th>Di Buat</th>
                            <th>Di Ubah</th>
                            <th width="50px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <br><br><br>
                <br><br><br>
            </div>
        </div>
        <div id="tempat-modal"></div>
    </div>
</section>

<script type="text/javascript">
    // untuk datepicker
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    //untuk load data table ajax	
    var save_method; //for save method string
    var table_data;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var id_customer = $("#id_customer").val();
        var is_approved = $("#is_approved").val();
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        var all_date = 0;
        if (document.getElementById("all_date").checked == true) {
            all_date = 1;
        }

        table_data = $('#table_data').DataTable({
            "aLengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "pageLength": 25,
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
            // "scrollX": true,
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "No data found in the server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('Transaction/Sale/ajaxList') ?>",
                "type": "POST",
                data: {id_customer: id_customer, is_approved: is_approved, tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir, all_date: all_date},
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table_data.destroy();
        reloadTable();
    });

    $(document).on("click", ".hapus-penjualan", function () {
        var id_transaction_sales = $(this).attr("data-id");

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").html("<button type='button' class='btn btn-danger hapus-penjualan' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button>");
                },
                method: "POST",
                url: "<?php echo base_url('Transaction/Sale/prosesDelete'); ?>",
                data: "id_transaction_sales=" + id_transaction_sales,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == 'berhasil') {
                        $("tr[data-id='" + id_transaction_sales + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        $('#detail-sale').modal('hide');
                        hapus_berhasil();
                        table_data.destroy();
                        reloadTable();
                        setTimeout("window.location='<?php echo site_url('Transaction/Sale'); ?>'", 450);
                    } else {
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        swal("Peringatan", result.status, "warning");
                    }
                }
            });
        });
    });

    $(document).on("click", ".approve-penjualan", function () {
        var id_transaction_sales = $(this).attr("data-id");
        var paid = $('#paid').val();

        swal({
            title: "Approve Data?",
            text: "Yakin anda akan meng-approve data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Approve",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").html("<button type='button' class='btn btn-danger approve-penjualan' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button>");
                },
                method: "POST",
                url: "<?php echo base_url('Transaction/Sale/prosesApprove'); ?>",
                data: "id_transaction_sales=" + id_transaction_sales + "&paid=" + paid,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == 'berhasil') {
                        $("tr[data-id='" + id_transaction_sales + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        $('#detail-sale').modal('hide');
                        approve_berhasil();
                        table_data.destroy();
                        reloadTable();
                        setTimeout("window.location='<?php echo site_url('Transaction/Sale'); ?>'", 450);
                    } else {
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        swal("Peringatan", result.status, "warning");
                    }
                }
            });
        });
    });

    $(document).on("click", ".cancel-penjualan", function () {
        var id_transaction_sales = $(this).attr("data-id");

        swal({
            title: "Batal Konfirmasi?",
            text: "Yakin anda akan membatalkan konfirmasi data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Batalkan Konfirmasi",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").html("<button type='button' class='btn btn-danger cancel-penjualan' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button>");
                },
                method: "POST",
                url: "<?php echo base_url('Transaction/Sale/prosesCancel'); ?>",
                data: "id_transaction_sales=" + id_transaction_sales,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == 'berhasil') {
                        $("tr[data-id='" + id_transaction_sales + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        table_data.destroy();
                        $('#detail-sale').modal('hide');
                        cancel_berhasil();
                        reloadTable();
                        setTimeout("window.location='<?php echo site_url('Transaction/Sale'); ?>'", 450);
                    } else {
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        swal("Peringatan", result.status, "warning");
                    }
                }
            });
        });
    });

    $(document).on("click", ".detail-penjualan", function () {
        var id = $(this).attr("data-id");

        $.ajax({
            method: "POST",
            url: "<?php echo base_url('Transaction/Sale/Detail'); ?>",
            data: "id=" + id
        }).done(function (data) {
            $('#tempat-modal').html(data);
            $('#detail-sale').modal('show');
        })
    })

    $(document).on("click", ".conf-hapus-penjualan", function () {
        var id = $(this).attr("data-id");

        $.ajax({
            method: "POST",
            url: "<?php echo base_url('Transaction/Sale/Detail/1'); ?>",
            data: "id=" + id
        }).done(function (data) {
            $('#tempat-modal').html(data);
            $('#detail-sale').modal('show');
        })
    })

    $(document).on("click", ".conf-approve-penjualan", function () {
        var id = $(this).attr("data-id");

        $.ajax({
            method: "POST",
            url: "<?php echo base_url('Transaction/Sale/Detail/2'); ?>",
            data: "id=" + id
        }).done(function (data) {
            $('#tempat-modal').html(data);
            $('#detail-sale').modal('show');
        })
    })

    $(document).on("click", ".conf-cancel-penjualan", function () {
        var id = $(this).attr("data-id");

        $.ajax({
            method: "POST",
            url: "<?php echo base_url('Transaction/Sale/Detail/3'); ?>",
            data: "id=" + id
        }).done(function (data) {
            $('#tempat-modal').html(data);
            $('#detail-sale').modal('show');
        })
    })

    // untuk select2 ajak pilih tipe
    $(function () {
        $(".select-customer").select2({
            placeholder: " -- pilih customer -- "
        });
        $(".is-approved").select2({
            placeholder: " -- pilih status -- "
        });
    });
</script>







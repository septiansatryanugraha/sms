<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <title><?php echo $branch->description ?> | Surat Jalan</title>
        <link rel="icon" href="<?php echo base_url() ?>assets/tambahan/gambar/logo-sms.png"></link>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/print_css/style_surat_jalan.css"></link>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/print_css/print.css" media="print"></link>
        <style>
            .hormat {
                margin-top:30px;
                margin-bottom:40px;
                margin-left:80px;
            }
            .garis { margin-left:50px;}
            .hormat1{
                margin-top:30px;
                margin-bottom:40px;
                margin-left:60px;
            }

            .garis1 { margin-left:60px;}

            #kiri{
                width:50%;
                float:left;
            }
            #kanan{
                width:50%;
                float:right;
            }
            page[size="custom"] {  
                width: 14.8cm;
                height: 21cm; 
            }
            @media print {
                body, page {
                    margin: 0;
                    box-shadow: 0;
                }
            }
        </style>
    </head>
    <page size="custom">
        <body>
            <div id="page-wrap">
                <div id="header" style="padding-bottom: 20px;">SURAT JALAN<br/>PT. <?php echo strtoupper($branch->description) ?></div>
                <hr size="3px" color="black"></hr><br/><br/>
                <div style="clear:both"></div>
                <div id="customer">
                    <div id="address">
                        <ul style="list-style: none;">
                            <li><?php echo $namaCustomer; ?></li>
                            <li><?php echo $alamatCustomer; ?></li>
                            <li>Phone: <?php echo $phoneCustomer; ?></li>
                        </ul>
                    </div>
                    <div>
                        <table id="meta">
                            <tr>
                                <td class="meta-head">No Surat</td>
                                <td><?php echo $dataSale->code ?></td>
                            </tr>
                            <tr>
                                <td class="meta-head">Tanggal</td>
                                <td><div id="date"><?php echo date('d-m-Y', strtotime($dataSale->date)); ?></div></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table id="items">
                    <tr>
                        <th width="250px">Nama Barang</th>
                        <th width="150px">Quantity</th>
                        <th width="600px">Deskripsi</th>
                    </tr>
                    <!-- fungsi php-->
                    <?php if ($jmlLiter > 0) { ?>
                        <tr class="item-row">
                            <td class="item-name">Oli Bekas</td>
                            <td><?php echo $jmlLiter; ?> Liter</td>
                            <td class="description"><?php echo $descriptionOli; ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($isCheckedDrum > 0) { ?>
                        <tr class="item-row">
                            <td class="item-name">Drum</td>
                            <td><?php echo $jmlDrum; ?> Pcs</td>
                            <td class="description"><?php echo $descriptionDrum; ?></td>
                        </tr>
                    <?php } ?>
                    <!--fungsi php -->
                    <tr>
                        <td colspan="2" class="description" valign="top">
                            Catatan :<br/>
                            <?php echo $dataSale->description ?>
                        </td>
                        <td class="description" valign="top">
                            <font style="font-size:10px">Perhatian :</font><br/>
                            <ol style="margin-left:15px;" type="3">
                                <li style="font-size:10px";>Surat Jalan ini merupakan bukti resmi penerimaan barang.</li>
                                <li style="font-size:10px";>Surat Jalan ini merupakan bukti resmi penjualan.</li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <div id="kanan">
                    <div class="hormat">
                        <h4><strong>Pengirim</strong></h4>
                    </div>
                    <div class="garis">
                        <hr width="60%"></hr>
                    </div>
                </div>
                <div id="kiri">
                    <div class="hormat1">
                        <h4><strong>Penerima / Pembeli</strong></h4>
                    </div>
                    <div class="garis1">
                        <hr width="60%"></hr>
                    </div>
                </div>
            </div>
        </body>
    </page>
</html>

<script>
    window.print();
</script>
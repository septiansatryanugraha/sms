<?php $this->load->view('_heading/_headerContent') ?>
<!-- style loading -->
<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 40%;
    }
</style>
<div class="loading2"></div>
<!-- -->
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-ubah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kode Penjualan </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="code" name="code" class="form-control" placeholder="Kode"  aria-describedby="sizing-addon2" value="<?php echo $dataSale->code; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Customer </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="customer" name="customer" class="form-control" placeholder="Customer"  aria-describedby="sizing-addon2" value="<?php echo $namaCustomer; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Penjualan </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="date" name="date" class="form-control" placeholder="Tanggal" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Berat Oli (Kg) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="weight" name="weight" class="form-control number_decimal" placeholder="Berat"  aria-describedby="sizing-addon2" value="<?php echo $weight; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Berat Jenis Oli </label>
                                        <div class="col-sm-5">
                                            <input type="text" style="text-align: right;" id="specific_gravity" name="specific_gravity" class="form-control number_decimal" placeholder="Berat Jenis"  aria-describedby="sizing-addon2" value="<?php echo $specificGravity; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kadar Air (%) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="water_content" name="water_content" class="form-control number_only" placeholder="Kadar Air"  aria-describedby="sizing-addon2" value="<?php echo $waterContent; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Catatan Oli</label>
                                        <div class="col-sm-7">
                                            <textarea id="description_oli" name="description_oli" class="form-control" placeholder="Catatan Oli"  aria-describedby="sizing-addon2" disabled><?php echo $descriptionOli; ?></textarea>
                                        </div>
                                    </div>
                                    <div id="menu_no_drum" class="menu-no-drum" style="display:block">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label"><span id="label_kemasan">Kemasan / Liter</span> </label>
                                            <div class="col-sm-7">
                                                <input type="text" id="jml_kemasan" name="jml_kemasan" class="form-control jml-style number_only hitungGrandTotal" placeholder="Jumlah Kemasan"  aria-describedby="sizing-addon2" value="<?php echo $jmlKemasan; ?>" disabled>
                                                @<input type="text" id="liter_per_kemasan" name="liter_per_kemasan" class="form-control liter-style number_decimal hitungGrandTotal" placeholder="Liter per Kemasan" aria-describedby="sizing-addon2" value="<?php echo $literPerKemasan; ?>" disabled>Liter
                                            </div>
                                        </div>
                                    </div>
                                    <div id="menu_drum" class="menu-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Catatan Drum </label>
                                            <div class="col-sm-7">
                                                <textarea id="description_drum" name="description_drum" class="form-control" placeholder="Catatan Drum"  aria-describedby="sizing-addon2" disabled><?php echo $descriptionDrum; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Drum / Liter </label>
                                            <div class="col-sm-7">
                                                <input type="text" id="jml_drum" name="jml_drum" class="form-control jml-style number_only hitungGrandTotal" placeholder="Jumlah Drum"  aria-describedby="sizing-addon2" value="<?php echo $jmlDrum; ?>" disabled>
                                                @<input type="text" id="liter_per_drum" name="liter_per_drum" class="form-control liter-style number_only hitungGrandTotal" placeholder="Liter per Drum" aria-describedby="sizing-addon2" value="<?php echo $literPerDrum; ?>" disabled>Liter
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Total Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="total_liter" name="total_liter" class="form-control number_decimal" placeholder="Total Liter"  aria-describedby="sizing-addon2" value="<?php echo ($literPerDrum * $jmlDrum); ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Penambahan Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="penambahan_liter" name="penambahan_liter" class="form-control number_only hitungGrandTotal" placeholder="Penambahan Liter"  aria-describedby="sizing-addon2" value="<?php echo $penambahanLiter; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Penyusutan Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="penyusutan_liter" name="penyusutan_liter" class="form-control number_only hitungGrandTotal" placeholder="Penyusutan Liter"  aria-describedby="sizing-addon2" value="<?php echo $penyusutanLiter; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grandtotal_liter" name="grandtotal_liter" class="form-control number_decimal" placeholder="Grand Total Liter"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div id="additional_drum" class="additional-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Penambahan Drum </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="penambahan_drum" name="penambahan_drum" class="form-control number_only" placeholder="Penambahan Drum"  aria-describedby="sizing-addon2" value="<?php echo $penambahanDrum; ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"><?php echo $dataSale->description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div id="harga_drum" class="harga-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Harga per Drum </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="harga_per_drum" name="harga_per_drum" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Drum"  aria-describedby="sizing-addon2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-5">
                                            <input type="radio" id="tipe_harga" name="tipe_harga" class="tipe-harga" <?php echo ($type == "liter") ? "checked" : "" ?> value="liter"> Liter
                                            <input type="radio" id="tipe_harga" name="tipe_harga" class="tipe-harga" <?php echo ($type == "kilogram") ? "checked" : "" ?> value="kilogram"> Kg
                                        </div>
                                    </div>
                                    <div id="div_harga_per_liter">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Harga per Liter </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="harga_per_liter" name="harga_per_liter" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Liter"  aria-describedby="sizing-addon2">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="div_harga_per_kilogram" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Harga per Kg </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="harga_per_kilogram" name="harga_per_kilogram" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Kg"  aria-describedby="sizing-addon2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sub-total" style="display: none;">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Sub Total</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="sub_total" name="sub_total" class="form-control number_only" placeholder="Sub Total" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_ppn" name="is_ppn" value="1" onclick="isPpn();"/>PPN <?php echo ($ppn * 100) ?> % ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="div-ppn" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="ppn" name="ppn" class="form-control number_only" placeholder="PPN <?php echo ($ppn * 100) ?> %"  aria-describedby="sizing-addon2" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grand_total" name="grand_total" class="form-control number_only formatCurrency" placeholder="Grand Total"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer pull-right">
                                <button id="conf_simpan" type="button" data-toggle="modal" data-target="#exampleModal"  class="btn btn-success btn-flat approve"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php //<editor-fold defaultstate="collapsed" desc="Modal Confirmation">?>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="col-md-12 well">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h3 style="display:block; text-align:center;">Konfirmasi Penjualan</h3>
                <div class="box box-body">
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Penjualan</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Penjualan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_code"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Customer </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_customer"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Penjualan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_date"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Oli (Kg) </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_weight"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Jenis Oli </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_specific_gravity"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Kadar Air (%) </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_water_content"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Catatan Oli</label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_description_oli"><?= $descriptionOli; ?></label></div>
                                    </div>
                                    <div id="menu_no_drum" class="menu-no-drum" style="display:block">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Kemasan / Liter </label>
                                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_jml_kemasan"></label> <b>@</b> <label for="inputEmail3" class="control-label" id="c_liter_per_kemasan"></label> <b>Liter</b></div>
                                        </div>
                                    </div>
                                    <div id="menu_drum" class="menu-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Catatan Drum </label>
                                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_description_drum"></label></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Drum / Liter </label>
                                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_jml_drum"></label> <b>@</b> <label for="inputEmail3" class="control-label" id="c_liter_per_drum"></label> <b>Liter</b></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Total Liter </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_total_liter"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Liter </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_penambahan_liter"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Penyusutan Liter </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_penyusutan_liter"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total Liter </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_grandtotal_liter"></label></div>
                                    </div>
                                    <div id="c_additional_drum" class="c-additional-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Drum </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_penambahan_drum"></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_description"></label></div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div id="c_harga_drum" class="c-harga-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_drum"></label></div>
                                        </div>
                                    </div>
                                    <div id="c_div_harga_per_liter">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Liter </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_liter"></label></div>
                                        </div>
                                    </div>
                                    <div id="c_div_harga_per_kilogram" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Kg </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_kilogram"></label></div>
                                        </div>
                                    </div>
                                    <div class="c-sub-total" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Sub Total </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_sub_total" name="sub_total"></label></div>
                                        </div>
                                    </div>
                                    <div class="div-ppn" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_ppn" name="ppn"></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_grand_total"></label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div id="btn_loading"></div>
                    <div id="buka">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" name="simpan" id="simpan">Konfirmasi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //</editor-fold> ?>

<script type="text/javascript">
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;
    var isCheckedPpn = <?php echo $isPpn; ?>;
    var tipe_harga = "<?php echo $type; ?>";

    $(document).ready(function () {
        $("#code").val('<?php echo $dataSale->code; ?>');
        $("#c_code").html('<?php echo $dataSale->code; ?>');
        $("#customer").val('<?php echo $namaCustomer; ?>');
        $("#c_customer").html('<?php echo $namaCustomer; ?>');
        $("#date").val('<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>');
        $("#c_date").html('<?php echo date('d-m-Y', strtotime($dataSale->date)); ?>');
        $("#weight").val('<?php echo $weight; ?>');
        $("#c_weight").html('<?php echo $weight; ?>');
        $("#specific_gravity").val('<?php echo $specificGravity; ?>');
        $("#c_specific_gravity").html('<?php echo $specificGravity; ?>');
        $("#water_content").val('<?php echo $waterContent; ?>');
        $("#c_water_content").html('<?php echo $waterContent; ?>');

//        $("#description_oli").val('<?php // echo $descriptionOli;  ?>');
//        $("#c_description_oli").html('<?php // echo $descriptionOli;  ?>');
        $("#total_liter").val('<?php echo $totalLiter; ?>');
        $("#c_total_liter").html('<?php echo $totalLiter; ?>');
        $("#penambahan_liter").val('<?php echo $penambahanLiter; ?>');
        $("#c_penambahan_liter").html('<?php echo $penambahanLiter; ?>');
        $("#penyusutan_liter").val('<?php echo $penyusutanLiter; ?>');
        $("#c_penyusutan_liter").html('<?php echo $penyusutanLiter; ?>');
        $("#grandtotal_liter").val('<?php echo $grandTotalLiter; ?>');
        $("#c_grandtotal_liter").html('<?php echo $grandTotalLiter; ?>');
        $("#penambahan_drum").val('<?php echo $penambahanDrum; ?>');
        $("#c_penambahan_drum").html('<?php echo $penambahanDrum; ?>');

        var menu_drum = document.getElementById("menu_drum");
        var menu_no_drum = document.getElementById("menu_no_drum");
        if (isCheckedDrum > 0) {
            menu_drum.style.display = "block";
            menu_no_drum.style.display = "none";
            $("#description_drum").val('<?php echo $descriptionDrum; ?>');
            $("#c_description_drum").html('<?php echo $descriptionDrum; ?>');
            $("#jml_drum").val('<?php echo $jmlDrum; ?>');
            $("#c_jml_drum").html('<?php echo $jmlDrum; ?>');
            $("#liter_per_drum").val('<?php echo $literPerDrum; ?>');
            $("#c_liter_per_drum").html('<?php echo $literPerDrum; ?>');
        } else {
            menu_no_drum.style.display = "block";
            menu_drum.style.display = "none";
            $("#jml_kemasan").val('<?php echo $jmlKemasan; ?>');
            $("#c_jml_kemasan").html('<?php echo $jmlKemasan; ?>');
            $("#liter_per_kemasan").val('<?php echo $literPerKemasan; ?>');
            $("#c_liter_per_kemasan").html('<?php echo $literPerKemasan; ?>');
        }

        if (isCheckedPpn > 0) {
            document.getElementById("is_ppn").checked = true;
        }

        var additional_drum = document.getElementById("additional_drum");
        if (isCheckedAdditionalDrum > 0) {
            additional_drum.style.display = "block";
            $("#penambahan_drum").val(<?php echo $penambahanDrum; ?>);
            $("#c_penambahan_drum").html(<?php echo $penambahanDrum; ?>);
        } else {
            additional_drum.style.display = "none";
        }

        var harga_drum = document.getElementById("harga_drum");
        var c_harga_drum = document.getElementById("c_harga_drum");
        if (isCheckedDrum > 0 || isCheckedAdditionalDrum > 0) {
            harga_drum.style.display = "block";
            c_harga_drum.style.display = "block";
            $("#harga_per_drum").val(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));
        }

        var div_harga_per_kilogram = document.getElementById("div_harga_per_kilogram");
        var div_harga_per_liter = document.getElementById("div_harga_per_liter");
        if (tipe_harga == 'liter') {
            div_harga_per_liter.style.display = "block";
            div_harga_per_kilogram.style.display = "none";
            $("#harga_per_liter").val(accounting.formatMoney('<?php echo $hargaPerLiter; ?>'));
        } else if (tipe_harga == 'kilogram') {
            div_harga_per_kilogram.style.display = "block";
            div_harga_per_liter.style.display = "none";
            $("#harga_per_kilogram").val(accounting.formatMoney('<?php echo $hargaPerKilogram; ?>'));
        }

        hitungGrandTotal();
    });

    // ===== Proses Tipe Harga ===== //
    $('.tipe-harga').change(function () {
        var tipe_harga = $("#tipe_harga:checked").val();

        var div_harga_per_kilogram = document.getElementById("div_harga_per_kilogram");
        var div_harga_per_liter = document.getElementById("div_harga_per_liter");
        if (tipe_harga == 'liter') {
            div_harga_per_liter.style.display = "block";
            div_harga_per_kilogram.style.display = "none";
            $("#harga_per_liter").val(0);
        } else if (tipe_harga == 'kilogram') {
            div_harga_per_kilogram.style.display = "block";
            div_harga_per_liter.style.display = "none";
            $("#harga_per_kilogram").val(0);
        }
        hitungGrandTotal();
    });

    // ===== Action PPN ===== //
    function isPpn() {
        var is_ppn = document.getElementById("is_ppn");
        if (is_ppn.checked == true) {
            $(".div-ppn").fadeIn();
            $(".sub-total").fadeIn('slow');
            $(".c-sub-total").fadeIn('slow');
        } else {
            $(".div-ppn").fadeOut();
            $(".sub-total").fadeOut('slow');
            $(".c-sub-total").fadeOut('slow');
        }
        hitungGrandTotal();
    }

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var total_drum = 0;
        var penambahan_drum = 0;
        var harga_per_drum = 0;
        var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
        var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;
        var tipe_harga = $("#tipe_harga:checked").val();
        var ppn = 0;
        var is_ppn = document.getElementById("is_ppn");

        var harga_per_liter = $("#harga_per_liter").val();
        if (harga_per_liter.length == 0) {
            harga_per_liter = 0;
        }
        harga_per_liter = accounting.unformat(harga_per_liter);
        var harga_per_kilogram = $("#harga_per_kilogram").val();
        if (harga_per_kilogram.length == 0) {
            harga_per_kilogram = 0;
        }
        harga_per_kilogram = accounting.unformat(harga_per_kilogram);
        var total_liter_label = 0;
        if (isCheckedDrum > 0) {
            var jml_drum = $("#jml_drum").val();
            if (jml_drum.length == 0) {
                jml_drum = 0;
            }
            var liter_per_drum = $("#liter_per_drum").val();
            if (liter_per_drum.length == 0) {
                liter_per_drum = 0;
            }
            var total_liter = jml_drum * liter_per_drum;
            total_liter_label = total_liter;
            var total_drum = jml_drum;
        } else {
            var jml_kemasan = $("#jml_kemasan").val();
            if (jml_kemasan.length == 0) {
                jml_kemasan = 0;
            }
            var liter_per_kemasan = $("#liter_per_kemasan").val();
            var liter_per_kemasan_coma = liter_per_kemasan.split(',').length;
            if (liter_per_kemasan.length == 0) {
                liter_per_kemasan = 0;
            }
            if (liter_per_kemasan_coma == 2) {
                liter_per_kemasan = liter_per_kemasan.replace(",", ".");
            }
            var total_liter = jml_kemasan * liter_per_kemasan;
            total_liter_label = total_liter;

            if (total_liter % 1 !== 0) {
                total_liter = parseFloat(total_liter).toFixed(2);
                total_liter_label = total_liter.replace(".", ",");
            }
        }
        var penambahan_liter = $("#penambahan_liter").val();
        if (penambahan_liter.length == 0) {
            penambahan_liter = 0;
        }
        var penyusutan_liter = $("#penyusutan_liter").val();
        if (penyusutan_liter.length == 0) {
            penyusutan_liter = 0;
        }
        if (isCheckedAdditionalDrum > 0) {
            var penambahan_drum = $("#penambahan_drum").val();
            if (penambahan_drum.length == 0) {
                penambahan_drum = 0;
            }
        }
        if (isCheckedAdditionalDrum > 0 || isCheckedDrum > 0) {
            harga_per_drum = $("#harga_per_drum").val();
            if (harga_per_drum.length == 0) {
                harga_per_drum = 0;
            }
            harga_per_drum = accounting.unformat(harga_per_drum);
        }
        $("#total_liter").val(total_liter_label);
        $("#c_total_liter").html(total_liter_label);

        var total_drum = parseInt(total_drum) + parseInt(penambahan_drum);
        var total_liter_all = parseFloat(total_liter) + parseInt(penambahan_liter) - parseInt(penyusutan_liter);

        var total_harga_oli = 0;
        if (tipe_harga == 'liter') {
            total_harga_oli = total_liter_all * harga_per_liter;
        } else if (tipe_harga == 'kilogram') {
            var weight = $("#weight").val();
            total_harga_oli = parseFloat(weight) * harga_per_kilogram;
        }
        var total_harga_drum = total_drum * harga_per_drum;
        var grand_total = total_harga_oli + total_harga_drum;

        if (is_ppn.checked == true) {
            $("#sub_total").val(accounting.formatMoney(grand_total));
            $("#c_sub_total").html(accounting.formatMoney(grand_total));

            ppn = <?php echo $ppn; ?>;
            var total_ppn = ppn * grand_total;
            grand_total = grand_total + (total_ppn);
            $("#ppn").val(accounting.formatMoney(total_ppn));
        }

        $("#grand_total").val(accounting.formatMoney(grand_total));
        $("#c_grand_total").html(accounting.formatMoney(grand_total));
    }

    $('.hitungGrandTotal').on('focusout', function () {
        hitungGrandTotal();
    });

    $('.hitungGrandTotal').on('keyup', function () {
        hitungGrandTotal();
    });

    // ===== Confirmation ===== //
    $("#conf_simpan").click(function () {
        if (isCheckedDrum > 0) {
            $("#c_harga_per_drum").html($("#harga_per_drum").val());
        }

        var tipe_harga = $("#tipe_harga:checked").val();
        var div_harga_per_kilogram = document.getElementById("c_div_harga_per_kilogram");
        var div_harga_per_liter = document.getElementById("c_div_harga_per_liter");
        if (tipe_harga == 'liter') {
            div_harga_per_liter.style.display = "block";
            div_harga_per_kilogram.style.display = "none";
            $("#c_harga_per_liter").html($("#harga_per_liter").val());
        } else if (tipe_harga == 'kilogram') {
            div_harga_per_kilogram.style.display = "block";
            div_harga_per_liter.style.display = "none";
            $("#c_harga_per_kilogram").html($("#harga_per_kilogram").val());
        }

        var additional_drum = document.getElementById("c_additional_drum");
        var harga_drum = document.getElementById("c_harga_drum");
        if (isCheckedAdditionalDrum > 0) {
            additional_drum.style.display = "block";
            harga_drum.style.display = "block";
            $("#c_harga_per_drum").html($("#harga_per_drum").val());
        }

//        $("#c_date_paid").html($("#date_paid").val());
        $("#c_ppn").html($("#ppn").val());
        $("#c_description").html($("#description").val());
    });

    // ===== Proses Hitung Berat ===== //
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
                function () {
                    $(".confirm").attr('disabled', 'disabled');
                    var data = $("#newContain>form").serialize();
                    $.ajax({
                        method: 'POST',
                        beforeSend: function () {
                            $(".loading2").show();
                            $(".loading2").modal('show');
                        },
                        url: '<?php echo site_url('Transaction/Sale/prosesConfirm/' . $idTransactionSales); ?>',
                        data: data,
                    }).done(function (data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == 'berhasil') {
                            document.getElementById("form-ubah").reset();
                            $(".loading2").hide();
                            $(".loading2").modal('hide');
                            save_berhasil();
                            setTimeout("window.location='<?php echo site_url('Transaction/Sale'); ?>'", 450);
                        } else {
                            $(".loading2").hide();
                            $(".loading2").modal('hide');
                            swal("Peringatan", result.status, "warning");
                        }
                    })
                });
    });

    // ===== Open Modal Confirmation Approve ===== //
    $(document).on("click", ".approve", function () {
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded

    });
</script>
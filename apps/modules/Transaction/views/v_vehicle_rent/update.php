<?php
$this->load->view('_heading/_headerContent');
/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
?>
<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }
    .number_only {
        text-align: right;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-ubah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Supplier </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" placeholder="Supplier"  aria-describedby="sizing-addon2" value="<?php echo $supplier_name; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Armada </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" placeholder="Supplier"  aria-describedby="sizing-addon2" value="<?php echo $vehicle; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tanggal Sewa</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="date_rent" class="form-control datepicker" id="date_rent" placeholder="Tanggal Sewa"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y', strtotime($dataVehicleRent->date)); ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Harga Sewa (per hari)</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="amount_rent" id="amount_rent" class="form-control number_only formatCurrency" placeholder="Harga Sewa"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Keterangan</label>
                                        <div class="col-sm-7">
                                            <textarea name="description" id="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"><?php echo $dataVehicleRent->description; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $("#amount_rent").val(accounting.formatMoney(<?php echo $dataVehicleRent->nominal_rent; ?>));

        //Number Format
        $(document).on('keypress', '.number_only', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        //Format Currency
        $('.formatCurrency').on('focusin', function () {
            var x = $(this).val();
            $(this).val(accounting.unformat(x));
        });
        $('.formatCurrency').on('focusout', function () {
            var x = $(this).val();
            $(this).val(accounting.formatMoney(x));
        });

        $('#amount_rent').on('focusout', function () {
            var amount_rent = $("#amount_rent").val();
            if (amount_rent.length == 0) {
                $("#amount_rent").val(0);
            }
        });
    });

    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/VehicleRent/prosesUpdate/' . $idVehicleRent); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-ubah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo site_url('Transaction/VehicleRent'); ?>'", 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    // untuk select2 ajak pilih menu
    $(function () {
        $(".select-supplier").select2({
            placeholder: " -- Pilih Supplier -- "
        });
    });
    $(function () {
        $(".select-vehicle").select2({
            placeholder: " -- Pilih Kendaraan -- "
        });
    });

</script>

<?php
$this->load->view('_heading/_headerContent');
/**
 *
 * @author Septian Satrya Nugraha
 * @since  Apr 20, 2019
 * @license Susi Susanti Group
 */
?>
<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }
    .number_only {
        text-align: right;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-kembali" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Supplier </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" placeholder="Supplier"  aria-describedby="sizing-addon2" value="<?php echo $supplier_name; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Armada </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" placeholder="Supplier"  aria-describedby="sizing-addon2" value="<?php echo $vehicle; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Sewa </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" placeholder="Supplier"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y', strtotime($dataVehicleRent->date)); ?>" disabled>
                                            <input type="hidden" name="date_rent" class="form-control" id="date_rent" placeholder="Tanggal Kembali"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y', strtotime($dataVehicleRent->date)); ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tanggal Kembali</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="date_back" class="form-control datepicker" id="date_back" placeholder="Tanggal Kembali"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y'); ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Jumlah Hari </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="count_day" name="count_day" class="form-control number_only hitungTotalBayar" placeholder="Jumlah Hari" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_rent_again" name="is_rent_again" value="1" onclick="isRentAgain();"/>Sewa kembali ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div id="div_rent" class="div-rent" style="display:none">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tanggal Sewa Kembali</label>
                                            <div class="col-sm-5">
                                                <input type="text" name="date_rent_again" class="form-control datepicker" id="date_rent_again" placeholder="Tanggal Dewa Kembali"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"><?php echo $dataVehicleRent->description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-7">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_maintenance_vehicle" name="is_maintenance_vehicle" value="1" onclick="isMaintenanceVehicle();"/>biaya maintenance ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Harga Sewa (per hari) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="nominal_rent" name="nominal_rent" class="form-control number_only formatCurrency" placeholder="Harga Sewa (per hari)"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="menu-maintenance" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Subtotal</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="subtotal" name="subtotal" class="form-control number_only formatCurrency" placeholder="Total Bayar"  aria-describedby="sizing-addon2" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Biaya Maintenance</label>
                                            <div class="col-sm-5">
                                                <input type="text" name="amount_maintenance" id="amount_maintenance" class="form-control number_only formatCurrency hitungTotalBayar" placeholder="Biaya Armada"  aria-describedby="sizing-addon2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Keterangan</label>
                                            <div class="col-sm-7">
                                                <textarea id="description_maintenance" name="description_maintenance" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Total Bayar</label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grandtotal" name="grandtotal" class="form-control number_only formatCurrency" placeholder="Total Bayar"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Konfirmasi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $("#count_day").val(1);
    $("#nominal_rent").val(accounting.formatMoney(<?php echo $dataVehicleRent->nominal_rent; ?>));

    //Number Format
    $(document).on('keypress', '.number_only', function (event) {
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //Format Currency
    $('.formatCurrency').on('focusin', function () {
        var x = $(this).val();
        $(this).val(accounting.unformat(x));
    });
    $('.formatCurrency').on('focusout', function () {
        var x = $(this).val();
        $(this).val(accounting.formatMoney(x));
    });

    $('.hitungTotalBayar').on('focusout', function () {
        var count_day = $("#count_day").val();
        var nominal_rent = $("#nominal_rent").val();
        nominal_rent = accounting.unformat($("#nominal_rent").val());
        var subtotal = $("#subtotal").val();
//        var nominal_rent = <?php echo $dataVehicleRent->nominal_rent; ?>;
        if (count_day.length == 0) {
            $("#count_day").val(0);
        }
        if (subtotal.length == 0) {
            $("#subtotal").val(0);
        }
        if (nominal_rent.length == 0) {
            $("#nominal_rent").val(0);
        }
    });

    $('.hitungTotalBayar').on('keyup', function () {
        hitungTotalBayar();
    });

    function hitungTotalBayar() {
        var is_maintenance_vehicle = document.getElementById("is_maintenance_vehicle");
        var count_day = $("#count_day").val();
        var nominal_rent = $("#nominal_rent").val();
        nominal_rent = accounting.unformat($("#nominal_rent").val());
        var subtotal = $("#subtotal").val();
//        var nominal_rent = <?php echo $dataVehicleRent->nominal_rent; ?>;
        if (count_day.length == 0) {
            count_day = 0;
        }
        if (subtotal.length == 0) {
            subtotal = 0;
        }
        if (nominal_rent.length == 0) {
            nominal_rent = 0;
        }

        subtotal = count_day * nominal_rent;
        if (is_maintenance_vehicle.checked == true) {
            $("#subtotal").val(accounting.formatMoney(subtotal));
            var amount_maintenance = accounting.unformat($("#amount_maintenance").val());
            subtotal = subtotal - amount_maintenance;
        }
        $("#grandtotal").val(accounting.formatMoney(subtotal));
    }

    hitungTotalBayar();

    //Action Checkbox
    function isRentAgain() {
        var is_rent_again = document.getElementById("is_rent_again");
        var div_rent = document.getElementById("div_rent");
        if (is_rent_again.checked == true) {
            $('.div-rent').fadeIn('slow');
            div_rent.style.display = "block";
//            $("#penyusutan_liter").val(0);
        } else {
            $('.div-rent').fadeOut('slow');
            div_rent.style.display = "none";
//            $("#jml_liter").val(0);
        }
//        $("#harga_per_liter").val(0);
//        $("#grand_total").val(0);
    }

    // Action Checkbox
    function isMaintenanceVehicle() {
        var is_maintenance_vehicle = document.getElementById("is_maintenance_vehicle");
        if (is_maintenance_vehicle.checked == true) {
            $('.menu-maintenance').fadeIn('slow');
            $("#amount_maintenance").val(0);
        } else {
            $('.menu-maintenance').fadeOut('slow');
        }
        $('#amount_maintenance').on('focusout', function () {
            var amount_maintenance = $("#amount_maintenance").val();
            if (amount_maintenance.length == 0) {
                $("#amount_maintenance").val(0);
            }
        });
        hitungTotalBayar();
    }

    // untuk datetime date_rent
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy',
        })
    });

    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/VehicleRent/prosesKembali/' . $idVehicleRent); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-kembali").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo site_url('Transaction/VehicleRent'); ?>'", 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

</script>

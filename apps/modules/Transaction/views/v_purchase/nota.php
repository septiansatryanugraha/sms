<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <title><?php echo $branch->description ?> | Nota Pembayaran</title>
        <link rel="icon" href="<?php echo base_url() ?>assets/tambahan/gambar/logo-sms.png"></link>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/print_css/style.css"></link>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/print_css/print.css" media="print"></link>
        <style>
            .hormat {
                margin-top:30px;
                margin-bottom:50px;
                margin-left:45px;
            }
            .garis { margin-left:50px;}
            .hormat1{
                margin-top:30px;
                margin-bottom:50px;
                margin-left:30px;
            }
            .garis1 { margin-left:35px;}
            #kiri{
                width:50%;
                float:left;
            }
            #kanan{
                width:50%;
                float:right;
            }
            page[size="custom"] {  
                width: 9.5cm;
                height: 11cm; 
            }
            @media print {
                body, page {
                    margin: 0;
                    box-shadow: 0;
                }
            }
        </style>
    </head>
    <page size="custom">
        <body>
            <div id="page-wrap">
                <div id="header">NOTA PEMBAYARAN<br/>PT. <?php echo strtoupper($branch->description) ?></div>
                <hr size="3px" color="black"></hr><br/><br/>
                <div id="customer">
                    <div id="address">
                        <ul style="list-style: none;">
                            <li><?php echo $namaSupplier; ?></li>
                            <li><?php echo $alamatSupplier; ?></li>
                            <li>Phone: <?php echo $phoneSupplier; ?></li>
                        </ul>
                    </div>
                    <table id="meta">
                        <tr>
                            <td class="meta-head">No nota </td>
                            <td><?php echo $dataPurchase->code ?></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Tanggal</td>
                            <td><div id="date"><?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?></div></td>
                        </tr>
                    </table>
                </div>
                <table id="items">
                    <tr>
                        <th>Jumlah</th>
                        <th>Barang</th>
                        <th width="100px">Satuan</th>
                        <th>Berat Jenis</th>
                        <th>Kadar Air</th>
                        <th>Keterangan</th>
                    </tr>
                    <?php foreach ($dataPurchaseDetail as $key => $value) { ?>
                        <?php if ($value->id_item == 1) { ?>
                            <tr class="item-row">
                                <td><div class="cost"><?php echo $value->qty ?></td>
                                <td>Oli Bekas</td>
                                <td>Liter</td>
                                <td><span class="cost"><?php echo $specificGravity ?></span></td>
                                <td><span class="price"><?php echo $waterContent ?></span></td>
                                <td><span class="price"><?php echo $description ?></span></td>
                            </tr>
                        <?php } ?>
                        <?php if ($value->id_item == 2) { ?>
                            <tr class="item-row">
                                <td><div class="cost"><?php echo $value->qty ?></td>
                                <td>Drum</td>
                                <td>Pcs</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
                <div id="kanan">
                    <div class="hormat">
                        <h4><strong>Hormat Kami</strong></h4>
                    </div>
                    <div class="garis">
                        <hr width="70%"></hr>
                    </div>
                </div>
                <div id="kiri">
                    <div class="hormat1">
                        <h4><strong>Tanda Terima</strong></h4>
                    </div>
                    <div class="garis1">
                        <hr width="70%"></hr>
                    </div>
                </div>
            </div>
        </body>
    </page>
</html>

<script>
    window.print();
</script>
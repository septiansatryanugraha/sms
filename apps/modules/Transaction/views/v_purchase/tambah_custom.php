<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
</style>
<div class="loading2"></div>
<section class="content">
    <div class="loading2"></div>
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-tambah" method="POST">
                            <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Supplier </label>
                                        <div class="col-sm-7">
                                            <select name="id_supplier" class="form-control select-supplier" id="id_supplier" aria-describedby="sizing-addon2">
                                                <?php foreach ($supplier as $data) { ?>
                                                    <option></option>
                                                    <option value="<?php echo $data->id_supplier; ?>">
                                                        <?php echo $data->name; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pembelian </label>
                                        <div class="col-sm-5">
                                            <?php if ($privilegeId == 1) { ?>
                                                <input type="text" name="date" class="form-control datepicker" id="date" placeholder="Tanggal Pembelian"  aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y') ?>">
                                            <?php } else { ?>
                                                <input type="text" class="form-control" placeholder="Tanggal Pembelian" aria-describedby="sizing-addon2" value="<?php echo date('d-m-Y') ?>" disabled>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Jumlah Drum </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="jml_drum" name="jml_drum" class="form-control number_only" placeholder="Jumlah Drum"  aria-describedby="sizing-addon2" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-warning btn-flat"><i class="fa fa-retweet"></i> Bersihkan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    // ===== Proses Controller logic ajax ===== //
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/Purchase/prosesAddCustom'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        // ===== untuk select2 ajak pilih tipe ===== //
        $(".select-supplier").select2({
            placeholder: " -- pilih supplier -- "
        });
    });
</script>
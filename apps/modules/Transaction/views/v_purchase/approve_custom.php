<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
</style>
<!-- style loading -->
<div class="loading2"></div>
<!-- -->
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-ubah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kode Pembelian </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="code" name="code" class="form-control" placeholder="Kode" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pembelian </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="date" name="date" class="form-control" placeholder="Tanggal" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Supplier </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="supplier" name="supplier" class="form-control" placeholder="Supplier" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Bank </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="bank" name="bank" class="form-control" placeholder="Bank" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Nomor Rekening </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="account_number" name="account_number" class="form-control" placeholder="Nomor Rekening" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Pemilik Rekening </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="account_holder" name="account_holder" class="form-control" placeholder="Pemilik Rekening" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Jumlah Drum </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="jml_drum" name="jml_drum" class="form-control number_only" placeholder="Jumlah Drum" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan" aria-describedby="sizing-addon2"><?php echo $dataPurchase->description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Harga per Drum </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="harga_per_drum" name="harga_per_drum" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Drum" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="sub-total" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Sub Total </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="sub_total" name="sub_total" class="form-control number_only" placeholder="Sub Total" aria-describedby="sizing-addon2" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_ppn" name="is_ppn" value="1" onclick="isPpn();"/>PPN <?php echo ($ppn * 100) ?> % ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="div-ppn" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="ppn" name="ppn" class="form-control number_only" placeholder="PPN <?php echo ($ppn * 100) ?> %"  aria-describedby="sizing-addon2" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (count($listSupplierLoan) > 0) { ?>
                                        <div class="form-group"><div>&nbsp;</div></div>
                                        <div class="form-group">
                                            <div class="box-header with-border no-padding">
                                                <h3 class="box-title"><b>&#8226; Detail Piutang</b></h3>
                                            </div>
                                        </div>
                                        <?php foreach ($listSupplierLoan as $key => $value) { ?>
                                            <input type="hidden" id="id_supplier_loan[]" name="id_supplier_loan[]" value='<?php echo $value['id_supplier_loan']; ?>'>
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Keterangan : <?php echo $value['description']; ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Piutang</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control number_only" aria-describedby="sizing-addon2" value='<?php echo number_format($value['nominal'], 0, ",", "."); ?>' disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Belum Dibayar</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control number_only" aria-describedby="sizing-addon2" value='<?php echo number_format($value['debt'], 0, ",", "."); ?>' disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 control-label">Cicilan Ke <?php echo $value['installment']; ?></label><div id="nominal_supplier"></div>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control number_only hitungGrandTotal formatCurrency nominal_installment" id="nominal_installment_<?php echo $key; ?>" name="nominal_installment[]" placeholder="Nominal Cicilan" aria-describedby="sizing-addon2">
                                                </div>
                                            </div>
                                            <div class="form-group"><div>&nbsp;</div></div>
                                        <?php } ?>
                                        <input type="hidden" id="sum_installment" name="installment" value="0">
                                    <?php } ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grand_total" name="grand_total" class="form-control number_only" placeholder="Grand Total" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer pull-right">
                                <button id="conf_simpan" type="button" data-toggle="modal" data-target="#exampleModal"  class="btn btn-success btn-flat approve"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php //<editor-fold defaultstate="collapsed" desc="Modal Confirmation">?>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="col-md-12 well">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h3 style="display:block; text-align:center;">Konfirmasi Pembelian</h3>
                <div class="box box-body">
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Pembelian </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_code"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Pembelian </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_date"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Supplier </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_supplier"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Bank </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_bank"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Nomor Rekening </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_account_number"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Pemilik Rekening </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_account_holder"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Jumlah Drum </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_jml_drum"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_description"></label></div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_drum"></label></div>
                                    </div>
                                    <div class="c-sub-total" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Sub Total </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_sub_total"></label></div>
                                        </div>
                                    </div>
                                    <div class="div-ppn" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_ppn" name="ppn"></label></div>
                                        </div>
                                    </div>
                                    <?php if (count($listSupplierLoan) > 0) { ?>
                                        <div class="form-group"><div>&nbsp;</div></div>
                                        <div class="form-group">
                                            <div class="box-header with-border no-padding">
                                                <h3 class="box-title"><b>&#8226; Detail Piutang</b></h3>
                                            </div>
                                        </div>
                                        <?php foreach ($listSupplierLoan as $key => $value) { ?>
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Keterangan : <?php echo $value['description']; ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Piutang</label>
                                                <div class="col-sm-7">: <label for="inputEmail3" class="control-label"><?php echo number_format($value['nominal'], 0, ",", "."); ?></label></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Belum Dibayar</label>
                                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label"><?php echo number_format($value['debt'], 0, ",", "."); ?></label></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-5 control-label">Cicilan Ke <?php echo $value['installment']; ?></label><div id="nominal_supplier"></div>
                                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_nominal_installment_<?php echo $key; ?>"></label></div>
                                            </div>
                                            <div class="form-group"><div>&nbsp;</div></div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_grand_total"></label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div id="btn_loading"></div>
                    <div id="buka">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" name="simpan" id="simpan">Konfirmasi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //</editor-fold> ?>

<script type="text/javascript">
    var is_supplier_loan = <?php echo $isSupplierLoan; ?>;

    $(document).ready(function () {
        $("#code").val('<?php echo $dataPurchase->code; ?>');
        $("#c_code").html('<?php echo $dataPurchase->code; ?>');
        $("#date").val('<?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?>');
        $("#c_date").html('<?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?>');
        $("#supplier").val('<?php echo $namaSupplier; ?>');
        $("#c_supplier").html('<?php echo $namaSupplier; ?>');
        $("#bank").val('<?php echo $bank; ?>');
        $("#c_bank").html('<?php echo $bank; ?>');
        $("#account_number").val('<?php echo $dataSupplier->account_number; ?>');
        $("#c_account_number").html('<?php echo $dataSupplier->account_number; ?>');
        $("#account_holder").val('<?php echo $dataSupplier->account_holder; ?>');
        $("#c_account_holder").html('<?php echo $dataSupplier->account_holder; ?>');

        $("#jml_drum").val('<?php echo $penambahanDrum; ?>');
        $("#c_jml_drum").html('<?php echo $penambahanDrum; ?>');

        $(".nominal-installment").fadeOut();
        $(".sub-total").fadeOut();
        $(".supplier-loan").hide();
        $(".nominal_installment").val(0);
        $("#harga_per_drum").val(accounting.formatMoney('<?php echo $drumPrice; ?>'));

        if (is_supplier_loan > 0) {
            $(".sub-total").fadeIn('slow');
            $(".c-sub-total").fadeIn('slow');
        }

        hitungGrandTotal();
    });

    // ===== Action PPN ===== //
    function isPpn() {
        var is_ppn = document.getElementById("is_ppn");
        if (is_supplier_loan > 0) {
            $(".sub-total").fadeIn('slow');
            $(".c-sub-total").fadeIn('slow');
            if (is_ppn.checked == true) {
                $(".div-ppn").fadeIn();
            } else {
                $(".div-ppn").fadeOut();
            }
        }
        else {
            if (is_ppn.checked == true) {
                $(".div-ppn").fadeIn();
                $(".sub-total").fadeIn('slow');
                $(".c-sub-total").fadeIn('slow');
            } else {
                $(".div-ppn").fadeOut();
                $(".sub-total").fadeOut('slow');
                $(".c-sub-total").fadeOut('slow');
            }
        }
        hitungGrandTotal();
    }

    // ===== Action Sum Installment ===== //
    function nominalInstallment() {
        var sum = 0;
        $(".nominal_installment").each(function () {
            sum += +accounting.unformat($(this).val());
        });
        $("#sum_installment").val(sum);
        hitungGrandTotal();
    }

    $('.nominal_installment').on('keyup', function () {
        nominalInstallment();
    });

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var ppn = 0;
        var is_ppn = document.getElementById("is_ppn");
        var total_drum = '<?php echo $penambahanDrum; ?>';
        var harga_per_drum = $("#harga_per_drum").val();
        harga_per_drum = accounting.unformat(harga_per_drum);
        if (harga_per_drum.length == 0) {
            $("#harga_per_drum").val(0);
            harga_per_drum = 0;
        }
        var total_harga_drum = total_drum * harga_per_drum;
        var grand_total = total_harga_drum;
        var grand_total = grand_total - (ppn * grand_total);
        
        if (is_supplier_loan > 0 || is_ppn.checked == true) {
            $("#sub_total").val(accounting.formatMoney(grand_total));
            $("#c_sub_total").html(accounting.formatMoney(grand_total));
        }
        if (is_ppn.checked == true) {
            ppn = <?php echo $ppn; ?>;
            var total_ppn = ppn * grand_total;
            grand_total = grand_total + (total_ppn);
            $("#ppn").val(accounting.formatMoney(total_ppn));
        }
        if (is_supplier_loan > 0) {
            var sum_installment = $("#sum_installment").val();
            grand_total = grand_total - sum_installment;
        }
        
        $("#grand_total").val(accounting.formatMoney(grand_total));
        $("#c_grand_total").html(accounting.formatMoney(grand_total));
    }

    $('.hitungGrandTotal').on('focusout', function () {
        hitungGrandTotal();
    });

    $('.hitungGrandTotal').on('keyup', function () {
        hitungGrandTotal();
    });

    // ===== Confirmation ===== //
    $("#conf_simpan").click(function () {
        $("#c_ppn").html($("#ppn").val());
        $("#c_description").html($("#description").val());
        $("#c_harga_per_drum").html($("#harga_per_drum").val());
<?php
if (count($listSupplierLoan) > 0) {
    foreach ($listSupplierLoan as $key => $value) {
        ?>
                $("#c_nominal_installment_<?php echo $key; ?>").html($("#nominal_installment_<?php echo $key; ?>").val());
        <?php
    }
}
?>
    });

    // ===== Proses Controller logic ajax ===== //
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/Purchase/prosesConfirm/' . $idTransactionPurchase); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-ubah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo site_url('Transaction/Purchase'); ?>'", 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    // ===== Open Modal Confirmation Approve ===== //
    $(document).on("click", ".approve", function () {
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded

    });
</script>
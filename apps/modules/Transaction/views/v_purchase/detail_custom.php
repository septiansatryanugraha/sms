<?php
$paid = "-";
if (strlen($dataPurchase->paid) > 0) {
    $paid = date('d-m-Y', strtotime($dataPurchase->paid));
}
?>
<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 50%;
    }
</style>
<div class="col-md-12 well">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;">
        <i class="fa fa-location-arrow"></i> <?php echo $judul; ?>
    </h3>
    <div class="col-sm-12">
        <form class="form-horizontal" id="form-detail" method="POST">
            <div class="box box-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="box-header with-border no-padding">
                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Pembelian </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="code"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Pembelian </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="date"></label></div>
                    </div>
                    <?php if ($isApproved > 0) { ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Bayar </label>
                            <div class="col-sm-7">
                                : <label for="inputEmail3" class="control-label" id="label_paid" name="label_paid"></label>
                            </div>
                        </div>
                    <?php } else { ?>
                        <?php if ($isApprove > 0) { ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Bayar </label>
                                <div class="col-sm-7">
                                    <input type="text" name="paid" class="form-control datepicker" id="paid" placeholder="Tanggal Bayar" style="border-color: red;" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Supplier </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="supplier"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Jumlah Drum </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="jml_drum"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="description"><?php echo $dataPurchase->description; ?></label></div>
                    </div>
                    <div class="form-group"><div>&nbsp;</div></div>
                </div>
                <div class="col-sm-6">
                    <div class="is-approved" style="display:none">
                        <div class="form-group">
                            <div class="box-header with-border no-padding">
                                <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="harga_per_drum" name="harga_per_drum"></label></div>
                        </div>
                        <?php if ($ppn > 0) { ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Total </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="total" name="total"></label></div>
                            </div>
                            <div class="form-group">  
                                <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="total_ppn" name="total_ppn"></label></div>
                            </div>
                        <?php } ?>
                        <div class="sub-total" style="display:none">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Sub Total </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="sub_total" name="sub_total"></label></div>
                            </div>
                        </div>
                        <?php if (count($listSupplierLoan) > 0) { ?>
                            <div class="form-group"><div>&nbsp;</div></div>
                            <div class="form-group">
                                <div class="box-header with-border no-padding">
                                    <h3 class="box-title"><b>&#8226; Detail Piutang</b></h3>
                                </div>
                            </div>
                            <?php foreach ($listSupplierLoan as $key => $value) { ?>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Keterangan : <?php echo $value['description']; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Piutang</label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label"><?php echo number_format($value['nominal'], 0, ",", "."); ?></label></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">Cicilan Ke <?php echo $value['installment']; ?></label><div id="nominal_supplier"></div>
                                    <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="nominal_installment_<?php echo $key; ?>"><?php echo number_format($value['nominal_installment'], 0, ",", "."); ?></label></div>
                                </div>
                                <div class="form-group"><div>&nbsp;</div></div>
                            <?php } ?>
                            <input type="hidden" id="sum_installment" name="installment" value="<?php echo $subtotalInstallment; ?>">
                            <div class="form-group"><div>&nbsp;</div></div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="grand_total" name="grand_total"></label></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="text-right">
            <button class="btn btn-danger" data-dismiss="modal"> Close</button>
            <?php if ($isCancel > 0) { ?>
                <button type="button" class="btn btn-danger cancel-pembelian" data-id="<?php echo $dataPurchase->id_transaction_purchase; ?>"><i class="fa fa-close"></i> &nbsp;Batalkan Konfirmasi</button>
            <?php } ?>
            <?php if ($isApprove > 0) { ?>
                <button type="button" class="btn btn-danger approve-pembelian" data-id="<?php echo $dataPurchase->id_transaction_purchase; ?>"><i class="fa fa-check-square-o"></i> &nbsp;Approve</button>
            <?php } ?>
            <?php if ($isDelete > 0) { ?>
                <button type="button" class="btn btn-danger hapus-pembelian" data-id="<?php echo $dataPurchase->id_transaction_purchase; ?>"><i class="fa fa-trash"></i> &nbsp;Hapus</button>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    var is_approved = <?php echo $isApproved; ?>;
    var is_supplier_loan = <?php echo $isSupplierLoan; ?>;
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;

    $(document).ready(function () {
        var nominal = <?php echo $dataPurchase->nominal; ?>;
        $(".is-approved").fadeOut();
        if (nominal > 0) {
            $(".is-approved").fadeIn();
        }

        $("#code").html('<?php echo $dataPurchase->code; ?>');
        $("#date").html('<?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?>');
        $("#label_paid").html('<?php echo $paid; ?>');
        $("#supplier").html('<?php echo $namaSupplier; ?>');

        $("#jml_drum").html('<?php echo $penambahanDrum; ?>');

        $(".nominal-installment").fadeOut();
        $(".sub-total").fadeOut();
        $(".supplier-loan").hide();
        $("#harga_per_drum").html(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));
        $("#total").html(accounting.formatMoney('<?php echo $price; ?>'));
        $("#total_ppn").html(accounting.formatMoney('<?php echo $totalPpn; ?>'));

        if (isCheckedDrum > 0) {
            $("#label_kemasan").html('Drum / Liter');
        } else {
            $("#label_kemasan").html('Kemasan / Liter');
        }

        hitungGrandTotal();
        isSubtotal();
    });

    // ===== Action Subtotal ===== //
    function isSubtotal() {
        if (is_supplier_loan > 0) {
            $(".sub-total").fadeIn('slow');
        } else {
            $(".sub-total").fadeOut('slow');
        }
        hitungGrandTotal();
    }

    // ===== Action Sum Installment ===== //
    function nominalInstallment() {
        var sum = 0;
        $(".nominal_installment").each(function () {
            sum += +accounting.unformat($(this).val());
        });
        $("#sum_installment").val(sum);
        hitungGrandTotal();
    }

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var total_drum = '<?php echo $penambahanDrum; ?>';
        var harga_per_drum = $("#harga_per_drum").html();
        harga_per_drum = accounting.unformat(harga_per_drum);
        if (harga_per_drum.length == 0) {
            $("#harga_per_drum").html(0);
        }
        var grand_total = parseFloat('<?php echo $subTotalHarga; ?>');
        if (is_supplier_loan > 0) {
            $("#sub_total").html(accounting.formatMoney(grand_total));
            var sum_installment = $("#sum_installment").val();
            grand_total = grand_total - sum_installment;
        }
        $("#grand_total").html(accounting.formatMoney(grand_total));
    }

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $('.datepicker').attr('readonly', 'readonly');
        $('.datepicker').css('background-color', '#fff', 'important');
    });

</script>
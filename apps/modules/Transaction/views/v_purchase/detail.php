<?php
$paid = "-";
if (strlen($dataPurchase->paid) > 0) {
    $paid = date('d-m-Y', strtotime($dataPurchase->paid));
}
?>
<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 50%;
    }
</style>
<div class="col-md-12 well">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;">
        <i class="fa fa-location-arrow"></i> <?php echo $judul; ?>
    </h3>
    <div class="col-sm-12">
        <form class="form-horizontal" id="form-detail" method="POST">
            <div class="box box-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="box-header with-border no-padding">
                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Pembelian </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="code"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Pembelian </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="date"></label></div>
                    </div>
                    <?php if ($isApproved > 0) { ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Bayar </label>
                            <div class="col-sm-7">
                                : <label for="inputEmail3" class="control-label" id="label_paid" name="label_paid"></label>
                            </div>
                        </div>
                    <?php } else { ?>
                        <?php if ($isApprove > 0) { ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Bayar </label>
                                <div class="col-sm-7">
                                    <input type="text" name="paid" class="form-control datepicker" id="paid" placeholder="Tanggal Bayar" style="border-color: red;" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Supplier </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="supplier"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Oli (Kg) </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="weight"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Jenis Oli </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="specific_gravity"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Kadar Air (%) </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="water_content"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label"><span id="label_kemasan">Kemasan / Liter</span> </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="jml_kemasan"></label> <b>@</b> <label for="inputEmail3" class="control-label" id="liter_per_kemasan"></label> <b>Liter</b></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Total Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="total_liter"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="penambahan_liter"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Penyusutan Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="penyusutan_liter"></label></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total Liter </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="grandtotal_liter"></label></div>
                    </div>
                    <div id="additional_drum" class="additional-drum" style="display:none">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Drum </label>
                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="penambahan_drum"></label></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="description"><?php echo $dataPurchase->description; ?></label></div>
                    </div>
                    <div class="form-group"><div></div></div>
                </div>
                <div class="col-sm-6">
                    <div class="is-approved" style="display:none">
                        <div class="form-group">
                            <div class="box-header with-border no-padding">
                                <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                            </div>
                        </div>
                        <div id="harga_drum" class="harga-drum" style="display:none">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="harga_per_drum" name="harga_per_drum"></label></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Liter </label>
                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="harga_per_liter" name="harga_per_liter"></label></div>
                        </div>
                        <?php if ($ppn > 0) { ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Total </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="total" name="total"></label></div>
                            </div>
                            <div class="form-group">  
                                <label for="inputEmail3" class="col-sm-5 control-label">PPN <?php echo ($ppn * 100) ?> % </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="total_ppn" name="total_ppn"></label></div>
                            </div>
                        <?php } ?>
                        <div class="sub-total" style="display:none">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Sub Total </label>
                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="sub_total" name="sub_total"></label></div>
                            </div>
                        </div>
                        <?php if (count($listSupplierLoan) > 0) { ?>
                            <div class="form-group"><div>&nbsp;</div></div>
                            <div class="form-group">
                                <div class="box-header with-border no-padding">
                                    <h3 class="box-title"><b>&#8226; Detail Piutang</b></h3>
                                </div>
                            </div>
                            <?php foreach ($listSupplierLoan as $key => $value) { ?>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Keterangan : <?php echo $value['description']; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Piutang</label>
                                    <div class="col-sm-7">: <label for="inputEmail3" class="control-label"><?php echo number_format($value['nominal'], 0, ",", "."); ?></label></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">Cicilan Ke <?php echo $value['installment']; ?></label><div id="nominal_supplier"></div>
                                    <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="nominal_installment_<?php echo $key; ?>"><?php echo number_format($value['nominal_installment'], 0, ",", "."); ?></label></div>
                                </div>
                                <div class="form-group"><div>&nbsp;</div></div>
                            <?php } ?>
                            <input type="hidden" id="sum_installment" name="installment" value="<?php echo $subtotalInstallment; ?>">
                            <div class="form-group"><div>&nbsp;</div></div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="grand_total" name="grand_total"></label></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="text-right">
            <button class="btn btn-danger" data-dismiss="modal"> Close</button>
            <?php if ($isCancel > 0) { ?>
                <button type="button" class="btn btn-danger cancel-pembelian" data-id="<?php echo $dataPurchase->id_transaction_purchase; ?>"><i class="fa fa-close"></i> &nbsp;Batalkan Konfirmasi</button>
            <?php } ?>
            <?php if ($isApprove > 0) { ?>
                <button type="button" class="btn btn-danger approve-pembelian" data-id="<?php echo $dataPurchase->id_transaction_purchase; ?>"><i class="fa fa-check-square-o"></i> &nbsp;Approve</button>
            <?php } ?>
            <?php if ($isDelete > 0) { ?>
                <button type="button" class="btn btn-danger hapus-pembelian" data-id="<?php echo $dataPurchase->id_transaction_purchase; ?>"><i class="fa fa-trash"></i> &nbsp;Hapus</button>
            <?php } ?>
        </div>
    </div>
</div>  

<script type="text/javascript">
    var is_approved = <?php echo $isApproved; ?>;
    var is_supplier_loan = <?php echo $isSupplierLoan; ?>;
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;

    $(document).ready(function () {
        var nominal = <?php echo $dataPurchase->nominal; ?>;
        $(".is-approved").fadeOut();
        if (nominal > 0) {
            $(".is-approved").fadeIn();
        }

        $("#code").html('<?php echo $dataPurchase->code; ?>');
        $("#date").html('<?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?>');
        $("#label_paid").html('<?php echo $paid; ?>');
        $("#supplier").html('<?php echo $namaSupplier; ?>');
        $("#bank").html('<?php echo $bank; ?>');
        $("#account_number").html('<?php echo $dataSupplier->account_number; ?>');
        $("#account_holder").html('<?php echo $dataSupplier->account_holder; ?>');
        $("#weight").html('<?php echo $weight; ?>');
        $("#specific_gravity").html('<?php echo $specificGravity; ?>');
        $("#water_content").html('<?php echo $waterContent; ?>');
//        $("#description").html('<?php // echo $dataPurchase->description; ?>');

        $("#jml_kemasan").html('<?php echo $jmlKemasan; ?>');
        $("#liter_per_kemasan").html('<?php echo $literPerKemasan; ?>');
        $("#penambahan_liter").html('<?php echo $penambahanLiter; ?>');
        $("#penyusutan_liter").html('<?php echo $penyusutanLiter; ?>');
        $("#total_liter").html('<?php echo $totalLiter; ?>');
        $("#grandtotal_liter").html('<?php echo $grandTotalLiter; ?>');
        $("#penambahan_drum").html('<?php echo $penambahanDrum; ?>');
        $(".nominal-installment").fadeOut();
        $(".sub-total").fadeOut();
        $(".supplier-loan").hide();
        $("#harga_per_liter").html(accounting.formatMoney('<?php echo $hargaPerLiter; ?>'));
        $("#harga_per_drum").html(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));
        $("#total").html(accounting.formatMoney('<?php echo $price; ?>'));
        $("#total_ppn").html(accounting.formatMoney('<?php echo $totalPpn; ?>'));

        if (isCheckedDrum > 0) {
            $("#label_kemasan").html('Drum / Liter');
        } else {
            $("#label_kemasan").html('Kemasan / Liter');
        }

        hitungGrandTotal();
        isHargaDrum();
        isSubtotal();
    });

    // ===== Action Harga Drum ===== //
    function isHargaDrum() {
        var additional_drum = document.getElementById("additional_drum");
        var harga_drum = document.getElementById("harga_drum");
        if (isCheckedAdditionalDrum > 0) {
            additional_drum.style.display = "block";
            harga_drum.style.display = "block";
            var harga_per_drum = $("#harga_per_drum").html();
            if (harga_per_drum.length == 0) {
                $("#harga_per_drum").html(0);
                harga_per_drum = 0;
            }
        }
    }

    // ===== Action Subtotal ===== //
    function isSubtotal() {
        if (is_supplier_loan > 0) {
            $(".sub-total").fadeIn('slow');
        } else {
            $(".sub-total").fadeOut('slow');
        }
        hitungGrandTotal();
    }

    // ===== Action Sum Installment ===== //
    function nominalInstallment() {
        var sum = 0;
        $(".nominal_installment").each(function () {
            sum += +accounting.unformat($(this).val());
        });
        $("#sum_installment").val(sum);
        hitungGrandTotal();
    }

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var total_drum = 0;
        var penambahan_drum = 0;
        var harga_per_drum = 0;
        var total_liter_label = 0;

        var harga_per_liter = $("#harga_per_liter").html();
        var penambahan_liter = $("#penambahan_liter").html();
        var penyusutan_liter = $("#penyusutan_liter").html();

        if (harga_per_liter.length == 0) {
            harga_per_liter = 0;
        }
        harga_per_liter = accounting.unformat(harga_per_liter);

        var jml_kemasan = $("#jml_kemasan").html();
        if (jml_kemasan.length == 0) {
            $("#jml_kemasan").html(0);
            jml_kemasan = 0;
        }
        var liter_per_kemasan = $("#liter_per_kemasan").html();
        var liter_per_kemasan_coma = liter_per_kemasan.split(',').length;
        if (liter_per_kemasan.length == 0) {
            $("#liter_per_kemasan").html(0);
            liter_per_kemasan = 0;
        }
        if (liter_per_kemasan_coma == 2) {
            liter_per_kemasan = liter_per_kemasan.replace(",", ".");
        }
        var total_liter = jml_kemasan * liter_per_kemasan;
        total_liter_label = total_liter;

        if (total_liter % 1 !== 0) {
            total_liter = parseFloat(total_liter).toFixed(2);
            total_liter_label = total_liter.replace(".", ",");
        }

        var penambahan_liter = $("#penambahan_liter").html();
        if (penambahan_liter.length == 0) {
            $("#penambahan_liter").html(0);
        }
        var penyusutan_liter = $("#penyusutan_liter").html();
        if (penyusutan_liter.length == 0) {
            $("#penyusutan_liter").html(0);
        }
        if (isCheckedAdditionalDrum > 0) {
            var penambahan_drum = $("#penambahan_drum").html();
            if (penambahan_drum.length == 0) {
                $("#penambahan_drum").html(0);
            }
        }
        if (isCheckedAdditionalDrum > 0 || isCheckedDrum > 0) {
            harga_per_drum = $("#harga_per_drum").html();
            if (harga_per_drum.length == 0) {
                $("#harga_per_drum").html(0);
            }
        }
        $("#total_liter").html(total_liter_label);

        var grand_total = parseFloat('<?php echo $subTotalHarga; ?>');
        if (is_supplier_loan > 0) {
            $("#sub_total").html(accounting.formatMoney(grand_total));
            var sum_installment = $("#sum_installment").val();
            grand_total = grand_total - sum_installment;
        }

        $("#grand_total").html(accounting.formatMoney(grand_total));
    }

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
        $('.datepicker').attr('readonly', 'readonly');
        $('.datepicker').css('background-color', '#fff', 'important');
    });

</script>
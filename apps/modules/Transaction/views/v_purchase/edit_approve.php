<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #nominal_supplier {
        margin-left:80px;
        position:absolute;
    }
    .number_only {
        text-align: right;
    }
    .number_decimal {
        text-align: right;
    }
    .jml-style {
        display: inline-block; 
        width: 20%;
    }
    .liter-style {
        display: inline-block; 
        width: 40%;
    }
</style>
<!-- style loading -->
<div class="loading2"></div>
<!-- -->
<section class="content">
    <div class="loading2"></div>
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-ubah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kode Pembelian </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="code" name="code" class="form-control" placeholder="Kode"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pembelian </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="date" name="date" class="form-control" placeholder="Tanggal" aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Supplier </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="supplier" name="supplier" class="form-control" placeholder="Supplier"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Bank </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="bank" name="bank" class="form-control" placeholder="Bank"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Nomor Rekening </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="account_number" name="account_number" class="form-control" placeholder="Nomor Rekening"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Pemilik Rekening </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="account_holder" name="account_holder" class="form-control" placeholder="Pemilik Rekening"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Berat Oli (Kg) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="weight" name="weight" class="form-control number_decimal" placeholder="Berat"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Berat Jenis Oli </label>
                                        <div class="col-sm-5">
                                            <input type="text" style="text-align: right;" id="specific_gravity" name="specific_gravity" class="form-control number_decimal" placeholder="Berat Jenis"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Kadar Air (%) </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="water_content" name="water_content" class="form-control number_only" placeholder="Kadar Air"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label"><span id="label_kemasan">Kemasan / Liter</span> </label>
                                        <div class="col-sm-7">
                                            <input type="text" id="jml_kemasan" name="jml_kemasan" class="form-control jml-style number_only hitungGrandTotal" placeholder="Jumlah Kemasan"  aria-describedby="sizing-addon2" disabled>
                                            @<input type="text" id="liter_per_kemasan" name="liter_per_kemasan" class="form-control liter-style number_decimal hitungGrandTotal" placeholder="Liter per Kemasan" aria-describedby="sizing-addon2" disabled>Liter
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Total Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="total_liter" name="total_liter" class="form-control number_decimal" placeholder="Total Liter"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Penambahan Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="penambahan_liter" name="penambahan_liter" class="form-control number_only" placeholder="Penambahan Liter"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Penyusutan Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="penyusutan_liter" name="penyusutan_liter" class="form-control number_only" placeholder="Penyusutan Liter"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grandtotal_liter" name="grandtotal_liter" class="form-control number_decimal" placeholder="Grand Total Liter"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                    <div id="additional_drum" class="additional-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Penambahan Drum </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="penambahan_drum" name="penambahan_drum" class="form-control number_only" placeholder="Penambahan Drum"  aria-describedby="sizing-addon2" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                                        <div class="col-sm-7">
                                            <textarea id="description" name="description" class="form-control" placeholder="Keterangan"  aria-describedby="sizing-addon2"><?php echo $dataPurchase->description; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div id="harga_drum" class="harga-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Harga per Drum </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="harga_per_drum" name="harga_per_drum" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Drum"  aria-describedby="sizing-addon2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Harga per Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="harga_per_liter" name="harga_per_liter" class="form-control number_only hitungGrandTotal formatCurrency" placeholder="Harga per Liter"  aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">  
                                        <label for="inputEmail3" class="col-sm-3 control-label"></label>       
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="is_ppn" name="is_ppn" value="1" onclick="isPpn();"/>PPN <?php echo ($ppn * 100) ?> % ? 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="sub-total" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Sub Total </label>
                                            <div class="col-sm-5">
                                                <input type="text" id="sub_total" name="sub_total" class="form-control number_only" placeholder="Sub Total"  aria-describedby="sizing-addon2" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (count($listSupplierLoan) > 0) { ?>
                                        <div class="nav-tabs-custom nominal-installment" style="display:none"></div>
                                        <div class="form-group"><div>&nbsp;</div></div>
                                        <div class="form-group">
                                            <div class="box-header with-border no-padding">
                                                <h3 class="box-title"><b>&#8226; Detail Piutang</b></h3>
                                            </div>
                                        </div>
                                        <?php foreach ($listSupplierLoan as $key => $value) { ?>
                                            <input type="hidden" id="id_supplier_loan_installment[]" name="id_supplier_loan_installment[]" value='<?php echo $value['id_supplier_loan_installment']; ?>'>
                                            <input type="hidden" id="id_supplier_loan[]" name="id_supplier_loan[]" value='<?php echo $value['id_supplier_loan']; ?>'>
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Keterangan : <?php echo $value['description']; ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Piutang</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control number_only" aria-describedby="sizing-addon2" value='<?php echo number_format($value['nominal'], 0, ",", "."); ?>' disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Belum Dibayar</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control number_only" aria-describedby="sizing-addon2" value='<?php echo number_format($value['debt'], 0, ",", "."); ?>' disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 control-label">Cicilan Ke <?php echo $value['installment']; ?></label><div id="nominal_supplier"></div>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control number_only hitungGrandTotal formatCurrency nominal_installment" id="nominal_installment_<?php echo $key; ?>" name="nominal_installment[]" placeholder="Nominal Cicilan" aria-describedby="sizing-addon2" value="<?php echo number_format($value['nominal_installment'], 0, ",", "."); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group"><div>&nbsp;</div></div>
                                        <?php } ?>
                                        <input type="hidden" id="sum_installment" name="installment" value="<?php echo $subtotalInstallment; ?>">
                                    <?php } ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Grand Total </label>
                                        <div class="col-sm-5">
                                            <input type="text" id="grand_total" name="grand_total" class="form-control number_only" placeholder="Grand Total"  aria-describedby="sizing-addon2" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer pull-right">
                                <button id="conf_simpan" type="button" data-toggle="modal" data-target="#exampleModal"  class="btn btn-success btn-flat approve"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php //<editor-fold defaultstate="collapsed" desc="Modal Confirmation">?>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="col-md-12 well">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">?</span></button>
                <h3 style="display:block; text-align:center;">Konfirmasi Pembelian</h3>
                <div class="box box-body">
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Pembelian</b></h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Kode Pembelian </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_code"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Tanggal Pembelian </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_date"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Supplier </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_supplier"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Bank </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_bank"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Nomor Rekening </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_account_number"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Pemilik Rekening </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_account_holder"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Oli (Kg) </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_weight"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Berat Jenis Oli </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_specific_gravity"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Kadar Air (%) </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_water_content"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label"><span id="c_label_kemasan">Kemasan / Liter</span> </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_jml_kemasan"></label> <b>@</b> <label for="inputEmail3" class="control-label" id="c_liter_per_kemasan"></label> <b>Liter</b></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Total Liter </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_total_liter"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Liter </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_penambahan_liter"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Penyusutan Liter </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_penyusutan_liter"></label></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total Liter </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_grandtotal_liter"></label></div>
                                    </div>
                                    <div id="c_additional_drum" class="c-additional-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Penambahan Drum </label>
                                            <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_penambahan_drum"></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Keterangan </label>
                                        <div class="col-sm-7">: <label for="inputEmail3" class="control-label" id="c_description"></label></div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="box-header with-border no-padding">
                                            <h3 class="box-title"><b>&#8226; Detail Transaksi</b></h3>
                                        </div>
                                    </div>
                                    <div id="c_harga_drum" class="c-harga-drum" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Harga per Drum </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_drum" name="c_harga_per_drum"></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Harga per Liter </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_harga_per_liter" name="harga_per_liter"></label></div>
                                    </div>
                                    <div class="c-sub-total" style="display:none">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label">Sub Total </label>
                                            <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_sub_total" name="sub_total"></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group"><div>&nbsp;</div></div>
                                    <?php if (count($listSupplierLoan) > 0) { ?>
                                        <div class="form-group">
                                            <div class="box-header with-border no-padding">
                                                <h3 class="box-title"><b>&#8226; Detail Piutang</b></h3>
                                            </div>
                                        </div>
                                        <?php foreach ($listSupplierLoan as $key => $value) { ?>
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Keterangan : <?php echo $value['description']; ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Piutang</label>
                                                <div class="col-sm-7">: <label for="inputEmail3" class="control-label"><?php echo number_format($value['nominal'], 0, ",", "."); ?></label></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Belum Dibayar</label>
                                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label"><?php echo number_format($value['debt'], 0, ",", "."); ?></label></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-5 control-label">Cicilan Ke <?php echo $value['installment']; ?></label><div id="nominal_supplier"></div>
                                                <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_nominal_installment_<?php echo $key; ?>"></label></div>
                                            </div>
                                            <div class="form-group"><div>&nbsp;</div></div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Grand Total </label>
                                        <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="c_grand_total" name="c_grand_total"></label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <div id="btn_loading"></div>
                    <div id="buka">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" name="simpan" id="simpan">Konfirmasi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //</editor-fold> ?>

<script type="text/javascript">
    var is_supplier_loan = <?php echo $isSupplierLoan; ?>;
    var isCheckedDrum = <?php echo $isCheckedDrum; ?>;
    var isCheckedPpn = <?php echo $isPpn; ?>;
    var isCheckedAdditionalDrum = <?php echo $isCheckedAdditionalDrum; ?>;

    $(document).ready(function () {
        $("#code").val('<?php echo $dataPurchase->code; ?>');
        $("#c_code").html('<?php echo $dataPurchase->code; ?>');
        $("#date").val('<?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?>');
        $("#c_date").html('<?php echo date('d-m-Y', strtotime($dataPurchase->date)); ?>');
        $("#supplier").val('<?php echo $namaSupplier; ?>');
        $("#c_supplier").html('<?php echo $namaSupplier; ?>');
        $("#bank").val('<?php echo $bank; ?>');
        $("#c_bank").html('<?php echo $bank; ?>');
        $("#account_number").val('<?php echo $dataSupplier->account_number; ?>');
        $("#c_account_number").html('<?php echo $dataSupplier->account_number; ?>');
        $("#account_holder").val('<?php echo $dataSupplier->account_holder; ?>');
        $("#c_account_holder").html('<?php echo $dataSupplier->account_holder; ?>');
        $("#weight").val('<?php echo $weight; ?>');
        $("#c_weight").html('<?php echo $weight; ?>');
        $("#specific_gravity").val('<?php echo $specificGravity; ?>');
        $("#c_specific_gravity").html('<?php echo $specificGravity; ?>');
        $("#water_content").val('<?php echo $waterContent; ?>');
        $("#c_water_content").html('<?php echo $waterContent; ?>');

        $("#jml_kemasan").val('<?php echo $jmlKemasan; ?>');
        $("#c_jml_kemasan").html('<?php echo $jmlKemasan; ?>');
        $("#liter_per_kemasan").val('<?php echo $literPerKemasan; ?>');
        $("#c_liter_per_kemasan").html('<?php echo $literPerKemasan; ?>');
        $("#penambahan_liter").val('<?php echo $penambahanLiter; ?>');
        $("#c_penambahan_liter").html('<?php echo $penambahanLiter; ?>');
        $("#penyusutan_liter").val('<?php echo $penyusutanLiter; ?>');
        $("#c_penyusutan_liter").html('<?php echo $penyusutanLiter; ?>');
        $("#total_liter").val('<?php echo $totalLiter; ?>');
        $("#c_total_liter").html('<?php echo $totalLiter; ?>');
        $("#grandtotal_liter").val('<?php echo $grandTotalLiter; ?>');
        $("#c_grandtotal_liter").html('<?php echo $grandTotalLiter; ?>');
        $("#penambahan_drum").val('<?php echo $penambahanDrum; ?>');
        $("#c_penambahan_drum").html('<?php echo $penambahanDrum; ?>');

        $(".nominal-installment").fadeOut();
        $(".sub-total").fadeOut();
        $(".supplier-loan").hide();
        $("#harga_per_liter").val(accounting.formatMoney('<?php echo $hargaPerLiter; ?>'));
        $("#harga_per_drum").val(accounting.formatMoney('<?php echo $hargaPerDrum; ?>'));

        if (isCheckedDrum > 0) {
            $("#label_kemasan").html('Drum / Liter');
        } else {
            $("#label_kemasan").html('Kemasan / Liter');
        }

        if (isCheckedPpn > 0) {
            document.getElementById("is_ppn").checked = true;
        }

        hitungGrandTotal();
        isHargaDrum();
        isSubtotal();
    });

    $("#conf_simpan").click(function () {
        $("#c_description").html($("#description").val());
        $("#c_harga_per_liter").html($("#harga_per_liter").val());
        if (isCheckedDrum > 0) {
            $("#c_label_kemasan").html('Drum / Liter');
        } else {
            $("#c_label_kemasan").html('Kemasan / Liter');
        }
        var additional_drum = document.getElementById("c_additional_drum");
        var harga_drum = document.getElementById("c_harga_drum");
        if (isCheckedAdditionalDrum > 0) {
            additional_drum.style.display = "block";
            harga_drum.style.display = "block";
            $("#c_harga_per_drum").html($("#harga_per_drum").val());
        }
        if (is_supplier_loan > 0) {
            $(".c-sub-total").fadeIn('slow');
        } else {
            $(".c-sub-total").fadeOut('slow');
        }
<?php
if (count($listSupplierLoan) > 0) {
    foreach ($listSupplierLoan as $key => $value) {
        ?>
                $("#c_nominal_installment_<?php echo $key; ?>").html($("#nominal_installment_<?php echo $key; ?>").val());
        <?php
    }
}
?>
    });

    // ===== Action Harga Drum ===== //
    function isHargaDrum() {
        var additional_drum = document.getElementById("additional_drum");
        var harga_drum = document.getElementById("harga_drum");
        if (isCheckedAdditionalDrum > 0) {
            additional_drum.style.display = "block";
            harga_drum.style.display = "block";
            var harga_per_drum = $("#harga_per_drum").val();
            if (harga_per_drum.length == 0) {
                $("#harga_per_drum").val(0);
                harga_per_drum = 0;
            }
        }
    }

    // ===== Action Subtotal ===== //
    function isSubtotal() {
        if (is_supplier_loan > 0) {
            $(".sub-total").fadeIn('slow');
        } else {
            $(".sub-total").fadeOut('slow');
        }
        hitungGrandTotal();
    }

    // ===== Action PPN ===== //
    function isPpn() {
        hitungGrandTotal();
    }

    // ===== Action Sum Installment ===== //
    function nominalInstallment() {
        var sum = 0;
        $(".nominal_installment").each(function () {
            sum += +accounting.unformat($(this).val());
        });
        $("#sum_installment").val(sum);
        hitungGrandTotal();
    }

    $('.nominal_installment').on('keyup', function () {
        nominalInstallment();
    });

    // ===== Proses Perhitungan ===== //
    function hitungGrandTotal() {
        var total_drum = 0;
        var penambahan_drum = 0;
        var harga_per_drum = 0;
        var total_liter_label = 0;
        var ppn = 0;

        var is_ppn = document.getElementById("is_ppn");
        if (is_ppn.checked == true) {
            ppn = <?php echo $ppn; ?>;
        }

        var harga_per_liter = $("#harga_per_liter").val();

        var penambahan_liter = $("#penambahan_liter").val();
        var penyusutan_liter = $("#penyusutan_liter").val();

        if (harga_per_liter.length == 0) {
            harga_per_liter = 0;
        }
        harga_per_liter = accounting.unformat(harga_per_liter);

        var jml_kemasan = $("#jml_kemasan").val();
        if (jml_kemasan.length == 0) {
            $("#jml_kemasan").val(0);
            jml_kemasan = 0;
        }
        var liter_per_kemasan = $("#liter_per_kemasan").val();
        var liter_per_kemasan_coma = liter_per_kemasan.split(',').length;
        if (liter_per_kemasan.length == 0) {
            $("#liter_per_kemasan").val(0);
            liter_per_kemasan = 0;
        }
        if (liter_per_kemasan_coma == 2) {
            liter_per_kemasan = liter_per_kemasan.replace(",", ".");
        }
        var total_liter = jml_kemasan * liter_per_kemasan;
        total_liter_label = total_liter;

        if (total_liter % 1 !== 0) {
            total_liter = parseFloat(total_liter).toFixed(2);
            total_liter_label = total_liter.replace(".", ",");
        }

        var penambahan_liter = $("#penambahan_liter").val();
        if (penambahan_liter.length == 0) {
            $("#penambahan_liter").val(0);
            penambahan_liter = 0;
        }
        var penyusutan_liter = $("#penyusutan_liter").val();
        if (penyusutan_liter.length == 0) {
            $("#penyusutan_liter").val(0);
            penyusutan_liter = 0;
        }
        if (isCheckedAdditionalDrum > 0) {
            var penambahan_drum = $("#penambahan_drum").val();
            if (penambahan_drum.length == 0) {
                $("#penambahan_drum").val(0);
                penambahan_drum = 0;
            }
        }
        if (isCheckedAdditionalDrum > 0 || isCheckedDrum > 0) {
            harga_per_drum = $("#harga_per_drum").val();
            if (harga_per_drum.length == 0) {
                $("#harga_per_drum").val(0);
                harga_per_drum = 0;
            }
            harga_per_drum = accounting.unformat(harga_per_drum);
        }
        $("#total_liter").val(total_liter_label);

        var total_drum = parseInt(total_drum) + parseInt(penambahan_drum);
        var total_liter_all = parseFloat(total_liter) + parseInt(penambahan_liter) - parseInt(penyusutan_liter);
        var total_harga_oli = total_liter_all * harga_per_liter;
        var total_harga_drum = total_drum * harga_per_drum;
        var grand_total = total_harga_oli + total_harga_drum;
        var grand_total = grand_total - (ppn * grand_total);
        if (is_supplier_loan > 0) {
            $("#sub_total").val(accounting.formatMoney(grand_total));
            $("#c_sub_total").html(accounting.formatMoney(grand_total));
            var sum_installment = $("#sum_installment").val();
            grand_total = grand_total - sum_installment;
        }

        $("#grand_total").val(accounting.formatMoney(grand_total));
        $("#c_grand_total").html(accounting.formatMoney(grand_total));
    }

    $('.hitungGrandTotal').on('keyup', function () {
        hitungGrandTotal();
    });

    // Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST', beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Transaction/Purchase/prosesUpdateConfirm/' . $idTransactionPurchase); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-ubah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo site_url('Transaction/Purchase'); ?>'", 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    $(function () {
        $(".select-supplier-loan").select2({
            placeholder: " -- Pilih Piutang Supplier -- "
        });
        $(".select-vehicle").select2({
            placeholder: " -- Pilih Kendaraan -- "
        });
    });

    $(document).on("click", ".approve", function () {
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded

    });
</script>